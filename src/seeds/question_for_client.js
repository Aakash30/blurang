exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex("questions_for_client")
    .del()
    .then(function () {
      // Inserts seed entries
      return knex("questions_for_client").insert([
        {
          question_id: 1,
          question:
            "Do you want the company to provide end to end support in hosting and maintenance?",
          type: "boolean",
        },
        {
          question_id: 2,
          question:
            "Do you want your idea to be kept confidential by the company?",
          type: "boolean",
        },
        {
          question_id: 3,
          question:
            "Will your project require both design and development, Do you have any reference, suggestions?",
          type: "text",
        },
        {
          question_id: 4,
          question:
            "Who are all your competitors when it comes to this product, how you differ from them?",
          type: "text",
        },
        {
          question_id: 5,
          question:
            "What features this app or website contain (contact form, photos, videos, etc.)?",
          type: "text",
        },
        {
          question_id: 6,
          question: "Are there any particular points we should consider before recommending agencies?",
          type: "text"
        }
      ]);
    });
};
