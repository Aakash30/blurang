
exports.seed = function(knex) {
  // Deletes ALL existing entries
 return knex('faq_questions').del()
 .then(function () {
   // Inserts seed entries
   return knex('faq_questions').insert([
     {question_id: 1, question: 'Who are your ideal customers? (Startups, Enterprises)', is_default: true},
     {question_id: 2, question: 'What all you need to give your deadline and budget?', is_default: true},
     {question_id: 3, question: 'If there are any changes in scope like adding or removing some features then how will this affect time and cost?', is_default: true},
     {question_id: 4, question: 'How does maintenance contract work? How many months do you provide the support, if we need to extend the support, what would be the extra cost?', is_default: true},
     {question_id: 5, question: 'How difficult to developed ecommerce App?', is_default: true},
     {question_id: 6, question: 'How many of your clients are repeat clients or from reference?', is_default: false},
     {question_id: 7, question: 'Can you tell me about the delivery process, when will I get an update and whom to contact?', is_default: false},
     {question_id: 8, question: 'How does code review or QA work?', is_default: false},
     {question_id: 9, question: 'How many of your apps are live and have more than 10,000 customers?', is_default: false},
     {question_id: 10, question: 'Are there any hidden charges like 3rd party API or server cost, etc.?', is_default: false},
     {question_id: 11, question: 'In how many languages have you done the projects?', is_default: false},
     {question_id: 12, question: 'If any project gets canceled in the middle, what would be the refund policy?', is_default: false},
     {question_id: 13, question: 'Does your company provide any marketing services for the project?', is_default: false},
     {question_id: 14, question: 'What would be the mode of payment and describe payment milestones.', is_default: false},
     {question_id: 15, question: 'Do you protect the idea and keep it confidential?', is_default: false}
   ]);
 });
};
