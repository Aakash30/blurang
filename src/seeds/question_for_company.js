
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('questions_for_company_profile').del()
    .then(function () {
      // Inserts seed entries
      return knex('questions_for_company_profile').insert([
        {question_id: 1, question: 'Open on weekend'},
        {question_id: 2, question: 'Does your company provide free support after development?'},
        {question_id: 3, question: 'Does your company work on fix price?'},
        {question_id: 4, question: 'Does your company keep ideas confidential and provide security of all kinds?'},
        {question_id: 5, question: 'Can a customer visit you?'},
        {question_id: 6, question: 'Can you visit a customer location?'},
        {question_id: 7, question: 'Is your company ok with a penalty if delivery is not promised date?'},
        {question_id: 8, question: 'Do you provide hosting and maintenance support?'}
      ]);
    });
};
