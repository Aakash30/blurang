CONFIG = {};

CONFIG.jwt_encryption 	= 'xIMEE1vdvlvTjac1tGyiJHZusIFBtl';


CLIENT_COMPANY_URL = 'http://localhost:4200/'; //'http://3.6.40.92/company/';
CLIENT_URL = 'http://3.6.40.92/';
GLOBAL_ADMIN_URL = '';

NOTIFICATION_PAYLOAD = {
    "COMPANY_PROFILE_SUBMITTED_MESSAGE_TO_ADMIN": {
        "body": "[Business] has requested for [number] vehicle(s).",
        "urlToRedirect": GLOBAL_ADMIN_URL + "notification"
    },
    "COMPANY_PROFILE_APPROVE_ACTION_TO_COMPANY": {
        "body": "Your profile has been approved. Time to begin with Bluerang!",
        "urlToRedirect": CLIENT_COMPANY_URL + "notification"
    },
    "COMPANY_PROFILE_REJECT_ACTION_TO_COMPANY": {
        "body": "Your profile has been approved. Time to begin with Bluerang!",
        "urlToRedirect": CLIENT_COMPANY_URL + "notification"
    },
    "QUOTATION_REQUEST": {
        "body": "You've received a new quotation request from [CLIENT].",
        "urlToRedirect": CLIENT_COMPANY_URL + "notification"
    },
    "QUOTATION_RECEIVE_ACTION":{
        "body": "You've received a new quotation from [COMPANY].",
        "urlToRedirect": CLIENT_URL + "notification"
    },
    "QUOTATION_REJECT_ACTION_TO_CLIENT":{
        "body": "[COMPANY] company has reject your quotation request.",
        "urlToRedirect": CLIENT_URL + "notification"
    },
    "QUOTATION_HIRE_ACTION_TO_COMPANY":{
        "body": "Congratulations! You got hired for [CLIENT] project.",
        "urlToRedirect": CLIENT_COMPANY_URL + "notification"
    },
    "QUOTATION_WITHDRAW_ACTION_TO_CLIENT":{
        "body": "[COMPANY] company has withdrawn its quotation.",
        "urlToRedirect": CLIENT_URL + "notification"
    },
    "QUOTATION_REJECT_ACTION_TO_CCOMPANY":{
        "body": "[CLIENT] has rejected your quotation.",
        "urlToRedirect": CLIENT_COMPANY_URL + "notification"
    },
    "REQUEST_FOR_COMPLETION_TO_CLIENT":{
        "body": "[COMPANY] has requested you to mark [PROJECT_NAME] project as complete.",
        "urlToRedirect": CLIENT_URL + "notification"
    },
    "PROJECT_COMPLETION_TO_COMPMANY":{
        "body": "Project [PROJECT_NAME] has been marked as complete. Add this in your portfolio",
        "urlToRedirect": CLIENT_COMPANY_URL + "notification"
    },
    "GIVE_FEEDBACK_TO_CLIENT":{
        "body": "[COMPANY] has requested your feedback on [PROJECT_NAME] project.",
        "urlToRedirect": CLIENT_URL + "notification"
    },
    "REVIEW_SUBMITTED_TO_COMPANY":{
        "body": "You recieved feedback for [PORTFOLIO]",
        "urlToRedirect": CLIENT_COMPANY_URL + "notification"
    },
    "NEW_QUERY_TO_COMPANY":{
        "body": "You've received a new quotation from [xyz].",
        "urlToRedirect": CLIENT_COMPANY_URL + "notification"
    }
};

module.exports = {
    aws_key_id : 'AKIAIKMFT42SSJJWAWRA',
    aws_secret : '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',
    aws_region : 'ap-south-1',
    sendgrid: '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',
    clientBusinessUrl : CLIENT_COMPANY_URL, //localhost:4200/
    clientUrl : CLIENT_URL, //localhost:4200/
    per_page: 10,
    NOTIFICATION_PAYLOAD: NOTIFICATION_PAYLOAD
    
};

