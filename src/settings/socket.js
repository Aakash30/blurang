const {
    io
} = require('./server');

const Notification = require('../models/Notification');
const UserAuth = require('../models/UserAuth');
const UserLogin = require('../models/UserLogin');
const ChatProjectCompanyMap = require('../models/ChatProjectCompanyMap');

const knexConfig = require('./../../config/knex');
const Project = require('../models/Project');
const { base64decode } = require('nodejs-base64');
const ChatMessage = require('../models/ChatMessage');
const Knex = require("knex")(knexConfig.development);

io.on('connection', (socket) => {
    console.log("-------connected----------"+socket.handshake.query.token)
    if (socket.handshake.query.token != undefined) {
        //console.log(socket.handshake.query.token)
        socket.join(socket.handshake.query.token);
      //  console.log("hi")
    }

    //console.log(socket.handshake.query.chatRoom)
    // if (socket.handshake.query.chatRoom != undefined) {
    //     console.log(socket.handshake.query.chatRoom +"a")
    //     socket.join(socket.handshake.query.chatRoom);
    //   //  console.log("hi")
    // }

    socket.on("checkNotification", async function (data) {

        let getToken = JSON.parse(data);

        console.log(getToken)
        let token = getToken.token.replace("Bearer ", "");

        let userdata = await UserAuth.query().select("user_id").where("token", token).first();
      

        let notificationList = await Notification.query()
        .select("notification_id", "message", "read_status", "created_at")
        .where("to_user_id", userdata.user_id).where("clear", false)
        .orderBy("read_status", "DESC")
        .offset(0)
        .limit(10);

        let notificationDot = await Notification.query()
        .count("notification_id")
        .where({ "to_user_id": userdata.user_id, read_status: false }).where("clear", false).first();

        io.to(token).emit("notificationUpdate", {
            notification: notificationList,
            notificationDot: notificationDot.count
        });
    });

    socket.on("connectChatRoom", function (roomId) {
        socket.join(roomId);
        
    });

    socket.on("getCompanyChatList", async function(data){
       let getToken = JSON.parse(data);

        //console.log(getToken)
        let tokenData = getToken.token.replace("Bearer ", "");

        let userdata = await UserLogin.query().select("user_login.user_id", "default_company", "user_type").innerJoinRelated("authToken").where("authToken.token", tokenData).first().runBefore((result, builder)=>{
          console.log(builder.toKnexQuery().toQuery());
          return result;
        });

        let chatBox = await ChatProjectCompanyMap
      .query()
      .select(
        "map_id",
        "chat_box_id",
        "status",
        "project_id",
        "updated_at",
                Knex.raw(
                  "(SELECT count(message_id) FROM chat_messages_list where chat_id = inbox.chat_id and status = 'unread' AND sender != "+userdata.user_id+" GROUP BY inbox.chat_id) AS unread"
                )
      )
      .innerJoinRelated("inbox")
      .where((builder) => {
        
        builder.where("company_id", userdata.default_company);
        
      })
      .withGraphFetched("project")
      .modifyGraph("project", (builder) => {
        builder
          .select("project_name");
      })
      .orderBy("updated_at", "DESC").runAfter((result, builder)=>{
         
          return result;
      });

    //   let responseData = {
    //     chatBox,
    //   }
      io.to(tokenData).emit("updateSideBarChatList", {
        chatUser: chatBox
    });
    });

    socket.on("getClientChatList", async function(data){
      let getToken = JSON.parse(data);

      
       let tokenData = getToken.token.replace("Bearer ", "");

       let userdata = await UserAuth.query().select("user_id").where("token", tokenData).first().runBefore((result, builder)=>{
         console.log(builder.toKnexQuery().toQuery());
         return result;
       });

      let chatId =  base64decode(getToken.markRead);
      if(chatId != 0){
        await ChatMessage.query().update({status: "read"}).where({"receiver": req.user.user_id,"chat_id": chatId});
      }
       let chatBox = await ChatProjectCompanyMap
       .query().select(
        "map_id",
        "chat_box_id",
        "status",
        "project_id",
          "updated_at",
        Knex.raw(
          "(SELECT count(message_id) FROM chat_messages_list where chat_id = inbox.chat_id and status = 'unread' AND sender != "+userdata.user_id+" GROUP BY inbox.chat_id) AS unread"
        )
      )
      .innerJoinRelated("inbox")
      .where((builder) => {
       
          builder.where("client_id", userdata.user_id);
        
      })
      .withGraphFetched("company")
      .modifyGraph("company", (builder) => {
        builder
          .select("company_name")
          .withGraphFetched("address", { maxBatchSize: 1 })
          .modifyGraph("address", (builder) => {
            builder.select("location").limit(1);
          });
      })
      .orderBy("updated_at", "DESC");

      
        io.to(tokenData).emit("updateSideBarChatList", {
          chatUser: chatBox
      });
   })

    socket.on("leave_room", function(roomId){
        console.log(roomId)
        socket.leave(roomId);
    })
    socket.on('close', function () {
        console.log('user disconnected');
    });
});