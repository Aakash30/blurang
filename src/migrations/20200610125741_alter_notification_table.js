
exports.up = function(knex) {
  return knex.schema 
  .alterTable("notification", table =>{
      table.string("redirectUrl");
      table.string("redirectComponent");
      table.unique(["notification_for", "notification_for_id", "to_user_id", "by_user_id"])
  })
};

exports.down = function(knex) {
  
};
