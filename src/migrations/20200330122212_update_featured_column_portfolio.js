
exports.up = function(knex) {
   return knex.schema
      .dropTable("featured_portfolio")
      .table("portfolio", table => {
      	table.boolean("is_featured").defaultTo(false);
      });
};

exports.down = function(knex) {
  
};
