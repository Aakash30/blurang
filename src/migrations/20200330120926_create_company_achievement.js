exports.up = function(knex) {
  return knex.schema
    .createTable("company_achievement_certificate", table => {
      table.increments("certificate_id");
      table.string("certificate");
      table.string("organization");
      table.date("receiving_date");
      table.text("reason");
      table.date("validity_date");
      table.string("location");
      table.integer("company_id");
      table.enum("status", ["active", "delete"]).defaultTo("active");
      table.timestamps(true);
    })
    .createTable("company_achievement_award", table => {
      table.increments("award_id");
      table.string("award");
      table.string("category");
      table.date("receiving_date");
      table.text("reason");
      table.string("location");
      table.integer("company_id");
      table.enum("status", ["active", "delete"]).defaultTo("active");
      table.timestamps(true);
    })
    .createTable("company_achievement_featured", table=>{
        table.increments("feature_id");
        table.date("when_featured");
        table.text("links");
        table.string("special_mentioned");
        table.text("reason");
        table.integer("company_id");
        table.enum("status", ["active", "delete"]).defaultTo("active");
        table.timestamps(true);
    })
    .createTable("company_success_story", table=>{
        table.increments("story_id");
        table.integer("portfolio_id");
        table.string("success_category");
        table.double("funding");
        table.text("award");
        table.text("reason");
        table.integer("company_id");
        table.enum("status", ["active", "delete"]).defaultTo("active");
        table.timestamps(true);
    });
};

exports.down = function(knex) {};
