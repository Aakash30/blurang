
exports.up = function(knex) {
  return knex.schema
  .createTable("domain_list", table=>{
      table.increments("domain_id");
      table.string("domain");
      table.enum("status", ['active', 'block']);
      table.timestamps(false);
  })
  .createTable("skillset_list", table=>{
      table.increments("skill_id");
      table.string("skills");
      table.enum("status", ['active', 'block']);
      table.timestamps(false);
  })
  .createTable("service_list", table=>{
      table.increments("service_id");
      table.string("Service_name");
      table.enum("status", ['active', 'block']);
      table.timestamps(false);
  });
};

exports.down = function(knex) {
  
};
