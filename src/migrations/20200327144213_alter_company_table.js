
exports.up = function(knex) {
  return knex.schema 
  .alterTable("company", table=>{
      table.enum("status", ["draft", "submitted", "approved", "rejected"]).defaultTo("draft");
      table.integer("steps_completed");
  })
};

exports.down = function(knex) {
  
};
