
exports.up = function(knex) {
    return knex.schema.raw(`
    ALTER TABLE "project"
    DROP CONSTRAINT "project_status_check",
    ADD CONSTRAINT "project_status_check" 
    CHECK (status IN ('draft', 'submitted', 'requested-quotation', 'under-discussion', 'in-progress', 'complete', 'archieve'))
  `);
}; []

exports.down = function(knex) {
  
};
