
exports.up = knex => {
    return knex.schema
    .alterTable('project', table => {
        table.boolean('is_completed_request').defaultTo(false);
    })
};

exports.down = knex => {

};