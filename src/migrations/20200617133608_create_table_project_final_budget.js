
exports.up = function(knex) {
    return knex.schema
      .createTable("project_final_budget", table=>{
          table.increments("project_final_budget_id").primary();
          table.integer("company_id");
          table.integer("client_id");
          table.integer("project_id");
          table.integer("portfolio_id");
          table.integer("final_budget");
          table.timestamps(true);
      })
};

exports.down = function(knex) {

};