
exports.up = function(knex) {
  return knex.schema 
  .alterTable("shortlisted_companies", table =>{
      table.dropColumn("status");
     
  })
  .alterTable("shortlisted_companies", table =>{
    table.enum("status", ['shortlisted', 'quotation_request', 'quotation_pending', 'quotation_received', 'selected', 'withdraw', 'reject', 'under_discussion'])
  })
  .alterTable("project", table =>{
    table.integer("assigned_to_company");
  });
};

exports.down = function(knex) {
  
};
