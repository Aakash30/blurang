
exports.up = function(knex) {
  return knex.schema 
  .createTable("project", table=>{
      table.increments("project_id");
      table.integer("domain_id").comment("domain id for the related project");
      table.string("project_name").comment("stores project name / title");
      table.string("one_line_description", 200).comment("stores one line description."); //ask to put validations accordingly on frontend 
      table.string("project_logo").comment("stores project logo");
      table.decimal("budget").comment("stores budget of the project");
      table.text("about_the_project").comment("stores a detail description of project"); // ask to put validations accordingly on frontend
      table.integer("user_id").comment("stores the related client user");
      table.string("payment_type");
      table.string("location");
      table.string("expected_timeline");
      table.string("ideal_team_size");
      table.integer("domain_experience_in_months");
      table.enum("status",  ['draft', 'requested-quotation', 'under-discussion', 'in-progress', 'complete', 'archieve']);
      table.timestamps(true);
  }).createTable("project_service_mapping", table=>{
      table.increments("project_service_id");
      table.integer("project_id");
      table.integer("service_id");
  })
  .createTable("project_skill_mapping", table=>{
      table.increments("project_skill_id");
      table.integer("project_id");
      table.integer("skill_id");
  });
};

/**
 * project services, skills
 */
exports.down = function(knex) {
  
};
