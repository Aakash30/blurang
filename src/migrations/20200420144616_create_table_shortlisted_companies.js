
exports.up = function(knex) {
    return knex.schema
    .createTable("shortlisted_companies", table=>{
        table.increments("shortlisted_id");
        table.string("company_id");
        table.string("project_id");
        table.enum("status", ['shortlisted', 'quotation_request', 'quotation_pending', 'discuss_with', 'work_in_progress']).defaultTo('shortlisted');
        table.timestamps(false);
    })
  };
  
  exports.down = function(knex) {
    
  };
  