
exports.up = function(knex) {
  return knex.schema 
  .alterTable("reviews", table=>{
      table.boolean("is_valid").defaultTo(true).comment("Remains valid until portfolio is not deleted");
  })
};

exports.down = function(knex) {
  
};
