
exports.up = function(knex) {
  return knex.schema
  .createTable("company", table=>{
      table.increments("company_id");
      table.string("company_name");
      table.string("logo");
      table.date("incorporation_date");
      table.string("director_name");
      table.string("company_email_id");
      table.string("company_website");
      table.string("contact_number");
      table.integer("team_size");
      table.string("fixed_budget");
      table.text("description");
      table.string("video_link");
      table.string("brochure");
      table.string("cin_number");
      table.string("cin_number_file");
      table.string("company_registration");
      table.time("opens_at");
      table.time("closes_at");
      table.integer("domain_id");
      table.integer("domain_experience_in_months");
      table.integer("created_by");
      table.integer("updated_by");
      table.timestamps(false);
  })
  .createTable("company_media", table=>{
      table.increments("media_id");
      table.integer("company_id");
      table.string("media_link");
      table.enum("media_type", ['gallery', 'testimonial']);
      table.timestamps(false);
  })
  .createTable("company_address", table=>{
      table.increments("address_id");
      table.string("address");
      table.integer("company_id");
      table.string("latitude");
      table.string("longitude");
      table.timestamps(false);
  })
  .createTable("company_skills_mapping", table=>{
      table.increments("skill_map_id");
      table.integer("skill_id");
      table.integer("company_id");
  })
  .createTable("company_service_mapping", table=>{
      table.increments("service_map_id");
      table.integer("service_id");
      table.integer("company_id");
  })
  .createTable("company_language", table=>{
      table.increments("map_id");
      table.string("language");
      table.integer("company_id");
  })
  .createTable("company_mapping", table=>{
      table.increments("company_map_id");
      table.integer("user_id");
      table.integer("company_id");
      table.timestamps(false);
  });
};

exports.down = function(knex) {
  
};
