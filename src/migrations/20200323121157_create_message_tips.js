
exports.up = function(knex) {
  return knex.schema 
  .createTable('message_tips', table=>{
      table.increments("message_id");
      table.text("message").comment("stores the message you need to display on page.");
      table.string("module").comment("stores the module name where you need to display the text.");
  })
};

exports.down = function(knex) {
  
};
