
exports.up = function(knex) {
    return knex.schema 
    .alterTable("shortlisted_companies", table =>{
        table.dropColumn("project_id");
    })
    .alterTable("shortlisted_companies", table =>{
        table.dropColumn("company_id");
    })
    .alterTable("shortlisted_companies", table =>{
        table.integer("project_id");
    })
    .alterTable("shortlisted_companies", table =>{
        table.integer("company_id");
    })
  };
  
  exports.down = function(knex) {
    
  };