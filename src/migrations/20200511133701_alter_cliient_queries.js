
exports.up = function(knex) {
  return knex.schema 
  .alterTable("client_queries", table =>{
      table.dropColumn("status");
  }).alterTable("client_queries", table =>{
      table.enum("status", ['active', 'pending', 'block']).defaultTo("pending");
      table.timestamp("query_posted_at");
  })
};

exports.down = function(knex) {
  
};
