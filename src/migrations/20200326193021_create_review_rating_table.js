
exports.up = function(knex) {
  return knex.schema
	.createTable("review_rating", table=>{
		table.increments("review_rating_id");
		table.integer("portfolio_id");
		table.integer("rating_id");
		table.integer("review_id");
		table.integer("stars");
	});
};

exports.down = function(knex) {
  
};
