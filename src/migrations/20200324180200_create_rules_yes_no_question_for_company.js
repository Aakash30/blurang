
exports.up = function(knex) {
  return knex.schema 
  .createTable("questions_for_company_profile", table=>{
      table.increments("question_id");
      table.string("question");
      table.timestamps(true);
  })
  .createTable("company_rules_answer", table=>{
      table.increments("rule_id");
      table.boolean("answer").defaultTo(true);
      table.integer("company_id");
      table.timestamps(true);
  })
};

exports.down = function(knex) {
  
};
