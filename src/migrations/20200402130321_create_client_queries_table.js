
exports.up = function(knex) {
  return knex.schema
	.createTable("client_queries", table => {
		table.increments("query_id").primary();
		table.integer("company_id");
		table.text("question");
		table.text("answer");
		table.enum("status", ['active', 'block']);
		table.timestamps(false);
	})
};

exports.down = function(knex) {
  
};
