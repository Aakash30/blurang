
exports.up = knex => {
    return knex.schema
    .createTable('notification', table => {
        table.increments('notification_id').primary();
        table.integer('to_user_id');
        table.integer('by_user_id');
        table.string('notification_for');
        table.integer('notification_for_id');
        table.string('message');
        table.boolean('read_status').defaultTo(false);
        table.timestamps(false);
    })
};

exports.down = knex => {

};