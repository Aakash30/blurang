exports.up = function(knex) {
    return knex.schema
    .createTable("company_membership_history", table=>{
        table.increments("company_membership_history_id");
        table.integer("company_id");
        table.integer("membership_plan_id");
        table.integer("membership_plan_amount");
        table.date('start_date');
        table.date('end_date');
        table.enum("status", ['active', 'expire']).defaultTo('active');
        table.timestamps(false);
              
    });
  };
  
  exports.down = function(knex) {
    
  };