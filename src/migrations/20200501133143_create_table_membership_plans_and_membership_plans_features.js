
exports.up = function(knex) {
    return knex.schema
    .createTable("membership_plans", table=>{
        table.increments("plan_id");
        table.text("plan_name");
        table.integer("plan_duration_month");
        table.integer("plan_amount");
        table.enum("status", ['active', 'block']).defaultTo('active');
        table.timestamps(false);
              
    }).createTable("membership_plans_features", table=>{
        table.increments("feature_id");
        table.text("feature");
        table.integer('plan_id', 11);
        table.enum("status", ['active', 'block']).defaultTo('active');
        table.timestamps(false, true);
    });
  };
  
  exports.down = function(knex) {
    
  };