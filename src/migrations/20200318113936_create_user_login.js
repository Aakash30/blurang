
exports.up = function(knex) {
  return knex.schema
  .createTable("user_login", table=>{
      table.increments("user_id");
      table.string("first_name");
      table.string("last_name");
      table.string("email_id");
      table.string("mobile_number");
      table.text("password");
      table.text("token");
      table.boolean("is_user_verified").defaultTo(false);
      table.enum("user_type", ['admin', 'company', 'client']);
      table.enum("user_status", ['active', 'pending', 'block']).defaultTo('pending');
      table.timestamps(false);
            
  }).createTable("user_auth_token", table=>{
      table.increments("token_id");
      table.text("token");
      table.integer('user_id', 11);
        table.string('ip');
        table.timestamp('last_login_at', true);
        table.timestamps(false, true);
  });
};

exports.down = function(knex) {
  
};
