
exports.up = function(knex) {
  return knex.schema 
  .createTable("chat_project_company_map", table=>{
    table.increments("map_id");
    table.integer("project_id");
    table.integer("company_id");
    table.integer("chat_box_id");
  })
  .alterTable("chat_inbox_thread", table =>{
      table.dropColumn("product_id");
  })
  .renameTable("chat_message_tips", "chat_messages_list")
};

exports.down = function(knex) {
  
};
