
exports.up = function(knex) {
  return knex.schema 
  .createTable("chat_inbox_thread", table =>{
      table.increments("chat_id");
      table.integer("product_id");
      table.integer("quotation_id");
      table.enum("status", ['active', 'in-active']).defaultTo("active");
      table.timestamps(true, true);
  })
  .createTable("chat_message_tips", table =>{
      table.increments("message_id");
      table.text("message");
      table.string("message_type");
      table.integer("sender");
      table.integer("receiver");
      table.integer("chat_id");
      table.enum("status", ['read', 'unread']);
      table.timestamp("last_updated");
  });
  
};

exports.down = function(knex) {
  
};
