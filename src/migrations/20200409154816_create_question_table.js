
exports.up = function(knex) {
  return knex.schema 
  .createTable("questions_for_client", table=>{
      table.increments("question_id");
      table.string("question");
      table.enum("type", ["boolean", "text"]);
  })
  .createTable("answer_for_client", table=>{
      table.increments("answer_id");
      table.text("answer");
      table.integer("question_id");
  })
};

exports.down = function(knex) {
  
};
