
exports.up = function(knex) {
  return knex.schema 
  .createTable("quotation_details", table =>{
      table.increments("quotation_id");
      table.string("estimated_timeline");
      table.integer("team_size");
      table.double("fixed_budget");
      table.text("comments");
      table.timestamp("quotation_sent_at");
      table.timestamp("quotation_updated_at");
      table.integer("project_id");
      table.integer("company_id");
  })
  .createTable("quotation_service", table =>{
    table.increments("quotation_map_id");
    table.integer("quotation_id");
    table.integer("service_id");
  })
  .createTable("quotation_data_documents_from_company", table =>{
    table.increments("document_id");
    table.string("file_link");
    table.string("file_type");
    table.integer("quotation_id");
  });
};

exports.down = function(knex) {
  
};
