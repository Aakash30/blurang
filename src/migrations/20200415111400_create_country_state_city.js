
exports.up = function(knex) {
  return knex.schema 
  .createTable("countries", table=>{
      table.increments("country_id");
      table.string("country");
      table.string("alpha2_code");
      table.string("alpha3_code");
      table.string("region");
      table.string("subregion");
      table.string("time_zones");
      table.string("calling_codes");
      table.string("currency_code");
      table.string("flag");
      table.timestamps(true, true);
  })
  .createTable("states", table =>{
      table.increments("state_id");
      table.integer("country_id");
      table.string("state");
      table.string("code");
      table.timestamps(true, true);
  })
  .createTable("cities", table =>{
      table.increments("city_id");
      table.integer("country_id");
      table.integer("state_id");
      table.string("city");
      table.timestamps(true, true);
  })
};

exports.down = function(knex) {
  
};
