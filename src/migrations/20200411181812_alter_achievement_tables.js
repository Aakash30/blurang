exports.up = function (knex) {
  return knex.schema
    .alterTable("company_achievement_certificate", (table) => {
      table.boolean("is_hidden").defaultTo(false);
    })
    .alterTable("company_achievement_award", (table) => {
      table.boolean("is_hidden").defaultTo(false);
    })
    .alterTable("company_achievement_featured", (table) => {
      table.boolean("is_hidden").defaultTo(false);
    })
    .alterTable("company_success_story", (table) => {
      table.boolean("is_hidden").defaultTo(false);
    });
};

exports.down = function (knex) {};
