
exports.up = function(knex) {
    return knex.schema
      .createTable("company_docx", table=>{
          table.increments("company_docx_id").primary();
          table.integer("company_id");
          table.string("company_docx");
          table.string("company_docx_file_type");
          table.enum("status", ['active', 'delete']).defaultTo("active");
          table.timestamps(false);
      })
  };
  
  exports.down = function(knex) {
    
  };