
exports.up = function(knex) {
  return knex.schema
	.createTable("faq_questions", table=>{
		table.increments("question_id").primary();
		table.text("question");
		table.enum("status", ['active', 'block']);
		table.timestamps(false);
	})
	.createTable("company_faq", table=>{
		table.increments("faq_id").primary();
		table.integer("company_id");
		table.integer("question_id");
		table.text("answer");
	});
};

exports.down = function(knex) {
  
};
