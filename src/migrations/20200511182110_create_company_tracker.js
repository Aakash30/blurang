
exports.up = function(knex) {
  return knex.schema 
  .createTable("track_company_views", table =>{
      table.increments("tracking_id");
      table.integer("company_id");
      table.string("ip_address");
      table.timestamps(true, true);
  })
  .alterTable("company", table =>{
      table.integer("viewer_count");
  })
};

exports.down = function(knex) {
  
};
