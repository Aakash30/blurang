'use strict';
const { base64encode, base64decode } = require('nodejs-base64');
const globalCalls = require('../../settings/functions');
const globalConst = require("../../settings/constants");
const messageTips = require('../../models/MessageTips');
const company = require('../../models/Company');
const domain = require('../../models/Domain');
const service = require('../../models/Service');
const companyQuestion = require('../../models/CompanyQuestion');
const users = require('../../models/UserLogin');

const knexConfig        = require('../../../config/knex');
const Knex              = require('knex')(knexConfig.development);
const moment = require("moment");

// const updateProfile = require('../AuthController').updateProfile;

const {
    transaction
} = require('objection');

const ListAgencyOwner = async(req, res)=>{
    try {    
        let page = req.query.page ? req.query.page : 1;
        let limit = req.query.limit ? req.query.limit : globalConst.per_page;
        let offset = req.query.offset ? req.query.offset : limit * (page - 1);

        let orderbydata;
        if(req.body.order_by)
        {
        if(req.query.order_by_type)
            orderbydata=req.query.order_by+' '+req.query.order_by_type;
        else
            orderbydata=req.query.order_by+' DESC';
        }
        orderbydata="user_id DESC";

        let clientData = await users.query().select(
            "user_id", "first_name", "last_name", "email_id", "mobile_number", "is_user_verified", "user_status", 
            "created_at",
            Knex.raw("(SELECT count(company_id) AS no_company from company_mapping where user_login.user_id=company_mapping.user_id)"),
            )
        .withGraphFetched("[company]")//, company_count
        .modifyGraph("company", (builder) => {
            builder.withGraphFetched('address', {maxBatchSize: 1}).modifyGraph('address', builder => {
                builder.select('location').limit(1)
            })
            builder.select("company.company_id", "company_name");
        })
        .where((builder) => {
            builder.where("user_type", "company");
            if (req.query.search) {
                // builder.orWhere("location", "like", "%"+req.query.search+"%");
                builder.orWhere(Knex.raw("concat(first_name, ' ', last_name)"), "like", "%"+req.query.search+"%");
                builder.orWhere("email_id", "like", "%"+req.query.search+"%");
                builder.orWhere("mobile_number", "like", "%"+req.query.search+"%");
            }
        })
        .orderByRaw(orderbydata)
        .offset(offset)
        .limit(limit);
        
        let totalCount = await users
        .query()
        .count()
        .where((builder) => {
            builder.where("user_type", "client");
        })
        .first();

        if(!clientData) {
        throw globalCalls.badRequestError("No Client found.");
        } else {

            let responseData = {
                clientData,
                total: totalCount.count
            };

            return globalCalls.okResponse(res, responseData, "");
        }
    } catch (error){
    console.log(error);
    throw globalCalls.badRequestError(error.message)
    }
}

const AgencyOwnerDetails = async(req, res)=>{
    try {
        if(req.params.id)
        {
        var user_id = base64decode(req.params.id); 
        if(isNaN(parseInt(user_id)))
	        throw globalCalls.notFoundError("Error! You have passed invalid-id.");
    
        let clientData = await users.query().select("user_id", "first_name", "last_name", "email_id", "mobile_number", "is_user_verified", "user_status", "created_at","profile_pic", "dateofbirth", "gender", "designation")
        .withGraphFetched("[company]")//, company_count
        .modifyGraph("company", (builder) => {
            builder.withGraphFetched("[domain, address,user]")
            .modifyGraph("domain", (builder) => {
                builder.select("domain");
            })
            .modifyGraph("address", builder =>{
                builder.select("location").limit(1);
            })
            .modifyGraph("user", builder =>{
                builder.select("first_name","last_name","email_id","mobile_number").limit(1);
            })
            builder.select(
                "company.company_id", "company_name", "logo", "incorporation_date", "director_name", "company_email_id", "company_website", "contact_number", "team_size", 
                "fixed_budget", "no_of_project_completed",
                // Knex.raw("(SELECT count(assigned_to_company) AS project_done from project where project.assigned_to_company=company.company_id)"),
                Knex.raw("(SELECT Round (AVG ((SELECT AVG(stars) AS starsAvg FROM review_rating WHERE review_id = reviews.review_id)),2)  from reviews where company_id = company.company_id and feedback_received = true group by company_id) as overall_rating")
                )
        })
        .where("user_id", user_id);

        if(!clientData) {
            throw globalCalls.badRequestError("Client not found.");
        } else {

            let responseData = {
                clientData,
            };

            return globalCalls.okResponse(res, responseData, "");
        }
        } 
        else {
        }
    } catch (error){
    console.log(error);
    throw globalCalls.badRequestError(error.message)
    }
}

const ActiveBlockAgencyOwner = async(req, res)=>{
    try{
            let msgdata;
            let data = req.body;
            
            let companyData = await users.query().select("user_status").where("user_id", data.user_id);
            if(!companyData) {
                throw globalCalls.badRequestError("Agency Owner not found.");
            }
            else
            {
                if(companyData[0].user_status=='block')
                {
                    data.user_status='active';
                    msgdata="Agency Owner has unblocked successfully";
                }
                else
                {
                    data.user_status='block';
                    msgdata="Agency Owner has blocked successfully";
                }            
            }    

            let verified = await users.query().upsertGraph(data).returning("*");
            if(!verified) {
                throw globalCalls.badRequestError("something went wrong.");
            } else {
                return globalCalls.okResponse(res,verified,msgdata);
            }
        
    } catch (error){
    console.log(error);
    throw globalCalls.badRequestError(error.message)
    }
}

// const updateAgencyOwnerProfileAdmin = async(req, res) =>{
//     console.log(req);
//     return updateProfile(req, res);
// }

const updateAgencyOwnerProfileAdmin = async (req, res)=>{
    try
    {
    console.log('kkkkkkkkkkkkkk');
    let data = req.body;
    console.log(req.body);
    if(!data.first_name) {
        throw globalCalls.badRequestError("Please fill up first name.");
    }
    if(!data.mobile_number) {
        throw globalCalls.badRequestError("Please fill up mobile no.");
    }
  
    if(req.file)
    {
        if (req.file.fieldname == 'profile_pic_file') {
            data.profile_pic = req.file.location;
        }
        delete data['profile_pic_file'];
    }
    
    if(data.email_id) {
      delete data['email_id'];
    }
  
    
        data.update_by='Admin';
        
        let updated = await users.query().upsertGraph(data).returning("*");
        if(!updated) {
            throw globalCalls.badRequestError("something went wrong.");
        } else {
            return globalCalls.okResponse(res,updated,"Profile updated successfully.");
        }
  
    } catch (error) {
        throw globalCalls.badRequestError(error.message)
    }
  }

module.exports = {
    ListAgencyOwner,
    AgencyOwnerDetails,
    ActiveBlockAgencyOwner,
    updateAgencyOwnerProfileAdmin
}