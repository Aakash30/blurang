const ContactUs = require("../models/ContactUs")
const globalCalls = require('../settings/functions');
const postContactInfo = async (req, res) => {
    try {
        const { body } = req
        if (!body.name) {
            throw globalCalls.badRequestError("Please provide fullname.");
        } 
        if (!body.email_id) {
            throw globalCalls.badRequestError("Please provide email-id.");
        }
        let contact = await ContactUs.query().insert(body)
        return globalCalls.okResponse(res, "", "Contact Information Sent Successfully,We will be reaching out to you shortly");
    }
    catch (e) {
        console.log(e)
        throw globalCalls.badRequestError(e.message);
    }

}
module.exports = { postContactInfo }