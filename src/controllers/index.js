const AuthController = require('../controllers/AuthController');
const ContactUsController=require("../controllers/ContactusController")
const CompanyController = require('../controllers/comapny/CompanyController');
const CompanyPortfolioController = require('../controllers/comapny/CompanyPortfolioController');
const ReviewsController = require('../controllers/reviews/ReviewsController');
const CompanyAchievement = require('./comapny/CompanyAchievementController');
const DomainController = require('./admin_master/DomainController');
const ServiceController = require('../controllers/admin_master/ServiceController');
const SkillController = require('../controllers/admin_master/SkillController');
const AdmincompanyController = require('../controllers/admin_company/AdmincompanyController');
const AdminProjectController=require("../controllers/admin_projects/adminprojectsController")
const AdminDashboardController=require("./admin_dashboard/dashboardcontroller")
const CompanyFaqController = require('./comapny/CompanyFaqController');
const CompanyProject = require('./comapny/CompanyProject');
const CompanyChatProjectListing=require("./comapny/CompanyChatController")

const Notification=require("./notification/notificationcontroller")
const CompanyQuotations = require('./comapny/CompanyQuotations');
const ProjectController = require('./client/ProjectController');

const Common = require('./common');
const AdminclientController = require('../controllers/admin_client/AdminclientController');
const AdminAgencyOwnerController = require('../controllers/admin_agencyowner/AdminAgencyOwnerController');
const LandingpageController = require('../controllers/landing_page/LandingpageController');
const CompanyMembershipController = require('../controllers/comapny/CompanyMembershipController');
const ChatClientController = require("../controllers/client/ChatClientController");

const Dashboard = require('../controllers/dashboard/dashboard')
module.exports = {
    AdminProjectController,
    AuthController,
    CompanyController,
    AdminDashboardController,
    CompanyPortfolioController,
    CompanyChatProjectListing,
    CompanyAchievement,
    ContactUsController,
    DomainController,
    ServiceController,
    SkillController,
    AdmincompanyController,
    ReviewsController,
    CompanyFaqController,
    CompanyProject,
    Common,
    Notification,
    ProjectController,
    AdminclientController,
    LandingpageController,
    CompanyQuotations,
    CompanyMembershipController,
    ChatClientController,
    AdminAgencyOwnerController,
    Dashboard
}