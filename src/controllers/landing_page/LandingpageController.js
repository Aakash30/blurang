"use strict";

const { base64encode, base64decode } = require("nodejs-base64");
const globalCalls = require("../../settings/functions");
const globalConst = require("../../settings/constants");
const company = require("../../models/Company");
const domain = require("../../models/Domain");
const companyAddress = require("../../models/CompanyAddress");
const service = require("../../models/Service");
const skills = require("../../models/Skills");
const project = require("../../models/Project");
const projectQuestion = require("../../models/ClientQuestion");
const knexConfig        = require('../../../config/knex');
const Knex              = require('knex')(knexConfig.development);
const ClientQueries = require("../../models/ClientQueries");

const { transaction, ref } = require("objection");

const getDomain_home = async (req, res) => {
  let domainData = await domain
    .query()
    .select("domain_id", "domain", "domain_image", "domain_description");
  // .where("status", '');

  if (!domainData) {
    throw globalCalls.badRequestError("Dimain not found.");
  } else {
    let responseData = {
      domainData,
    };
    return globalCalls.okResponse(res, responseData, "");
  }
};

const getServices_home = async (req, res) => {
  let serviceData = await service
    .query()
    .select("service_id", "service_name", "service_icon");
  // .where("status", '');

  if (!serviceData) {
    throw globalCalls.badRequestError("Services not found.");
  } else {
    let responseData = {
      serviceData,
    };
    return globalCalls.okResponse(res, responseData, "");
  }
};

const BrowseListCompany = async (req, res) => {
  let page = req.query.page ? req.query.page : 1;
  let limit = req.query.limit ? req.query.limit : globalConst.per_page;
  let offset = req.query.offset ? req.query.offset : limit * (page - 1);

  try {
    let companyData = await company
      .query()
      .select(
        "company_id",
        "company_name",
        "logo",
        "incorporation_date",
        "fixed_budget",
        "hourly_budget",
        "viewer_count",
        Knex.raw("(SELECT Round (AVG ((SELECT AVG(stars) AS starsAvg FROM review_rating WHERE review_id = reviews.review_id)),2)  from reviews where company_id = company.company_id and feedback_received = true group by company_id) as overall_rating")
      )
      .withGraphFetched("[domain, address, serviceLabels]")
      .modifyGraph("domain", (builder) => {
        builder.select("domain");
      })
      .modifyGraph("address", (builder) => {
        builder.select("location");
      })
      .modifyGraph("serviceLabels", (builder) => {
       
            builder.select("service_name");
      })
      .where((builder) => {
        if (req.query.domain_id) {
          builder.where("domain_id", req.query.domain_id);
        }

        if (req.query.location) {
          builder.whereIn("company_id", (builder) => {
            builder
              .select("company_id")
              .from("company_address")
              .where("location", "ilike", "%"+req.query.location+"%");
          });
        }
      }).where("status", "approved")
      .orderByRaw("overall_rating DESC NULLS LAST")
      .offset(offset)
      .limit(limit);

      let totalCount  = await company.query().count().where((builder) => {
        if (req.query.domain_id) {
          builder.where("domain_id", req.query.domain_id);
        }

        if (req.query.location) {
          builder.whereIn("company_id", (builder) => {
            builder
              .select("company_id")
              .from("company_address")
              .where("location", "ilike", "%"+req.query.location+"%");
          });
        }
      }).where("status", "approved").first();

      let responseData = {};

      if(Object.keys(req.query).length == 0){
        let domainList = await domain.query().select("domain_id", "domain").where("status", "active");
        responseData["domainList"] = domainList;
      }
   
    if (!companyData) {
      throw globalCalls.badRequestError("No company found.");
    } else {
      responseData['companyData'] = companyData;
      responseData['total'] = totalCount.count;

      
    }

    return globalCalls.okResponse(res, responseData, "");
  } catch (error) {
   
    throw globalCalls.badRequestError(error.message);
  }
};

const getproject_home = async (req, res) => {
  let projectData = await project
    .query()
    .select(
      "user_id",
      "project_id",
      "project_name",
      "one_line_description",
      "about_the_project",
      "budget",
      "payment_type",
      "location",
      "expected_timeline",
      "ideal_team_size",
      "domain_experience_in_months",
      "company_expected_experience"
    )
    .withGraphFetched("[services, skills, projectAnswer]")
    .modifyGraph("services", (builder) => {
      builder
        .withGraphFetched("service_details")
        .modifyGraph("service_details", (builder) => {
          builder.select("service_id", "service_name", "service_icon");
        });
      builder.select("service_id", "project_service_id");
    })
    .modifyGraph("skills", (builder) => {
      builder
        .withGraphFetched("skill_details")
        .modifyGraph("skill_details", (builder) => {
          builder.select("skill_id", "skills");
        });
      builder.select("skill_id", "project_skill_id");
    })
    .modifyGraph("projectAnswer", (builder) => {
      builder
        .withGraphFetched("question")
        .modifyGraph("question", (builder) => {
          builder.select("question_id", "question", "type");
        });
      builder.select("answer_id", "question_id", "answer");
    })
    .orderBy("project_id", "DESC");

  if (!projectData) {
    throw globalCalls.badRequestError("No Project found.");
  } else {
    let responseData = {
      projectData,
    };

    return globalCalls.okResponse(res, responseData, "");
  }
};

const ProjectDetails_home = async (req, res) => {
  if (req.params.id) {
    var project_id = base64decode(req.params.id);
    if(isNaN(parseInt(project_id)))
      throw globalCalls.notFoundError("Error! You have passed invalid-id.");
  
    let projectData = await project
      .query()
      .select(
        "project_id",
        "project_name",
        "one_line_description",
        "about_the_project",
        "budget",
        "payment_type",
        "location",
        "expected_timeline",
        "ideal_team_size",
        "domain_experience_in_months",
        "company_expected_experience"
      )
      .withGraphFetched("[services, skills, projectAnswer]")
      .modifyGraph("services", (builder) => {
        builder
          .withGraphFetched("service_details")
          .modifyGraph("service_details", (builder) => {
            builder.select("service_id", "service_name", "service_icon");
          });
        builder.select("service_id", "project_service_id");
      })
      .modifyGraph("skills", (builder) => {
        builder
          .withGraphFetched("skill_details")
          .modifyGraph("skill_details", (builder) => {
            builder.select("skill_id", "skills");
          });
        builder.select("skill_id", "project_skill_id");
      })
      .modifyGraph("projectAnswer", (builder) => {
        builder
          .withGraphFetched("question")
          .modifyGraph("question", (builder) => {
            builder.select("question_id", "question", "type");
          });
        builder.select("answer_id", "question_id", "answer");
      })
      .where("project_id", project_id);

    if (!projectData) {
      throw globalCalls.badRequestError("Project details not found.");
    } else {
      let responseData = {
        projectData,
      };

      return globalCalls.okResponse(res, responseData, "");
    }
  } else {
  }
};

const postClientQuery = async(req, res) => {

  let data = req.body;

  if(!data.question) {
    throw globalCalls.badRequestError("Please enter Your Question.");
  }

  if(!data.company_id){
    throw globalCalls.badRequestError("Invalid Requests.");
  }

  data["query_posted_at"] = new Date().toISOString();
  let questionPost = await ClientQueries.query().insert(data);

  if(!questionPost){
    throw globalCalls.badRequestError("Something went wrong.");
  }

  return globalCalls.okResponse(res, questionPost, "Your question has been posted successfully.");
}

const companyDetailAskQueries = async(req, res)=>{
  if(!req.params.id) {
    throw globalCalls.badRequestError("Invalid Requests.");
  }

  let companyId = base64decode(req.params.id);
  if(isNaN(parseInt(companyId)))
    throw globalCalls.notFoundError("Error! You have passed invalid-id.");

  let clientData = await ClientQueries.query().select("question", "answer").where({"company_id": companyId, "status": "active"});

  if(!clientData){
    throw globalCalls.badRequestError("Something went wrong.");
  }

  return globalCalls.okResponse(res, clientData, "Your question has been posted successfully.");
}

const contentForListYourCompany = async (req, res) => {

 try{
  let recentHireCompany = await company.query()
  .select( "company_id", "company_name", "logo",
        "fixed_budget",
        "hourly_budget",
        "viewer_count",
        Knex.raw("(SELECT Round (AVG ((SELECT AVG(stars) AS starsAvg FROM review_rating WHERE review_id = reviews.review_id)),2)  from reviews where company_id = company.company_id and feedback_received = true group by company_id) as overall_rating"))
  .whereIn(
    'company_id', project.query().select("assigned_to_company").whereIn('status', ['in-progress', 'complete']).orderBy("hire_date", "DESC").offset('0').limit('3')
  )
  .withGraphFetched("[domain, serviceLabels]")
  .modifyGraph("domain", (builder) => {
    builder.select("domain");
  })
  .modifyGraph("serviceLabels", (builder) => {
    builder.select("service_name");
  }).orderByRaw("overall_rating DESC NULLS LAST")


let projectData = await project
  .query()
  .select(
    "project_id",
    "project_name",
    "budget",
    "expected_timeline"
  )
  .withGraphFetched("[servicesLabels, domain]")
  .modifyGraph("servicesLabels", (builder) => {
     builder.select("service_name");
  })
  .modifyGraph("domain", builder =>{
    builder.select("domain");
  })
  .whereNotIn('status', ['draft', 'complete', 'archieve'])
  .orderBy("project_id", "DESC");

  let responseData = {
    recentHireCompany,
    projectData
  };

  return globalCalls.okResponse(res, responseData, "");
 } catch(error){
   console.log(error);
   throw globalCalls.badRequestError(error.message);
 }
};

module.exports = {
  getDomain_home,
  getServices_home,
  BrowseListCompany,
  getproject_home,
  ProjectDetails_home,
  postClientQuery,
  companyDetailAskQueries,
  contentForListYourCompany
};
