'use strict';

const { base64encode, base64decode } = require('nodejs-base64');
const globalCalls = require('../../settings/functions');
const globalConst = require("../../settings/constants");

const messageTips = require('../../models/MessageTips');
const users = require('../../models/UserLogin');
const project = require("../../models/Project")

const knexConfig = require('../../../config/knex');
const Knex = require('knex')(knexConfig.development);
const moment = require("moment");

const {
    transaction
} = require('objection');
const Company = require('../../models/Company');
const DomainWiseEarning=require("../../models/ProjectFinalBudget")

const ListClient = async (req, res) => {
    try {

        let page = req.query.page ? req.query.page : 1;
        let limit = req.query.limit ? req.query.limit : globalConst.per_page;
        let offset = req.query.offset ? req.query.offset : limit * (page - 1);

        let orderbydata;
        if (req.body.order_by) {
            if (req.query.order_by_type)
                orderbydata = req.query.order_by + ' ' + req.query.order_by_type;
            else
                orderbydata = req.query.order_by + ' DESC';
        }
        orderbydata = "user_id DESC";

        let clientData = await users.query()
            .select(
                "user_id", "first_name", "last_name", "email_id", "mobile_number", "is_user_verified", "user_status", "created_at",
                "location",
                Knex.raw("(SELECT CASE WHEN count(project_id) IS NULL THEN 0 ELSE count(project_id) END AS project_done from project where project.user_id=user_login.user_id and project.status='complete')"),
                Knex.raw("(SELECT CASE WHEN count(project_id) IS NULL THEN 0 ELSE count(project_id) END AS project_running from project where project.user_id=user_login.user_id and project.status='in-progress')"),
                Knex.raw("case when (SELECT sum(final_budget) FROM project_final_budget WHERE client_id = user_login.user_id) IS NULL THEN 0 else (SELECT sum(final_budget) FROM project_final_budget WHERE client_id = user_login.user_id) END as total_project_amount "),
            )
            .where((builder) => {
                builder.where("user_type", "client");
                if (req.query.project_running) {
                    builder.where("project_running", '>=', req.query.project_running);
                }
                if (req.query.total_project_amount) {
                    builder.where("total_project_amount", '>=', req.query.total_project_amount);
                }
                if (req.query.search) {
                    builder.orWhere("location", "like", "%" + req.query.search + "%");
                    builder.orWhere(Knex.raw("concat(first_name, ' ', last_name)"), "like", "%" + req.query.search + "%");
                    builder.orWhere("email_id", "like", "%" + req.query.search + "%");
                    builder.orWhere("mobile_number", "like", "%" + req.query.search + "%");
                }
            })
            .orderByRaw(orderbydata)
            .offset(offset)
            .limit(limit);

        let totalCount = await users
            .query()
            .count()
            .where((builder) => {
                builder.where("user_type", "client");
                if (req.query.total_project_amount) {
                    builder.where("total_project_amount", '>=', req.query.total_project_amount);
                }
            })
            .first();

        if (!clientData) {
            throw globalCalls.badRequestError("No Client found.");
        } else {

            let responseData = {
                clientData,
                total: totalCount.count
            };
            return globalCalls.okResponse(res, responseData, "");
        }
    } catch (error) {
        console.log(error);
        throw globalCalls.badRequestError(error.message)
    }
}

const ClientDetails = async (req, res) => {
    try {
        if (req.params.id) {
            var user_id = base64decode(req.params.id);
            if (isNaN(parseInt(user_id)))
                throw globalCalls.notFoundError("Error! You have passed invalid-id.");

            let clientData = await users.query()
                .select("user_id", "first_name", "last_name", "email_id", "mobile_number", "is_user_verified", "user_status", "created_at", "profile_pic", "dateofbirth", "gender", "designation")
                // .withGraphFetched("project_data")
                // .modifyGraph("project_data", (builder) => {
                //     builder.withGraphFetched("[domain]")
                //       .modifyGraph("domain", builder =>{
                //         builder.select("domain");
                //       })
                //       builder.select(
                //         // "project_id",
                //         "project_name",
                //         "one_line_description",
                //         "budget",
                //         "payment_type",
                //         "expected_timeline",
                //         "ideal_team_size",
                //         "domain_id",
                //         "status",
                //         "created_at"
                //       )
                // })
                .where("user_id", user_id);

            let page = req.query.page ? req.query.page : 1;
            let limit = req.query.limit ? req.query.limit : globalConst.per_page;
            let offset = req.query.offset ? req.query.offset : limit * (page - 1);
            let projectData = await project.query()
                .select(
                    // "project_id",
                    "project_name",
                    "one_line_description",
                    "budget",
                    "payment_type",
                    "expected_timeline",
                    "ideal_team_size",
                    "domain_id",
                    "status",
                    "created_at"
                )
                .withGraphFetched("[domain]")
                .modifyGraph("domain", builder => {
                    builder.select("domain");
                })
                .where((builder) => {
                    builder.where("user_id", user_id);
                    if (req.query.page) {
                        builder.where("status", req.query.page);
                    }
                })
        }
    } catch (error) {
        console.log(error);
        throw globalCalls.badRequestError(error.message)
    }
}

const ActiveBlockClient = async (req, res) => {
    try {
        let msgdata;
        let data = req.body;

        let companyData = await users.query().select("user_status").where("user_id", data.user_id);
        if (!companyData) {
            throw globalCalls.badRequestError("Client not found.");
        }
        else {
            if (companyData[0].user_status == 'block') {
                data.user_status = 'active';
                msgdata = "Client has unblocked successfully";
            }
            else {
                data.user_status = 'block';
                msgdata = "Client has blocked successfully";
            }
        }

        let verified = await users.query().upsertGraph(data).returning("*");
        if (!verified) {
            throw globalCalls.badRequestError("something went wrong.");
        } else {
            return globalCalls.okResponse(res, verified, msgdata);
        }

    } catch (error) {
        console.log(error);
        throw globalCalls.badRequestError(error.message)
    }
}

//Reports Section

//Report Client Registrations
const client_registration_by_country = async (req, res) => {
    try {

        let totalCount = await users.query().select("location").count()
            .groupBy('location')
            .havingNotNull('location')

        let Client_registered_this_month = await users
            .query()
            .select('location')
            .count()
            .where(function () {
                this.whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
                    moment().startOf("month").format("YYYY-MM-DD"),
                    moment().format("YYYY-MM-DD"),
                ]);
            }).groupBy("location").havingNotNull('location')

        let Client_registered_last_month = await users
            .query()
            .select('location')
            .count()
            .where(function () {
                this.whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
                    moment().subtract(1, "months").startOf("month").format("YYYY-MM-DD"),
                    moment().subtract(1, "month").format("YYYY-MM-DD"),
                ]);
            }).groupBy("location").havingNotNull('location')

        let Client_registered_secondlast_month = await users
            .query()
            .select('location')
            .count()
            .where(function () {
                this.whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
                    moment().subtract(2, "months").startOf("month").format("YYYY-MM-DD"),
                    moment().subtract(2, "month").format("YYYY-MM-DD"),
                ]);
            }).groupBy("location").havingNotNull('location')

        console.log(Client_registered_this_month)
        if (!totalCount) {
            throw globalCalls.badRequestError("No Client found.");
        } else {
            let responseData = {
                totalCount,
                Client_registered_this_month,
                Client_registered_last_month,
                Client_registered_secondlast_month
            };
            return globalCalls.okResponse(res, responseData, "");
        }
    }
    catch (error) {
        console.log(error)
        throw globalCalls.badRequestError(error.message)
    }
}

// const client_project_post = async (req, res) => {
//     try {
//         let project_data = await users.query().
//             select("first_name", Knex.raw("(SELECT count(project_id) AS project_count from project where project.user_id=user_login.user_id )"), Knex.raw("(SELECT count(project_id) AS project_count from project where project.user_id=user_login.user_id)"))
//             .where("user_type", "client")
//             .orderBy("project_count", "desc").limit(5)
//         if (!project_data) {
//             throw globalCalls.badRequestError("No Client found.");
//         } else {
//             let responseData = { project_data }
//             return globalCalls.okResponse(res, responseData, "");
//         }
//     }
//     catch (error) {
//         console.log(error)
//         throw globalCalls.badRequestError(error.message)
//     }
// }

//Reports Client Project Post
const client_project_post = async (req, res) => {
    try {
        let project_data = await users.query()
            .select("user_id", "first_name", Knex.raw("(SELECT count(project_id) AS project_count from project where project.user_id=user_login.user_id )"),)
            .withGraphFetched("[project_details, project_details_last_month, project_details_secondlast_month]")

            .modifyGraph("project_details", (builder) => {
                builder.select( Knex.raw("(SELECT count(project_id)"+ moment().startOf("month").format('MMMM')+")"))
                    .whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
                        moment().startOf("month").format("YYYY-MM-DD"),
                        moment().format("YYYY-MM-DD"),
                    ]).groupBy("project.user_id");
            })
            .modifyGraph("project_details_last_month", (builder) => {
                builder.select( Knex.raw("(SELECT count(project_id)"+ moment().subtract(1,"months").startOf("month").format('MMMM')+")"))
                    .whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
                        moment().subtract(1, "months").startOf("month").format("YYYY-MM-DD"),
                        moment().subtract(1, "month").format("YYYY-MM-DD"),
                    ]).groupBy("project.user_id");
            })
            .modifyGraph("project_details_secondlast_month", (builder) => {
                builder.select(Knex.raw("(SELECT count(project_id)"+ moment().subtract(1,"months").startOf("month").format('MMMM')+")"))
                    .whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
                        moment().subtract(2, "months").startOf("month").format("YYYY-MM-DD"),
                        moment().subtract(2, "month").format("YYYY-MM-DD"),
                    ]).groupBy("project.user_id");
            })
            .where("user_type", "client")
            .orderBy("project_count", "desc").limit(5)

        let project_by_month = await project.query().select(Knex.raw('COUNT(CASE WHEN "created_at"::date >= \'' + moment().startOf("month").format("YYYY-MM-DD") + '\' AND "created_at"::date <= \'' + moment().format("YYYY-MM-DD") + '\' THEN 1 ELSE NULL END) AS ' + moment().startOf("month").format('MMMM') + ', COUNT(CASE WHEN "created_at"::date >= \'' + moment().subtract(1, "months").startOf("month").format("YYYY-MM-DD") + '\' AND "created_at"::date <= \'' + moment().subtract(1, "month").format("YYYY-MM-DD") + '\' THEN 1 ELSE NULL END) AS ' + moment().subtract(1, "month").startOf("month").format('MMMM') + ', COUNT(CASE WHEN "created_at"::date >= \'' + moment().subtract(2, "months").startOf("month").format("YYYY-MM-DD") + '\' AND "created_at"::date <= \'' + moment().subtract(2, "month").format("YYYY-MM-DD") + '\' THEN 1 ELSE NULL END) AS ' + moment().subtract(2, "month").startOf("month").format('MMMM') + ', COUNT(CASE WHEN "created_at"::date >= \'' + moment().subtract(3, "months").startOf("month").format("YYYY-MM-DD") + '\' AND "created_at"::date <= \'' + moment().subtract(3, "month").format("YYYY-MM-DD") + '\' THEN 1 ELSE NULL END) AS ' + moment().subtract(3, "month").startOf("month").format('MMMM') + ', COUNT(CASE WHEN "created_at"::date >= \'' + moment().subtract(4, "months").startOf("month").format("YYYY-MM-DD") + '\' AND "created_at"::date <= \'' + moment().subtract(4, "month").format("YYYY-MM-DD") + '\' THEN 1 ELSE NULL END) AS ' + moment().subtract(4, "month").startOf("month").format('MMMM') + ', COUNT(CASE WHEN "created_at"::date >= \'' + moment().subtract(5, "months").startOf("month").format("YYYY-MM-DD") + '\' AND "created_at"::date <= \'' + moment().subtract(5, "month").format("YYYY-MM-DD") + '\' THEN 1 ELSE NULL END) AS ' + moment().subtract(5, "month").startOf("month").format('MMMM') + ', COUNT(CASE WHEN "created_at"::date >= \'' + moment().subtract(6, "months").startOf("month").format("YYYY-MM-DD") + '\' AND "created_at"::date <= \'' + moment().subtract(6, "month").format("YYYY-MM-DD") + '\' THEN 1 ELSE NULL END) AS ' + moment().subtract(6, "month").startOf("month").format('MMMM') + ''))
            .whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
                moment().subtract(1, "months").subtract(2, "months").subtract(3, "months").subtract(4, "months").subtract(5, "months").subtract(6, "months").startOf("month").format("YYYY-MM-DD"),
                moment().format("YYYY-MM-DD")])

        if (!project_data) {
            throw globalCalls.badRequestError("No Client found.");
        }
        else {
            let responseData = { project_data, project_by_month }
            return globalCalls.okResponse(res, responseData, "");
        }


    }
    catch (error) {
        console.log(error)
        throw globalCalls.badRequestError(error.message)
    }
}
// Report Client Project Amount
const client_project_amount = async (req, res) => {
    try {
        let project_data = await users.query()
            .select("user_id", "first_name", Knex.raw("(SELECT count(project_id) AS project_count from project where project.user_id=user_login.user_id )"),)
            .withGraphFetched("[project_details, project_details_last_month, project_details_secondlast_month]")

            .modifyGraph("project_details", (builder) => {
                builder.select("user_id", Knex.raw("(SELECT count(project_id) AS project_count_this_month)"))
                    .whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
                        moment().startOf("month").format("YYYY-MM-DD"),
                        moment().format("YYYY-MM-DD"),
                    ]).groupBy("project.user_id");
            })
            .modifyGraph("project_details_last_month", (builder) => {
                builder.select("user_id", Knex.raw("(SELECT count(project_id) AS project_count_this_month)"))
                    .whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
                        moment().subtract(1, "months").startOf("month").format("YYYY-MM-DD"),
                        moment().subtract(1, "month").format("YYYY-MM-DD"),
                    ]).groupBy("project.user_id");
            })
            .modifyGraph("project_details_secondlast_month", (builder) => {
                builder.select("user_id", Knex.raw("(SELECT count(project_id) AS project_count_this_month)"))
                    .whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
                        moment().subtract(2, "months").startOf("month").format("YYYY-MM-DD"),
                        moment().subtract(2, "month").format("YYYY-MM-DD"),
                    ]).groupBy("project.user_id");
            })
            .where("user_type", "client")
            .orderBy("project_count", "desc").limit(5)

        let project_by_month = await project.query().select(Knex.raw('COUNT(CASE WHEN "created_at"::date >= \'' + moment().startOf("month").format("YYYY-MM-DD") + '\' AND "created_at"::date <= \'' + moment().format("YYYY-MM-DD") + '\' THEN 1 ELSE NULL END) AS ' + moment().startOf("month").format('MMMM') + ', COUNT(CASE WHEN "created_at"::date >= \'' + moment().subtract(1, "months").startOf("month").format("YYYY-MM-DD") + '\' AND "created_at"::date <= \'' + moment().subtract(1, "month").format("YYYY-MM-DD") + '\' THEN 1 ELSE NULL END) AS ' + moment().subtract(1, "month").startOf("month").format('MMMM') + ', COUNT(CASE WHEN "created_at"::date >= \'' + moment().subtract(2, "months").startOf("month").format("YYYY-MM-DD") + '\' AND "created_at"::date <= \'' + moment().subtract(2, "month").format("YYYY-MM-DD") + '\' THEN 1 ELSE NULL END) AS ' + moment().subtract(2, "month").startOf("month").format('MMMM') + ', COUNT(CASE WHEN "created_at"::date >= \'' + moment().subtract(3, "months").startOf("month").format("YYYY-MM-DD") + '\' AND "created_at"::date <= \'' + moment().subtract(3, "month").format("YYYY-MM-DD") + '\' THEN 1 ELSE NULL END) AS ' + moment().subtract(3, "month").startOf("month").format('MMMM') + ', COUNT(CASE WHEN "created_at"::date >= \'' + moment().subtract(4, "months").startOf("month").format("YYYY-MM-DD") + '\' AND "created_at"::date <= \'' + moment().subtract(4, "month").format("YYYY-MM-DD") + '\' THEN 1 ELSE NULL END) AS ' + moment().subtract(4, "month").startOf("month").format('MMMM') + ', COUNT(CASE WHEN "created_at"::date >= \'' + moment().subtract(5, "months").startOf("month").format("YYYY-MM-DD") + '\' AND "created_at"::date <= \'' + moment().subtract(5, "month").format("YYYY-MM-DD") + '\' THEN 1 ELSE NULL END) AS ' + moment().subtract(5, "month").startOf("month").format('MMMM') + ', COUNT(CASE WHEN "created_at"::date >= \'' + moment().subtract(6, "months").startOf("month").format("YYYY-MM-DD") + '\' AND "created_at"::date <= \'' + moment().subtract(6, "month").format("YYYY-MM-DD") + '\' THEN 1 ELSE NULL END) AS ' + moment().subtract(6, "month").startOf("month").format('MMMM') + ''))
            .whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
                moment().subtract(1, "months").subtract(2, "months").subtract(3, "months").subtract(4, "months").subtract(5, "months").subtract(6, "months").startOf("month").format("YYYY-MM-DD"),
                moment().format("YYYY-MM-DD")])

        if (!project_data) {
            throw globalCalls.badRequestError("No Client found.");
        }
        else {
            let responseData = { project_data, project_by_month }
            return globalCalls.okResponse(res, responseData, "");
        }

        console.log(project_data)
    }
    catch (error) {
        console.log(error)
        throw globalCalls.badRequestError(error.message)
    }
}

//Reports Company Earning
const company_earning = async (req, res) => {
    try {
        let earning_data = await Company.query().select("company_name", "company_id", Knex.raw("case when (SELECT sum(final_budget) FROM project_final_budget) IS NULL THEN 0 else (SELECT sum(final_budget) FROM project_final_budget) END as earning "))
            .withGraphFetched("[earning_this_month, earning_last_month, earning_secondlast_month]")

            .modifyGraph("earning_this_month", (builder) => {
                builder.select(Knex.raw("case when (SELECT sum(final_budget)) IS NULL THEN 0 else (SELECT sum(final_budget) FROM project_final_budget) END as " + moment().startOf("month").format('MMMM')))
                    .whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
                        moment().startOf("month").format("YYYY-MM-DD"),
                        moment().format("YYYY-MM-DD"),
                    ]).groupBy("project_final_budget.company_id");
            })
            .modifyGraph("earning_last_month", (builder) => {
                builder.select(Knex.raw("case when (SELECT sum(final_budget)) IS NULL THEN 0 else (SELECT sum(final_budget) FROM project_final_budget) END as" + moment().subtract(1, "months").startOf("month").format('MMMM')))
                    .whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
                        moment().subtract(1, "months").startOf("month").format("YYYY-MM-DD"),
                        moment().subtract(1, "month").format("YYYY-MM-DD"),
                    ]).groupBy("project_final_budget.company_id");
            })
            .modifyGraph("earning_secondlast_month", (builder) => {
                builder.select("company_id", Knex.raw("case when (SELECT sum(final_budget)) IS NULL THEN 0 else (SELECT sum(final_budget) FROM project_final_budget) END as " + moment().subtract(1, "months").startOf("month").format('MMMM')))
                    .whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
                        moment().subtract(2, "months").startOf("month").format("YYYY-MM-DD"),
                        moment().subtract(2, "month").format("YYYY-MM-DD"),
                    ]).groupBy("project_final_budget.company_id");
            }).orderBy("earning", "desc").limit(5)
        console.log(earning_data)
        if (!earning_data) {
            throw globalCalls.badRequestError("No Client found.");
        }
        else {
            let responseData = { earning_data }
            return globalCalls.okResponse(res, responseData, "");
        }
    }
    catch (err) {
        console.log(err)
        throw globalCalls.badRequestError(error.message)
    }
}


//Report Categories
const reportCategories = async (req, res) => {
    let responseData={}
    try {
        let CompanyRegistrations = await Company.query().select("domain_list.domain", Knex.raw("(SELECT count(company_id) AS company_count)"))
            .leftJoin("domain_list", function () {
                this.on(
                    "company.domain_id",
                    "=",
                    "domain_list.domain_id"
                );
            }).groupBy("domain_list.domain").orderBy("company_count", "DESC").havingNotNull("domain_list.domain")
        console.log(CompanyRegistrations)
        
        let ProjectPost=await project.query().select("domain_list.domain", Knex.raw("(SELECT count(project_id) AS project_count)"))
        .leftJoin("domain_list", function () {
            this.on(
                "project.domain_id",
                "=",
                "domain_list.domain_id"
            );
        }).groupBy("domain_list.domain").orderBy("project_count", "DESC").havingNotNull("domain_list.domain")
        
        console.log(ProjectPost)
        responseData.CompanyRegistrations=CompanyRegistrations
        responseData.ProjectRegistrations=ProjectPost
        return globalCalls.okResponse(res, responseData, "");
        
    }
    catch (error) {
        console.log(error);
    }
}
 //Report Earning Category(Domain wise Earning)

 const Domain_Wise_Earning=async(req,res)=>
 {
     const earning_data=await await DomainWiseEarning.query().select("domain_list.domain", Knex.raw("(SELECT sum(final_budget) AS earning)"))
     .leftJoin("project", function () {
        this.on(
            "project_final_budget.project_id",
            "=",
            "project.project_id"
        );
    })
     .leftJoin("domain_list", function () {
         this.on(
             "project.domain_id",
             "=",
             "domain_list.domain_id"
         );
     }).groupBy("domain_list.domain").orderBy("company_count", "DESC").havingNotNull("domain_list.domain")

     console.log(earning_data)
 }




module.exports = {
    ListClient,
    company_earning,
    ClientDetails,
    ActiveBlockClient,
    client_registration_by_country,
    reportCategories,
    client_project_post,
    client_project_amount,
    Domain_Wise_Earning
}