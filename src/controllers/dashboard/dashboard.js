const company = require('../../models/Company');
const CompanyVisitTracker = require('../../models/CompanyVisitTracker');

const globalCalls = require('../../settings/functions');

const moment = require('moment');

const knexConfig        = require('../../../config/knex');
const ShortlistedCompanies = require('../../models/ShortlistedCompanies');
const Project = require('../../models/Project');
const ProjectRecommendedCompanies = require('../../models/ProjectRecommendedCompanies');
const CompanyProgress = require('../../models/CompanyProgress');
const ProjectFinalBudget = require('../../models/ProjectFinalBudget');
const Knex              = require('knex')(knexConfig.development);

const dashboard_company = async (req, res) => {
  try {
    let company_id = req.user.default_company;
    
    let responseData = {
      showWelcome: true
    };

    if(company_id != null){
      let companyProgress = await CompanyProgress.query().select("progress").where({"company_id": company_id, "progress": "Company Info"}).first();

      if(companyProgress){
        let graphCards = await CompanyVisitTracker.query().select(Knex.raw('COUNT(CASE WHEN "created_at"::date >= \''+moment().startOf("month").format("YYYY-MM-DD")+'\' AND "created_at"::date <= \''+ moment().format("YYYY-MM-DD")+'\' THEN 1 ELSE NULL END) AS currentMonth, COUNT(CASE WHEN "created_at"::date >= \''+moment().subtract(1, "months").startOf("month").format("YYYY-MM-DD")+'\' AND "created_at"::date <= \''+moment().subtract(1, "month").format("YYYY-MM-DD")+'\' THEN 1 ELSE NULL END) AS lastMonth, \'Profile Visit\' AS title, \'card1.svg\' AS icon')).whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
          moment().subtract(1, "months").startOf("month").format("YYYY-MM-DD"),
          moment().format("YYYY-MM-DD"),
        ]).where("company_id", req.user.default_company).unionAll([ProjectRecommendedCompanies.query().select(Knex.raw('COUNT(CASE WHEN "created_at"::date >= \''+moment().startOf("month").format("YYYY-MM-DD")+'\' AND "created_at"::date <= \''+ moment().format("YYYY-MM-DD")+'\' THEN 1 ELSE NULL END) AS currentMonth, COUNT(CASE WHEN "created_at"::date >= \''+moment().subtract(1, "months").startOf("month").format("YYYY-MM-DD")+'\' AND "created_at"::date <= \''+moment().subtract(1, "month").format("YYYY-MM-DD")+'\' THEN 1 ELSE NULL END) AS lastMonth, \'Recommended\' AS title, \'card2.svg\' AS icon')).whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
          moment().subtract(1, "months").startOf("month").format("YYYY-MM-DD"),
          moment().format("YYYY-MM-DD"),
        ]).where("company_id", company_id), Project.query().select(Knex.raw('COUNT(CASE WHEN "created_at"::date >= \''+moment().startOf("month").format("YYYY-MM-DD")+'\' AND "created_at"::date <= \''+ moment().format("YYYY-MM-DD")+'\' THEN 1 ELSE NULL END) AS currentMonth, COUNT(CASE WHEN "created_at"::date >= \''+moment().subtract(1, "months").startOf("month").format("YYYY-MM-DD")+'\' AND "created_at"::date <= \''+moment().subtract(1, "month").format("YYYY-MM-DD")+'\' THEN 1 ELSE NULL END) AS lastMonth, \'New Project Post\' AS title,  \'icon-project.svg\' AS icon')).whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
          moment().subtract(1, "months").startOf("month").format("YYYY-MM-DD"),
          moment().format("YYYY-MM-DD"),
        ]), ShortlistedCompanies.query().select(Knex.raw('COUNT(CASE WHEN "created_at"::date >= \''+moment().startOf("month").format("YYYY-MM-DD")+'\' AND "created_at"::date <= \''+ moment().format("YYYY-MM-DD")+'\' THEN 1 ELSE NULL END) AS currentMonth, COUNT(CASE WHEN "created_at"::date >= \''+moment().subtract(1, "months").startOf("month").format("YYYY-MM-DD")+'\' AND "created_at"::date <= \''+moment().subtract(1, "month").format("YYYY-MM-DD")+'\' THEN 1 ELSE NULL END) AS lastMonth, \'Quotation Request Receive\' AS title,  \'icon-new-quotation.svg\' AS icon')).whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
          moment().subtract(1, "months").startOf("month").format("YYYY-MM-DD"),
          moment().format("YYYY-MM-DD"),
        ]).where("company_id", req.user.default_company)]);
  
        let recommendadedData;
  
        let companyVisitHeading = await CompanyVisitTracker
        .query()
        .select(Knex.raw('COUNT(CASE WHEN "created_at"::date >= \''+moment().subtract(30, "days").format("YYYY-MM-DD")+'\' AND "created_at"::date <= \''+ moment().format("YYYY-MM-DD")+'\' THEN 1 ELSE NULL END) AS last30days, COUNT(CASE WHEN "created_at"::date >= \''+moment().startOf('week').format("YYYY-MM-DD")+'\' AND "created_at"::date <= \''+moment().format("YYYY-MM-DD")+'\' THEN 1 ELSE NULL END) AS thisweek,  COUNT(CASE WHEN "created_at"::date >= \''+moment().subtract(1, "weeks").startOf('week').format("YYYY-MM-DD")+'\' AND "created_at"::date <= \''+moment().subtract(1, "weeks").endOf('week').format("YYYY-MM-DD")+'\' THEN 1 ELSE NULL END) AS lastweek'))
        .whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
            moment().subtract(30, "days").format("YYYY-MM-DD"),
            moment().format("YYYY-MM-DD"),
          ]).where("company_id", company_id)
        .first();
  
        let graphDataQuery = await CompanyVisitTracker.query().select(Knex.raw('COUNT(tracking_id), "created_at"::date')).whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
          moment().subtract(7, "days").format("YYYY-MM-DD"),
          moment().format("YYYY-MM-DD"),
        ]).where("company_id", company_id).groupByRaw('"created_at"::date').orderBy("created_at");
  
        let graphData = {
          data: [],
          label: []
        };
  
        let startDate = moment().subtract(7, "days").format("YYYY-MM-DD");
        for (let i = 1; i < 8; i++){
           let indexFound = graphDataQuery.findIndex((x) => moment(x.created_at).format('YYYY-MM-DD') == startDate);
           
           if(indexFound !== -1){
            graphData.data.push(parseInt(graphDataQuery[indexFound].count));
            graphData.label.push(moment(startDate).format('MMM DD'));
           } else {
           
            graphData.data.push(0);
            graphData.label.push(moment(startDate).format('MMM DD'));
           }
           startDate = moment(startDate).add(1, "day").format("YYYY-MM-DD");
         }
  
      let companyRankData = await company
        .query()
        .select("company_id", "company_name", "logo")
        .withGraphFetched("[shortlisted, hired, reviews]")
        .modifyGraph("hired", (builder) => {
          builder
            .count()
            .where("status", "selected")
            .groupBy("shortlisted_companies.company_id");
        })
        .modifyGraph("shortlisted", (builder) => {
          builder
            .count()
            .groupBy("shortlisted_companies.company_id");
        })
        .modifyGraph("reviews", (builder) => {
          builder
          .count()
            .groupBy("reviews.company_id");
        })
        .where("domain_id", "=", company.query().select('domain_id').where("company_id", req.user.default_company)).offset(0)
        .limit(5);
  
        let myRankData = await company
        .query()
        .select("company_id", "company_name", "logo")
        .withGraphFetched("[shortlisted, hired, reviews]")
        .modifyGraph("hired", (builder) => {
          builder
            .count()
            .where("status", "selected")
            .groupBy("shortlisted_companies.company_id");
        })
        .modifyGraph("shortlisted", (builder) => {
          builder
            .count()
            .groupBy("shortlisted_companies.company_id");
        })
        .modifyGraph("reviews", (builder) => {
          builder
          .count()
            .groupBy("reviews.company_id");
        })
        .where({
          company_id: company_id,
        }).first();
  
        responseData = {
          graphCards,
          companyVisitHeading,
          graphData,
          companyRankData,
          myRankData,
          companyProgress,
          showWelcome: false
        }
      } else {
        responseData = {
          companyProgress: companyProgress,
          showWelcome: true
        }
      }
      
    }

    return globalCalls.okResponse(res, responseData, "");
  } catch (error) {
    console.log(error);
    throw globalCalls.badRequestError(error.message);
  }
};

const dashboard_client = async (req, res) => {
  try {
    
    let projectBasisCount = await Project.query().select(Knex.raw("COUNT(CASE WHEN status = 'draft' THEN 1 ELSE NULL END) AS draftCount, COUNT(CASE WHEN status = 'complete' THEN 1 ELSE NULL END) AS completedProject,  COUNT(CASE WHEN status = 'archieve' THEN 1 ELSE NULL END) AS archiveProject, COUNT(CASE WHEN status = 'in-progress' THEN 1 ELSE NULL END) AS inProgress"))
      .where("user_id", req.user.user_id)
      .first();

      let quotationBasedCount = await ShortlistedCompanies.query().select(Knex.raw("COUNT(CASE WHEN shortlisted_companies.status = 'quotation_request' THEN 1 ELSE NULL END) AS pendingQuotation, COUNT(CASE WHEN shortlisted_companies.status = 'quotation_received' THEN 1 ELSE NULL END) AS approvedQuotation, COUNT(CASE WHEN shortlisted_companies.status = 'under_discussion' THEN 1 ELSE NULL END) AS underDiscussion")).innerJoinRelated("project").where("client_id", req.user.user_id).where("project.status", "!=", "archieve").first();

   
    let all_project = await Project.query().whereIn("status", ["submitted", "requested-quotation", "in-progress", "complete"]).count().first();

    let all_company = await company.query().count().where("status", "approved").first();

    let total_budget = await ProjectFinalBudget.query().sum("final_budget").first();

    let domainWiseList = await Project.query().count().select("domain").innerJoinRelated("domain").whereIn("project.status", ["submitted", "requested-quotation", "in-progress", "complete"]).groupBy("domain.domain_id").orderBy("count", "desc").limit(5);

    let techWiseList = await Project.query().count().select("skills").innerJoinRelated("skillLabel").whereIn("project.status", ["submitted", "requested-quotation", "in-progress", "complete"]).groupBy("skillLabel.skill_id").orderBy("count", "desc").limit(5)
    

    // let location_data = await project
    //   .query()
    //   .select("location")
    //   .count("project_id  as project_count")
    //   .whereNot("location", "")
    //   .groupBy("project.location")
    //   .orderBy("project_count", "DESC");

          

    let responseData = {
      projectBasisCount,
      quotationBasedCount,
      all_project,
      all_company,
      total_budget,
      domainWiseList,
      techWiseList
    }

    return globalCalls.okResponse(res, responseData, "");
  } catch (error) {
   
    throw globalCalls.badRequestError(error.message);
  }
};
module.exports = {
  dashboard_company,
  dashboard_client
}