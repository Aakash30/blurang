'use strict';

const globalCalls = require('../settings/functions');
const companyProgress = require('../models/CompanyProgress');
const { fn, ref } = require('objection');
const city = require('../models/City');

const knexConfig        = require('../../config/knex');
const Knex              = require('knex')(knexConfig.development);

const fetchCompanyProgress = async(req, res) => {

    let companyProgressBar = await companyProgress.query().select("progress").where("company_id", req.user.default_company);
    
    let progressBar = [];
    if(companyProgressBar.length > 0) {
        companyProgressBar.forEach((value)=>{
            progressBar.push(value.progress)
        })
    }
    return globalCalls.okResponse(res, progressBar, "");
}

const fetchCountryStateCityLocation = async (req, res) =>{
   try{
    let locationData =[];
    
    if(req.query.keyword.trim()!=""){
        locationData = await city.query().select(Knex.raw("concat(city, ', ', state, ', ', country) as location")).innerJoinRelated("states").innerJoinRelated("countries")
        .whereRaw("concat(city, ', ', state, ', ', country) ilike ?",  '%'+req.query.keyword+'%').limit(5).offset(0);
    }
   
    let responseData = [];
    if(locationData.length != 0){

        locationData.forEach((value) =>{
            responseData.push(value.location);
        })
     
    }
    return globalCalls.okResponse(res, responseData, "");
   } catch (error) {
       console.log(error)
       throw globalCalls.badRequestError(error.message);
   }
}

const checkAndUpdateProgress = async (company_id, user_id, progress) => {

    let checkIfcompanyProgress = await companyProgress.query().select("").where("company_id", company_id).where("progress", progress).first();
  
    if(!checkIfcompanyProgress){
      let updateCompanyProgress = await companyProgress.query().insert({
        progress: progress,
        company_id: company_id,
        user_id: user_id
      });
    }
    
  }
module.exports = {
    fetchCompanyProgress,
    fetchCountryStateCityLocation,
    checkAndUpdateProgress
}