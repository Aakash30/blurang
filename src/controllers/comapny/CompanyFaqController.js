"use strict";
const { base64encode, base64decode } = require("nodejs-base64");
const globalCalls = require("../../settings/functions");
const CompanyFaq = require("../../models/CompanyFaq");
const FaqQuestions = require("../../models/FaqQuestions");
const ClientQueries = require("../../models/ClientQueries");
const messageTips = require("../../models/MessageTips");

const checkAndUpdateProgress = require("../../controllers/common").checkAndUpdateProgress;
module.exports = {
  AddUpdateFaqQuestion: async (req, res) => {
    try {
      let company_id = req.user.default_company;
      
      if (!req.body.questions && !req.body.questions.length) {
        throw globalCalls.badRequestError("Please enter your answer.");
      }

	  let questions = req.body.questions;
	  let newAnswerArray = [];
	  let error = false;
      questions.forEach((element, index) => {
		questions[index].company_id = company_id;

		if(element.faq_id == null){
			delete element.faq_id;
		}

		if ((element.answer == null ||  element.answer.trim() == "") && element.is_default) {
			error = true;
			delete element.is_default;
		} else if (element.answer == null || element.answer.trim() == ""){
			delete questions[index];
		} else {
			delete element.is_default;
		}

		if(questions[index]){
			newAnswerArray.push(element)
		}
	  });
	  
      let faqQusRes = await CompanyFaq.query().upsertGraph(newAnswerArray, {
          relate: true,
          unrelate: true
      });

      if(faqQusRes) {

        await checkAndUpdateProgress(req.user.default_company, req.user.user_id, 'Q&A');
          return globalCalls.okResponse(res, faqQusRes, "FAQ answer successfully added.");
      } else{
        throw globalCalls.badRequestError("Something went wrong.");
      }
     
    } catch (e) {
      console.log("Error in CompanyFaqController.AddUpdateQuestion", e);
      throw globalCalls.badRequestError(e.message);
    }
  },
  GetCompanyFaq: async (req, res) => {
    try {
      let company_id = req.user.default_company;
      let faqs = await FaqQuestions.query()
        .select("faq_questions.question_id", "question", "is_default", "answer", "faq_id")
        .leftJoin("company_faq", function () {
          this.on(
            "faq_questions.question_id",
            "=",
            "company_faq.question_id"
          ).andOn("company_id", company_id);
        })
        .orderBy("is_default", "desc");
      let tips = await messageTips
        .query()
        .select("message")
        .where("module", "Fill up Company FAQ")
		.first();
		
		let responseData = {
			faqs,
			tips
		}
      return globalCalls.okResponse(res, responseData, "");
    } catch (e) {
      console.log("Error in CompanyFaqController.GetComapnyFaq", e);
      throw globalCalls.badRequestError(e.message);
    }
  },

  /****************** Queries Ans And Qus ******************/
  SendQueryAnswer: async (req, res) => {
    try {
      let data = req.body;

      if(!data.query_id){
        throw globalCalls.badRequestError("Invalid Reqeuest.");
      }

      if(!data.answer){
        throw globalCalls.badRequestError("Please enter answer.");
      }

      let update = await ClientQueries.query().update({
        answer: data.answer,
        updated_at: new Date().toISOString(),
        status: "active"
      }).where("query_id", data.query_id).where("company_id", req.user.default_company).returning("*");

      if(!update){
        throw globalCalls.badRequestError("Something went wrong.");
      }

      return globalCalls.okResponse(res, {update}, "Answer has been updated successfully.");

    } catch (e) {
      throw globalCalls.badRequestError(e.message);
    }
  },
  /**
   * Fetches Query List
   */
  fetchQueryList: async (req, res) =>{

    try{
      let questionList = await ClientQueries.query().select("query_id", "question", "answer", "query_posted_at").where("company_id", req.user.default_company).where((builder) =>{
        if(req.query.list_for == "unanswered"){
          builder.where("status", "pending");
        } else {
          builder.where("status", "active");
        }
      });
      if(!questionList){
        throw globalCalls.badRequestError("Something went wrong");
      }

      let responseData = {
        questionList
      }

      return globalCalls.okResponse(res, responseData, "");
    } catch (error){
      throw globalCalls.badRequestError(error.message);
    }
  },

  deleteQueryQuestion: async(req, res) =>{

    if(!req.params.id){
      throw globalCalls.badRequestError("Invalid Request.");
    }

    let queryId = base64decode(req.params.id);
    if(isNaN(parseInt(queryId)))
      throw globalCalls.badRequestError("Error! You have passed invalid-id.");
  
    let deleteQuery = await ClientQueries.query().update({
      status: "block"
    }).where("query_id", queryId).returning("*");

    if(!deleteQuery){
      throw globalCalls.badRequestError("Something went wrong.");
    }

    return globalCalls.okResponse(res, deleteQuery, "The query has been deleted successfully.");
  }
};
