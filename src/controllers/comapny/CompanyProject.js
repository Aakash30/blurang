"use strict";

const { base64decode } = require("nodejs-base64");
const globalCalls = require("../../settings/functions");
const globalConst = require("../../settings/constants");
const PERPAGE = require("../../settings/constants").per_page;

const messageTips = require("../../models/MessageTips");

const quotationListing = require("../../models/ShortlistedCompanies");
const quotationData = require("../../models/QuotationData");
const shortlistedCompany = require("../../models/ShortlistedCompanies");
const project = require("../../models/Project");
const company = require("../../models/Company");
const notification = require("../../models/Notification");
const companyVisitTracker = require("../../models/CompanyVisitTracker");
const projectRecommendedCompanies = require("../../models/ProjectRecommendedCompanies");

const NotificationUpdate = require('../notification/notificationcontroller').updateNotification;

const notificationcontroller = require('../notification/notificationcontroller');


const knexConfig = require("../../../config/knex");
const Knex = require("knex")(knexConfig.development);

const { transaction, ref } = require("objection");
const moment = require("moment");
const Company = require("../../models/Company");
/**
 * Fetch quotation request and sent quotation
 * @param {*} req
 * @param {*} res
 */

const getproject_company = async (req, res) => {
  try {
    let company_id = req.user.default_company;

    let page = req.query.page ? req.query.page : 1;
    let limit = req.query.limit ? req.query.limit : globalConst.per_page;
    let offset = req.query.offset ? req.query.offset : limit * (page - 1);

    let projectData = await project
      .query()
      .select(
        "project_id",
        "project_name",
        "one_line_description",
        "about_the_project",
        "budget",
        "payment_type",
        "location",
        "expected_timeline",
        "ideal_team_size",
        "domain_experience_in_months",
        "company_expected_experience",
        "hire_date",
        "completion_datetime",
        "archived_by_type",
        "is_completed_request"
      )
      .withGraphFetched("[services, domain]")
      .modifyGraph("services", (builder) => {
        builder
          .withGraphFetched("service_details")
          .modifyGraph("service_details", (builder) => {
            builder.select("service_id", "service_name", "service_icon");
          });
        builder.select("service_id", "project_service_id");
      })
      .modifyGraph("domain", (builder) => {
        builder.select("domain");
      })
      .where((builder) => {
        builder.where("assigned_to_company", company_id);
        if (req.query.type) {
          builder.where("status", req.query.type);
        }
      })
      .orderBy("project_id", "DESC")
      .offset(offset)
      .limit(limit);

    let totalCount = await project
      .query()
      .count()
      .where((builder) => {
        builder.where("assigned_to_company", company_id);
        if (req.query.type) {
          builder.where("status", req.query.type);
        }
      })
      .first();

    if (!projectData) {
      throw globalCalls.badRequestError("No Project found.");
    } else {
      let responseData = {
        projectData,
        total: totalCount.count,
      };

      return globalCalls.okResponse(res, responseData, "");
    }
  } catch (error) {
    console.log(error);
    throw globalCalls.badRequestError(error.message);
  }
};

const getcompleteproject_company = async (req, res) => {
  let company_id = req.user.default_company;

  let page = req.query.page ? req.query.page : 1;
  let limit = req.query.limit ? req.query.limit : globalConst.per_page;
  let offset = req.query.offset ? req.query.offset : limit * (page - 1);

  let projectData = await project
    .query()
    .select(
      "project_id",
      "project_name",
      "one_line_description",
      "about_the_project",
      "budget",
      "payment_type",
      "location",
      "expected_timeline",
      "ideal_team_size",
      "domain_experience_in_months",
      "company_expected_experience",
      "hire_date",
      "completion_datetime"
    )
    .withGraphFetched("[services]")
    .modifyGraph("services", (builder) => {
      builder
        .withGraphFetched("service_details")
        .modifyGraph("service_details", (builder) => {
          builder.select("service_id", "service_name", "service_icon");
        });
      builder.select("service_id", "project_service_id");
    })
    // .where((builder) => {
    //   builder.where("assigned_to_company", req.user.user_id);
    //  builder.where("status", 'complete');
    // })
    .where({
      assigned_to_company: company_id,
      status: "complete",
    })
    .orderBy("completion_datetime", "DESC")
    .offset(offset)
    .limit(limit);

  let totalCount = await project
    .query()
    .count()
    .where({
      assigned_to_company: company_id,
      status: "complete",
    })
    .first();

  if (!projectData) {
    throw globalCalls.badRequestError("No Project found.");
  } else {
    let responseData = {
      projectData,
      total: totalCount.count,
    };

    return globalCalls.okResponse(res, responseData, "");
  }
};

const ProjectDetails_company = async (req, res) => {
  if (req.params.id) {
    try {
      let company_id = req.user.default_company;

      var project_id = base64decode(req.params.id);
      if(isNaN(parseInt(project_id)))
        throw globalCalls.notFoundError("Error! You have passed invalid-id.");
  
      let projectData = await project
        .query()
        .select(
          "project_id",
          "project_name",
          "one_line_description",
          "about_the_project",
          "budget",
          "payment_type",
          "location",
          "expected_timeline",
          "ideal_team_size",
          "domain_experience_in_months",
          "company_expected_experience"
        )
        .withGraphFetched(
          "[servicesLabels, skillLabel, projectAnswer, project_docx, quotation, domain]"
        )
        .modifyGraph("servicesLabels", (builder) => {
          builder.select("service_name");
        })
        .modifyGraph("skillLabel", (builder) => {
          builder.select("skills");
        })
        .modifyGraph("domain", (builder) => {
          builder.select("domain");
        })
        .modifyGraph("projectAnswer", (builder) => {
          builder
            .select("answer", "question", "type")
            .innerJoinRelated("question")
            .orderBy("type");
        })
        .modifyGraph("project_docx", (builder) => {
          builder.select("docx_url", "file_type");
        })
        .modifyGraph("quotation", (builder) => {
          builder
            .select(
              "quotation_id",
              "estimated_timeline",
              "team_size",
              "fixed_budget",
              "comments",
              "company_id"
            )
            .withGraphFetched("[technologies_label, quotation_docx]")
            .modifyGraph("quotation_docx", (builder) => {
              builder.select("file_link", "file_type");
            })
            .modifyGraph("technologies_label", (builder) => {
              builder.select("skills");
            })
            .where({
              company_id: company_id,
            });
        })
        .where({
          assigned_to_company: company_id,
          project_id: project_id,
        });
      

      if (!projectData) {
        throw globalCalls.badRequestError("Project details not found.");
      }

      if (req.query.readNotice && req.query.readNotice != '') {
        let id = base64decode(req.query.readNotice);
  
        notificationcontroller.updateNotificationReadStatus(id);
      }
      let responseData = {
        projectData,
      };

      return globalCalls.okResponse(res, responseData, "");
    } catch (error) {
      throw globalCalls.badRequestError(error.message);
    }
  } else {
    return globalCalls.badRequestError("Invalid Request");
  }
};

const completeProjectDetails_company = async (req, res) => {
  if (req.params.id) {
    let company_id = req.user.default_company;

    var project_id = base64decode(req.params.id);
    if(isNaN(parseInt(project_id)))
      throw globalCalls.notFoundError("Error! You have passed invalid-id.");
  
    let projectData = await project
      .query()
      .select(
        "project_id",
        "project_name",
        "one_line_description",
        "about_the_project",
        "budget",
        "payment_type",
        "location",
        "expected_timeline",
        "ideal_team_size",
        "domain_experience_in_months",
        "company_expected_experience"
      )
      .withGraphFetched(
        "[services, skills, project_docx,projectAnswer, quotation, portfolio]"
      )
      .modifyGraph("services", (builder) => {
        builder
          .withGraphFetched("service_details")
          .modifyGraph("service_details", (builder) => {
            builder.select("service_id", "service_name", "service_icon");
          });
        builder.select("service_id", "project_service_id");
      })
      .modifyGraph("skills", (builder) => {
        builder
          .withGraphFetched("skill_details")
          .modifyGraph("skill_details", (builder) => {
            builder.select("skill_id", "skills");
          });
        builder.select("skill_id", "project_skill_id");
      })
      .modifyGraph("projectAnswer", (builder) => {
        builder
          .withGraphFetched("question")
          .modifyGraph("question", (builder) => {
            builder.select("question_id", "question", "type");
          });
        builder.select("answer_id", "question_id", "answer");
      })
      .modifyGraph("quotation", (builder) => {
        builder
          .withGraphFetched("quotation_docx")
          .modifyGraph("skill_details", (builder) => {
            builder.select("skill_id", "file_type");
          });
        builder
          .select("estimated_timeline", "team_size", "fixed_budget", "comments")
          .where(
            "company_id",
            project
              .query()
              .select("assigned_to_company")
              .where({ project_id: project_id })
          );
      })
      .modifyGraph("portfolio", (builder) => {
        builder
          .select(
            "portfolio_id",
            "project_name",
            "one_line_description",
            "project_logo",
            "timeline",
            "budget",
            "about_the_project",
            "client_location",
            "status"
          )
          .withGraphFetched("[reviews,gallery,links,portfolioSkills]")
          .modifyGraph("reviews", (builder) => {
            builder
              .select(
                "reviews",
                "feedback_received",
                Knex.raw(
                  "(SELECT CASE WHEN starsAvg IS NULL THEN 0 ELSE starsAvg END FROM (SELECT ROUND(AVG(stars),2) AS starsAvg FROM review_rating WHERE review_id = reviews.review_id) AS averageStars) AS rating_avg"
                )
              )
              .withGraphFetched("ratings")
              .modifyGraph("ratings", (builder) => {
                builder
                  .select("stars", "rating_title")
                  .innerJoinRelated("ratingPoint")
                  .orderBy("review_rating_id");
              })
              .where("is_valid", true);
          })
          .where((builder) => {
            builder.where("status", "submitted").orWhere("status", "published");
          });
      })
      .where({
        assigned_to_company: company_id,
        project_id: project_id,
      });
    // .where("project_id", project_id);

    if (!projectData) {
      throw globalCalls.badRequestError("Project details not found.");
    } else {
      let responseData = {
        projectData,
      };

      return globalCalls.okResponse(res, responseData, "");
    }
  } else {
  }
};

const RequestForCompleteProject = async (req, res) => {
  try {
    if (req.params.id) {
      var data = {};
      var project_id = base64decode(req.params.id);
      if(isNaN(parseInt(project_id)))
	      throw globalCalls.badRequestError("Error! You have passed invalid-id.");
      
      let projectData = await project
        .query()
        .select("user_id")
        .where({
          assigned_to_company: req.user.user_id,
          project_id: project_id,
        })
        .first();
      if (!projectData) {
        throw globalCalls.badRequestError("Project belongs to other company.");
      }

      let update_project = await project
        .query()
        .upsertGraph({ is_completed_request: true, project_id: project_id })
        .returning("*");

      let companyName = await Company.query().select("company_name").where("company_id", req.user.default_company).first();

      let messageNotify = Object.assign({}, globalConst.NOTIFICATION_PAYLOAD["REQUEST_FOR_COMPLETION_TO_CLIENT"]);

      let bodyMessage = messageNotify.body.replace("[COMPANY]", "<b>" + companyName.company_name + "</b>");
      messageNotify.body = bodyMessage.replace("[PROJECT_NAME]", "<b>" + update_project.project_name + "</b>");
      let notifyData = [];


      notifyData.push({
        "message": messageNotify.body,
        "notification_for": "PROJECT",
        "notification_for_id": project_id,
        "to_user_id": projectData.user_id,
        "by_user_id": req.user.user_id,
        "redirect_url": "/project-details/" + base64encode(project_id),
        "redirect_component": "PROJECT",
        "created_at": new Date().toISOString()
      });

      if (notifyData.length > 0) {
        await NotificationUpdate(notifyData)
      }

      if (!inserted) {
        throw globalCalls.badRequestError("something went wrong.");
      } else {
        return globalCalls.okResponse(
          res,
          inserted,
          "Request sent successfully."
        );
      }
    } else {
    }
  } catch (error) {
    console.log(error);
    throw globalCalls.badRequestError(error.message);
  }
};

const dashboard_company = async (req, res) => {
  try {
    let company_id = req.user.default_company;
    
    let for_domain = await company
      .query()
      .select("domain_id")
      .where("company_id", company_id)
      .first();
    let company_domain_id = for_domain.domain_id;

    //profile_visit block start
    let profile_visit = await company
      .query()
      .select("viewer_count")
      .where("company_id", company_id)
      .first();

    let profile_visit_thismonth = await companyVisitTracker
      .query()
      .count()
      .where(function () {
        this.where("company_id", company_id);
        this.whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
          moment().startOf("month").format("YYYY-MM-DD"),
          moment().format("YYYY-MM-DD"),
        ]);
      })
      .first();

    let profile_visit_lastmonth = await companyVisitTracker
      .query()
      .count()
      .where(function () {
        this.where("company_id", company_id);
        this.whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
          moment().subtract(1, "months").startOf("month").format("YYYY-MM-DD"),
          moment().subtract(1, "month").format("YYYY-MM-DD"),
        ]);
      })
      .first();
    profile_visit.statistics =
      ((profile_visit_thismonth.count - profile_visit_lastmonth.count) /
        profile_visit_lastmonth.count) *
      100;
    //profile_visit block start

    //shortlisting block start
    let shortlisting = await shortlistedCompany
      .query()
      .count()
      .where(function () {
        this.where("company_id", company_id);
        this.whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
          moment().startOf("month").format("YYYY-MM-DD"),
          moment().format("YYYY-MM-DD"),
        ]);
      })
      .first();

    let shortlisting_lastmonth = await shortlistedCompany
      .query()
      .count()
      .where(function () {
        this.where("company_id", company_id);
        this.whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
          moment().subtract(1, "months").startOf("month").format("YYYY-MM-DD"),
          moment().subtract(1, "month").format("YYYY-MM-DD"),
        ]);
      })
      .first();
    shortlisting.statistics =
      ((shortlisting.count - shortlisting_lastmonth.count) /
        shortlisting_lastmonth.count) *
      100;
    //shortlisting block start

    //new project block start
    let new_project = await project
      .query()
      .count()
      .where(function () {
        this.whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
          moment().startOf("month").format("YYYY-MM-DD"),
          moment().format("YYYY-MM-DD"),
        ]);
      })
      .first();

    let new_project_lastmonth = await project
      .query()
      .count()
      .where(function () {
        this.whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
          moment().subtract(1, "months").startOf("month").format("YYYY-MM-DD"),
          moment().subtract(1, "month").format("YYYY-MM-DD"),
        ]);
      })
      .first();
    new_project.statistics =
      ((new_project.count - new_project_lastmonth.count) /
        new_project_lastmonth.count) *
      100;
    //new project block end

    //quotation_request_recieve block start
    let quotation_request_recieve = await shortlistedCompany
      .query()
      .count()
      .where(function () {
        this.whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
          moment().startOf("month").format("YYYY-MM-DD"),
          moment().format("YYYY-MM-DD"),
        ]);
        this.where("company_id", company_id);
      })
      .first();

    let quotation_request_recieve_last = await shortlistedCompany
      .query()
      .count()
      .where(function () {
        this.whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
          moment().subtract(1, "months").startOf("month").format("YYYY-MM-DD"),
          moment().subtract(1, "month").format("YYYY-MM-DD"),
        ]);
        this.where("company_id", company_id);
      })
      .first();
    quotation_request_recieve.statistics =
      ((quotation_request_recieve.count -
        quotation_request_recieve_last.count) /
        quotation_request_recieve_last.count) *
      100;
    //quotation_request_recieve block start

    //companyVisitmap block start
    //companyVisitmap days calculation block start
    let companyVisitmap = [];
    let day1 = await companyVisitTracker
      .query()
      .count()
      .where(function () {
        this.whereRaw('"created_at"::date = ?', [
          moment().format("YYYY-MM-DD"),
        ]);
        this.where("company_id", company_id);
      })
      .first();
    day1.days = moment().format("YYYY-MM-DD") + "T18:30:00.000Z";
    companyVisitmap.push(day1);

    let day = [];
    for (let i = 2; i < 8; i++) {
      day = await companyVisitTracker
        .query()
        .count()
        .where(function () {
          this.whereRaw('"created_at"::date = ?', [
            moment()
              .subtract(i - 1, "days")
              .format("YYYY-MM-DD"),
          ]);
          this.where("company_id", company_id);
        })
        .first();
      day.days =
        moment()
          .subtract(i - 1, "days")
          .format("YYYY-MM-DD") + "T18:30:00.000Z";
      companyVisitmap.push(day);
    }
    //companyVisitmap days calculation block start

    let companyVisitmap_thisweek = await companyVisitTracker
      .query()
      .count()
      .where(function () {
        this.whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
          moment().subtract(7, "days").format("YYYY-MM-DD"),
          moment().format("YYYY-MM-DD"),
        ]);
        this.where("company_id", company_id);
      })
      .first();

    let companyVisitmap_lastweek = await companyVisitTracker
      .query()
      .count()
      .where(function () {
        this.whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
          moment().subtract(15, "days").format("YYYY-MM-DD"),
          moment().subtract(8, "days").format("YYYY-MM-DD"),
        ]);
        this.where("company_id", company_id);
      })
      .first();
    let companyVisit_week_statistics =
      ((companyVisitmap_thisweek.count - companyVisitmap_lastweek.count) /
        companyVisitmap_lastweek.count) *
      100;

    let companyVisitmap_last30days = await companyVisitTracker
      .query()
      .count()
      .where(function () {
        this.whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
          moment().subtract(30, "days").format("YYYY-MM-DD"),
          moment().format("YYYY-MM-DD"),
        ]);
        this.where("company_id", company_id);
      })
      .first();

    let companyVisiterData = {
      companyVisitmap,
      companyVisit_week_statistics,
      companyVisitmap_last30days,
    };
    //companyVisitmap block end

    /////////list company shortlisted and other data start////////////
    let companyRankData = await company
      .query()
      .select("company_id", "company_name", "logo")
      .withGraphFetched("[shortlisted, hired, reviews]")
      .modifyGraph("hired", (builder) => {
        builder
          .count("company_id as count IS NULL THEN 0")
          // .where({ status: 'quotation_request' })
          .whereIn("status", ["selected"])
          .groupBy("shortlisted_companies.company_id");
      })
      .modifyGraph("shortlisted", (builder) => {
        builder
          .select(Knex.raw("count(company_id)"))
          .groupBy("shortlisted_companies.company_id");
      })
      .modifyGraph("reviews", (builder) => {
        builder
          .select(Knex.raw("count(company_id)"))
          .groupBy("reviews.company_id");
      })
      .where({
        domain_id: company_domain_id,
      })
      .offset(0)
      .limit(5);
    // .runAfter((result, builder) =>{
    //   console.log(builder.toKnexQuery().toQuery());
    //   return result;
    // });
    /////////list company shortlisted and other data end////////////

    /////////list company shortlisted and other data start////////////
    let myRankData = await company
      .query()
      .select("company_id", "company_name", "logo")
      .withGraphFetched("[shortlisted, hired, reviews]")
      .modifyGraph("hired", (builder) => {
        builder
          .count("company_id as count IS NULL THEN 0")
          // .where({ status: 'quotation_request' })
          .whereIn("status", ["selected"])
          .groupBy("shortlisted_companies.company_id");
      })
      .modifyGraph("shortlisted", (builder) => {
        builder
          .select(Knex.raw("count(company_id)"))
          // builder.select(Knex.raw("CASE WHEN count(company_id) as count IS NULL THEN 0 ELSE count"))
          // .where({ status: 'quotation_request' })
          .groupBy("shortlisted_companies.company_id");
      })
      .modifyGraph("reviews", (builder) => {
        builder
          .select(Knex.raw("count(company_id)"))
          .groupBy("reviews.company_id");
      })
      .where({
        company_id: company_id,
      });
    /////////list company shortlisted and other data end////////////

    let responseData = {
      profile_visit,
      shortlisting,
      new_project,
      quotation_request_recieve,
      companyVisiterData,
      companyRankData,
      myRankData,
    };

    return globalCalls.okResponse(res, responseData, "");
  } catch (error) {
    // console.log(error);
    throw globalCalls.badRequestError(error.message);
  }
};

const get_matched_project = async (req, res) => {
  try {
    let company_id = req.user.default_company;
    
    let page = req.query.page ? req.query.page : 1;
    let limit = req.query.limit ? req.query.limit : globalConst.per_page;
    let offset = req.query.offset ? req.query.offset : limit * (page - 1);

    let matchedData = await projectRecommendedCompanies
      .query()
      .select("project_id", "company_id")
      .withGraphFetched("[project]")
      .modifyGraph("project", (builder) => {
        builder
          .withGraphFetched("[services,hired_company]")
          .modifyGraph("services", (builder) => {
            builder
              .withGraphFetched("service_details")
              .modifyGraph("service_details", (builder) => {
                builder.select("service_id", "service_name");
              });
            builder.select("service_id", "project_service_id");
          })
          .modifyGraph("hired_company", (builder) => {
            builder
              .withGraphFetched("address", { maxBatchSize: 1 })
              .modifyGraph("address", (builder) => {
                builder.select("location").limit(1);
              });
            builder.select("company_name");
          })
          .select(
            "project_id",
            "project_name",
            "one_line_description",
            "budget",
            "payment_type",
            "expected_timeline",
            "ideal_team_size",
            "created_at"
          );
      })
      .where({
        company_id: company_id,
      })
      .orderBy("recommended_id", "DESC")
      .offset(offset)
      .limit(limit);

    let totalCount = await projectRecommendedCompanies
      .query()
      .count()
      .where((builder) => {
        builder.where("company_id", company_id);
      })
      .first();

    if (!matchedData) {
      throw globalCalls.badRequestError("No Project found.");
    } else {
      let responseData = {
        matchedData,
        total: totalCount.count,
      };

      return globalCalls.okResponse(res, responseData, "");
    }
  } catch (error) {
    console.log(error);
    throw globalCalls.badRequestError(error.message);
  }
};

module.exports = {
  getproject_company,
  getcompleteproject_company,
  ProjectDetails_company,
  completeProjectDetails_company,
  RequestForCompleteProject,
  dashboard_company,
  get_matched_project,
};
