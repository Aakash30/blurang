"use strict";

const { base64decode } = require("nodejs-base64");
const globalCalls = require("../../settings/functions");
const PERPAGE = require("../../settings/constants").per_page;

const knexConfig = require('../../../config/knex');
const Knex = require('knex')(knexConfig.development);

const messageTips = require("../../models/MessageTips");
const company = require("../../models/Company");
const domain = require("../../models/Domain");
const service = require("../../models/Service");
const User = require("../../models/UserLogin");

const companyQuestion = require("../../models/CompanyQuestion");
const companyPortfolio = require("../../models/CompanyPortfolio");
const CompanyFaq = require('../../models/CompanyFaq');
const CompanyVisitTracker = require("../../models/CompanyVisitTracker");
const checkAndUpdateProgress = require("../../controllers/common").checkAndUpdateProgress;
const NotificationUpdate = require('../../controllers/notification/notificationcontroller').updateNotification;

/**
 * export function
 */

const fetchCompanyCertificate = require('./CompanyAchievementController').fetchCompanyCertificate;
const fetchCompanyAwards = require('./CompanyAchievementController').fetchCompanyAwards;
const fetchCompanyFeaturedData = require('./CompanyAchievementController').fetchCompanyFeaturedData;
const fetchCompanySuccessStories = require('./CompanyAchievementController').fetchCompanySuccessStories;

const { transaction, ref } = require("objection");
const moment = require("moment");
const UserLogin = require("../../models/UserLogin");

/**
 * fetches
 * @param {*} req
 * @param {*} res
 */
const fetchCompanyInformationData = async (req, res) => {
  if (req.params.id) {
    var company_id = base64decode(req.params.id);
    if (isNaN(parseInt(company_id)))
      throw globalCalls.notFoundError("Error! You have passed invalid-id.");

    let companyData;
    if (company_id != 0) {
      companyData = await company
        .query()
        .select(
          "company_id",
          "company_name",
          "logo",
          "incorporation_date",
          "director_name",
          "company_email_id",
          "company_website",
          "contact_number",
          "team_size",
          "fixed_budget",
          "hourly_budget"
        )
        .where("company_id", company_id)
        .first();

      if (!companyData) {
        throw globalCalls.badRequestError("company not found.");
      } else {
      }
    }

    let tips = await messageTips
      .query()
      .select("message")
      .where("module", "Company Profile Step #1").first();
    let responseData = {
      companyData,
      tips
    };

    return globalCalls.okResponse(res, responseData, "");
  } else {
  }
};

/**
 * function to add company information step #1
 * @param {*} req
 * @param {*} res
 */
const addCompanyInformation = async (req, res) => {
  let data = req.body;

  if (!data.company_name) {
    throw globalCalls.badRequestError("Please fill up company name.");
  }

  if (!data.incorporation_date) {
    throw globalCalls.badRequestError("Please Choose incorporation date.");
  }

  if (!data.director_name) {
    throw globalCalls.badRequestError(
      "Please fill up atleast one director name."
    );
  }

  if (!data.company_email_id) {
    throw globalCalls.badRequestError("Please fill up your company email id.");
  }

  if (!data.company_website) {
    throw globalCalls.badRequestError("Please fill up your company website.");
  }

  if (!data.contact_number) {
    throw globalCalls.badRequestError("Please add atleast one contact number.");
  }

  if (!data.team_size) {
    throw globalCalls.badRequestError("Please enter your team size.");
  }

  if (!data.fixed_budget) {
    throw globalCalls.badRequestError("Please enter your budget.");
  }

  if (req.file) {
    if (req.file.fieldname == "logo_file") {
      data.logo = req.file.location;
    }
  }
  delete data.logo_file;
  try {
    if (data.company_id == "null") {
      delete data.company_id;
      data["companyMapping"] = { user_id: req.user.user_id };
      data["defaultUser"] = { user_id: req.user.user_id };
    }

    let addCompanyInformation = await transaction(company.knex(), trx => {
      return company.query(trx).upsertGraph(data, {
        relate: true,
        unrelate: false
      });
    });

    if (!addCompanyInformation) {
      throw globalCalls.badRequestError("something went wrong.");
    } else {

      let user = await User.query().update({ "default_company": addCompanyInformation.company_id }).where("user_id", req.user.user_id);

      return globalCalls.okResponse(
        res,
        addCompanyInformation,
        "Successfully added."
      );
    }
  } catch (error) {
    throw globalCalls.badRequestError(error.message);
  }
};

/**
 * fetches data for company description
 * @param {*} req
 * @param {*} res
 */
const fetchAboutCompany = async (req, res) => {
  if (!req.params.id) {
    throw globalCalls.badRequestError("Invalid Request");
  }
  let company_id = base64decode(req.params.id);
  if (isNaN(parseInt(company_id)))
    throw globalCalls.notFoundError("Error! You have passed invalid-id.");

  let companyData = await company
    .query()
    .select("company_id", "description", "video_link", "brochure")
    .withGraphFetched("[media, address]")
    .modifyGraph("media", builder => {
      builder.select("media_id", "media_link", "media_type");
    })
    .modifyGraph("address", builder => {
      builder.select("address_id", "address", "location");
    })
    .where("company_id", company_id)
    .first();

  let tips = await messageTips
    .query()
    .select("message")
    .where("module", "Company Profile Step #2")
    .first();
  let responseData = {
    companyData,
    tips
  };

  return globalCalls.okResponse(res, responseData, "");
};
/**
 * function to add company description step #2
 * @param {*} req
 * @param {*} res
 */
const addAboutCompany = async (req, res) => {
  let data = req.body;

  let mediaFiles = [];
  try {
    if (!data.company_id) {
      throw globalCalls.badRequestError("Not a valid request.");
    }

    if (!data.description) {
      throw globalCalls.badRequestError("Please add description");
    }

    if (!data.video_link) {
      throw globalCalls.badRequestError("Please add a video");
    }

    if (!data.media && !req.files.gallery_image) {
      throw globalCalls.badRequestError("Please add atleast one media file.");
    } else {
      // mediaFiles = data.media != "" ? JSON.parse(data.media) : [];

      if (data.media != "") {
        let media = JSON.parse(data.media);
        //console.log(typeof media)
        media.forEach(innerdata => {
          if (innerdata.media_id == null) {
            delete innerdata.media_id;
          }
          mediaFiles.push(innerdata);
        });
      } else {
        mediaFiles = [];
      }

      // data.media = mediaFiles;
    }

    if (!data.address) {
      throw globalCalls.badRequestError("Please add atleast one address.");
    } else {
      if (data.address != "") {
        let address = JSON.parse(data.address);
        let addressArray = [];
        address.forEach(innerdata => {
          if (data.address_id == null) {
            delete innerdata.address_id;
          }
          addressArray.push(innerdata);
        });

        data.address = addressArray;
      } else {
      }
    }
    if (req.files && req.files.brochure_file) {
      data.brochure = req.files.brochure_file[0].location;
    } else if (!data.brochure) {
      throw globalCalls.badRequestError("Please upload brochure.");
    }

    if (req.files && req.files.gallery_image) {
      req.files.gallery_image.map(file => {
        mediaFiles.push({ media_link: file.location, media_type: "gallery" });
      });
    }

    data.media = mediaFiles;
    delete data.brochure_file;
    delete data.gallery_image;
    let updateCompanyData = await transaction(company.knex(), trx => {
      return company.query(trx).upsertGraph(data, {
        relate: true,
        unrelate: false
      });
    });

    if (!updateCompanyData) {
      throw globalCalls.badRequestError("Something went wrong");
    }

    return globalCalls.okResponse(res, updateCompanyData, "");
  } catch (error) {
    throw globalCalls.badRequestError(error.message);
  }
};

/**
 * fetches skill set data
 * @param {*} req
 * @param {*} res
 */
const fetchSkillsetData = async (req, res) => {
  if (!req.params.id) {
    throw globalCalls.badRequestError("Invalid Request");
  }
  let domainList = await domain.query().select("domain_id", "domain", "status");

  let serviceList = await service
    .query()
    .select("service_id", "service_name", "status")
    .withGraphFetched("skills")
    .modifyGraph("skills", builder => {
      builder.select("skill_id", "skills", "status");
    });

  let company_id = base64decode(req.params.id);
  if (isNaN(parseInt(company_id)))
    throw globalCalls.notFoundError("Error! You have passed invalid-id.");

  let company_data = await company
    .query()
    .select(
      "company_id",
      "domain_id",
      "domain_experience_in_months",
      "language"
    )
    .withGraphFetched("[services, skills]")
    .where("company_id", company_id)
    .first();

  let tips = await messageTips
    .query()
    .select("message")
    .where("module", "Company Profile Step #3").first();

  let responseData = {
    domainList,
    serviceList,
    tips,
    company_data
  };
  return globalCalls.okResponse(res, responseData, "");
};

/**
 * post company skillset
 * @param {*} req
 * @param {*} res
 */
const addCompanySkillset = async (req, res) => {
  let data = req.body;

  if (!data.company_id) {
    throw globalCalls.badRequestError("Invalid Request.");
  }
  if (!data.domain_id) {
    throw globalCalls.badRequestError("Choose your working domain.");
  }

  if (!data.domain_experience_in_months) {
    throw globalCalls.badRequestError("Choose your experience");
  }

  if (!data.services) {
    throw globalCalls.badRequestError("Choose atleast one service");
  } else {
    if (data.services.length != 0) {
      let service = data.services;
      let serviceArray = [];
      service.forEach(innerdata => {
        if (innerdata.service_map_id == null) {
          delete innerdata.service_map_id;
        }
        serviceArray.push(innerdata);
      });
      data.services = serviceArray;
    }
  }

  if (!data.skills) {
    throw globalCalls.badRequestError("Choose atleast one skill.");
  } else {
    if (data.skills.length != 0) {
      let skills = data.skills;
      let skillsArray = [];
      skills.forEach(innerdata => {
        if (innerdata.skill_map_id == null) {
          delete innerdata.skill_map_id;
        }
        skillsArray.push(innerdata);
      });
      data.skills = skillsArray;
    }
  }

  if (!data.language) {
    throw globalCalls.badRequestError("Enter atleast one language.");
  }
  try {
    let companyUpdateSkills = await transaction(company.knex(), trx => {
      return company.query(trx).upsertGraph(data, {
        relate: true,
        unrelate: false
      });
    });

    globalCalls.okResponse(res, companyUpdateSkills, "");
  } catch (error) {
    globalCalls.badRequestError(error.message);
  }
};

/**
 * check unique verification number
 * @param {*} req
 * @param {*} res
 */
const checkUniqueCINNumber = async (req, res) => {
  let data = req.body;

  if (!data.company_id) {
    throw globalCalls.badRequestError("Invalid Requests.");
  }
  if (data.cin_number != "") {
    let checkDuplicate = await company
      .query()
      .select("company_id")
      .where("company_id", "!=", data.company_id)
      .where("cin_number", data.cin_number)
      .first();

    if (checkDuplicate) {
      return globalCalls.okResponse(
        res,
        { duplicate: true },
        "CIN Number already exists."
      );
    } else {
      return globalCalls.okResponse(res, { duplicate: false }, "");
    }
  } else {
    return globalCalls.okResponse(res, { duplicate: false }, "");
  }
};

/**
 * fetches company profile verification data
 * @param {*} req
 * @param {*} res
 */
const fetchCompanyVerificationForEdit = async (req, res) => {
  try {
    let data = req.body;

    if (!req.params.id) {
      throw globalCalls.badRequestError("Invalid Requests.");
    }

    let company_id = base64decode(req.params.id);
    if (isNaN(parseInt(company_id)))
      throw globalCalls.badRequestError("Error! You have passed invalid-id.");

    let companyVerification = await company
      .query()
      .select(
        "company_id",
        "cin_number",
        "cin_number_file",
        "company_registration",
        "cin_number_file_input_type",
        "company_registration_file_type"
      )
      .withGraphFetched("[company_docx]")
      .modifyGraph("company_docx", (builder) => {
        builder.select("company_docx","company_docx_file_type")
      })
      .where("company_id", company_id)
      .first();

    let tips = await messageTips
      .query()
      .select("message")
      .where("module", "Company Profile Step #4").first();

    let responseData = {
      companyVerification,
      tips
    };
    return globalCalls.okResponse(res, responseData, "");
  } catch (error) {
    throw globalCalls.badRequestError(error.message);
  }
};
/**
 * adds company verification data
 * @param {*} req
 * @param {*} res
 */
const addCompanyVerification = async (req, res) => {
  let data = req.body;


  if (!data.company_id) {
    throw globalCalls.badRequestError("Invalid Requests.");
  }

  if (!data.cin_number != "") {
    throw globalCalls.badRequestError("Please enter your CIN Number.");
  }

  if (req.files.cin_number_file_input) {
    data.cin_number_file = req.files.cin_number_file_input[0].location;
  } else if (!data.cin_number_file) {
    throw globalCalls.badRequestError("Please upload your CIN Number image.");
  }

  if (req.files.company_registration_file) {
    data.company_registration = req.files.company_registration_file[0].location;
  } else if (!data.company_registration) {
    throw globalCalls.badRequestError(
      "Please upload your Company Registration document for verification."
    );
  }

  try {
    let updateCompanyVerification = await transaction(company.knex(), trx => {
      return company.query(trx).upsertGraph(data, {
        relate: true,
        unrelate: false
      });
    });

    if (!updateCompanyVerification) {
      globalCalls.badRequestError("Something went wrong.");
    }

    return globalCalls.okResponse(res, updateCompanyVerification, "");
  } catch (error) {

    throw globalCalls.badRequestError(error.message);
  }
};

/**
 * Fetches company rules
 * @param {*} req
 * @param {*} res
 */
const fetchCompanyRule = async (req, res) => {
  if (!req.params.id) {
    throw globalCalls.badRequestError("Invalid Requests");
  }

  let company_id = base64decode(req.params.id);
  if (isNaN(parseInt(company_id)))
    throw globalCalls.notFoundError("Error! You have passed invalid-id.");

  try {
    let companyRules = await companyQuestion
      .query()
      .select("question_id", "question");

    let companyHours = await company
      .query()
      .select("company_id", "opens_at", "closes_at").withGraphFetched("rules").modifyGraph("rules", builder => {
        builder.select("rule_id", "answer", "question_id");
      })
      .where("company_id", company_id)
      .first();

    let tips = await messageTips
      .query()
      .select("message")
      .where("module", "Company Profile Step #5").first();

    let responseData = {
      companyRules,
      companyHours,
      tips
    };
    return globalCalls.okResponse(res, responseData, "");
  } catch (error) {
    throw globalCalls.badRequestError(error.message);
  }
};

/**
 * adds company rules
 * @param {*} req
 * @param {*} res
 */
const addCompanyRules = async (req, res) => {
  let data = req.body;

  if (!data.company_id) {
    throw globalCalls.badRequestError("Invalid Request.");
  }
  if (!data.opens_at) {
    throw globalCalls.badRequestError("Please enter opening time.");
  }

  if (!data.closes_at) {
    throw globalCalls.badRequestError("Please enter closing time.");
  }

  if (!data.rules) {
    throw globalCalls.badRequestError("Please answer the questions");
  }

  let i = 0;
  data.rules.forEach((element) => {
    if (element.rule_id == null) {
      delete data.rules[i].rule_id;
    }
    i++;
  });

  try {

    let updateRules = await transaction(company.knex(), trx => {
      return company.query(trx).upsertGraph(data, {
        relate: true,
        unrelate: false
      });
    });

    if (!updateRules) {
      globalCalls.badRequestError("Something went wrong.");
    }

    if (data.status == "submitted") {
      let updateProgress = await checkAndUpdateProgress(req.user.default_company, req.user.user_id, 'Company Info');

      if (updateProgress) {
        // let notifyData = [];

        // let getUser = await UserLogin.query().select("user_id").where("user_type", "super_admin").first();
        // notifyData.push({
        //   "message": messageNotify.body,
        //   "notification_for": "COMPANY_PROFILE",
        //   "notification_for_id": data.company_id,
        //   "to_user_id": getUser.user_id,
        //   "by_user_id": req.user.user_id,
        //   "redirect_url": "",
        //   "redirect_component": "",
        //   "created_at": new Date().toISOString()
        // });

        // if (notifyData.length > 0) {
        //   await NotificationUpdate(notifyData);
        // }
      }
    }

    return globalCalls.okResponse(
      res,
      updateRules,
      "Profile has been submitted for review."
    );
  } catch (error) {
    console.log(error)
    throw globalCalls.badRequestError(error.message);
  }
};

/**
 * fetches company data for preview
 * @param {*} req
 * @param {*} res
 */
const companyProfilePreview = async (req, res) => {

  if (!req.params.id) {
    throw globalCalls.badRequestError("Invalid Requests");
  }

  let company_id = base64decode(req.params.id);
  if (isNaN(parseInt(company_id)))
    throw globalCalls.notFoundError("Error! You have passed invalid-id.");

  try {

    let companyData = await company
      .query()
      .select(
        "company_id",
        "company_name",
        "logo",
        "incorporation_date",
        "director_name",
        "company_email_id",
        "company_website",
        "contact_number",
        "team_size",
        "fixed_budget",
        "hourly_budget",
        "description",
        "video_link",
        "brochure",
        "domain",
        "domain_experience_in_months",
        "language",
        "cin_number",
        "cin_number_file",
        "company_registration",
        "cin_number_file_input_type",
        "company_registration_file_type",
        "opens_at",
        "closes_at",
        "company.status"
      ).leftJoinRelated("domain")
      .withGraphFetched("[media, address, services, skills, rules, company_docx]")
      .modifyGraph("company_docx", (builder) => {
        builder.select("company_docx","company_docx_file_type")
      })
      .modifyGraph("media", builder => {
        builder.select("media_link", "media_type");
      })
      .modifyGraph("address", builder => {
        builder.select("address", "location");
      })
      .modifyGraph("services", builder => {
        builder.select("service_name", "service_icon").innerJoinRelated("service");
      })
      .modifyGraph("skills", builder => {
        builder.select("skills").innerJoinRelated("skills");
      })
      .modifyGraph("rules", builder => {
        builder.select("question", "answer").innerJoinRelated("question").orderBy("rule_id");
      })
      .where("company_id", company_id)
      .first();

    return globalCalls.okResponse(res, companyData, "");
  } catch (error) {
    throw globalCalls.badRequestError(error.message);
  }
};

/**
 * submit for company profile
 * @param {*} req
 * @param {*} res
 */
const submitForCompanyReview = async (req, res) => {
  if (!req.params.id) {
    throw globalCalls.badRequestError("Invalid Requests");
  }
  let company_id = base64decode(req.params.id);
  if (isNaN(parseInt(company_id)))
    throw globalCalls.notFoundError("Error! You have passed invalid-id.");

  let submitCompanyProfile = await company
    .query()
    .update({ status: "submitted" })
    .where("company_id", company_id);

  if (!submitCompanyProfile) {
    throw globalCalls.badRequestError("something went wrong.");
  }

  await checkAndUpdateProgress(req.user.default_company, req.user.user_id, 'Company Info');

  return globalCalls.okResponse(
    res,
    {},
    "Profile has been submitted for review."
  );
};

/**
 * deactivates company profile
 * @param {*} req 
 * @param {*} res 
 */
const deactivateMyProfile = async (req, res) => {

  if (!req.params.id) {
    throw globalCalls.badRequestError("Invalid Request.");
  }

  let company_id = base64decode(req.params.id);
  if (isNaN(parseInt(company_id)))
    throw globalCalls.badRequestError("Error! You have passed invalid-id.");

  let deactivateCompany = await company.query().update({ is_blocked: true }).where("company_id", company_id);

  if (!deactivateCompany) {
    throw globalCalls.badRequestError("Something went wrong.");
  }

  let user = await User.query().update({ "default_company": null }).where("user_id", req.user.user_id);

  return globalCalls.okResponse(res, {}, "This company has been deactivated.");
}

const viewMyCompanyPublicProfileHeader = async (req, res) => {
  try {
    if (!req.params.id) {
      throw globalCalls.badRequestError("Invalid Requests.");
    }

    console.log(req.clientIp)

    let company_id = base64decode(req.params.id);
    if (isNaN(parseInt(company_id)))
      throw globalCalls.notFoundError("Error! You have passed invalid-id.");

    if (req.clientIp) {
      let checkIfIPExists = await CompanyVisitTracker.query().select("tracking_id").where({ "company_id": company_id, "ip_address": req.clientIp }).whereRaw('\"created_at\"::date = ?', [moment().format("YYYY-MM-DD")]).first();
      if (!checkIfIPExists) {
        let insertIP = await CompanyVisitTracker.query().insert({
          company_id: company_id,
          ip_address: req.clientIp
        });

        let updateCompany = await company.query().patch({
          viewer_count: company.raw('viewer_count + 1')
        }).where("company_id", company_id);
      }
    }



    let companyProfile = await company.query().select("company_name", "domain_experience_in_months", "incorporation_date", "domain").leftJoinRelated("domain").withGraphFetched("[reviews, address]").modifyGraph("reviews", builder => {
      builder.avg("stars").innerJoinRelated("ratings").groupBy("reviews.company_id")
    }).modifyGraph("address", (builder) => {
      builder.select("location");
    }).where("company.company_id", company_id).first();


    if (!companyProfile) {
      throw globalCalls.badRequestError("Something went wrong.");
    }

    return globalCalls.okResponse(res, companyProfile, "");
  } catch (error) {
    throw globalCalls.badRequestError(error.message)
  }
}

const portfolioListForPublicProfile = async (req, res) => {

  if (!req.params.id) {
    throw globalCalls.badRequestError("Invalid Requests.");
  }
  let page = (req.query.page) ? req.query.page : 1;
  let limit = req.query.limit ? req.query.limit : PERPAGE;
  let offset = (req.query.offset) ? req.query.offset : limit * (page - 1);
  let company_id = base64decode(req.params.id);
  if (isNaN(parseInt(company_id)))
    throw globalCalls.notFoundError("Error! You have passed invalid-id.");

  let portfolio = await companyPortfolio.query().select("project_name", "project_logo", "one_line_description", "timeline", "budget", "about_the_project", "is_featured").withGraphFetched("reviews").modifyGraph("reviews", builder => {
    builder.avg("stars").innerJoinRelated("ratings").groupBy("reviews.review_id");
  })
    .whereIn('status', ['submitted', 'hidden', 'published'])
    .where(function () {
      if (req.user && req.user.user_type == 'company') {
        this.where('created_by', req.user.user_id)
      }
      this.where("company_id", company_id);
    })
    .limit(limit)
    .offset(offset)
    .orderBy('is_featured');

  let totalCount = await companyPortfolio.query().count("portfolio_id").whereIn('status', ['submitted', 'hidden', 'published'])
    .where(function () {
      if (req.user && req.user.user_type == 'company') {
        this.where('created_by', req.user.user_id)
      }
      this.where("company_id", company_id);
    }).first();

  let responseData = {
    portfolio,
    total: totalCount.count
  }
  return globalCalls.okResponse(res, responseData, "");
}

const reviewsListForPublicProfile = async (req, res) => {

  if (!req.params.id) {
    throw globalCalls.badRequestError("Invalid Requests.");
  }
  let page = (req.query.page) ? req.query.page : 1;
  let limit = req.query.limit ? req.query.limit : PERPAGE;
  let offset = (req.query.offset) ? req.query.offset : limit * (page - 1);
  let company_id = base64decode(req.params.id);
  if (isNaN(parseInt(company_id)))
    throw globalCalls.notFoundError("Error! You have passed invalid-id.");

  try {
    let reviews = await companyPortfolio.query().select("first_name", "last_name", "project_name", "reviews.review_id", Knex.raw("reviews as description"), Knex.raw("(SELECT CASE WHEN starsAvg IS NULL THEN 0 ELSE starsAvg END FROM (SELECT ROUND(AVG(stars),2) AS starsAvg FROM review_rating WHERE review_id = reviews.review_id) AS averageStars) AS rating_avg")).innerJoinRelated("reviews").withGraphFetched("reviews").modifyGraph("reviews", builder => {
      builder.select("stars", "rating_title").innerJoinRelated("ratingList").orderBy("ratingList.rating_id");
    })
      .whereIn('status', ['submitted', 'hidden', 'published'])
      .where(function () {
        if (req.user && req.user.user_type == 'company') {
          this.where('created_by', req.user.user_id)
        }
        this.where("portfolio.company_id", company_id);
        this.where("reviews.feedback_received", true);
      })
      .limit(limit)
      .offset(offset)
      .orderBy('is_featured');

    let reviewsTotal = await companyPortfolio.query().count("portfolio.portfolio_id").innerJoinRelated("reviews").whereIn('status', ['submitted', 'hidden', 'published'])
      .where(function () {
        if (req.user && req.user.user_type == 'company') {
          this.where('created_by', req.user.user_id)
        }
        this.where("portfolio.company_id", company_id);
        this.where("reviews.feedback_received", true);
      }).first();

    let responseData = {
      reviews,
      total: reviewsTotal.count
    }
    return globalCalls.okResponse(res, responseData, "");
  } catch (error) {
    console.log(error);
    throw globalCalls.badRequestError(error.message)
  }
}

const achievementCertificateListForPublicProfile = async (req, res) => {
  return fetchCompanyCertificate(req, res);
}

const achievementAwardsListForPublicProfile = async (req, res) => {
  return fetchCompanyAwards(req, res);
}

const achivementFeaturedListForPublicProfile = async (req, res) => {
  return fetchCompanyFeaturedData(req, res);
}

const successStoriesForPublicProfile = async (req, res) => {
  return fetchCompanySuccessStories(req, res);
}

const fetchCompanyInfoForPublicProfile = async (req, res) => {

  return companyProfilePreview(req, res);

}

const fetchCompanyFAQForPublicProfile = async (req, res) => {

  if (!req.params.id) {
    throw globalCalls.badRequestError("Invalid Requests.");
  }

  let company_id = base64decode(req.params.id);
  if (isNaN(parseInt(company_id)))
    throw globalCalls.notFoundError("Error! You have passed invalid-id.");

  let companyFaqs = await CompanyFaq.query().select("question.question", "company_faq.answer").innerJoinRelated("question").where("company_id", company_id);

  if (!companyFaqs) {
    throw globalCalls.badRequestError("Something went wrong.");
  }
  return globalCalls.okResponse(res, companyFaqs, "");
}
module.exports = {
  fetchCompanyInformationData,
  addCompanyInformation,
  fetchAboutCompany,
  addAboutCompany,
  fetchSkillsetData,
  addCompanySkillset,
  fetchCompanyVerificationForEdit,
  checkUniqueCINNumber,
  addCompanyVerification,
  fetchCompanyRule,
  addCompanyRules,
  companyProfilePreview,
  submitForCompanyReview,
  deactivateMyProfile,
  viewMyCompanyPublicProfileHeader,
  portfolioListForPublicProfile,
  reviewsListForPublicProfile,
  achievementCertificateListForPublicProfile,
  achievementAwardsListForPublicProfile,
  achivementFeaturedListForPublicProfile,
  successStoriesForPublicProfile,
  fetchCompanyInfoForPublicProfile,
  fetchCompanyFAQForPublicProfile
};
