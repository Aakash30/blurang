const { base64decode } = require("nodejs-base64");
const globalCalls = require("../../settings/functions");
const chatMap = require("../../models/ChatProjectCompanyMap");
const knexConfig = require("../../../config/knex");
const Knex = require("knex")(knexConfig.development);
const shortlistedcompany = require("../../models/ShortlistedCompanies");
const { okResponse } = require("../../settings/functions");

const getprojectlist = async (req, res) => {
    try {
        //Fetching Project with respect to Company_id
        let project = await shortlistedcompany.query().select("project_id","company_id","status").
        where((builder)=>{
        //    builder.orWhere("status","selected")
        //    builder.orWhere("status","under_discussion")
        })
        .where("company_id", req.user.default_company).withGraphFetched("[project]").modifyGraph("project",(builder) => {
            builder.select("project_id","project_name","one_line_description","budget","about_the_project","expected_timeline")
        })
        let responseData = {
           // project,
        };
        let chatBox = await chatMap
            .query()
            .select(
                "map_id",
                "chat_box_id",
                "status",
                "project_id",
                "updated_at",
                Knex.raw(
                  "(SELECT count(message_id) FROM chat_messages_list where chat_id = inbox.chat_id and status = 'unread' AND sender != "+req.user.user_id+" GROUP BY inbox.chat_id) AS unread"
                )
            )
            .innerJoinRelated("inbox")
            .where((builder) => {
                builder.where("company_id",req.user.default_company)
                // if (req.params.id) {
                //     let projectId = base64decode(req.params.id);
                //     builder.where("project_id", projectId);
                // }
            })
            .withGraphFetched("[client, quotation, chatMessage, Shortlistedcompanies.project]")
            .modifyGraph("client", (builder) => {
                builder
                    .select(
                    "first_name",
                    "last_name",
                    "location",
                    "profile_pic",
                    "gender")
            }).modifyGraph("quotation", (builder) => {
                builder
                    .select(
                        "quotation_details.quotation_id",
                        "estimated_timeline",
                        "fixed_budget",
                        "team_size",
                        "quotation_sent_at",
                        "quotation_updated_at",
                        "comments"
                    )
                    .withGraphFetched("[technologies_label, quotation_docx]")
                    .modifyGraph("technologies_label", (builder) => {
                        builder.select("skills");
                    })
                    .modifyGraph("quotation_docx", (builder) => {
                        builder.select("file_link", "file_type");
                    });
            })
            .modifyGraph("chatMessage", (builder) => {
                builder
                    .select(
                        "message",
                        "message_type",
                        "sender",
                        "receiver",
                        "chat_messages_list.status",
                        "last_updated"
                    )
                    .orderBy("last_updated", "DESC");
            })
            .modifyGraph("Shortlistedcompanies", (builder) => {
                builder.select("project_id","company_id","status");
                builder.where((builder)=>{
                        builder.orWhere("status","selected")
                        builder.orWhere("status","under_discussion")
                    })
            })
            .orderBy("inbox.updated_at", "DESC");
            responseData["chatbox"]=chatBox
        return globalCalls.okResponse(res, responseData, "");
    }
    catch (e) {
        console.log(e)
    }
}

const getCompanyChatList = async(req, res) =>{
    let chatBox = await chatMap
      .query()
      .select(
        "map_id",
        "chat_box_id",
        "status",
        "project_id",
        "updated_at",
                Knex.raw(
                  "(SELECT count(message_id) FROM chat_messages_list where chat_id = inbox.chat_id and status = 'unread' AND sender != "+req.user.user_id+" GROUP BY inbox.chat_id) AS unread"
                )
      )
      .innerJoinRelated("inbox")
      .where((builder) => {
        
        builder.where("company_id", req.user.default_company);
        
      })
      .withGraphFetched("project")
      .modifyGraph("project", (builder) => {
        builder
          .select("project_name");
      })
      .orderBy("updated_at", "DESC").runAfter((result, builder)=>{
         
          return result;
      });

      let responseData = {
        chatBox,
      }
      return globalCalls.okResponse(res, responseData, "");
}
module.exports =
{
    getprojectlist,
    getCompanyChatList
}