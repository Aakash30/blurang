"use strict";
const { base64encode, base64decode } = require("nodejs-base64");
const globalCalls = require("../../settings/functions");
const globalConst = require("../../settings/constants");
const CompanyPortfolio = require("../../models/CompanyPortfolio");
const Reviews = require("../../models/PortfolioProjectReviews");
const skills = require("../../models/Skills");
const messageTips = require("../../models/MessageTips");
const companyProgress = require("../../models/CompanyProgress");
const ratingPointers = require("../../models/RatingPoints");
const company = require("../../models/Company");

const validator = require("validator");
const bcrypt = require("bcryptjs");
const EMAIL = require("./../../middlewares/send-email");
const CONFIG = require("../../settings/constants");
const { transaction } = require("objection");
const moment = require("moment");
const knexConfig = require('../../../config/knex');
const Company = require("../../models/Company");
const Project = require("../../models/Project");
const Knex = require('knex')(knexConfig.development);

const checkAndUpdateProgress = require("../../controllers/common").checkAndUpdateProgress;


const NotificationUpdate = require('../notification/notificationcontroller').updateNotification;

module.exports = {
  /**
   * function to add/update company portfolio
   * @param {*} req
   * @param {*} res
   */
  AddPortfolioProject: async (req, res) => {
    try {
      let data = req.body;
      let message = "";

      if (data.portfolio_id) {
        message = "Portfolio updated successfully";
        let checkProject = await CompanyPortfolio.query()
          .where("portfolio_id", data.portfolio_id)
          .first();
        if (!checkProject) {
          throw globalCalls.badRequestError("Invalid request!");
        }

        if (checkProject.status === "published") {
          throw globalCalls.badRequestError("Invalid request!");
        }
      } else {
        message = "Portfolio added successfully";
        data.company_id = req.user.default_company;
        data.created_by = req.user.user_id;
      }

      data.is_complete = true;

      if (!data.project_name) {
        if (data.submit_action === "submitted")
          throw globalCalls.badRequestError("Please fill up project name.");
        else data.is_complete = false;
      }

      if (!data.one_line_description) {
        if (data.submit_action === "submitted")
          throw globalCalls.badRequestError(
            "Please fill up one line description."
          );
        else data.is_complete = false;
      }

      if (!data.first_name) {
        data.is_complete = false;
      }

      if (!data.client_location) {
        data.is_complete = false;
      }

      if (!data.budget) {
        if (data.submit_action === "submitted")
          throw globalCalls.badRequestError("Please enter budget.");
        else data.is_complete = false;
      }

      if (!data.about_the_project) {
        if (data.submit_action === "submitted")
          throw globalCalls.badRequestError(
            "Please fill up about the project."
          );
        else data.is_complete = false;
      }

      if (!data.timeline) {
        if (data.submit_action === "submitted")
          throw globalCalls.badRequestError(
            "Please enter your project timeline."
          );
        else data.is_complete = false;
      }

      if (!data.skills) {
        if (data.submit_action === "submitted")
          throw globalCalls.badRequestError(
            "Please choose atleast one technology."
          );
        else data.is_complete = false;
      }

      if (data.skills) {
        data.skills = JSON.parse(data.skills);
      }

      if (data.links) {
        data.links = JSON.parse(data.links);
      } else {
        data.is_complete = false;
      }

      if (req.files && req.files.project_logo_file) {
        data.project_logo = req.files.project_logo_file[0].location;
      } else {
        if (data.submit_action === "submitted" && data.project_logo == "") {
          throw globalCalls.badRequestError("Please upload project logo.");
        } else data.is_complete = false;
      }

      data.gallery = data.gallery != "" ? JSON.parse(data.gallery) : [];

      if (req.files && req.files.gallery_file) {
        for (let i = 0; i < req.files.gallery_file.length; i++) {
          let tmp = {};
          tmp.gallery_image = req.files.gallery_file[i].location;
          data.gallery.push(tmp);
        }
      } else {
        if (data.submit_action === "submitted" && data.gallery.length == 0) {
          throw globalCalls.badRequestError("Please upload project gallery.");
        } else {
          data.is_complete = false;
        }
      }

      data.status = "draft";
      if (data.submit_action === "submitted") {
        data.status = "submitted";
        message = "Portfolio submitted successfully";
      }

      delete data.submit_action;

      let addCompanyPoject = await transaction(
        CompanyPortfolio.knex(),
        (trx) => {
          return CompanyPortfolio.query(trx).upsertGraph(data, {
            relate: true,
            noDelete: false
          });
        }
      );

      if (!addCompanyPoject) {
        throw globalCalls.badRequestError("Something went wrong.");
      } else {
        if (data.status == "submitted") {
          await checkAndUpdateProgress(req.user.default_company, req.user.user_id, 'Portfolio');
        }

        return globalCalls.okResponse(res, addCompanyPoject, message);
      }
    } catch (error) {
      console.log(error)
      throw globalCalls.badRequestError(error.message);
    }
  },

  /**
   * function to edit / add portfolio data fetch
   */
  fetchEditPortfolio: async (req, res) => {
    let skillSetList = await skills
      .query()
      .select("skill_id", "skills", "status");

    let tips = await messageTips
      .query()
      .select("message")
      .where("module", "Add Portfolio")
      .first();
    let responseData = {
      skillSetList,
      tips,
    };

    if (req.params.id) {
      let portfolio_id = base64decode(req.params.id);
      if(isNaN(parseInt(portfolio_id)))
        throw globalCalls.badRequestError("Error! You have passed invalid-id.");
  
      let portfolio = await CompanyPortfolio.query()
        .withGraphFetched("[gallery, skills, links]")
        .modifyGraph("gallery", (builder) => {
          builder.select("gallery_id", "gallery_image");
        })
        .modifyGraph("skills", (builder) => {
          builder.select("skill_id", "portfolio_skill_id");
        })
        .modifyGraph("links", (builder) => {
          builder.select("link_id", "link", "link_for");
        })
        .where("portfolio_id", portfolio_id);
      responseData["portfolio"] = portfolio;
    }

    return globalCalls.okResponse(res, responseData, "");
  },
  /**
   * function to get company portfolio [draft, pending, published, single] list / detail
   * @param {*} req
   * @param {*} res
   */
  GetCompanyPortfolio: async (req, res) => {
    try {
      let msg = "Portfolio listed";
      let portfolio = [];
      if (req.params.id) {
        let portfolio_id = base64decode(req.params.id);
        if(isNaN(parseInt(portfolio_id)))
          throw globalCalls.notFoundError("Error! You have passed invalid-id.");
  
        portfolio = await CompanyPortfolio.query()
          .select(
            "portfolio_id",
            "project_name",
            "one_line_description",
            "project_logo",
            "timeline",
            "budget",
            "first_name",
            "last_name",
            "client_location",
            "client_email",
            "is_featured"
          )
          .withGraphFetched("[gallery, portfolioSkills, reviews, links]")
          .modifyGraph("gallery", (builder) => {
            builder.select("gallery_image");
          })
          .modifyGraph("portfolioSkills", (builder) => {
            builder.select("skills");
          })
          .modifyGraph("reviews", (builder) => {
            builder
              .select("reviews", "feedback_received")
              .withGraphFetched("ratings")
              .modifyGraph("ratings", (builder) => {
                builder
                  .select("stars", "rating_title")
                  .innerJoinRelated("ratingPoint")
                  .orderBy("review_rating_id");
              }).where("is_valid", true);
          })
          .modifyGraph("links", (builder) => {
            builder.select("link", "link_for");
          })
          .where("portfolio_id", portfolio_id)
          .first();
        if (!portfolio) {
          throw globalCalls.badRequestError("Invalid request!");
        }
        msg = "Portfolio details";

        return globalCalls.okResponse(res, portfolio, "Portfolio listed");
      } else {
        let validReview = true;
        let page = req.query.page ? req.query.page : 1;
        let limit = req.query.limit ? req.query.limit : CONFIG.per_page;
        let offset = req.query.offset ? req.query.offset : limit * (page - 1);

        portfolio = await CompanyPortfolio.query()
          .select(
            "portfolio.portfolio_id",
            "project_name",
            "one_line_description",
            "project_logo",
            "is_complete",
            "status",
            "is_featured",
            "feedback_received",
            "belongs_to_project"
          )
          .leftJoin("reviews", function () {
            this.on(
              "portfolio.portfolio_id",
              "=",
              "reviews.portfolio_id"
            ).andOn("is_valid", Knex.raw(true));
          })
          .where(function () {
            if (req.query.type == "draft") {
              this.where("status", "draft");
            } else {
              this.whereIn("status", ["submitted", "hidden", "published"]);
            }

            if (req.query.filter == "new") {
              this.whereRaw(
                '\"created_at\"::date <= ? AND \"created_at\"::date >= ?',
                [moment().format("YYYY-MM-DD"), moment().subtract(1, "month").format("YYYY-MM-DD")]
              );

            } else if (req.query.filter == "review") {
              this.where("feedback_received", true);
            } else if (req.query.filter == "pending-review") {
              this.where("feedback_received", false);
            } else if (req.query.filter == "no-review") {
              this.where("feedback_received", null);
            } else if (req.query.filter == "featured") {
              this.where("is_featured", true);
            }
          })
          .where("status", "!=", "deleted")
          .where(function () {
            if (req.user.user_type == "company") {
              //this.where("created_by", req.user.user_id);
            }
          })
          .where("portfolio.company_id", req.user.default_company) //portfolio. added by zubear bczof company_id is ambiguous
          .limit(limit)
          .offset(offset)
          .orderBy("portfolio.portfolio_id", "desc", "is_featured");

        let totalCount = await CompanyPortfolio.query()
          .count("portfolio.portfolio_id")
          .leftJoin("reviews", function () {
            this.on(
              "portfolio.portfolio_id",
              "=",
              "reviews.portfolio_id"
            ).andOn("is_valid", Knex.raw(true));
          })
          .where(function () {
            if (req.query.type == "draft") {
              this.where("status", "draft");
            } else {
              this.whereIn("status", ["submitted", "hidden", "published"]);
            }

            if (req.query.filter == "new") {
              this.whereRaw(
                '\"created_at\"::date <= ? AND \"created_at\"::date >= ?',
                [moment().format("YYYY-MM-DD"), moment().subtract(1, "month").format("YYYY-MM-DD")]
              );
            } else if (req.query.filter == "review") {
              this.where("feedback_received", true);
            } else if (req.query.filter == "pending-review") {
              this.where("feedback_received", false);
            } else if (req.query.filter == "no-review") {
              this.where("feedback_received", null);
            } else if (req.query.filter == "featured") {
              this.where("is_featured", true);
            }
          })
          .where("portfolio.company_id", req.user.default_company) //portfolio. added by zubear bczof company_id is ambiguous
          .where("status", "!=", "deleted")
          .where(function () {
            if (req.user.user_type == "company") {
              //this.where("created_by", req.user.user_id);
            }
          })
          .first();

        let responseData = {
          portfolio,
          total: totalCount.count,
        };
        return globalCalls.okResponse(res, responseData, "");
      }
    } catch (e) {
      console.log(
        "Error in CompanyPortfolioController.DeletePortfolioProject",
        e
      );
      throw globalCalls.badRequestError(e.message);
    }
  },

  /**
   * function to send feedback to client
   * @param {*} req
   * @param {*} res
   */
  SendFeedbackToClient: async (req, res) => {
    try {
      let data = req.body;

      if (!data.portfolio_id) {
        throw globalCalls.badRequestError("Invalid request!");
      }

      if (!data.reviews.reviews) {
        throw globalCalls.badRequestError("Please write review for client.");
      }

      if (!data.client_email) {
        throw globalCalls.badRequestError("Please write review for client.");
      }

      if (!validator.isEmail(data.client_email)) {
        throw globalCalls.badRequestError("Please enter a valid email id.");
      }

      if (data.reviews.review_id == null) {
        delete data.reviews.review_id;


      }
      let expire_date;
      let today = new Date();
      expire_date = new Date();
      expire_date.setDate(today.getDate() + 7);
      data.reviews.link_expire = expire_date;
      data.reviews.company_id = req.user.default_company;

      let verify_token =
        globalCalls.generateToken(15) + "-" + data.portfolio_id;
      verify_token = await bcrypt.hash(verify_token, 10);
      verify_token = verify_token.replace(/\.|\/+/g, "");
      data.reviews.review_verification_link = verify_token;
      data.reviews.portfolio_id = data.portfolio_id;
      let review;
      //Check Portfolio project is exist with submitted status //
      let portfolio = await transaction(
        CompanyPortfolio.knex(),
        async (trx) => {
          const portfolioUpdate = await CompanyPortfolio.query(trx)
            .update({ client_email: data.client_email })
            .where({ status: "submitted", portfolio_id: data.portfolio_id })
            .returning("*");

          review = await Reviews.query(trx).upsertGraph(data.reviews);
          return portfolioUpdate;
        }
      );

      let companyData = await company.query().select("company_name").where("company_id", req.user.default_company).first();
      /************ SEND GET FEEDBACK EMAIL TO CLIENT ************/
      let link =
        portfolio[0].belongs_to_project != null ? CONFIG.clientUrl + "completed-project/edit-feedback/" + base64encode(data.portfolio_id) : CONFIG.clientUrl + "client-review/" + verify_token;
      let emailData = { link: link, project: portfolio[0].project_name, company: companyData.company_name };

      /**
       * send email functionality here
       */

      EMAIL.sendEmail(
        data.client_email,
        "Give Feedback for Your Project - " + portfolio[0].project_name,
        emailData,
        "get_client_Feedback.pug"
      );

      if (portfolio[0].belongs_to_project != null) {

        let projectData = await Project.query().select("project_name", "user_id").where("project_id", portfolio[0].belongs_to_project).first();

        let messageNotify = Object.assign({}, globalConst.NOTIFICATION_PAYLOAD["GIVE_FEEDBACK_TO_CLIENT"]);

        let bodyMessage = messageNotify.body.replace("[COMPANY]", "<b>" + companyData.company_name + "</b>");
        messageNotify.body = bodyMessage.replace("[PROJECT_NAME]", "<b>" + portfolio.project_name + "</b>");
        let notifyData = [];

        notifyData.push({
          "message": messageNotify.body,
          "notification_for": "PROJECT",
          "notification_for_id": data.portfolio_id,
          "to_user_id": projectData.user_id,
          "by_user_id": req.user.user_id,
          "redirect_url": "/completed-project/edit-feedback/" + base64encode(data.portfolio_id),
          "redirect_component": "PROJECT",
          "created_at": new Date().toISOString()
        });

        if (notifyData.length > 0) {
          await NotificationUpdate(notifyData)
        }
      }

      if (review) {
        return globalCalls.okResponse(
          res,
          { review },
          "Feedback send successfully to client email."
        );
      }
      return globalCalls.badRequestError("Something went wrong!");
    } catch (e) {
      console.log(
        "Error in CompanyPortfolioController.SendFeedbackToClient",
        e
      );
      throw globalCalls.badRequestError(e.message);
    }
  },

  fetchFeedbackForm: async (req, res) => {
    try {
      let portfolio_id = base64decode(req.params.id);
      if(isNaN(parseInt(portfolio_id)))
	      throw globalCalls.notFoundError("Error! You have passed invalid-id.");

      let companyReview = await CompanyPortfolio.query()
        .select("portfolio_id", "project_name", "client_email", "belongs_to_project")
        .withGraphFetched("reviews", { maxBatchSize: 1 })
        .modifyGraph("reviews", (builder) => {
          builder.select(
            "review_id",
            "reviews",
            "link_expire",
            "feedback_received",
            "company_id"
          ).where("is_valid", true);
        })
        .where("portfolio_id", portfolio_id)
        .where("status", "submitted")
        .where("company_id", req.user.default_company)
        .first();

      if (!companyReview) {
        throw globalCalls.badRequestError("Project not found.");
      }

      let tips = await messageTips.query().select("message").where("module", "Get Feedback").first();

      return globalCalls.okResponse(res, {
        companyReview,
        tips
      }, "");
    } catch (error) {
      throw globalCalls.badRequestError(error.message);
    }
  },
  /**
   * function to verify feedback
   * @param {*} req
   * @param {*} res
   */
  VerifyClientFeedback: async (req, res) => {
    try {
      let data = req.body;
      let verify = await Reviews.query().withGraphFetched("portfolio").modifyGraph("portfolio", builder => {
        builder.select("portfolio_id", "project_name", "one_line_description", "project_logo", "about_the_project", "timeline", "budget")
      })
        .where("review_verification_link", data.token)
        .first();
      if (!verify) {
        throw globalCalls.badRequestError("Invalid request!");
      }

      if (new Date() > new Date(verify.link_expire) || !verify.is_valid) {
        throw globalCalls.badRequestError("Link expired!");
      }

      let ratingList = await ratingPointers.query().select("rating_id", "rating_title");

      return globalCalls.okResponse(
        res,
        {
          feedback: verify,
          ratingList: ratingList
        },
        "Link verified."
      );
    } catch (e) {
      console.log(
        "Error in CompanyPortfolioController.VerifyClientFeedback",
        e
      );
      throw globalCalls.badRequestError(e.message);
    }
  },

  /**
   * function to get client feedback
   * @param {*} req
   * @param {*} res
   */
  ClientFeedback: async (req, res) => {
    try {
      let data = req.body;
      if (!data.portfolio_id) {
        throw globalCalls.badRequestError("Invalid request!");
      }

      let portfolio = await Reviews.query()
        .select(
          "portfolio.*",
          "reviews.review_id",
          "reviews.reviews",
          "reviews.client_email as review_email",
          "reviews.link_expire",
          "reviews.feedback_received",
          "reviews.review_verification_link"
        )
        .joinRelated("portfolio")
        .where("reviews.portfolio_id", data.portfolio_id)
        .first();

      if (!portfolio) {
        throw globalCalls.badRequestError("Invalid request!");
      }

      if (portfolio.status === "published" && portfolio.feedback_received) {
        throw globalCalls.badRequestError(
          "Feedback already recived for this project!"
        );
      }

      if (new Date() > new Date(portfolio.link_expire)) {
        throw globalCalls.badRequestError("Feedback link expired!");
      }

      delete data.portfolio_id;
      data.review_verification_link = "";
      data.feedback_received = true;

      let rating = await Reviews.query().upsertGraph(data, {
        relate: true,
        unrelate: false,
      });

      if (rating) {
        //UPDATE STATUS OF COMPANY PORTFOLIO PROJECT
        await CompanyPortfolio.query().patchAndFetchById(
          portfolio.portfolio_id,
          {
            status: "published",
            published_at: new Date(),
          }
        );
      }

      return globalCalls.okResponse(
        res,
        { portfolio },
        "Feedback successfully submitted."
      );
    } catch (e) {
      console.log(
        "Error in CompanyPortfolioController.VerifyClientFeedback",
        e
      );
      throw globalCalls.badRequestError(e.message);
    }
  },

  /**
   * function to add/remove featured portfolio project
   * @param {*} req
   * @param {*} res
   */
  AddRemoveToFeatured: async (req, res) => {
    try {
      let data = req.body;
      if (!data.portfolio_id) {
        throw globalCalls.badRequestError("Invalid request!");
      }

      let portfolio = await CompanyPortfolio.query()
        .where("portfolio_id", data.portfolio_id)
        .first();

      if (!portfolio) {
        throw globalCalls.badRequestError("Invalid request!");
      }

      if (portfolio.status !== "published") {
        throw globalCalls.badRequestError("Portfolio not published!");
      }

      let msg = "Project added successfully to featured list.";
      let act = portfolio.is_featured ? false : true;
      if (act) {
        /* ***** CHECK FEATURED PORTFOLIO LIMIT ***** */
        let featuredCount = await CompanyPortfolio.query()
          .where("company_id", portfolio.company_id)
          .where("is_featured", true)
          .count("portfolio_id")
          .first();
        if (featuredCount.count >= 3) {
          throw globalCalls.badRequestError(
            "Limit exceeded! You can add only 3 projects."
          );
        }
      } else {
        msg = "Project removed successfully from featured list.";
      }

      let updated = await CompanyPortfolio.query().patchAndFetchById(
        data.portfolio_id,
        { is_featured: act }
      );
      if (!updated) {
        throw globalCalls.badRequestError("Something went wrong!");
      }
      return globalCalls.okResponse(res, updated, msg);
    } catch (e) {
      console.log("Error in CompanyPortfolioController.AddRemoveToFeatured", e);
      throw globalCalls.badRequestError(e.message);
    }
  },

  /**
   * function to delete company portfolio
   * @param {*} req
   * @param {*} res
   */
  DeletePortfolioProject: async (req, res) => {
    try {
      if (!req.params.id) {
        return globalCalls.badRequestError("Invalid request!");
      }

      let portfolio_id = base64decode(req.params.id);
      if(isNaN(parseInt(portfolio_id)))
        throw globalCalls.badRequestError("Error! You have passed invalid-id.");
  
      let deleted = await CompanyPortfolio.query().patchAndFetchById(
        portfolio_id,
        { status: "deleted" }
      );
      if (deleted) {
        return globalCalls.okResponse(res, {}, "Successfully deleted.");
      }
      return globalCalls.badRequestError("Something went wrong!");
    } catch (e) {
      console.log(
        "Error in CompanyPortfolioController.DeletePortfolioProject",
        e
      );
      return globalCalls.badRequestError("Something went wrong!");
    }
  },
};
