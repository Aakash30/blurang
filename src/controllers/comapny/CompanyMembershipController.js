"use strict";

const { base64decode } = require("nodejs-base64");
const globalCalls = require("../../settings/functions");
const PERPAGE = require("../../settings/constants").per_page;

const messageTips = require("../../models/MessageTips");
const company = require("../../models/Company");
const MembershipPlans = require("../../models/MembershipPlans");
const domain = require("../../models/Domain");
const service = require("../../models/Service");
const User = require("../../models/UserLogin");

const companyQuestion = require("../../models/CompanyQuestion");
const companyPortfolio = require("../../models/CompanyPortfolio");
const CompanyFaq		= require('../../models/CompanyFaq');
const companyProgress = require("../../models/CompanyProgress");


/**
 * export function
 */

 const fetchCompanyCertificate = require('./CompanyAchievementController').fetchCompanyCertificate;
 const fetchCompanyAwards = require('./CompanyAchievementController').fetchCompanyAwards;
 const fetchCompanyFeaturedData = require('./CompanyAchievementController').fetchCompanyFeaturedData;
 const fetchCompanySuccessStories = require('./CompanyAchievementController').fetchCompanySuccessStories;

const { transaction } = require("objection");
const moment = require("moment");

/**
 * fetches
 * @param {*} req
 * @param {*} res
 */
const buyMembership = async (req, res) => {
  let data = req.body;
  var membership_history_data = {};

  if (!data.company_id) {
    throw globalCalls.badRequestError("no company found.");
  }

  if (!data.membership_plan_id) {
    throw globalCalls.badRequestError("Please choose any membership plan.");
  }

  
  try {
    data.is_expired_membership_plan='false';
    membership_history_data.company_id=data.company_id;
    membership_history_data.membership_plan_id=data.membership_plan_id;
    membership_history_data.membership_plan_amount=data.amount;
    membership_history_data.start_date=moment().format("YYYY-MM-DD");;
    membership_history_data.end_date=moment().add(1, 'months').calendar();
    
    data.membership_history=membership_history_data;
    
    delete data.amount;
    // console.log(data)

    let addCompanyInformation = await transaction(company.knex(), trx => {
      return company.query(trx).upsertGraph(data, {
        relate: false,
        unrelate: true
      });
    });

    if (!addCompanyInformation) {
      throw globalCalls.badRequestError("something went wrong.");
    } else {
      return globalCalls.okResponse(
        res,
        addCompanyInformation,
        "Membership successfully bought."
      );
    }
  } catch (error) {
    throw globalCalls.badRequestError(error.message);
  }
};


module.exports = {
  buyMembership
};
