"use strict";

const { base64decode, base64encode } = require("nodejs-base64");
const globalCalls = require("../../settings/functions");
const PERPAGE = require("../../settings/constants").per_page;
const globalConst = require("../../settings/constants");

const company = require("../../models/Company");
const quotationListing = require("../../models/ShortlistedCompanies");
const quotationData = require("../../models/QuotationData");
const shorlistedCompany = require("../../models/ShortlistedCompanies");
const technology = require("../../models/CompanySkills");

const project = require("../../models/Project");
const notification = require("../../controllers/notification/notificationcontroller");

const { transaction, ref } = require("objection");
const Company = require("../../models/Company");

const NotificationUpdate = require("../notification/notificationcontroller")
  .updateNotification;

  const updateRunTimeQuotation = require("../notification/notificationcontroller").updateRunTimeQuotation;
/**
 * Fetch quotation request and sent quotation
 * @param {*} req
 * @param {*} res
 */
const fetchQuotation = async (req, res) => {
  try {
    let questionFetch = await quotationListing
      .query()
      .select("shortlisted_id", "created_at")
      .withGraphFetched("project")
      .modifyGraph("project", (builder) => {
        builder
          .select(
            "project_id",
            "project_name",
            "one_line_description",
            "about_the_project",
            "expected_timeline",
            "budget",
            "payment_type"
          )
          .withGraphFetched("[domain, servicesLabels]")
          .modifyGraph("domain", (builder) => {
            builder.select("domain");
          })
          .modifyGraph("servicesLabels", (builder) => {
            builder.select("service_name");
          });
      })
      .where("company_id", req.user.default_company)
      .where((builder) => {
        if (req.query.filter && req.query.filter == "sent") {
          builder.where("status", "quotation_received");
        } else {
          builder.where("status", "quotation_request");
        }
      });

    // mugalsaray
    let totalQuotations = await quotationListing
      .query()
      .count("shortlisted_id")
      .where("company_id", req.user.default_company)
      .where((builder) => {
        if (req.query.filter && req.query.filter == "sent") {
          builder.where("status", "quotation_received");
        } else {
          builder.where("status", "quotation_request");
        }
      })
      .first();

    let skillsList = await technology
      .query()
      .select("skills.skill_id", "skills")
      .innerJoinRelated("skills")
      .where("company_id", req.user.default_company);

    let responseData = {
      count: totalQuotations.count,
      questionFetch,
      skillsList,
    };
    return globalCalls.okResponse(res, responseData, "");
  } catch (error) {
    throw globalCalls.badRequestError(error.message);
  }
};

//const add / update quotation data
/**
 *
 * @param {*} req
 * @param {*} res
 */
const addUpdateQuotation = async (req, res) => {
  let data = req.body;
  try {
    if (!data.project_id) {
      throw globalCalls.badRequestError("Invalid Requests");
    }

    if (!data.estimated_timeline) {
      throw globalCalls.badRequestError("Please add your estimated time.");
    }

    if (!data.team_size) {
      throw globalCalls.badRequestError("Please enter your team size.");
    }

    if (!data.fixed_budget) {
      throw globalCalls.badRequestError("Please enter your budget.");
    }

    if (!data.comments) {
      throw globalCalls.badRequestError("Please enter comments");
    }
    if (!data.quotation_technology) {
      throw globalCalls.badRequestError("Add atleast one technology");
    } else {
      let technology = JSON.parse(data.quotation_technology);
      let listOfTechnology = [];
      technology.forEach((value) => {
        if (value.quotation_tech_skill_id == null) {
          delete value.quotation_tech_skill_id;
        }
        listOfTechnology.push(value);
      });
      data.quotation_technology = listOfTechnology;
    }

    data.quotation_docx =
      data.quotation_docx != "" ? JSON.parse(data.quotation_docx) : [];

    if (req.files) {
      let fileTypeArray = JSON.parse(data.file_types);
      for (let i = 0; i < req.files.length; i++) {
        data.quotation_docx.push({
          file_link: req.files[i].location,
          file_type: fileTypeArray[i],
        });
      }
    }

    delete data.file_types;
    delete data.quotation_docx_files;
    data.company_id = req.user.default_company;
    let updateStatus;
    let updateQuotation = await transaction(
      quotationData.knex(),
      async (trx) => {
        let quotationResult = await quotationData
          .query(trx)
          .upsertGraph(data, {
            relate: true,
            unrelate: true,
          })
          .returning("*");

         // console.log(quotationResult)
        if (quotationResult) {
          updateStatus = await shorlistedCompany
            .query()
            .update({ status: "quotation_received" })
            .where({
              company_id: req.user.default_company,
              project_id: data.project_id,
            })
            .returning("*");
        }
        return {
          quotationResult,
          updateStatus,
        };
      }
    );

    if (!updateQuotation) {
      throw globalCalls.badRequestError("Something went wrong.");
    }
    let getUser = await project
    .query()
    .select("user_id")
    .where("project_id", data.project_id)
    .first();

    if (!data.quotation_id) {
      let companyData = await company
        .query()
        .select("company_name")
        .where("company_id", req.user.default_company)
        .first();
      

      let messageNotify = Object.assign(
        {},
        globalConst.NOTIFICATION_PAYLOAD["QUOTATION_RECEIVE_ACTION"]
      );

      let bodyMessage = messageNotify.body.replace(
        "[COMPANY]",
        "<b>" + companyData.company_name + "</b>"
      );
      let notifyData = [];
      notifyData.push({
        message: bodyMessage,
        notification_for: "QUOTATION_ACTION",
        notification_for_id: updateQuotation.updateStatus[0].shortlisted_id,
        to_user_id: getUser.user_id,
        by_user_id: req.user.user_id,
        redirect_url: "/project-details/" + base64encode(data.project_id),
        redirect_component: "QUOTATION",
        created_at: new Date().toISOString(),
      });

      if (notifyData.length > 0) {
        await NotificationUpdate(notifyData);
      }
    } else {
      await updateRunTimeQuotation(updateQuotation, getUser.user_id);
    }

    return globalCalls.okResponse(
      res,
      updateQuotation,
      "Quotation sent successfully."
    );
  } catch (error) {
    throw globalCalls.badRequestError(error.message);
  }
};

/**
 * get quotation details
 * @param {*} req
 * @param {*} res
 */
const getQuotationData = async (req, res) => {
  if (!req.params.id) {
    throw globalCalls.badRequestError("Invalid requests.");
  }

  let projectId = base64decode(req.params.id);
  if (isNaN(parseInt(projectId)))
    throw globalCalls.notFoundError("Error! You have passed invalid-id.");

  try {
    let getQuotationData = await shorlistedCompany
      .query()
      .select("shortlisted_id", "company_id", "status", "action_by")
      .withGraphFetched("[project, quotation_details_for_project]")
      .modifyGraph("project", (builder) => {
        builder
          .select(
            "project_id",
            "project_name",
            "domain",
            "budget",
            "one_line_description",
            "expected_timeline",
            "about_the_project",
            "expected_timeline",
            "domain_experience_in_months",
            "company_expected_experience",
            "quotation_remarks",
            "payment_type",
            "location"
          )
          .innerJoinRelated("domain")
          .withGraphFetched(
            "[servicesLabels, skillLabel, project_docx, projectAnswer]"
          )
          .modifyGraph("servicesLabels", (builder) => {
            builder.select("service_name");
          })
          .modifyGraph("skillLabel", (builder) => {
            builder.select("skills");
          })
          .modifyGraph("project_docx", (builder) => {
            builder.select("docx_url", "file_type");
          })
          .modifyGraph("projectAnswer", (builder) => {
            builder
              .select("answer", "question", "type")
              .innerJoinRelated("question")
              .orderBy("type");
          });
      })
      .modifyGraph("quotation_details_for_project", (builder) => {
        builder
          .select(
            "estimated_timeline",
            "team_size",
            "fixed_budget",
            "comments",
            "quotation_sent_at",
            "quotation_updated_at"
          )
          .withGraphFetched("[quotation_docx, technologies_label]")
          .modifyGraph("quotation_docx", (builder) => {
            builder.select("file_link", "file_type");
          })
          .modifyGraph("technologies_label", (builder) => {
            builder.select("skills");
          })
          .where("company_id", req.user.default_company);
      })
      .where("project_id", projectId)
      .where("company_id", req.user.default_company)
      .first();

    if (!getQuotationData) {
      throw globalCalls.notFoundError(
        "Either the link is incorrect or quotation doesn't belong to you."
      );
    }

    if (req.query.readNotice && req.query.readNotice != "") {
      let id = base64decode(req.query.readNotice);

      notification.updateNotificationReadStatus(id);
    }
    return globalCalls.okResponse(res, getQuotationData, "");
  } catch (error) {
    if (error.statusCode == 404) throw globalCalls.notFoundError(error.message);
    else throw globalCalls.badRequestError(error.message);
  }
};

/**
 * Withdraw or reject quotation - this action is performed by company user
 * @param {*} req
 * @param {*} res
 */
const withdrawOrRejectQuotation = async (req, res) => {
  let data = req.body;

  if (!data.id) {
    throw globalCalls.badRequestError("Invalid Requests.");
  }

  if (!data.action) {
    throw globalCalls.badRequestError("Invalid Action.");
  }

  try {
    let message = "";
    let messageNotify = {};
    if (data.action == "withdraw") {
      message = "Quotation has been withdrawn.";
      messageNotify = Object.assign(
        {},
        globalConst.NOTIFICATION_PAYLOAD["QUOTATION_WITHDRAW_ACTION_TO_CLIENT"]
      );
    } else {
      message = "Quotation has been rejected.";
      messageNotify = Object.assign(
        {},
        globalConst.NOTIFICATION_PAYLOAD["QUOTATION_REJECT_ACTION_TO_CLIENT"]
      );
    }
    let actionDone = await shorlistedCompany
      .query()
      .update({ status: data.action, action_by: "company" })
      .where({ shortlisted_id: data.id, company_id: req.user.default_company })
      .returning("*");

    if (!actionDone) {
      throw globalCalls.badRequestError("Something went wrong.");
    }

    let companyName = await Company.query()
      .select("company_name")
      .where("company_id", req.user.default_company)
      .first();

    let bodyMessage = messageNotify.body.replace(
      "[COMPANY]",
      "<b>" + companyName.company_name + "</b>"
    );
    messageNotify.body = bodyMessage;
    let notifyData = [];

    notifyData.push({
      message: messageNotify.body,
      notification_for: "QUOTATION_ACTION",
      notification_for_id: data.id,
      to_user_id: actionDone[0].client_id,
      by_user_id: req.user.user_id,
      redirect_url: "/new-quotation/all",
      redirect_component: "QUOTATION",
      created_at: new Date().toISOString(),
    });

    if (notifyData.length > 0) {
      await NotificationUpdate(notifyData);
    }
    return globalCalls.okResponse(res, actionDone, message);
  } catch (error) {
    throw globalCalls.badRequestError(error.message);
  }
};
module.exports = {
  fetchQuotation,
  addUpdateQuotation,
  getQuotationData,
  withdrawOrRejectQuotation,
};
