"use strict";

const { base64decode } = require("nodejs-base64");
const globalCalls = require("../../settings/functions");
const globalConst = require("../../settings/constants");

const companyCertificate = require("../../models/CompanyCertificates");
const companyAward = require("../../models/CompanyAwards");
const companyFeature = require("../../models/CompanyFeatured");
const companySuccessStory = require("../../models/CompanySuccessStory");
const messageTips = require("../../models/MessageTips");
const companyPortfolio = require('../../models/CompanyPortfolio');
const companyProgress = require('../../models/CompanyProgress');

/** CERTIFICATES START */
/**
 * adds/update company achievement - certificates
 * @param {*} req
 * @param {*} res
 */
const addUpdateAchievementCertificates = async (req, res) => {
  let data = req.body;

  if (!data.certificate) {
    throw globalCalls.badRequestError("Please enter certificate name.");
  }

  if (!data.organization) {
    throw globalCalls.badRequestError("");
  }

  if (!data.receiving_date) {
    throw globalCalls.badRequestError("Please enter issue date.");
  }

  if (!data.reason) {
    throw globalCalls.badRequestError("Please enter reasons.");
  }

  if (!data.location) {
    throw globalCalls.badRequestError("Please enter location.");
  }

  let message = "";
  if (data.certificate_id == null) {
    delete data.certificate_id;
    data.company_id = req.user.default_company;
    message = "Certificate has been added successfully.";
  } else {
    message = "Certificate has been updated successfully.";
  }

  try {
    let addUpdateCertificate = await companyCertificate
      .query()
      .upsertGraph(data);

    if (!addUpdateCertificate) {
      throw globalCalls.badRequestError("Something went wrong.");
    }

    if (!data.certificate_id) {
      await checkAndUpdateAchievementProgress(req.user.default_company, req.user.user_id);
    }
    return globalCalls.okResponse(res, addUpdateCertificate, message);
  } catch (error) {
    throw globalCalls.badRequestError(error.message);
  }
};

/**
 * fetch company achievement - certificates in edit based on certificate id
 * @param {*} req
 * @param {*} res
 */

const fetchCertificateForEditing = async (req, res) => {
  let tips = await messageTips
    .query()
    .select("message")
    .where("module", "Add Certificate");

  let responseData = {
    tips: tips
  };
  if (req.params.id) {
    let certificate_id = base64decode(req.params.id);
    if(isNaN(parseInt(certificate_id)))
      throw globalCalls.badRequestError("Error! You have passed invalid-id.");
  
    let certificateData = await companyCertificate
      .query()
      .select(
        "certificate_id",
        "certificate",
        "organization",
        "receiving_date",
        "reason",
        "validity_date",
        "location"
      )
      .where("certificate_id", certificate_id)
      .where("company_id", req.user.default_company)
      .where("status", "active")
      .first();

    if (!certificateData) {
      throw globalCalls.badRequestError(
        "Certificate not found. Either invalid link or certificate is deleted."
      );
    }
    responseData["certificateData"] = certificateData;
  }

  return globalCalls.okResponse(res, responseData, "");
};

/**
 * delete certificate
 * @param {*} req
 * @param {*} res
 */
const deleteCertificate = async (req, res) => {
  if (!req.params.id) {
    throw globalCalls.badRequestError("Invalid Request.");
  }
  let certificate_id = base64decode(req.params.id);
  if(isNaN(parseInt(certificate_id)))
    throw globalCalls.badRequestError("Error! You have passed invalid-id.");
  
  let deleteCertificate = await companyCertificate
    .query()
    .update({ status: "delete" })
    .where("certificate_id", certificate_id)
    .returning("*");

  if (!deleteCertificate) {
    throw globalCalls.badRequestError("Something went wrong.");
  }
  return globalCalls.okResponse(
    res,
    deleteCertificate,
    "Certificate has been deleted successfully."
  );
};

/**
 * fetch company certificate list
 * @param {*} req
 * @param {*} res
 */
const fetchCompanyCertificate = async (req, res) => {

  let company_id = req.user ? req.user.default_company : 0;
  if(req.params.id){
    company_id = base64decode(req.params.id);
    if(isNaN(parseInt(company_id)))
	    throw globalCalls.notFoundError("Error! You have passed invalid-id.");
  }
  /** Get parameters for pagination purpose */
  let page = req.query.page ? req.query.page : 1;
  let limit = req.query.limit ? req.query.limit : globalConst.per_page;
  let offset = req.query.offset ? req.query.offset : limit * (page - 1);

  let certificationList = await companyCertificate
    .query()
    .select(
      "certificate_id",
      "certificate",
      "organization",
      "receiving_date",
      "reason",
      "validity_date",
      "location",
      "is_hidden"
    )
    .where("company_id", company_id)
    .where("status","active")
    .offset(offset)
    .limit(limit);

  let totalCount = await companyCertificate
    .query()
    .count()
    .where("company_id", company_id)
    .where("status","active")
    .first();

  let responseData = {
    certificationList,
    total: totalCount.count
  };
  return globalCalls.okResponse(res, responseData, "");
};

/** CERTIFICATES END */

/** AWARDS START */

/**
 * add / update awards
 * @param {*} req
 * @param {*} res
 */
const addUpdateAchievementAwards = async (req, res) => {
  let data = req.body;

  if (!data.award) {
    throw globalCalls.badRequestError("Please enter award name");
  }

  if (!data.category) {
    throw globalCalls.badRequestError("Please enter category.");
  }

  if (!data.receiving_date) {
    throw globalCalls.badRequestError("Please enter issue date.");
  }

  if (!data.location) {
    throw globalCalls.badRequestError("Please enter location.");
  }

  if (!data.reason) {
    throw globalCalls.badRequestError("Please enter reason.");
  }

  let message = "";
  if (data.award_id == null) {
    data.company_id = req.user.default_company;
    delete data.award_id;
    message = "Award has been added successfully.";
  } else {
    message = "Award has been updated successfully.";
  }

  try {
    let addUpdateAward = await companyAward.query().upsertGraph(data);

    if (!addUpdateAward) {
      throw globalCalls.badRequestError("Something went wrong.");
    }

    if (!data.award_id) {
      await checkAndUpdateAchievementProgress(req.user.default_company, req.user.user_id);
    }

    return globalCalls.okResponse(res, addUpdateAward, message);
  } catch (error) {
    throw globalCalls.badRequestError(error.message);
  }
};

/**
 * fetch awards data for add / edit
 * @param {*} req
 * @param {*} res
 */
const fetchAwardEditData = async (req, res) => {
  let tips = await messageTips
    .query()
    .select("message")
    .where("module", "Add Award");

  let responseData = {
    tips: tips
  };

  if (req.params.id) {
    let award_id = base64decode(req.params.id);
    if(isNaN(parseInt(award_id)))
      throw globalCalls.badRequestError("Error! You have passed invalid-id.");
      
    let awardData = await companyAward
      .query()
      .select(
        "award_id",
        "award",
        "receiving_date",
        "reason",
        "location",
        "category"
      )
      .where("award_id", award_id)
      .where("company_id", req.user.default_company)
      .where("status", "active")
      .first();

    if (!awardData) {
      throw globalCalls.badRequestError(
        "Award not found. Either invalid link or award is deleted."
      );
    }
    responseData["awardData"] = awardData;
  }

  return globalCalls.okResponse(res, responseData, "");
};

/**
 * delete award
 * @param {*} req
 * @param {*} res
 */
const deleteAward = async (req, res) => {
  if (!req.params.id) {
    throw globalCalls.badRequestError("Invalid Request.");
  }
  let award_id = base64decode(req.params.id);
  if(isNaN(parseInt(award_id)))
      throw globalCalls.badRequestError("Error! You have passed invalid-id.");
      
  let deleteAwardData = await companyAward
    .query()
    .update({ status: "delete" })
    .where("award_id", award_id)
    .returning("*");

  if (!deleteAwardData) {
    throw globalCalls.badRequestError("Something went wrong.");
  }
  return globalCalls.okResponse(
    res,
    deleteAwardData,
    "Award has been deleted successfully."
  );
};

/**
 * fetch company award list
 * @param {*} req
 * @param {*} res
 */
const fetchCompanyAwards = async (req, res) => {

  let company_id = req.user ? req.user.default_company : 0;
  if(req.params.id){
    company_id = base64decode(req.params.id);
  }

  if(isNaN(parseInt(company_id)))
	  throw globalCalls.notFoundError("Error! You have passed invalid-id.");

  /** Get parameters for pagination purpose */
  let page = req.query.page ? req.query.page : 1;
  let limit = req.query.limit ? req.query.limit : globalConst.per_page;
  let offset = req.query.offset ? req.query.offset : limit * (page - 1);

  let awardList = await companyAward
    .query()
    .select(
      "award_id",
      "award",
      "category",
      "receiving_date",
      "reason",
      "location",
      "is_hidden"
    )
    .where("company_id", company_id)
    .where("status","active")
    .offset(offset)
    .limit(limit);

  let totalCount = await companyAward
    .query()
    .count()
    .where("company_id", company_id)
    .where("status","active")
    .first();

  let responseData = {
    awardList,
    total: totalCount.count
  };
  return globalCalls.okResponse(res, responseData, "");
};

/** AWARDS ENDS*/
/** FEATURES START */

/**
 * add / update feature data
 * @param {*} req
 * @param {*} res
 */
const addUpdateFeaturedData = async (req, res) => {
  let data = req.body;

  if (!data.when_featured) {
    throw globalCalls.badRequestError(
      "Please enter when you were featured in."
    );
  }

  if (!data.featured_in) {
    throw globalCalls.badRequestError("Please enter in what you had featured.");
  }

  if (!data.links) {
    throw globalCalls.badRequestError("Please enter atleast one link");
  }

  if (!data.reason) {
    throw globalCalls.badRequestError("Please specify reason for it.");
  }

  let message = "";

  if (data.feature_id == null) {
    delete data.feature_id;
    data.company_id = req.user.default_company;
    message = "Featured has been added successfully.";
  } else {
    message = "Featured has been updated successfully.";
  }
  try {
    let addUpdateFeaturing = await companyFeature.query().upsertGraph(data);
    if (!addUpdateFeaturing) {
      throw globalCalls.badRequestError("Something went wrong.");
    }

    if (!data.feature_id) {
      await checkAndUpdateAchievementProgress(req.user.default_company, req.user.user_id);
    }

    return globalCalls.okResponse(res, addUpdateFeaturing, message);
  } catch (error) {
    throw globalCalls.badRequestError(error.message);
  }
};

/**
 * fetch data for add / edit featured data
 * @param {*} req
 * @param {*} res
 */
const fetchFeaturesForEditing = async (req, res) => {
  let tips = await messageTips
    .query()
    .select("message")
    .where("module", "Add Featured").first();

  let responseData = {
    tips: tips
  };
  if (req.params.id) {
    let feature_id = base64decode(req.params.id);
  
  if(isNaN(parseInt(feature_id)))
	  throw globalCalls.badRequestError("Error! You have passed invalid-id.");

    let featuredData = await companyFeature
      .query()
      .select(
        "feature_id",
        "featured_in",
        "when_featured",
        "reason",
        "links",
        "special_mentioned"
      )
      .where("feature_id", feature_id)
      .where("company_id", req.user.default_company)
      .where("status", "active")
      .first();

    if (!featuredData) {
      throw globalCalls.badRequestError(
        "Featured data not found. Either invalid link or it is deleted."
      );
    }

    responseData["featuredData"] = featuredData;
  }

  return globalCalls.okResponse(res, responseData, "");
};

/**
 * delete featured data
 * @param {*} req
 * @param {*} res
 */
const deleteFeaturedData = async (req, res) => {
  if (!req.params.id) {
    throw globalCalls.badRequestError("Invalid Requests.");
  }

  let feature_id = base64decode(req.params.id);
  if(isNaN(parseInt(feature_id)))
    throw globalCalls.badRequestError("Error! You have passed invalid-id.");
  
  let deleteFeatured = await companyFeature
    .query()
    .update({ status: "delete" })
    .where("feature_id", feature_id)
    .where("company_id", req.user.default_company)
    .returning("*");

  if (!deleteFeatured) {
    throw globalCalls.badRequestError("Something went wrong.");
  }

  return globalCalls.okResponse(res, deleteFeatured, "Deleted successfully.");
};

/**
 * fetch company featured in data
 * @param {*} req
 * @param {*} res
 */
const fetchCompanyFeaturedData = async (req, res) => {

  let company_id = req.user ? req.user.default_company : 0;
  if(req.params.id){
    company_id = base64decode(req.params.id);
  }
  if(isNaN(parseInt(company_id)))
	  throw globalCalls.notFoundError("Error! You have passed invalid-id.");

  /** Get parameters for pagination purpose */
  let page = req.query.page ? req.query.page : 1;
  let limit = req.query.limit ? req.query.limit : globalConst.per_page;
  let offset = req.query.offset ? req.query.offset : limit * (page - 1);

  let featuredList = await companyFeature
    .query()
    .select(
      "feature_id",
      "featured_in",
      "when_featured",
      "reason",
      "links",
      "special_mentioned",
      "is_hidden"
    )
    .where("company_id", company_id)
    .where("status","active")
    .offset(offset)
    .limit(limit);

  let totalCount = await companyFeature
    .query()
    .count()
    .where("company_id", company_id)
    .where("status","active")
    .first();

  let responseData = {
    featuredList,
    total: totalCount.count
  };
  return globalCalls.okResponse(res, responseData, "");
};

/** FEATURES ENDS */
/** SUCCESS STORY STARTS */

/**
 * add / update success story
 * @param {*} req
 * @param {*} res
 */
const addUpdateSuccessStory = async (req, res) => {
  let data = req.body;

  if (!data.portfolio_id) {
    throw globalCalls.badRequestError("Please choose portfolio");
  }

  if (!data.success_category) {
    throw globalCalls.badRequestError("Please specify success category.");
  }

  if (!data.reason) {
    throw globalCalls.badRequestError("Please specify reason.");
  }

  let message = "";
  if (data.story_id == null) {
    delete data.story_id;
    data.company_id = req.user.default_company;
    message = "Story has been added successfully.";
  } else {
    message = "Story has been updated successfully.";
  }

  try {
    let addUpdateSuccessStoryData = await companySuccessStory
      .query()
      .upsertGraph(data);
    if (!addUpdateSuccessStoryData) {
      throw globalCalls.badRequestError("Something went wrong.");
    }

    if (!data.story_id) {
      await checkAndUpdateAchievementProgress(req.user.default_company, req.user.user_id);
    }

    return globalCalls.okResponse(res, addUpdateSuccessStoryData, message);
  } catch (error) {
    throw globalCalls.badRequestError(error.message);
  }
};

/**
 * fetch company success data in add /edit 
 * @param {*} req 
 * @param {*} res 
 */
const fetchCompanySuccessStoryToEdit = async (req, res) => {
  let tips = await messageTips
    .query()
    .select("message")
    .where("module", "Add Success Story");

    let portfolioList = await companyPortfolio.query().select("portfolio_id", "project_name").where("company_id", req.user.default_company).where("status", "published");
  let responseData = {
    tips: tips,
    portfolioList
  };

  if(req.params.id){
    let story_id = base64decode(req.params.id);

    if(isNaN(parseInt(story_id)))
	    throw globalCalls.badRequestError("Error! You have passed invalid-id.");

    let successStory = await companySuccessStory.query().select("story_id", "success_category", "reason", "number_of_downloads", "funding", "award", "portfolio_id").where("story_id", story_id).where("company_id", req.user.default_company).where("status", "active");

    if(!successStory){
      throw globalCalls.badRequestError("Either invalid link or success story has been deleted.");
    }
    responseData["successStory"] = successStory;
  }

  return globalCalls.okResponse(res, responseData, "");
};

/**
 * deletet company success story
 * @param {*} req 
 * @param {*} res 
 */
const deleteSuccessStory = async(req, res)=>{
  if(!req.params.id){
    throw globalCalls.badRequestError("Invalid Requests");
  }

  let story_id = base64decode(req.params.id);
  if(isNaN(parseInt(story_id)))
	  throw globalCalls.badRequestError("Error! You have passed invalid-id.");

  let deleteSuccess = await companySuccessStory.query().update({status: "delete"}).where("story_id", story_id).where("company_id", req.user.default_company);

  if(!deleteSuccess){
    throw globalCalls.badRequestError("Something went wrong.");
  }

  return globalCalls.okResponse(res, deleteSuccess, "Success story has been deleted successfully.");
};

/**
 * Fetches Company Success Stories
 * @param {*} req 
 * @param {*} res 
 */
const fetchCompanySuccessStories = async(req, res) =>{

  let company_id = req.user ? req.user.default_company : 0;
  if(req.params.id){
    company_id = base64decode(req.params.id);
  }
  if(isNaN(parseInt(company_id)))
	  throw globalCalls.notFoundError("Error! You have passed invalid-id.");

  /** Get parameters for pagination purpose */
  let page = req.query.page ? req.query.page : 1;
  let limit = req.query.limit ? req.query.limit : globalConst.per_page;
  let offset = req.query.offset ? req.query.offset : limit * (page - 1);

  let storyList = await companySuccessStory
    .query()
    .select(
      "story_id",
      "success_category",
      "reason", "number_of_downloads", "funding", "award", "is_hidden"
    ).withGraphFetched("project").modifyGraph("project", builder=>{
      builder.select("project_name")
    })
    .where("company_id", company_id)
    .where("status","active")
    .offset(offset)
    .limit(limit);

  let totalCount = await companySuccessStory
    .query()
    .count()
    .where("company_id", company_id)
    .where("status","active")
    .first();

    let responseData = {
      storyList,
      total: totalCount.count
    };
    return globalCalls.okResponse(res, responseData, "");

}

const checkAndUpdateAchievementProgress = async (company_id, user_id) => {

  let checkIfcompanyProgress = await companyProgress.query().select("").where("company_id", company_id).where("progress", "Achievement").first();

  if(!checkIfcompanyProgress){
    let updateCompanyProgress = await companyProgress.query().insert({
      progress: "Achievement",
      company_id: company_id,
      user_id: user_id
    });
  }
  
}
/** SUCCESS STORY ENDS */

module.exports = {
  addUpdateAchievementCertificates,
  fetchCertificateForEditing,
  deleteCertificate,
  fetchCompanyCertificate,
  addUpdateAchievementAwards,
  fetchAwardEditData,
  deleteAward,
  fetchCompanyAwards,
  addUpdateFeaturedData,
  fetchFeaturesForEditing,
  deleteFeaturedData,
  fetchCompanyFeaturedData,
  addUpdateSuccessStory,
  fetchCompanySuccessStoryToEdit,
  deleteSuccessStory,
  fetchCompanySuccessStories
};
