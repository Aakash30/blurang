'use strict';

const { base64encode, base64decode } = require('nodejs-base64');
const globalCalls = require('../../settings/functions');
const company = require('../../models/Company');
const domain = require('../../models/Domain');
const service = require('../../models/Service');
const skills = require('../../models/Skills');
const {
    transaction
} = require('objection');

const getSkills = async(req, res)=>{

    let skillData = await skills.query().select("skill_id", "skills", "service_id")
    
    if(!skillData) {
      throw globalCalls.badRequestError("Services not found.");
    } 
    else {
        let responseData = {
            skillData
        };
        return globalCalls.okResponse(res, responseData, "");
    }
}

const addSkill = async (req, res)=>{
    let data = req.body;
    if(!data.skills) {
        throw globalCalls.badRequestError("Please fill up skill name.");
    }
    if(!data.service_id) {
        throw globalCalls.badRequestError("Please fill up service id.");
    }

    try
    {
        if(!data.skill_id)
            delete data['skill_id'];
        else if(data.skill_id == null) {
            delete data['skill_id'];
        }

        let addservice = await service.query().upsertGraph(data).returning("*");
        if(!addservice) {
            throw globalCalls.badRequestError("something went wrong.");
        } else {
            return globalCalls.okResponse(res,addservice,"Successfully added.");
        }
    } catch (error) {
        throw globalCalls.badRequestError(error.message)
    }
}


module.exports = {
    addSkill,
    getSkills,
}