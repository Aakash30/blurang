'use strict';

const { base64encode, base64decode } = require('nodejs-base64');
const globalCalls = require('../../settings/functions');
const globalConst = require("../../settings/constants");
const company = require('../../models/Company');
const project = require('../../models/Project');
const projectFinalBudget = require('../../models/ProjectFinalBudget');

const domain = require('../../models/Domain');
const service = require('../../models/Service');
const skills = require('../../models/Skills');

const knexConfig = require("../../../config/knex");
const Knex = require("knex")(knexConfig.development);

const {
    transaction
} = require('objection');

const getDomain = async(req, res)=>{

    let domainData = await domain.query().select("domain_id", "domain", "domain_image", "domain_description")
    
    if(!domainData) {
      throw globalCalls.badRequestError("Domain not found.");
    } 
    else {
        let responseData = {
            domainData
        };
        return globalCalls.okResponse(res, responseData, "");
    }
}

const addDomain = async (req, res)=>{
    let data = req.body;
    if(!data.domain) {
        throw globalCalls.badRequestError("Please fill up domain name.");
    }
    if(!data.domain_description) {
        throw globalCalls.badRequestError("Please fill up domain description.");
    }

    if(req.files)
    {
        if (req.files.domain_image_file) {
            data.domain_image = req.files.domain_image_file[0].location;
        }
        delete data['domain_image_file'];
        
        if (req.files.domain_icon_file) {
            data.domain_icon = req.files.domain_icon_file[0].location;
        }
        delete data['domain_icon_file'];
    }
    
    try
    {
        if(!data.domain_id)
        {
            delete data['domain_id'];
        }
        else if(data.domain_id == null) {
            delete data['domain_id'];
        }

        let adddomain = await domain.query().upsertGraph(data).returning("*");
        if(!adddomain) {
            throw globalCalls.badRequestError("something went wrong.");
        } else {
            return globalCalls.okResponse(res,adddomain,"Successfully added.");
        }

    } catch (error) {
        throw globalCalls.badRequestError(error.message)
    }
}

const getServices = async(req, res)=>{

    let serviceData = await service.query().select("service_id", "service_name")
    
    if(!serviceData) {
      throw globalCalls.badRequestError("Services not found.");
    } 
    else {
        let responseData = {
            serviceData
        };
        return globalCalls.okResponse(res, responseData, "");
    }
}

const addService = async (req, res)=>{
    let data = req.body;
    if(!data.service_name) {
        throw globalCalls.badRequestError("Please fill up service name.");
    }

    try
    {
        if(!data.service_id)
            delete data['service_id'];
        else if(data.service_id == null) {
            delete data['service_id'];
        }

        let addservice = await service.query().upsertGraph(data).returning("*");
        if(!addservice) {
            throw globalCalls.badRequestError("something went wrong.");
        } else {
            return globalCalls.okResponse(res,addservice,"Successfully added.");
        }
    } catch (error) {
        throw globalCalls.badRequestError(error.message)
    }
}

const domainDetails = async(req, res)=>{
    try
    {
        if(req.params.id)
        {
        var domain_id = base64decode(req.params.id); 
        if(isNaN(parseInt(domain_id)))
            throw globalCalls.notFoundError("Error! You have passed invalid-id.");
        
        // domain_id=1;
        let page = req.query.page ? req.query.page : 1;
        let limit = req.query.limit ? req.query.limit : globalConst.per_page;
        let offset = req.query.offset ? req.query.offset : limit * (page - 1);
        
        let domainData = await domain.query().select("domain_id", "domain", "domain_image", "domain_description")
        .where((builder) => {
            builder.where("domain_id", domain_id);
        })

        let companyData = await company.query()
            .select(
              "company_id", "company_name", "director_name", "company_email_id", "contact_number",
              "fixed_budget", "domain_id", "no_of_project_completed",
              Knex.raw("case when (SELECT Round (AVG ((SELECT AVG(stars) AS starsAvg FROM review_rating WHERE review_id = reviews.review_id)),2)  from reviews where company_id = company.company_id and feedback_received = true group by company_id) IS NULL THEN 0 else (SELECT Round (AVG ((SELECT AVG(stars) AS starsAvg FROM review_rating WHERE review_id = reviews.review_id)),2)  from reviews where company_id = company.company_id and feedback_received = true group by company_id) END as overall_rating "),
              Knex.raw("case when (SELECT sum(final_budget) FROM project_final_budget WHERE company_id = company.company_id) IS NULL THEN 0 else (SELECT sum(final_budget) FROM project_final_budget WHERE company_id = company.company_id) END as earning "),
          )
          .withGraphFetched("[domain]")
          .modifyGraph("domain", (builder) => {
          builder.select("domain");
          })
          .where((builder) => {
            builder.where("domain_id", domain_id);
            builder.where("status", "approved");
          })
          .orderBy('no_of_project_completed', 'DESC')
          .offset(offset)
          .limit(limit)
        /** */      
        
        let projects = await project
        .query()
        .count()
        .where("domain_id", domain_id)
        .whereNot("status","draft")
        .first();

        let companies = await company
        .query()
        .count()
        .where("domain_id", domain_id)
        .where("status", "approved")
        .first();

        let earning = await projectFinalBudget.query()
        .sum('final_budget')
        .leftJoin("project", function () {
          this.on(
            "project.project_id",
            "=",
            "project_final_budget.project_id"
          );
        })
        .where("project.domain_id", domain_id) //portfolio. added by zubear bczof company_id is ambiguous
        .first();
        if(!earning.sum) 
          earning.sum=0;

        let highest_budget = await projectFinalBudget.query()
        .max('final_budget')
        .leftJoin("project", function () {
          this.on(
            "project.project_id",
            "=",
            "project_final_budget.project_id"
          );
        })
        .where("project.domain_id", domain_id) //portfolio. added by zubear bczof company_id is ambiguous
        .first();
        if(!highest_budget.max) 
        highest_budget.max=0;

        

        // let highest_budget = await projectFinalBudget
        // .query()
        // .max('final_budget')
        // .withGraphFetched("[project_details]")
        //   .modifyGraph("project_details", (builder) => {
        //   // builder.select("domain");
        //   builder.where("project.domain_id", domain_id);
        //   })
        // //   .where((builder) => {
        // //     builder.where("domain_id", domain_id);
        // // })
        // .first();

        if(!domainData) {
            throw globalCalls.badRequestError("Client not found.");
        } else {

            let responseData = {
              domainData,
              companyData,
              projects,
              companies,
              earning,
              highest_budget
            };

            return globalCalls.okResponse(res, responseData, "");
        }
        } 
        else {
        }
    } catch (error){
    console.log(error);
    throw globalCalls.badRequestError(error.message)
    }
}

const dashboard_company = async (req, res) => {
    try {
      let company_id = req.user.default_company;
      
      let responseData = {
        showWelcome: true
      };
  
      if(company_id != null){
        let companyProgress = await CompanyProgress.query().select("progress").where({"company_id": company_id, "progress": "Company Info"}).first();
  
        if(companyProgress){
          let graphCards = await CompanyVisitTracker.query().select(Knex.raw('COUNT(CASE WHEN "created_at"::date >= \''+moment().startOf("month").format("YYYY-MM-DD")+'\' AND "created_at"::date <= \''+ moment().format("YYYY-MM-DD")+'\' THEN 1 ELSE NULL END) AS currentMonth, COUNT(CASE WHEN "created_at"::date >= \''+moment().subtract(1, "months").startOf("month").format("YYYY-MM-DD")+'\' AND "created_at"::date <= \''+moment().subtract(1, "month").format("YYYY-MM-DD")+'\' THEN 1 ELSE NULL END) AS lastMonth, \'Profile Visit\' AS title, \'card1.svg\' AS icon')).whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
            moment().subtract(1, "months").startOf("month").format("YYYY-MM-DD"),
            moment().format("YYYY-MM-DD"),
          ]).where("company_id", req.user.default_company).unionAll([ProjectRecommendedCompanies.query().select(Knex.raw('COUNT(CASE WHEN "created_at"::date >= \''+moment().startOf("month").format("YYYY-MM-DD")+'\' AND "created_at"::date <= \''+ moment().format("YYYY-MM-DD")+'\' THEN 1 ELSE NULL END) AS currentMonth, COUNT(CASE WHEN "created_at"::date >= \''+moment().subtract(1, "months").startOf("month").format("YYYY-MM-DD")+'\' AND "created_at"::date <= \''+moment().subtract(1, "month").format("YYYY-MM-DD")+'\' THEN 1 ELSE NULL END) AS lastMonth, \'Recommended\' AS title, \'card2.svg\' AS icon')).whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
            moment().subtract(1, "months").startOf("month").format("YYYY-MM-DD"),
            moment().format("YYYY-MM-DD"),
          ]).where("company_id", company_id), Project.query().select(Knex.raw('COUNT(CASE WHEN "created_at"::date >= \''+moment().startOf("month").format("YYYY-MM-DD")+'\' AND "created_at"::date <= \''+ moment().format("YYYY-MM-DD")+'\' THEN 1 ELSE NULL END) AS currentMonth, COUNT(CASE WHEN "created_at"::date >= \''+moment().subtract(1, "months").startOf("month").format("YYYY-MM-DD")+'\' AND "created_at"::date <= \''+moment().subtract(1, "month").format("YYYY-MM-DD")+'\' THEN 1 ELSE NULL END) AS lastMonth, \'New Project Post\' AS title,  \'icon-project.svg\' AS icon')).whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
            moment().subtract(1, "months").startOf("month").format("YYYY-MM-DD"),
            moment().format("YYYY-MM-DD"),
          ]), ShortlistedCompanies.query().select(Knex.raw('COUNT(CASE WHEN "created_at"::date >= \''+moment().startOf("month").format("YYYY-MM-DD")+'\' AND "created_at"::date <= \''+ moment().format("YYYY-MM-DD")+'\' THEN 1 ELSE NULL END) AS currentMonth, COUNT(CASE WHEN "created_at"::date >= \''+moment().subtract(1, "months").startOf("month").format("YYYY-MM-DD")+'\' AND "created_at"::date <= \''+moment().subtract(1, "month").format("YYYY-MM-DD")+'\' THEN 1 ELSE NULL END) AS lastMonth, \'Quotation Request Receive\' AS title,  \'icon-new-quotation.svg\' AS icon')).whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
            moment().subtract(1, "months").startOf("month").format("YYYY-MM-DD"),
            moment().format("YYYY-MM-DD"),
          ]).where("company_id", req.user.default_company)]);
    
          let recommendadedData;
    
          let companyVisitHeading = await CompanyVisitTracker
          .query()
          .select(Knex.raw('COUNT(CASE WHEN "created_at"::date >= \''+moment().subtract(30, "days").format("YYYY-MM-DD")+'\' AND "created_at"::date <= \''+ moment().format("YYYY-MM-DD")+'\' THEN 1 ELSE NULL END) AS last30days, COUNT(CASE WHEN "created_at"::date >= \''+moment().startOf('week').format("YYYY-MM-DD")+'\' AND "created_at"::date <= \''+moment().format("YYYY-MM-DD")+'\' THEN 1 ELSE NULL END) AS thisweek,  COUNT(CASE WHEN "created_at"::date >= \''+moment().subtract(1, "weeks").startOf('week').format("YYYY-MM-DD")+'\' AND "created_at"::date <= \''+moment().subtract(1, "weeks").endOf('week').format("YYYY-MM-DD")+'\' THEN 1 ELSE NULL END) AS lastweek'))
          .whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
              moment().subtract(30, "days").format("YYYY-MM-DD"),
              moment().format("YYYY-MM-DD"),
            ]).where("company_id", company_id)
          .first();
    
          let graphDataQuery = await CompanyVisitTracker.query().select(Knex.raw('COUNT(tracking_id), "created_at"::date')).whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
            moment().subtract(7, "days").format("YYYY-MM-DD"),
            moment().format("YYYY-MM-DD"),
          ]).where("company_id", company_id).groupByRaw('"created_at"::date').orderBy("created_at");
    
          let graphData = {
            data: [],
            label: []
          };
    
          let startDate = moment().subtract(7, "days").format("YYYY-MM-DD");
          for (let i = 1; i < 8; i++){
             let indexFound = graphDataQuery.findIndex((x) => moment(x.created_at).format('YYYY-MM-DD') == startDate);
             
             if(indexFound !== -1){
              graphData.data.push(parseInt(graphDataQuery[indexFound].count));
              graphData.label.push(moment(startDate).format('MMM DD'));
             } else {
             
              graphData.data.push(0);
              graphData.label.push(moment(startDate).format('MMM DD'));
             }
             startDate = moment(startDate).add(1, "day").format("YYYY-MM-DD");
           }
    
        let companyRankData = await company
          .query()
          .select("company_id", "company_name", "logo")
          .withGraphFetched("[shortlisted, hired, reviews]")
          .modifyGraph("hired", (builder) => {
            builder
              .count()
              .where("status", "selected")
              .groupBy("shortlisted_companies.company_id");
          })
          .modifyGraph("shortlisted", (builder) => {
            builder
              .count()
              .groupBy("shortlisted_companies.company_id");
          })
          .modifyGraph("reviews", (builder) => {
            builder
            .count()
              .groupBy("reviews.company_id");
          })
          .where("domain_id", "=", company.query().select('domain_id').where("company_id", req.user.default_company)).offset(0)
          .limit(5);
    
          let myRankData = await company
          .query()
          .select("company_id", "company_name", "logo")
          .withGraphFetched("[shortlisted, hired, reviews]")
          .modifyGraph("hired", (builder) => {
            builder
              .count()
              .where("status", "selected")
              .groupBy("shortlisted_companies.company_id");
          })
          .modifyGraph("shortlisted", (builder) => {
            builder
              .count()
              .groupBy("shortlisted_companies.company_id");
          })
          .modifyGraph("reviews", (builder) => {
            builder
            .count()
              .groupBy("reviews.company_id");
          })
          .where({
            company_id: company_id,
          }).first();
    
          responseData = {
            graphCards,
            companyVisitHeading,
            graphData,
            companyRankData,
            myRankData,
            companyProgress,
            showWelcome: false
          }
        } else {
          responseData = {
            companyProgress: companyProgress,
            showWelcome: true
          }
        }
        
      }

      return globalCalls.okResponse(res, responseData, "");
    } catch (error) {
      console.log(error);
      throw globalCalls.badRequestError(error.message);
    }
  };

module.exports = {
    getDomain,
    addDomain,
    domainDetails,
    addService,
    getServices,
}