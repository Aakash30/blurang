'use strict';

const { base64encode, base64decode } = require('nodejs-base64');
const globalCalls = require('../../settings/functions');
const company = require('../../models/Company');
const domain = require('../../models/Domain');
const service = require('../../models/Service');
const skills = require('../../models/Skills');
const {
    transaction
} = require('objection');

const getServices = async(req, res)=>{

    let serviceData = await service.query().select("service_id", "service_name")
    // .where("status", '');

    if(!serviceData) {
      throw globalCalls.badRequestError("Services not found.");
    } 
    else {
        let responseData = {
            serviceData
        };
        return globalCalls.okResponse(res, responseData, "");
    }
}

const addService = async (req, res)=>{
    let data = req.body;
    // console.log(req.body);
    if(!data.service_name) {
        throw globalCalls.badRequestError("Please fill up service name.");
    }
    if(req.file)
    {
        if (req.file.fieldname == 'service_icon_file') {
            data.service_icon = req.file.location;
        }
        delete data['service_icon_file'];
    }

    try
    {
        if(!data.service_id)
            delete data['service_id'];
        else if(data.service_id == null) {
            delete data['service_id'];
        }

        let addservice = await service.query().upsertGraph(data).returning("*");
        if(!addservice) {
            throw globalCalls.badRequestError("something went wrong.");
        } else {
            return globalCalls.okResponse(res,addservice,"Successfully added.");
        }
    } catch (error) {
        throw globalCalls.badRequestError(error.message)
    }
}


module.exports = {
    addService,
    getServices,
}