const globalCalls = require("../../settings/functions");
const project = require("../../models/Project");
const shortlistedCompanies = require("../../models/ShortlistedCompanies")

const FetchAllProjects = async (req, res) => {
    try {
        let projectData = await project
            .query()
            .select(
                "project_id",
                "project_name",
                "created_at",
                "budget",
                "status"
            ).withGraphFetched("[domain, client_details]")
            .modifyGraph("domain", (builder) => {
                builder.select("domain");
            })
            .modifyGraph("client_details", (builder) => {
                builder.select("first_name", "last_name");
            })
        if (!projectData) {
            throw globalCalls.badRequestError("Project details not found.");
        } else {
            let responseData = {
                projectData,
            };
            return globalCalls.okResponse(res, responseData, "");
        }
    }
    catch (error) {
        console.log(error)
        throw globalCalls.badRequestError(error.message)
    }
}

const FetchProjectsQuotationPending = async (req, res) => {
    try {
        let projectData = await project
            .query()
            .select(
                "project_id",
                "project_name",
                "created_at",
                "budget",
                "status"
            ).withGraphFetched("[domain, client_details]")
            .modifyGraph("domain", (builder) => {
                builder.select("domain");
            })
            .modifyGraph("client_details", (builder) => {
                builder.select("first_name", "last_name");
            }).where("status")
        if (!projectData) {
            throw globalCalls.badRequestError("Project details not found.");
        } else {
            let responseData = {
                projectData,
            };
            return globalCalls.okResponse(res, responseData, "");
        }
    }
    catch (error) {
        console.log(error)
        throw globalCalls.badRequestError(error.message)
    }
}
const FetchInProgressProjects = async (req, res) => {
    try {
        let projectData = await project
            .query()
            .select(
                "project_id",
                "project_name",
                "created_at",
                "budget",
                "hire_date",
                "status"
            ).withGraphFetched("[domain, client_details, hired_company]")
            .modifyGraph("domain", (builder) => {
                builder.select("domain");
            })
            .modifyGraph("hired_company", (builder) => {
                builder.select("company_name", "company_email_id");
            })
            .modifyGraph("client_details", (builder) => {
                builder.select("first_name", "last_name");
            }).where("status", "in-progress")
        if (!projectData) {
            throw globalCalls.badRequestError("Project details not found.");
        } else {
            let responseData = {
                projectData,
            };
            return globalCalls.okResponse(res, responseData, "");
        }
    }
    catch (error) {
        console.log(error)
        throw globalCalls.badRequestError(error.message)
    }
}

const FetchDraftProjects = async (req, res) => {
    try {
        let projectData = await project
            .query()
            .select(
                "project_id",
                "project_name",
                "created_at",
                "budget",
                "hire_date",
                "status",
                "completion_datetime"
            ).withGraphFetched("[domain, client_details]")
            .modifyGraph("domain", (builder) => {
                builder.select("domain");
            })
            .modifyGraph("client_details", (builder) => {
                builder.select("first_name", "last_name");
            }).where("status", "draft")
        if (!projectData) {
            throw globalCalls.badRequestError("Project details not found.");
        } else {
            let responseData = {
                projectData,
            };
            return globalCalls.okResponse(res, responseData, "");
        }
    }
    catch (error) {
        console.log(error)
        throw globalCalls.badRequestError(error.message)
    }
}

const FetchUnderDiscussionProjects = async (req, res) => {
    try {
        let projectData = await project
            .query()
            .select(
                "project_id",
                "project_name",
                "created_at",
                "budget",
                "hire_date",
                "completion_datetime",
                "status"
            ).withGraphFetched("[domain, client_details, hired_company]")
            .modifyGraph("domain", (builder) => {
                builder.select("domain");
            })
            .modifyGraph("hired_company", (builder) => {
                builder.select("company_name", "company_email_id");
            })
            .modifyGraph("client_details", (builder) => {
                builder.select("first_name", "last_name");
            }).where("status", "under-discussion")
        if (!projectData) {
            throw globalCalls.badRequestError("Project details not found.");
        } else {
            let responseData = {
                projectData,
            };
            return globalCalls.okResponse(res, responseData, "");
        }
    }
    catch (error) {
        console.log(error)
        throw globalCalls.badRequestError(error.message)
    }
}
const FetchCompletedProjects = async (req, res) => {
    try {
        let projectData = await project
            .query()
            .select(
                "project_id",
                "project_name",
                "created_at",
                "budget",
                "hire_date",
                "completion_datetime",
                "status"
            ).withGraphFetched("[domain, client_details, hired_company]")
            .modifyGraph("domain", (builder) => {
                builder.select("domain");
            })
            .modifyGraph("hired_company", (builder) => {
                builder.select("company_name", "company_email_id");
            })
            .modifyGraph("client_details", (builder) => {
                builder.select("first_name", "last_name");
            }).where("status", "complete")
        if (!projectData) {
            throw globalCalls.badRequestError("Project details not found.");
        } else {
            let responseData = {
                projectData,
            };
            return globalCalls.okResponse(res, responseData, "");
        }
    }
    catch (error) {
        console.log(error)
        throw globalCalls.badRequestError(error.message)
    }
}
const FetchArchivedProjects = async (req, res) => {
    try {
        let projectData = await project
            .query()
            .select(
                "project_id",
                "project_name",
                "created_at",
                "budget",
                "hire_date",
                "completion_datetime",
                "status"
            ).withGraphFetched("[domain, client_details]")
            .modifyGraph("domain", (builder) => {
                builder.select("domain");
            })
            .modifyGraph("client_details", (builder) => {
                builder.select("first_name", "last_name");
            }).where("status", "archive")
        if (!projectData) {
            throw globalCalls.badRequestError("Project details not found.");
        } else {
            let responseData = {
                projectData,
            };
            return globalCalls.okResponse(res, responseData, "");
        }
    }
    catch (error) {
        console.log(error)
        throw globalCalls.badRequestError(error.message)
    }
}

const ProjectUnblished = async (req, res) => {
    try {
        console.log(req.params.id)
        const project_unblished = await project.query().update({ "archived_by": req.user.user_id, "archived_by_type": req.user.user_type }).where("project_id",req.params.id)
        if (!project_unblished) {
            throw globalCalls.badRequestError("Project details not found.");
        }
        return globalCalls.okResponse(res, "", "Operation Successfull");
    }
    catch (error) {
        console.log(error)
        throw globalCalls.badRequestError(error.message)
    }
}
const ProjectPublished = async (req, res) => {
    try {
        console.log(req.params.id)
        const project_published = await project.query().update({ "archived_by": null, "archived_by_type": null }).where("project_id",req.params.id)
        if (!project_published) {
            throw globalCalls.badRequestError("Project details not found.");
        }
        return globalCalls.okResponse(res, "", "Operation Successfull");
    }
    catch (error) {
        console.log(error)
        throw globalCalls.badRequestError(error.message)
    }
}



const FetchProjectsQuotationReceived = async (req, res) => {
    try {
        let projectData = await project
            .query()
            .select(
                "project_id",
                "project_name",
                "created_at",
                "budget",
                "status"
            ).withGraphFetched("[domain, client_details,shortlisted_companies]")
            .modifyGraph("domain", (builder) => {
                builder.select("domain");
            })
            .modifyGraph("client_details", (builder) => {
                builder.select("first_name", "last_name");
            })
            .where("status", "requested-quotation")
        if (!projectData) {
            throw globalCalls.badRequestError("Project details not found.");
        } else {
            let responseData = {
                projectData,
            };
            return globalCalls.okResponse(res, responseData, "");
        }
    }
    catch (error) {
        console.log(error)
        throw globalCalls.badRequestError(error.message)
    }
}
module.exports = { ProjectUnblished,ProjectPublished, FetchAllProjects, FetchUnderDiscussionProjects, FetchArchivedProjects, FetchInProgressProjects, FetchDraftProjects, FetchCompletedProjects }