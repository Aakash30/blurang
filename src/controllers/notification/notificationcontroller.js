
const { base64decode } = require("nodejs-base64");

const Notification = require("../../models/Notification")
const globalCalls = require('../../settings/functions');
const globalConst = require('../../settings/constants');
const knexConfig = require('../../../config/knex');
const Knex = require('knex')(knexConfig.development);

const {
    io
} = require('../../settings/server');

const companyMap = require('../../models/CompanyMapping');
const userAuth = require('../../models/UserAuth');

const GetNotification = async (req, res) => {
    try {

        let page = req.query.page ? req.query.page : 1;
        let limit = req.query.limit ? req.query.limit : globalConst.per_page;
        let offset = req.query.offset ? req.query.offset : limit * (page - 1);

        let notification = await Notification.query().select("notification_id", "message", "read_status", "redirect_url", "created_at", "sender")
            .where({ "to_user_id": req.user.user_id, "clear": false }).offset(offset).limit(limit)
            .orderByRaw("sender NULLS LAST")
            .orderBy("read_status", "DESC")
        if (!notification)
            throw globalCalls.badRequestError("No data found.");

        let totalCount = await Notification.query().count().where({ "to_user_id": req.user.user_id, "clear": false });

        let responseData = {
            notification,
            total: totalCount.count
        }
        return globalCalls.okResponse(res, responseData, "Operation Successful");
    }
    catch (error) {
        throw globalCalls.badRequestError(error.message)
    }
}


const updateNotification = async (data) => {

    let columnName = '';
    columnName += Object.keys(data[0]).filter(element => {
        return '\"' + element + '\"';

    }).join(", ");

    console.log(columnName)
    //let columnName = Object.keys(data[0]).join(",");
    let columnCount = Object.keys(data[0]).length;
    let valueString = "(";
    for (j = 0; j < columnCount; j++) {
        valueString += "?";
        if (j != columnCount - 1)
            valueString += ",";
    }
    valueString += ")";
    questionArray = [];
    valueArray = [];
    let i = 0;
    data.forEach(async (notify) => {
        // console.log(notify);
        questionArray.push(valueString);
        valueArray.push(Object.values(notify));
        let today = new Date().toISOString();

        let notificationUpdate = await Knex.raw('INSERT INTO notification (' + columnName + ') VALUES ' + valueString + ' ON CONFLICT ON CONSTRAINT notification_notification_for_notification_for_id_to_user_id_by DO UPDATE SET message = EXCLUDED.message, created_at = \'' + today + '\', read_status = false, redirect_component = EXCLUDED.redirect_component, clear = false, redirect_url = EXCLUDED.redirect_url', Object.values(notify));

        // console.log(Knex.raw('INSERT INTO notification (' + columnName + ') VALUES ' + valueString + 'ON CONFLICT ON CONSTRAINT notification_notification_for_notification_for_id_to_user_id_by DO UPDATE SET message = EXCLUDED.message, created_at = \''+today+'\', read_status = false, clear = false', Object.values(notify)).toQuery())
        console.log(notificationUpdate);
        if (notificationUpdate) {
            let tokenArray = []

            tokenArray = await userAuth.query().select("token").where("user_id", notify.to_user_id);

            if (tokenArray.length > 0) {
                let notificationList = await Notification.query()
                    .select("notification_id", "message", "read_status", "sender")
                    .where("to_user_id", notify.to_user_id).where("clear", false)
                    .orderBy("read_status", "DESC")
                    .offset(0)
                    .limit(10);

                let notificationDot = await Notification.query()
                    .count("notification_id")
                    .where({ "to_user_id": notify.to_user_id, read_status: false }).where("clear", false).first();

                tokenArray.forEach(tokenData => {
                    io.sockets.to(tokenData.token).emit("notificationUpdate", {
                        notification: notificationList,
                        notificationDot: notificationDot.count
                    });
                });

            }

        }
    });

}

const updateNotificationReadStatus = async (id) => {
    let updateNotification = await Notification.query().update({
        'read_status': true
    }).where("notification_id", id);

    if (updateNotification) {
        tokenArray = await userAuth.query().select("token").where("user_id", req.user.user_id);

        if (tokenArray.length > 0) {
            let notificationList = await Notification.query()
                .select("notification_id", "message", "read_status", "sender")
                .where("to_user_id", req.user.user_id).where("clear", false)
                .orderBy("read_status", "DESC")
                .offset(0)
                .limit(10);

            let notificationDot = await Notification.query()
                .count("notification_id")
                .where({ "to_user_id": req.user.user_id, read_status: false }).where("clear", false).first();

            tokenArray.forEach(tokenData => {
                io.sockets.to(tokenData.token).emit("notificationUpdate", {
                    notification: notificationList,
                    notificationDot: notificationDot.count
                });
            });

        }
    }
}

const readStatusOnClick = async (req, res) => {
    let id = "";

    if (req.params.id) {
        id = base64decode(req.params.id);

        if (isNaN(parseInt(id)))
            throw globalCalls.badRequestError("Error! You have passed invalid-id.");
    }


    let update_notification = await Notification.query().update({
        'read_status': true
    }).where({ "to_user_id": req.user.user_id }).where((builder) => {
        if (id != "") {
            builder.where("notification_id", id);
        }
    });

    if (!update_notification) {
        throw globalCalls.badRequestError("Something went wrong");
    }

    return globalCalls.okResponse(res, update_notification, "");

}

const clearStatusOnClick = async (req, res) => {
    let id = "";

    if (req.params.id) {
        id = base64decode(req.params.id);
        if (isNaN(parseInt(id)))
            throw globalCalls.badRequestError("Error! You have passed invalid-id.");
    }


    let update_notification = await Notification.query().update({
        'clear': true
    }).where({ "to_user_id": req.user.user_id }).where((builder) => {
        if (id != "") {
            builder.where("notification_id", id);
        }
    });

    if (!update_notification) {
        throw globalCalls.badRequestError("Something went wrong");
    }

    return globalCalls.okResponse(res, update_notification, "");
}

const updateRunTimeQuotation = async (updateQuotationData, userId) => {

    let tokenArray = await userAuth.query().select("token").where("user_id", userId);

        if (tokenArray.length > 0) {
           
            tokenArray.forEach(tokenData => {
                io.sockets.to(tokenData.token).emit("quotationUpdateInbox", {
                    quotationData: updateQuotationData.quotationResult,
                    quotationStatus: updateQuotationData.updateStatus[0].status
                });
            });

        }
}
module.exports = {
    GetNotification,
    updateNotification,
    updateNotificationReadStatus,
    readStatusOnClick,
    clearStatusOnClick,
    updateRunTimeQuotation
}