const User = require("../../models/UserLogin")
const knexConfig = require('../../../config/knex');
const globalCalls = require('../../settings/functions');
const Knex = require('knex')(knexConfig.development);
const Company = require("../../models/Company")
const Project = require("../../models/Project")
const ProjectEarning = require("../../models/ProjectFinalBudget")
const moment = require("moment");
const dashboard = async (req, res) => {
  let responsedata = {}
  try {
    if (!req.query.type) {
      const fetchCompanyCount = await User.query().
        select(User.knex().raw("'Total Client' AS card_title,count(user_type) AS total")).
        where("user_type", "client").
        unionAll([Company.query().select(Company.knex().raw("'Total Company' AS card_title,count(company_id) AS total")), Project.query().select(Project.knex().raw("'Total Project' AS card_title,count(project_id) AS total")), Project.query().select(Project.knex().raw("'Total Project' AS card_title,count(project_id) AS total")), ProjectEarning.query().select(ProjectEarning.knex().raw("'Total Earning' AS card_title,sum(final_budget) AS total"))])
      //Company.query().select(Company.knex().raw('"Total Company this month" AS card_title,COUNT(CASE WHEN "created_at"::date >= \''+moment().startOf("month").format("YYYY-MM-DD")+'\' AND "created_at"::date <= \''+ moment().format("YYYY-MM-DD")+'\' THEN 1 ELSE NULL END) AS total')).whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
      //moment().startOf("month").format("YYYY-MM-DD"),
      //moment().format("YYYY-MM-DD"),
      //])]
      let Company_registered_this_month = await Company
        .query()
        .count()
        .where(function () {
          this.whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
            moment().startOf("month").format("YYYY-MM-DD"),
            moment().format("YYYY-MM-DD"),
          ]);
        })

      //  let statistics_data=await User.query().select(Knex.raw('COUNT(CASE WHEN "created_at"::date >= \''+moment().startOf("month").format("YYYY-MM-DD")+'\' AND "created_at"::date <= \''+ moment().format("YYYY-MM-DD")+'\' THEN 1 ELSE NULL END) AS '+moment().startOf("month").format('MMMM')+', COUNT(CASE WHEN "created_at"::date >= \''+moment().subtract(1, "months").startOf("month").format("YYYY-MM-DD")+'\' AND "created_at"::date <= \''+moment().subtract(1, "month").format("YYYY-MM-DD")+'\' THEN 1 ELSE NULL END) AS '+moment().subtract(1, "month").startOf("month").format('MMMM')+', COUNT(CASE WHEN "created_at"::date >= \''+moment().subtract(2, "months").startOf("month").format("YYYY-MM-DD")+'\' AND "created_at"::date <= \''+moment().subtract(2, "month").format("YYYY-MM-DD")+'\' THEN 1 ELSE NULL END) AS '+moment().subtract(2, "month").startOf("month").format('MMMM')+', COUNT(CASE WHEN "created_at"::date >= \''+moment().subtract(3, "months").startOf("month").format("YYYY-MM-DD")+'\' AND "created_at"::date <= \''+moment().subtract(3, "month").format("YYYY-MM-DD")+'\' THEN 1 ELSE NULL END) AS '+moment().subtract(3, "month").startOf("month").format('MMMM')+', COUNT(CASE WHEN "created_at"::date >= \''+moment().subtract(4, "months").startOf("month").format("YYYY-MM-DD")+'\' AND "created_at"::date <= \''+moment().subtract(4, "month").format("YYYY-MM-DD")+'\' THEN 1 ELSE NULL END) AS '+moment().subtract(4, "month").startOf("month").format('MMMM')+''))
      //  .where(function (){
      //   this.whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
      //     moment().subtract(1,"months").subtract(2,"months").subtract(3,"months").subtract(4,"months").startOf("month").format("YYYY-MM-DD"),
      //     moment().format("YYYY-MM-DD"),
      //   ]);
      //  }).where("user_type","client")

      if (fetchCompanyCount) {
        responsedata["card-data"] = fetchCompanyCount
        responsedata["company-data"] = Company_registered_this_month
        //  responsedata["statistics_data"]=statistics_data
        return globalCalls.okResponse(res, responsedata, "");
      }
      else {
        throw globalCalls.badRequestError("No data found.");
      }
    }
    else if (req.query.type == "client") {
      const fetchCompanyCount = await User.query().count().where("user_type", "client").first()
      let Client_registered_this_month = await User
        .query()
        .count()
        .where(function () {
          this.whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
            moment().startOf("month").format("YYYY-MM-DD"),
            moment().format("YYYY-MM-DD"),
          ]);
          this.where("user_type", "client")
        }).first()
      console.log(Client_registered_this_month)
      responsedata["total-client"] = fetchCompanyCount
      responsedata["this_month"] = Client_registered_this_month
      return globalCalls.okResponse(res, responsedata, "");
    }
    else if (req.query.type == "projects") {
      //let projects=await Project.query().count().select("status").groupBy("status").where("status","complete").orWhere("status","in-progress")
      let projects = await Project.query().select(Knex.raw("(Select count(status) as completedproject from project where status='complete'),(Select count(status) as inprogress from project where status='in-progress')")).whereIn("status", ["complete", 'in-progress']).
        unionAll([Project.query().select(Knex.raw('"project_id",COUNT(CASE WHEN "created_at"::date >= \'' + moment().startOf("month").format("YYYY-MM-DD") + '\' AND "created_at"::date <= \'' + moment().format("YYYY-MM-DD") + '\' THEN 1 ELSE NULL END) AS currentMonth')).whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
          moment().startOf("month").format("YYYY-MM-DD"),
          moment().format("YYYY-MM-DD"),
        ]).groupBy("project_id")
        ]).first()
        console.log(projects)
      let project_registered_this_month = await Project
        .query()
        .count()
        .where(function () {
          this.whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
            moment().startOf("month").format("YYYY-MM-DD"),
            moment().format("YYYY-MM-DD"),
          ]);

        })
      projects.project_registered_this_month = project_registered_this_month[0].count
      if (projects) {

        responsedata["project-data"] = projects
        //responsedata["project-registered_this_month"]=project_registered_this_month
        return globalCalls.okResponse(res, responsedata, "");
      }
      else {
        throw globalCalls.badRequestError("No data found.");
      }
    }
    else if (req.query.type == "earnings") {
      // let project_earning_this_month = await ProjectEarning
      // .query()
      // .select(Knex.raw("case when (SELECT Sum(final_budget) FROM project_final_budget) IS NULL THEN 0 else (SELECT Sum(final_budget) FROM project_final_budget) END as earning_this_month "))
      // .where(function () {
      //   this.whereRaw('"created_at"::date >= ? AND "created_at"::date <= ?', [
      //     moment().startOf("month").format("YYYY-MM-DD"),
      //     moment().format("YYYY-MM-DD"),
      //   ]);
      //  }).first()
      //let earning_data=await ProjectEarning.query().select(Knex.raw("case when (SELECT Sum(final_budget) FROM project_final_budget) IS NULL THEN 0 else (SELECT Sum(final_budget) FROM project_final_budget) END as earning_this_month ")).groupBy()
      let earning_data = await ProjectEarning.query().select("domain_list.domain","project.domain_id",Knex.raw("case when (SELECT Sum(final_budget) FROM project_final_budget) IS NULL THEN 0 else (SELECT Sum(final_budget) FROM project_final_budget) END as earning "),Knex.raw("case when (SELECT Sum(final_budget) FROM project_final_budget) IS NULL THEN 0 else (SELECT Sum(final_budget) FROM project_final_budget) END as earning_this_month "))
     .leftJoin("project", function () {
          this.on(
            "project.project_id",
            "=",
            "project_final_budget.project_id"
          );
        }).leftJoin("domain_list", function () {
          this.on(
            "project.domain_id",
            "=",
            "domain_list.domain_id"
          );
        })
        .groupBy("project.domain_id","domain_list.domain")
        .orderBy("earning","DESC").first();
      console.log(earning_data)
    }

    else {
      throw globalCalls.badRequestError("Error Occured");
    }
  }
  catch (error) {
    console.log(error);
    throw globalCalls.badRequestError(error.message)

  }
}
module.exports = { dashboard }