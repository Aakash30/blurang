'use strict';
const { base64encode, base64decode } = require('nodejs-base64');
const globalCalls       = require('../../settings/functions');
const CompanyPortfolio  = require('../../models/CompanyPortfolio');
const Reviews           = require('../../models/PortfolioProjectReviews');
const Ratings           = require('../../models/PortfolioProjectReviewRating');
const company           = require('../../models/Company');
const validator         = require('validator');
const bcrypt            = require('bcryptjs');
const EMAIL             = require('./../../middlewares/send-email');
const CONFIG            = require('../../settings/constants');
const knexConfig        = require('../../../config/knex');
const Knex              = require('knex')(knexConfig.development);

module.exports = {

    /**
     * function to get reviews list
     * @param {*} req 
     * @param {*} res 
    */
    Reviews : async (req, res) => {
        try{
            let msg = 'Review listed.';
            let reviews = [];
            let total_average = {approved_reviews_count:0, average:0};

            if(req.params.id){
                try{

                    let id = base64decode(req.params.id);
                    if(isNaN(parseInt(id)))
                        throw globalCalls.notFoundError("Error! You have passed invalid-id.");

                    reviews = await Reviews.query().select("review_id", "reviews", Knex.raw("(SELECT CASE WHEN starsAvg IS NULL THEN 0 ELSE starsAvg END FROM (SELECT ROUND(AVG(stars),2) AS starsAvg FROM review_rating WHERE review_id = reviews.review_id) AS averageStars) AS rating_avg"))
                    .withGraphFetched("[portfolio, ratings]").modifyGraph("portfolio", builder =>{
                        builder.select("project_name", "one_line_description", "project_logo", "timeline", "budget", "first_name", "last_name", "about_the_project")
                    }).modifyGraph('ratings', builder => {
                        builder.select('review_rating.review_rating_id', 'review_rating.stars', 'ratingPoint.rating_title')
                        .joinRelated('[ratingPoint]');
                    })
                    .where("is_valid", true)
                    .where("company_id", req.user.default_company)
                    .where('review_id', id)
                    .first();
    
                    if(!reviews){
                        throw globalCalls.badRequestError('Invalid request!');
                    }
                    msg= 'Portfolio details';

                    return globalCalls.okResponse(res, {reviews:reviews});
                } catch (error){
                    console.log(error);
                    throw globalCalls.badRequestError(error.message);
                }
             
               

            }else{

                let page    = (req.query.page) ? req.query.page : 1;
                let limit   = req.query.limit ? req.query.limit : CONFIG.per_page;
                let offset  = req.query.offset ? req.query.offset : limit * (page - 1);
                let reviewsCount;
                if(req.query.type == 'pending'){
                    
                    reviews = await Reviews.query().select("review_id", "reviews", "review_verification_link", "link_expire", "feedback_received")
                    .withGraphFetched('[portfolio, ratings]')
                    .modifyGraph("portfolio", builder =>{
                        builder.select("portfolio_id", "project_name", "project_logo", "one_line_description", "timeline", "budget", "first_name", "last_name", "client_email")
                    })
                    .modifyGraph('ratings', builder => {
                        builder.select('review_rating.review_rating_id', 'review_rating.stars', 'ratingPoint.rating_title')
                        .joinRelated('[ratingPoint]');
                    })
                    .where('feedback_received', false)
                    .where("is_valid", true)
                    .where("company_id", req.user.default_company)
                    .limit(limit)
                    .offset(offset)
                    .orderBy('review_id', 'desc');

                    reviewsCount = await Reviews.query().count("review_id").where('feedback_received', false)
                    .where("is_valid", true).where("company_id", req.user.default_company).first();
                }else{

                    /**
                     * ROUND(AVG(CASE WHEN stars IS NULL THEN 0 ELSE stars END),2)
                     */

                    if(req.query.filterBy == 'highest' || req.query.filterBy == 'lowest'){

                        let order = (req.query.filterBy === 'highest') ? 'desc' : 'asc';
                        try{
                            reviews = await Reviews.query()
                        .select("review_id", "reviews", "review_verification_link", "link_expire", "feedback_received", Knex.raw("(SELECT CASE WHEN starsAvg IS NULL THEN 0 ELSE starsAvg END FROM (SELECT ROUND(AVG(stars),2) AS starsAvg FROM review_rating WHERE review_id = reviews.review_id) AS averageStars) AS rating_avg"))
                        .withGraphFetched('[portfolio, ratings]')
                        .modifyGraph("portfolio", builder =>{
                            builder.select("portfolio_id", "project_name", "project_logo", "one_line_description", "timeline", "budget", "first_name", "last_name", "client_email")
                        })
                        .modifyGraph('ratings', builder => {
                            builder.select('review_rating.review_rating_id', 'review_rating.stars', 'ratingPoint.rating_title')
                            .joinRelated('[ratingPoint]')
                            .orderBy('review_rating.stars', 'desc')
                        }).where("is_valid", true)
                        .where("company_id", req.user.default_company)
                        .orderBy('rating_avg', order)
                        .limit(limit)
                        .offset(offset);
                        } catch (error){
                            console.log(error);
                            throw globalCalls.badRequestError(error.message)
                        }
                    } else {
                      
                        try{
                       reviews = await Reviews.query().select("review_id", "reviews", "review_verification_link", "link_expire", "feedback_received", Knex.raw("(SELECT CASE WHEN starsAvg IS NULL THEN 0 ELSE starsAvg END FROM (SELECT ROUND(AVG(stars),2) AS starsAvg FROM review_rating WHERE review_id = reviews.review_id) AS averageStars) AS rating_avg"))
                            .withGraphFetched('[portfolio, ratings]')
                            .modifyGraph("portfolio", builder =>{
                                builder.select("portfolio_id", "project_name", "project_logo", "one_line_description", "timeline", "budget", "first_name", "last_name", "client_email")
                            })
                            .modifyGraph('ratings', builder => {
                                builder.select('review_rating.review_rating_id', 'review_rating.stars', 'ratingPoint.rating_title')
                                .joinRelated('[ratingPoint]');
                            })
                            .where("company_id", req.user.default_company).where("is_valid", true)
                            .limit(limit)
                            .offset(offset)
                            .orderBy('review_id', 'desc');
                        } catch (error){
                            console.log(error);
                            throw globalCalls.badRequestError(error.message)
                        }
                        
                    }

                    reviewsCount = await Reviews.query().count("review_id").where("company_id", req.user.default_company)
                    .where("is_valid", true).first();
                }

              
                let average_count = await Reviews.query()
                    .select(Knex.raw('(SELECT COUNT(*) FROM reviews WHERE feedback_received = true and company_id = '+req.user.default_company+') AS reviews_count'))
                    .joinRelated('ratings')
                    .avg('stars')
                    .where('feedback_received', true).where("company_id", req.user.default_company).where("is_valid", true)
                    .groupBy('reviews.company_id')
                    .first();
                    if(average_count){
                        total_average.approved_reviews_count = average_count.reviews_count;
                        total_average.average = average_count.avg;
                    }
                return globalCalls.okResponse(res, {reviews:reviews, total_average:total_average, total: reviewsCount.count}, msg);
            }

          

        }catch(e){
            console.log("Error in ReviewsController.Reviews", e);
            throw globalCalls.badRequestError(e.message);
        }
    },

    /**
     * function to withdraw review
     * @param {*} req 
     * @param {*} res 
    */
    WithdrawReview : async (req, res) => {
        try{
            if(!req.body.review_id){
                throw globalCalls.badRequestError("Invalid request!");
            }

            let review_id = req.body.review_id;
            let review = await Reviews.query()
            .withGraphFetched('[portfolio]')
            .where('review_id', review_id)
            .first();
            
            if(!review){
                throw globalCalls.badRequestError("Invalid request!");
            }

            if(review.feedback_received === true){
                throw globalCalls.badRequestError("Feedback recived by client for this review.");
            }

            let remove = await Reviews.query().update({is_valid: false}).where('review_id', review_id);
            if(remove){
                return globalCalls.okResponse(res, {}, "Review withdraw successfully.");
            }
            throw globalCalls.badRequestError("Something went wrong!");
        
        }catch(e){
            console.log('Error in ReviewsController.WithdrawReview', e);
            throw globalCalls.badRequestError(e.message);
        }
    },

    /**
     * function to resend feedback request
     * @param {*} req 
     * @param {*} res 
    */
    ResendFeedbackRequest : async (req, res) => {
        try{
            if(!req.body.review_id){
                throw globalCalls.badRequestError("Invalid request!");
            }

           
      
            let review_id = req.body.review_id;
            let review = await Reviews.query()
            .withGraphFetched('[portfolio]')
            .where('review_id', review_id)
            .first();

            if(!review){
                throw globalCalls.badRequestError("Invalid request!");
            }

            if(review.feedback_received === true){
                throw globalCalls.badRequestError("Feedback recived by client for this review.");
            }
            let data = {};
            let expire_date;
            let today = new Date();
            expire_date = new Date();
            expire_date.setDate(today.getDate()+7);
            data.link_expire = expire_date;
            
            let verify_token  = globalCalls.generateToken(15) + '-' + data.portfolio_id;
            verify_token      = await bcrypt.hash(verify_token, 10);
            verify_token      = verify_token.replace(/\.+$/, "");
            data.review_verification_link = verify_token;

            /************ SEND GET FEEDBACK EMAIL TO CLIENT ************/
            let link        = CONFIG.clientUrl + 'client-review/' + verify_token;            
         
            let project = req.body.project_name;
            let companyData = await company.query().select("company_name").where("company_id", req.user.default_company).first();
            let email = req.body.email;

            let emailData = { link: link, project: project, company: companyData.company_name };
                  
            /**
             * send email functionality here
             */
      
            EMAIL.sendEmail(
                email,
              "Give Feedback for Your Project - "+project,
              emailData,
              "get_client_Feedback.pug"
            );

            let updated = await Reviews.query().patchAndFetchById(review_id, data);
            if(updated){
                return globalCalls.okResponse(res, {updated}, 'Feedback send successfully to client email.');
            }
            throw globalCalls.badRequestError("Something went wrong!");

        }catch(e){
            console.log('Error in ReviewsController.ResendFeedbackRequest', e);
            throw globalCalls.badRequestError(e.message);
        }
    },
}