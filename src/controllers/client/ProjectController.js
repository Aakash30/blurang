"use strict";

const { base64decode, base64encode } = require("nodejs-base64");
const globalCalls = require("../../settings/functions");
const globalConst = require("../../settings/constants");
const { transaction, ref } = require("objection");

const project = require("../../models/Project");
const domain = require("../../models/Domain");
const service = require("../../models/Service");
const projectQuestion = require("../../models/ClientQuestion");

const company = require("../../models/Company");
const shortlistedCompanies = require("../../models/ShortlistedCompanies");
const cart = require("../../models/Cart");
const projectdocx = require("../../models/ProjectDocx");
const CompanyPortfolio = require("../../models/CompanyPortfolio");
const projectSkillMapping = require("../../models/ProjectSkills");
const ratingPointers = require("../../models/RatingPoints");
const skills = require("../../models/Skills");

const projectRecommendedCompanies = require("../../models/ProjectRecommendedCompanies");
const ChatInbox = require("../../models/ChatInbox");
const portfolioReviewRating = require("../../models/PortfolioProjectReviewRating");
const review = require("../../models/PortfolioProjectReviews");
const projectFinalBudget = require("../../models/ProjectFinalBudget");

const companyMap = require('../../models/CompanyMapping');

const NotificationUpdate = require('../notification/notificationcontroller').updateNotification;

const knexConfig = require("../../../config/knex");
const Knex = require("knex")(knexConfig.development);
const moment = require("moment");
const CompanyMapping = require("../../models/CompanyMapping");

/**
 * Post project - called when api is called
 * @param {*} req
 * @param {*} res
 */
const postProjectApi = async (req, res) => {
  try {

    /**
     * get the posted data
     */
    let data = req.body;
    data.user_id = req.user.user_id;

    let message = "";

    /**check if project is added or updated  */
    if (data.project_id == null) {
      message = "Project has been posted successfully.";
    } else {
      message = "Project has been updated successfully.";
    }

    let responseData = await postProject(data);

    /** check for error message and through accordingly */
    if (responseData.err != "") {
      throw globalCalls.badRequestError(responseData.err);
    } else {
      return globalCalls.okResponse(res, responseData.projectPost, message);
    }
  } catch (error) {
    throw globalCalls.badRequestError(error.message);
  }
};

/**
 * post project
 * @param {data stores the post project data} data
 */
const postProject = async (data) => {
  let err = "";
  
  try {
    /** check for empty data fields and validations */
    if (!data.domain_id && !data.category) {
      err = "Please choose a domain.";
    }

    if (!data.project_name) {
      err = "Please enter a title for your project.";
    }

    if (!data.one_line_description) {
      err = "Please enter One Liner for the Project.";
    }

    if (!data.services || data.services.length == 0) {
      err = "Please enter Services.";
    }

    if (!data.about_the_project) {
      err = "Please enter about your project.";
    }

    if (!data.budget) {
      err = "Please Select Budget.";
    }

    if (data.project_id == null) {
      delete data.project_id;
    }

    if (data.services.length != 0) {
      let service = data.services;
      let serviceArray = [];
      service.forEach((innerdata) => {
        if (innerdata.project_service_id == null) {
          delete innerdata.project_service_id;
        }
        serviceArray.push(innerdata);
      });
      data.services = serviceArray;
    }

    
    if (data.skills && data.skills.length != 0) {
      let skills = data.skills;
      let skillsArray = [];
      skills.forEach((innerdata) => {
        if (innerdata.project_skill_id == null) {
          delete innerdata.project_skill_id;
        }
        skillsArray.push(innerdata);
      });
      data.skills = skillsArray;
    }
    if (!data.domain_experience_in_months || data.domain_experience_in_months == "") {
      data.domain_experience_in_months = 0;
    }

    if (!data.company_expected_experience || data.company_expected_experience == "") {
      data.company_expected_experience = 0;
    }
    if (data.projectAnswer && data.projectAnswer.length != 0) {
      let projectAnswer = data.projectAnswer;
      let projectAnswerArray = [];
      projectAnswer.forEach((innerdata) => {
        if (innerdata.answer_id == null) {
          delete innerdata.answer_id;
        }
        projectAnswerArray.push(innerdata);
      });
      data.projectAnswer = projectAnswerArray;
    }

    /** If no error execute the query to perfolrm action else return error */
    if (err == "") {
      let projectPost = await transaction(project.knex(), (trx) => {
        return project.query(trx).upsertGraph(data, {
          relate: true,
          unrelate: true,
        });
      });

      if (!projectPost) {
        err = "Something went wrong.";
      }
      return { err, projectPost };
    } else {
      return { err };
    }
  } catch (error) {
    err = error.message;
    return { err };
  }
};

/**
 * fetches project data for client to edit
 * @param {*} req
 * @param {*} res
 */
const fetchProjectDataForClientToEdit = async (req, res) => {
  try {

    /** fetch domain list */
    let domainList = await domain
      .query()
      .select("domain_id", "domain", "status")
      .where((builder) => {
        if (!req.params.id) {
          builder.where("status", "active");
        }
      });

      /** fetch service list */
    let serviceList = await service
      .query()
      .select("service_id", "service_name", "status")
      .withGraphFetched("skills")
      .modifyGraph("skills", (builder) => {
        builder.select("skill_id", "skills", "status").where((builder) => {
          if (!req.params.id) {
            builder.where("status", "active");
          }
        });
      })
      .where((builder) => {
        if (!req.params.id) {
          builder.where("status", "active");
        }
      });

      /** fetch question list */
    let questionList = await projectQuestion
      .query()
      .select("question_id", "question", "type");
    let responseData = {
      domainList,
      serviceList,
      questionList,
    };

    /** if form is in edit mode fetch data for related project */
    if (req.params.id) {
      let project_id = base64decode(req.params.id);
      
      if(isNaN(parseInt(project_id)))
	      throw globalCalls.notFoundError("Error! You have passed invalid-id.");
      
      let projectData = await project
        .query()
        .select(
          "project_id",
          "project_name",
          "one_line_description",
          "about_the_project",
          "budget",
          "payment_type",
          "location",
          "expected_timeline",
          "ideal_team_size",
          "domain_experience_in_months",
          "company_expected_experience",
          "domain_id"
        )
        .withGraphFetched("[services, skills, projectAnswer]")
        .modifyGraph("services", (builder) => {
          builder.select("service_id", "project_service_id");
        })
        .modifyGraph("skills", (builder) => {
          builder.select("skill_id", "project_skill_id");
        })
        .modifyGraph("projectAnswer", (builder) => {
          builder.select("answer_id", "question_id", "answer");
        })
        .where({ project_id: project_id, user_id: req.user.user_id })
        .first();
     
        /** throw error if project not found */
      if (!projectData) {
        throw globalCalls.notFoundError("Project not found or invalid link");
      }
      responseData["projectData"] = projectData;
    }
    /** else send response */
    return globalCalls.okResponse(res, responseData, "");
  } catch (error) {
   
    if (error.statusCode == 404) {
      throw globalCalls.notFoundError(error.message);
    }
    throw globalCalls.badRequestError(error.message);
  }
};

const getRecommendation = async (req, res) => {
  if (!req.params.id) {
    throw globalCalls.badRequestError("Invalid Requests.");
  }

  let project_id = base64decode(req.params.id);
  if(isNaN(parseInt(project_id)))
    throw globalCalls.notFoundError("Error! You have passed invalid-id.");
  
  let projectData = [];

  //check all Recommendation are rejected start
  // let checkRecommendation = await shortlistedCompanies
  // .query()
  // .count()
  // .where("project_id", project_id)
  // .whereNotIn('status', ['reject','withdraw'])
  // .first();
  // if(checkRecommendation.count!=0) {
  //   throw globalCalls.badRequestError("Error! Please reject all last recommended companies than you will get new recommendation.");
  // }
  //check all Recommendation are rejected start

  let matchingCriteria = ["services", "budget"];
  try {
    let page = req.query.page ? req.query.page : 1;
    let limit = req.query.limit ? req.query.limit : globalConst.per_page;
    let offset = req.query.offset ? req.query.offset : limit * (page - 1);
    
    projectData = await project
      .query()
      .select(
        "project_id",
        "project_name",
        "one_line_description",
        "about_the_project",
        "budget",
        "payment_type",
        "location",
        "expected_timeline",
        "ideal_team_size",
        "domain_experience_in_months",
        "company_expected_experience",
        "domain_id"
      )
      .withGraphFetched("[servicesLabels, skillLabel, projectAnswer, domain]")
      .modifyGraph("domain", (builder) => {
        builder.select("domain");
      })
      .modifyGraph("servicesLabels", (builder) => {
        builder.select("service_list.service_id", "service_name");
      })
      .modifyGraph("skillLabel", (builder) => {
        builder.select("skillset_list.skill_id", "skills");
      })
      .modifyGraph("projectAnswer", (builder) => {
        builder
          .select("question", "answer")
          .innerJoinRelated("question")
          .orderBy("type", "ASC");
      })
      .where("project_id", project_id)
      .first();

    let companyData = await company
      .query()
      .select(
        "company_id",
        "company_name",
        "logo",
        "incorporation_date",
        "fixed_budget",
        "hourly_budget",
        "viewer_count",
        "no_of_project_completed",
        Knex.raw(
          "(SELECT Round (AVG ((SELECT AVG(stars) AS starsAvg FROM review_rating WHERE review_id = reviews.review_id)),2)  from reviews where company_id = company.company_id and feedback_received = true group by company_id) as overall_rating"
        )
      )
      .withGraphFetched("[serviceLabels, domain, address]")
      .modifyGraph("domain", (builder) => {
        builder.select("domain");
      })
      .modifyGraph("serviceLabels", (builder) => {
        builder.select("service_name");
      })
      .modifyGraph("address", (builder) => {
        builder.select("location");
      })
      .where({
          "company.domain_id": projectData.domain_id,
          "company.status": "approved"
      })
      // .where({
      //   "company.domain_id": projectData.domain_id,
      //   "company.fixed_budget": projectData.budget,
      //   "company.status": "approved"
      // });
      // .whereNotIn("company.company_id", (builder) => {
      //   builder
      //     .select("company_id")
      //     .from("shortlisted_companies")
      //     .where("project_id", project_id);
      // });
      .orderByRaw("overall_rating DESC NULLS LAST")
      .offset(offset)
      .limit(limit);

    let totalCount = await company
      .query()
      .count()
      .where({
        "company.domain_id": projectData.domain_id,
        "company.status": "approved"
      })
      // .where({
      //   "company.domain_id": projectData.domain_id,
      //   "company.fixed_budget": projectData.budget,
      //   "company.status": "approved"
      // });
      // .whereNotIn("company.company_id", (builder) => {
      //   builder
      //     .select("company_id")
      //     .from("shortlisted_companies")
      //     .where("project_id", project_id);
      // });
      .first();

    // save recommended company data start
    let checkinrecommended;
    // let is_saved_recommended_data, let recommended_data = [];
    if (companyData.length > 0) {
      for (let i = 0; i < companyData.length; i++) {
        checkinrecommended = await projectRecommendedCompanies
          .query()
          .count()
          .where({
            company_id: companyData[i].company_id,
            project_id: project_id,
          })
          .first();
        if (checkinrecommended.count == 0) {
          await projectRecommendedCompanies.query().upsertGraph({
            company_id: companyData[i].company_id,
            project_id: project_id,
          });
        }
      }
    }
    // save recommended company data end

    let responseData = {
      projectData,
      companyData,
      total: totalCount.count,
    };

    return globalCalls.okResponse(res, responseData, "");
  } catch (error) {
    console.log(error);
    throw globalCalls.badRequestError(error.message);
  }
};

const getMoreRecommendation = async (req, res) => {
  if (!req.params.id) {
    throw globalCalls.badRequestError("Invalid Requests.");
  }

  let project_id = base64decode(req.params.id);
  if(isNaN(parseInt(project_id)))
    throw globalCalls.notFoundError("Error! You have passed invalid-id.");

  //check all Recommendation are rejected start
  let checkRecommendation = await shortlistedCompanies
    .query()
    .count()
    .where("project_id", project_id)
    .whereNotIn("status", ["reject", "withdraw"])
    .first();
  if (checkRecommendation.count != 0) {
    throw globalCalls.badRequestError(
      "Error! Please reject all last recommended companies than you will get new recommendation."
    );
  }
  //check all Recommendation are rejected start

  let matchingCriteria = ["services", "budget"];
  try {
    let projectData = await project
      .query()
      .select(
        "project_id",
        "project_name",
        "one_line_description",
        "about_the_project",
        "budget",
        "payment_type",
        "location",
        "expected_timeline",
        "ideal_team_size",
        "domain_experience_in_months",
        "company_expected_experience",
        "domain_id"
      )
      .withGraphFetched("[servicesLabels, skillLabel, projectAnswer, domain]")
      .modifyGraph("domain", (builder) => {
        builder.select("domain");
      })
      .modifyGraph("servicesLabels", (builder) => {
        builder.select("service_list.service_id", "service_name");
      })
      .modifyGraph("skillLabel", (builder) => {
        builder.select("skillset_list.skill_id", "skills");
      })
      .modifyGraph("projectAnswer", (builder) => {
        builder
          .select("question", "answer")
          .innerJoinRelated("question")
          .orderBy("type", "ASC");
      })
      .where("project_id", project_id)
      .first();

    let page = req.query.page ? req.query.page : 1;
    let limit = req.query.limit ? req.query.limit : globalConst.per_page;
    let offset = req.query.offset ? req.query.offset : limit * (page - 1);

    let companyData = await company
      .query()
      .select(
        "company_id",
        "company_name",
        "logo",
        "incorporation_date",
        "fixed_budget",
        "hourly_budget",
        "viewer_count",
        "no_of_project_completed",
        Knex.raw(
          "(SELECT Round (AVG ((SELECT AVG(stars) AS starsAvg FROM review_rating WHERE review_id = reviews.review_id)),2)  from reviews where company_id = company.company_id and feedback_received = true group by company_id) as overall_rating"
        )
      )
      .withGraphFetched("[serviceLabels, domain, address]")
      // .modifyGraph("services", builder =>{
      //   builder.select("company_service_mapping.service_id", "service_name")
      //   .innerJoinRelated("service")
      //   .whereIn("company_service_mapping.service_id", projectServices);
      //   // console.log(builder.toSql())
      // })
      .modifyGraph("domain", (builder) => {
        builder.select("domain");
      })
      .modifyGraph("serviceLabels", (builder) => {
        builder.select("service_name");
      })
      .modifyGraph("address", (builder) => {
        builder.select("location");
      })
      // .where("company.domain_id", projectData.domain_id)
      .where({
        "company.domain_id": projectData.domain_id,
        "company.fixed_budget": projectData.budget,
        "company.status": "approved"
      })
      .whereNotIn("company.company_id", (builder) => {
        builder
          .select("company_id")
          .from("shortlisted_companies")
          .where("project_id", project_id);
      })
      .orderByRaw("overall_rating DESC NULLS LAST")
      .offset(offset)
      .limit(limit);

    let totalCount = await company
      .query()
      .count()
      .where("company.domain_id", projectData.domain_id)
      .first();

    // save recommended company data start
    let checkinrecommended;
    // let is_saved_recommended_data, let recommended_data = [];
    if (companyData.length > 0) {
      for (let i = 0; i < companyData.length; i++) {
        checkinrecommended = await projectRecommendedCompanies
          .query()
          .count()
          .where({
            company_id: companyData[i].company_id,
            project_id: project_id,
          })
          .first();
        if (checkinrecommended.count == 0) {
          await projectRecommendedCompanies.query().upsertGraph({
            company_id: companyData[i].company_id,
            project_id: project_id,
          });
        }
      }
    }
    // save recommended company data end

    let responseData = {
      projectData,
      companyData,
      total: totalCount.count,
    };

    return globalCalls.okResponse(res, responseData, "");
  } catch (error) {
    console.log(error);
    throw globalCalls.badRequestError(error.message);
  }
};


const getProjectListForClient = async (req, res) => {
  try {
    let page = req.query.page ? req.query.page : 1;
    let limit = req.query.limit ? req.query.limit : globalConst.per_page;
    let offset = req.query.offset ? req.query.offset : limit * (page - 1);

    let projectData = await project
      .query()
      .select(
        "project_id",
        "project_name",
        "one_line_description",
        "about_the_project",
        "budget",
        "payment_type",
        "location",
        "expected_timeline",
        "ideal_team_size",
        "domain_experience_in_months",
        "company_expected_experience",
        "created_at",
        "archived_by_type",
        Knex.raw(
         "(SELECT (CASE WHEN count(company_id) >0 THEN false ELSE true END) AS is_able from shortlisted_companies where shortlisted_companies.project_id=project.project_id and status!='rejected' and status != 'withdraw')"
        )
      )
      .withGraphFetched(
        "[servicesLabels, domain, shortlisted_companies, hired_company]"
      )
      .modifyGraph("servicesLabels", (builder) => {
        builder.select("service_name");
      })
      .modifyGraph("domain", (builder) => {
        builder.select("domain");
      })
      .modifyGraph("shortlisted_companies", (builder) => {
        builder
          .count("company_id")
          .where({ status: "quotation_request" })
          .groupBy("shortlisted_companies.project_id");
      })
      .modifyGraph("hired_company", (builder) => {
        builder
          .withGraphFetched("address", { maxBatchSize: 1 })
          .modifyGraph("address", (builder) => {
            builder.select("location").limit(1);
          });
        builder.select("company_name");
      })
      .where((builder) => {
        builder.where("user_id", req.user.user_id);
        if (req.query.type) {
          builder.where("status", req.query.type);
        }
      })
      .orderBy("project_id", "DESC")
      .offset(offset)
      .limit(limit);

    let totalCount = await project
      .query()
      .count()
      .where((builder) => {
        builder.where("user_id", req.user.user_id);
        if (req.query.type) {
          builder.where("status", req.query.type);
        }
      })
      .first();

    if (!projectData) {
      throw globalCalls.badRequestError("No Project found.");
    } else {
      let responseData = {
        projectData,
        total: totalCount.count,
      };

      return globalCalls.okResponse(res, responseData, "");
    }
  } catch (error) {
    throw globalCalls.badRequestError(error.message);
  }
};

const ProjectDetailsClient = async (req, res) => {
  if (req.params.id) {
    var project_id = base64decode(req.params.id);
    if(isNaN(parseInt(project_id)))
      throw globalCalls.notFoundError("Error! You have passed invalid-id.");

    let projectData = await project
      .query()
      .select(
        "project_id",
        "project_name",
        "one_line_description",
        "about_the_project",
        "budget",
        "payment_type",
        "location",
        "expected_timeline",
        "ideal_team_size",
        "domain_experience_in_months",
        "company_expected_experience",
        "domain_id",
        Knex.raw(
          "(SELECT (CASE WHEN count(company_id) >0 THEN false ELSE true END) AS is_able from shortlisted_companies where shortlisted_companies.project_id=project.project_id  and status!='rejected' and status != 'withdraw')"
        )
      )
      .withGraphFetched("[servicesLabels, skillLabel, projectAnswer, domain]")
      .modifyGraph("domain", (builder) => {
        builder.select("domain");
      })
      .modifyGraph("servicesLabels", (builder) => {
        builder.select("service_list.service_id", "service_name");
      })
      .modifyGraph("skillLabel", (builder) => {
        builder.select("skillset_list.skill_id", "skills");
      })
      .modifyGraph("projectAnswer", (builder) => {
        builder
          .select("question", "answer")
          .innerJoinRelated("question")
          .orderBy("type", "ASC");
      })
      .where({
        user_id: req.user.user_id,
        project_id: project_id,
      })
      .first();

    if (!projectData) {
      throw globalCalls.badRequestError("Project details not found.");
    } else {
      let responseData = {
        projectData,
      };

      return globalCalls.okResponse(res, responseData, "");
    }
  } else {
  }
};

const archiveProject = async (req, res) => {
  if (req.params.id) {
    var data = {};
    var project_id = base64decode(req.params.id);
    if(isNaN(parseInt(project_id)))
      throw globalCalls.badRequestError("Error! You have passed invalid-id.");

    let projectData = await project.query().select("project_id").where({
      user_id: req.user.user_id,
      project_id: project_id,
    });
    if (!projectData) {
      throw globalCalls.badRequestError("project not found.");
    }

    data.project_id = project_id;
    data.status = "archieve";
    data.archived_by_type = req.user.user_type;
    data.archived_by = req.user.user_id;

    let verified = await project.query().upsertGraph(data).returning("*");
    if (!verified) {
      throw globalCalls.badRequestError("something went wrong.");
    } else {
      return globalCalls.okResponse(
        res,
        verified,
        "Project archieve successfully."
      );
    }
  } else {
  }
};

/**
 * sends quotation request from client to company
 * @param {*} req
 * @param {*} res
 */

const sendQuotationRequest = async (req, res) => {
  let data = req.body;
  let project_docx = [];
  try {
    if (!data.project_id) {
      throw globalCalls.badRequestError("Please pass data with project id.");
    }

  let shortlistedCompanyId = [];
  let tokenArray = [];
    if (!data.shortlisted_companies || data.shortlisted_companies == "") {
      throw globalCalls.badRequestError(
        "Please shortlist atleast one company."
      );
    } else {
      data.shortlisted_companies = JSON.parse(data.shortlisted_companies);

      if (data.shortlisted_companies.length > 0) {
        for (let i = 0; i < data.shortlisted_companies.length; i++) {
          data.shortlisted_companies[i].client_id = req.user.user_id;
          shortlistedCompanyId.push(data.shortlisted_companies[i].company_id);
        }
      }

    }

    
    let fileTypeArray = JSON.parse(data.file_types);
    var i = 0;
    if (req.files && req.files.project_docx_files) {
      req.files.project_docx_files.map((file) => {
        project_docx.push({
          docx_url: file.location,
          file_type: fileTypeArray[i],
        });
        i++;
      });

      data.project_docx = project_docx;
    }

    delete data.file_types;
    delete data.project_docx_files;

    data.user_id = req.user.user_id;
    data.status = "requested-quotation";

    let shortlisted = await transaction(project.knex(), async (trx) => {
      let project_quotation = await project.query(trx).upsertGraph(data, {
          relate: true,
          unrelate: true,
        })
        .returning("*");

        if(project_quotation){
          let delete_in_cart = await cart.query().delete().where({
            user_id: req.user.user_id,
            project_id: data.project_id,
          });

          if(delete_in_cart){
            
          }

        }
        return project_quotation;
    });
    if (!shortlisted) {
      throw globalCalls.badRequestError("something went wrong.");
    } else {
      let messageNotify = Object.assign({}, globalConst.NOTIFICATION_PAYLOAD["QUOTATION_REQUEST"]);
      
      let bodyMessage = messageNotify.body.replace("[CLIENT]", "<b>"+req.user.first_name+" "+req.user.last_name+"</b>");
      messageNotify.body = bodyMessage;
       let data = [];
       
       let shorlistedArray = shortlisted.shortlisted_companies;
       for(const selected of shorlistedArray) {
        let getUser = await companyMap.query().select("user_id").where("company_id", selected.company_id).first();
        data.push({
          "message": messageNotify.body,
          "notification_for": "QUOTATION_ACTION",
          "notification_for_id": selected.shortlisted_id,
          "to_user_id": getUser.user_id,
          "by_user_id": req.user.user_id,
          "redirect_url": "/quotation/details/"+base64encode(selected.shortlisted_id)+"?routeName=request",
          "redirect_component": "QUOTATION",
          "created_at": new Date().toISOString()
        });
       };
       
       if(data.length > 0){
        await NotificationUpdate(data)
       }
            

     
      return globalCalls.okResponse(
        res,
        shortlisted,
        "Companies shortlisted successfully."
      );
    }
  } catch (error) {
    throw globalCalls.badRequestError(error.message);
  }
};

const AddtoCart = async (req, res) => {
  let data = req.body;
  // console.log(req.body);
  if (!data.project_id) {
    throw globalCalls.badRequestError("Please provide project id.");
  }
  if (!data.company_id) {
    throw globalCalls.badRequestError("Please choose any compay.");
  }

  try {
    console.log(req.user.user_id)
    //check other project exist in cart validation start
    let checkProjectValidation = await cart
      .query()
      .count()
      .where(function () {
        this.where("user_id", req.user.user_id).whereNot(
          "project_id",
          data.project_id
        );
      })
      .first();
    if (checkProjectValidation.count != 0) {
      throw globalCalls.badRequestError(
        "Error! Companies already added for other project please take action for that first."
      );
    }
    //check other project exist in cart validation end

    //check all company already exist validation start
    let checkvalidation = await cart
      .query()
      .count()
      .where({ project_id: data.project_id, company_id: data.company_id })
      .first();
    if (checkvalidation.count != 0) {
      throw globalCalls.badRequestError(
        "Error! Company already added in cart."
      );
    }
    //check all company already exist validation end

    data.user_id = req.user.user_id;
    let added_in_cart = await cart.query().upsertGraph(data).returning("*");
    if (!added_in_cart) {
      throw globalCalls.badRequestError("something went wrong.");
    } else {
      let companyData = await cart
        .query()
        .select("cart_id", "company_id", "project_id")
        .withGraphFetched("[company_details]")
        .modifyGraph("company_details", (builder) => {
          builder
            .withGraphFetched("address", { maxBatchSize: 1 })
            .modifyGraph("address", (builder) => {
              builder.select("location").limit(1);
            });
          builder.select(
            "company_id",
            "company_name",
            "logo",
            "director_name",
            "company_email_id",
            "company_website",
            "contact_number",
            Knex.raw(
              "(SELECT Round (AVG ((SELECT AVG(stars) AS starsAvg FROM review_rating WHERE review_id = reviews.review_id)),2)  from reviews where company_id = company.company_id and feedback_received = true group by company_id) as overall_rating"
            )
          );
        })
        .where({ project_id: data.project_id, company_id: data.company_id })
        .first();

      let responseData = {
        companyData,
      };

      return globalCalls.okResponse(
        res,
        responseData,
        "Successfully added in cart."
      );
    }
  } catch (error) {
    throw globalCalls.badRequestError(error.message);
  }
};

const RemovetoCart = async (req, res) => {
 
  let cart_id = base64decode(req.params.id);
  if(isNaN(parseInt(cart_id)))
    throw globalCalls.badRequestError("Error! You have passed invalid-id.");

  if (!cart_id) {
    throw globalCalls.badRequestError("Please provide cart id.");
  }

  try {
    //check all company already exist validation start
    let checkvalidation = await cart
      .query()
      .count()
      .where({ cart_id: cart_id })
      .first();
    if (checkvalidation.count == 0) {
      throw globalCalls.badRequestError("Error! Invalid cart ID.");
    }
    //check all company already exist validation end

    let added_in_cart = await cart.query().delete().where({
      user_id: req.user.user_id,
      cart_id: cart_id,
    });

    if (!added_in_cart) {
      throw globalCalls.badRequestError("something went wrong.");
    } else {
      return globalCalls.okResponse(
        res,
        added_in_cart,
        "Company remove successfully."
      );
    }
  } catch (error) {
    throw globalCalls.badRequestError(error.message);
  }
};

const getCartCompanies = async (req, res) => {
  try {
    let companyData = await cart
      .query()
      .select("cart_id", "company_id", "project_id")
      .withGraphFetched("[company_details]")
      .modifyGraph("company_details", (builder) => {
        builder
          .withGraphFetched("address", { maxBatchSize: 1 })
          .modifyGraph("address", (builder) => {
            builder.select("location").limit(1);
          });
        builder.select(
          "company_id",
          "company_name",
          "logo",
          "director_name",
          "company_email_id",
          "company_website",
          "contact_number",
          Knex.raw(
            "(SELECT Round (AVG ((SELECT AVG(stars) AS starsAvg FROM review_rating WHERE review_id = reviews.review_id)),2)  from reviews where company_id = company.company_id and feedback_received = true group by company_id) as overall_rating"
          )
        );
      })
      .where("user_id", req.user.user_id);

    //no data validation start
    let projectData = [];
    let countCompany = await cart
      .query()
      .count()
      .where("user_id", req.user.user_id)
      .first();

    // get project data start
    if (countCompany.count != 0) {
      projectData = await project
        .query()
        .select("project_id", "project_name", "one_line_description", "budget")
        .where({
          user_id: req.user.user_id,
          project_id: companyData[0].project_id,
        });
    }
    // get project data end

    let responseData = {
      companyData,
      projectData,
      total: countCompany.count,
    };

    return globalCalls.okResponse(res, responseData, "");

  } catch (error) {
    throw globalCalls.badRequestError(error.message);
  }
};

const deleteProjectDocx = async (req, res) => {
  if (req.params.id) {
    var data = {};
    var project_docx_id = base64decode(req.params.id);
    if(isNaN(parseInt(project_docx_id)))
      throw globalCalls.badRequestError("Error! You have passed invalid-id.");

    data.status = "archive";
    data.project_docx_id = project_docx_id;

    let deleted = await projectdocx.query().upsertGraph(data).returning("*");
    if (!deleted) {
      throw globalCalls.badRequestError("something went wrong.");
    } else {
      return globalCalls.okResponse(res, deleted, "Successfully removed.");
    }
  } else {
  }
};

const getShortistedCompanies = async (req, res) => {
  if (req.params.id) {
    var project_id = base64decode(req.params.id);
    if(isNaN(parseInt(project_id)))
      throw globalCalls.notFoundError("Error! You have passed invalid-id.");

    let companyData = await shortlistedCompanies
      .query()
      .select("shortlisted_id", "company_id", "project_id")
      .withGraphFetched("company_details")
      .modifyGraph("company_details", (builder) => {
        builder
          .withGraphFetched("[domain, address, services]") //media,
          .modifyGraph("domain", (builder) => {
            builder.select("domain_id", "domain");
          })
          // .modifyGraph("media", builder => {
          //   builder.select("media_id", "media_link", "media_type");
          // })
          .modifyGraph("address", (builder) => {
            builder.select("address_id", "address");
          })
          .modifyGraph("services", (builder) => {
            builder
              .withGraphFetched("service_details")
              .modifyGraph("service_details", (builder) => {
                builder.select("service_id", "service_name", "service_icon");
              });
            builder.select("service_map_id", "service_id");
          });
        builder.select(
          "company_id",
          "company_name",
          "logo",
          "incorporation_date",
          "director_name",
          "company_email_id",
          "company_website",
          "contact_number",
          "team_size",
          "fixed_budget"
        );
      })
      .where("project_id", project_id);

    if (!companyData) {
      throw globalCalls.badRequestError("Invalid Project ID.");
    } else {
      let responseData = {
        companyData,
      };

      return globalCalls.okResponse(res, responseData, "");
    }
  } else {
  }
};

const getquotationPendingCompanies = async (req, res) => {
  if (req.params.id) {
    var project_id = base64decode(req.params.id);
    if(isNaN(parseInt(project_id)))
      throw globalCalls.notFoundError("Error! You have passed invalid-id.");
    
    let companyData = await shortlistedCompanies
      .query()
      .select("shortlisted_id", "company_id", "project_id")
      .withGraphFetched("company_details")
      .modifyGraph("company_details", (builder) => {
        builder
          .withGraphFetched("[domain, address, services]") //media,
          .modifyGraph("domain", (builder) => {
            builder.select("domain_id", "domain");
          })
          .modifyGraph("address", (builder) => {
            builder.select("address_id", "address");
          })
          .modifyGraph("services", (builder) => {
            builder
              .withGraphFetched("service_details")
              .modifyGraph("service_details", (builder) => {
                builder.select("service_id", "service_name", "service_icon");
              });
            builder.select("service_map_id", "service_id");
          });
        builder.select(
          "company_id",
          "company_name",
          "logo",
          "incorporation_date",
          "director_name",
          "company_email_id",
          "company_website",
          "contact_number",
          "team_size",
          "fixed_budget"
        );
      })
      .where(function () {
        this.where("project_id", project_id).whereIn("status", [
          "shortlisted",
          "quotation_request",
          "quotation_pending",
        ]);
      });

    if (!companyData) {
      throw globalCalls.badRequestError("Invalid Project ID.");
    } else {
      let responseData = {
        companyData,
      };

      return globalCalls.okResponse(res, responseData, "");
    }
  } else {
  }
};

const getquotationReceivedCompanies = async (req, res) => {
  if (req.params.id) {
    var project_id = base64decode(req.params.id);
    if(isNaN(parseInt(project_id)))
      throw globalCalls.notFoundError("Error! You have passed invalid-id.");

    let companyData = await shortlistedCompanies
      .query()
      .select("shortlisted_id", "company_id", "project_id")
      .withGraphFetched("[company_details, quotation_details]")
      .modifyGraph("company_details", (builder) => {
        
        builder
          .withGraphFetched("[domain, address]") //media, , services
          .modifyGraph("domain", (builder) => {
            builder.select("domain_id", "domain");
          })
          .modifyGraph("address", (builder) => {
            builder.select("address_id", "address");
          });
        
        builder.select(
          "company_id",
          "company_name",
          "logo",
          "incorporation_date",
          "director_name",
          "company_email_id",
          "company_website",
          "contact_number",
          "team_size",
          "fixed_budget"
        );
      })
      .modifyGraph("quotation_details", (builder) => {
        builder
          .withGraphFetched("[quotation_docx, quotation_technology]")
          .modifyGraph("quotation_docx", (builder) => {
          })
          .modifyGraph("quotation_technology", (builder) => {
            builder
              .withGraphFetched("skills")
              .modifyGraph("skills", (builder) => {
              });
          });
        builder.where({ project_id: project_id });
      })
      .where(function () {
        this.where("project_id", project_id).whereIn("status", [
          "shortlisted",
          "quotation_request",
          "quotation_pending",
        ]);
      });

    if (!companyData) {
      throw globalCalls.badRequestError("Invalid Project ID.");
    } else {
      let responseData = {
        companyData,
      };

      return globalCalls.okResponse(res, responseData, "");
    }
  } else {
  }
};

/**
 * starts discussion of a user with the company
 * @param {*} req
 * @param {*} res
 */
const StartDiscussionCompany = async (req, res) => {
  try {
    if (req.params.id) {
      let quotationData = req.body;
      var data = {};
      var shortlisted_id = base64decode(req.params.id);
      if(isNaN(parseInt(shortlisted_id)))
        throw globalCalls.badRequestError("Error! You have passed invalid-id.");

      data.status = "under_discussion";
      data.shortlisted_id = shortlisted_id;

      let discussioned = await shortlistedCompanies
        .query()
        .patchAndFetchById(shortlisted_id, { status: "under_discussion" });
      if (!discussioned) {
        throw globalCalls.badRequestError("something went wrong.");
      } else {
        if (quotationData.quotation_id) {
          let quotationId = base64decode(quotationData.quotation_id);

          let startChatData = {
            quotation_id: quotationId,
            chatMap: {
              project_id: discussioned.project_id,
              company_id: discussioned.company_id,
            },
          };

          let startChat = await transaction(ChatInbox.knex(), (trx) => {
            return ChatInbox.query(trx).upsertGraph(startChatData, {
              relate: true,
              unrelate: false,
            });
          });
          
        }
        // update project status
        let project_data = await shortlistedCompanies
          .query()
          .select("project_id")
          .where({ shortlisted_id: shortlisted_id })
          .first();
        let is_projectupdate = await project
          .query()
          .update({
            status: "under-discussion",
          })
          .where("project_id", project_data.project_id);

        return globalCalls.okResponse(
          res,
          discussioned,
          "Discussion start successfully."
        );
      }
    }
  } catch (error) {
    console.log(error);
    throw globalCalls.badRequestError(error.message);
  }
};

const getUnderDiscussionCompanies = async (req, res) => {
  if (req.params.id) {
    var project_id = base64decode(req.params.id);
    if(isNaN(parseInt(project_id)))
      throw globalCalls.notFoundError("Error! You have passed invalid-id.");
    
    let companyData = await shortlistedCompanies
      .query()
      .select("shortlisted_id", "company_id", "project_id")
      .withGraphFetched("[company_details, quotation_details]")
      .modifyGraph("company_details", (builder) => {
        builder
          .withGraphFetched("[domain, address]") //media, , services
          .modifyGraph("domain", (builder) => {
            builder.select("domain_id", "domain");
          })
          .modifyGraph("address", (builder) => {
            builder.select("address_id", "address");
          });

        builder.select(
          "company_id",
          "company_name",
          "logo",
          "incorporation_date",
          "director_name",
          "company_email_id",
          "company_website",
          "contact_number",
          "team_size",
          "fixed_budget"
        );
       
      })
      .modifyGraph("quotation_details", (builder) => {
        builder
          .withGraphFetched("[quotation_docx, quotation_technology]")
          .modifyGraph("quotation_docx", (builder) => {
          })
          .modifyGraph("quotation_technology", (builder) => {
            builder
              .withGraphFetched("skills")
              .modifyGraph("skills", (builder) => {
              });
          });
        builder.where({ project_id: project_id });
      })
      .where({
        status: "under_discussion",
        project_id: project_id,
      });

    if (!companyData) {
      throw globalCalls.badRequestError("Invalid Project ID.");
    } else {
      let responseData = {
        companyData,
      };

      return globalCalls.okResponse(res, responseData, "");
    }
  } else {
  }
};

const RejectCompany = async (req, res) => {
  if (req.params.id) {
   
    var shortlisted_id = base64decode(req.params.id);
    if(isNaN(parseInt(shortlisted_id)))
      throw globalCalls.badRequestError("Error! You have passed invalid-id.");
    
    let discussioned = await shortlistedCompanies
      .query()
      .update({
        status: "reject",
        action_by: "client"
      }).where("shortlisted_id", shortlisted_id)
      .returning("*");
    if (!discussioned) {
      throw globalCalls.badRequestError("something went wrong.");
    } else {

      let messageNotify = Object.assign({}, globalConst.NOTIFICATION_PAYLOAD["QUOTATION_REJECT_ACTION_TO_CCOMPANY"]);

      let bodyMessage = messageNotify.body.replace("[CLIENT]", "<b>"+req.user.first_name+" "+req.user.last_name+"</b>");
      messageNotify.body = bodyMessage;
      let notifyData = [];

      let getUser = await companyMap.query().select("user_id").where("company_id", discussioned[0].company_id).first();
      notifyData.push({
        "message": messageNotify.body,
        "notification_for": "QUOTATION_ACTION",
        "notification_for_id": shortlisted_id,
        "to_user_id": getUser.user_id,
        "by_user_id": req.user.user_id,
        "redirect_url": "",
        "redirect_component": "",
        "created_at": new Date().toISOString()
      });

      if(notifyData.length > 0){
        await NotificationUpdate(notifyData)
       }
      return globalCalls.okResponse(
        res,
        discussioned,
        "Quotation reject successfully."
      );
    }
  } else {
  }
};

const HireCompany = async (req, res) => {
  if (req.params.id) {
    var updateData = {};
    var shortlisted_id = base64decode(req.params.id);
    if(isNaN(parseInt(shortlisted_id)))
      throw globalCalls.badRequestError("Error! You have passed invalid-id.");

    updateData.status = "selected";

    try {
      let is_projectupdate;
      let is_reject_all;
      let discussioned = await transaction(shortlistedCompanies.knex(),
      async (trx) => {
        let hire = await shortlistedCompanies.query()
        .update(updateData).where("shortlisted_id", shortlisted_id).returning("*");

        if(hire){
          is_projectupdate = await project
          .query()
          .update({
            hire_date: moment().format(),
            status: "in-progress",
            assigned_to_company: hire[0].company_id
          })
          .where("project_id", hire[0].project_id).returning("*");

          //  // make all company rejected when any hire
          is_reject_all = await shortlistedCompanies
          .query()
          .update({
            status: "reject",
          })
          .where(function () {
            this.where("project_id", hire[0].project_id).whereNot(
              "company_id",
              hire[0].company_id
            );
          }).returning("*");
        }
        return {
          hire,
          is_reject_all
        }
      });
      
      if (!discussioned) {
        throw globalCalls.badRequestError("something went wrong.");
      } else {
        // save hire date
        let messageNotifyHire = Object.assign({}, globalConst.NOTIFICATION_PAYLOAD["QUOTATION_HIRE_ACTION_TO_COMPANY"]);
      
      let bodyMessageHire = messageNotifyHire.body.replace("[CLIENT]", "<b>"+req.user.first_name+" "+req.user.last_name+"</b>");
      messageNotifyHire.body = bodyMessageHire;

      let messageNotify = Object.assign({}, globalConst.NOTIFICATION_PAYLOAD["QUOTATION_REJECT_ACTION_TO_CCOMPANY"]);

      let bodyMessage = messageNotify.body.replace("[CLIENT]", "<b>"+req.user.first_name+" "+req.user.last_name+"</b>");
      messageNotify.body = bodyMessage;
       let data = [];
       console.log(discussioned.hire[0].company_id);
       if(discussioned.hire[0].company_id){
        let getUser = await companyMap.query().select("user_id").where("company_id", discussioned.hire[0].company_id).first();
        data.push({
          "message": bodyMessageHire,
          "notification_for": "QUOTATION_ACTION",
          "notification_for_id": shortlisted_id,
          "to_user_id": getUser.user_id,
          "by_user_id": req.user.user_id,
          "redirect_url": globalConst.clientBusinessUrl+"project/details/"+base64encode(discussioned.hire[0].project_id)+"",
          "redirect_component": "PROJECT",
          "created_at": new Date().toISOString()
        });
       }
       let shorlistedArray = discussioned.is_reject_all;
       for(const selected of shorlistedArray) {
        let getUser = await companyMap.query().select("user_id").where("company_id", selected.company_id).first();
        data.push({
          "message": messageNotify.body,
          "notification_for": "QUOTATION_ACTION",
          "notification_for_id": selected.shortlisted_id,
          "to_user_id": getUser.user_id,
          "by_user_id": req.user.user_id,
          "redirect_url": "",
          "redirect_component": "",
          "created_at": new Date().toISOString()
        });
       };
       console.log(data)
       if(data.length > 0){
        await NotificationUpdate(data)
       }

        return globalCalls.okResponse(
          res,
          discussioned,
          "Company hired successfully."
        );
      }
    } catch (error) {
      console.log(error);
      throw globalCalls.badRequestError(error.message);
    }
  } else {
    throw globalCalls.badRequestError("Invalid Request.")
  }
};

const newQuotationProject = async (req, res) => {
  let page = req.query.page ? req.query.page : 1;
  let limit = req.query.limit ? req.query.limit : globalConst.per_page;
  let offset = req.query.offset ? req.query.offset : limit * (page - 1);

  try {

    let quotationData = await shortlistedCompanies.query().select("shortlisted_id", "project_name", "project.project_id", Knex.raw("(SELECT domain from domain_list where domain_list.domain_id = project.domain_id) AS domain")).innerJoinRelated("project").withGraphFetched("[quotation_details, company_details]").modifyGraph("quotation_details", builder =>{
      builder.select("estimated_timeline", "team_size", "fixed_budget", "comments").withGraphFetched("[quotation_docx, technologies_label, inbox_thread]").modifyGraph("quotation_docx", builder =>{
        builder.select("file_link", "file_type")
      }).modifyGraph("technologies_label", builder =>{
        builder.select("skills")
      }).modifyGraph("inbox_thread", builder =>{
        builder.select("chat_id");
      }).orderBy("quotation_sent_at")
    }).modifyGraph("company_details", builder =>{
      builder.select("company_name","logo", Knex.raw("(SELECT Round (AVG ((SELECT AVG(stars) AS starsAvg FROM review_rating WHERE review_id = reviews.review_id)),2)  from reviews where company_id = company.company_id and feedback_received = true group by company_id) as overall_rating")).withGraphFetched("address", {maxBatchSize: 1}).modifyGraph("address", builder =>{
        builder.select("location").limit(1);
      });
    }).where((builder) =>{
      if(req.query.type && req.query.type == 'discussion'){
        builder.where("shortlisted_companies.status", "under_discussion");
      } else {
        builder.where("shortlisted_companies.status", "quotation_received");
      }
    }).where((builder)=>{
      builder.where("project.status", "!=", "in-progress").where("project.status", "!=", "completed")
    }).where("project.user_id", req.user.user_id).offset(offset)
    .limit(limit);
    

    let totalCount = await shortlistedCompanies
      .query()
      .count().innerJoinRelated("project")
      .where((builder) =>{
        if(req.query.type && req.query.type == 'discussion'){
          builder.where("shortlisted_companies.status", "under_discussion");
        } else {
          builder.where("shortlisted_companies.status", "quotation_received");
        }
      }).where((builder)=>{
        builder.where("project.status", "!=", "in-progress").where("project.status", "!=", "completed")
      }).where("project.user_id", req.user.user_id)
      .first();

    if (!quotationData) {
      throw globalCalls.badRequestError("No New Quotation found.");
    } else {
      let responseData = {
        quotationData,
        total: totalCount.count,
      };

      return globalCalls.okResponse(res, responseData, "");
    }
  } catch (error) {
    
    throw globalCalls.badRequestError(error.message);
  }
};

/**
 * List down pending quotation
 * @param {*} req 
 * @param {*} res 
 */
const pendingQuotationList = async(req, res) =>{
  let page = req.query.page ? req.query.page : 1;
  let limit = req.query.limit ? req.query.limit : globalConst.per_page;
  let offset = req.query.offset ? req.query.offset : limit * (page - 1);

  try{

    let quotationData = await shortlistedCompanies.query().select("shortlisted_id", "project_name", "project.project_id", Knex.raw("(SELECT domain from domain_list where domain_list.domain_id = project.domain_id) AS domain")).innerJoinRelated("project").withGraphFetched("[company_details]").modifyGraph("company_details", builder =>{
      builder.select("company_name", "logo", "incorporation_date", "company_id", "viewer_count", "no_of_project_completed", "fixed_budget", "hourly_budget", Knex.raw("(SELECT Round (AVG ((SELECT AVG(stars) AS starsAvg FROM review_rating WHERE review_id = reviews.review_id)),2)  from reviews where company_id = company.company_id and feedback_received = true group by company_id) as overall_rating")).withGraphFetched("address", {maxBatchSize: 1}).modifyGraph("address", builder =>{
        builder.select("location").limit(1);
      }).withGraphFetched("serviceLabels").modifyGraph("serviceLabels", builder =>{
        builder.select("service_name")
      });
    }).where("shortlisted_companies.status", "quotation_request").where((builder)=>{
      builder.where("project.status", "!=", "in-progress").where("project.status", "!=", "completed")
    }).where("project.user_id", req.user.user_id);

    let totalCount = await shortlistedCompanies
    .query()
    .count().innerJoinRelated("project").where("shortlisted_companies.status", "quotation_request").where((builder)=>{
      builder.where("project.status", "!=", "in-progress").where("project.status", "!=", "completed")
    }).where("project.user_id", req.user.user_id).first();

    if (!quotationData) {
      throw globalCalls.badRequestError("No New Quotation found.");
    } else {
      let responseData = {
        quotationData,
        total: totalCount.count
      }
      return globalCalls.okResponse(res, responseData, "");
    }
  } catch(error) {
    throw globalCalls.badRequestError(error.message);
  }
}

/**
 * All Quotation Lists
 * @param {*} req 
 * @param {*} res 
 */
const allQuotationList = async(req, res) =>{
  let page = req.query.page ? req.query.page : 1;
  let limit = req.query.limit ? req.query.limit : globalConst.per_page;
  let offset = req.query.offset ? req.query.offset : limit * (page - 1);

  try{
    let quotationData = await shortlistedCompanies.query().select("shortlisted_id", "project_name", "project.project_id", "shortlisted_companies.status", Knex.raw("(SELECT domain from domain_list where domain_list.domain_id = project.domain_id) AS domain")).innerJoinRelated("project").withGraphFetched("[company_details, quotation_details]").modifyGraph("company_details", builder =>{
      builder.select("company_name").withGraphFetched("address", {maxBatchSize: 1}).modifyGraph("address", builder =>{
        builder.select("location").limit(1);
      })
    }).modifyGraph("quotation_details" , builder =>{
      builder.select("fixed_budget")
    }).where((builder)=>{
      builder.where("project.status", "!=", "in-progress").where("project.status", "!=", "completed")
    }).where("project.user_id", req.user.user_id).limit(limit).offset(offset);
  
    let totalCount = await shortlistedCompanies
    .query()
    .count().innerJoinRelated("project").where((builder)=>{
      builder.where("project.status", "!=", "in-progress").where("project.status", "!=", "completed")
    }).where("project.user_id", req.user.user_id).first();
  
    if (!quotationData) {
      throw globalCalls.badRequestError("No New Quotation found.");
    } else {
      let responseData = {
        quotationData,
        total: totalCount.count
      }
      return globalCalls.okResponse(res, responseData, "");
    }
  } catch(error){
    console.log(error);
    throw globalCalls.badRequestError(error.message);
  }

}

/**
 * Quotations Count
 * @param {*} req 
 * @param {*} res 
 */

 const getQuotationTabCount = async(req, res) =>{
  try{
    let countData = await shortlistedCompanies.query().select(Knex.raw("COUNT(CASE WHEN shortlisted_companies.status = 'quotation_request' THEN 1 ELSE null END) AS pending, COUNT(CASE WHEN shortlisted_companies.status = 'quotation_received' THEN 1 ELSE null END) AS new_quotation, COUNT(CASE WHEN shortlisted_companies.status = 'under_discussion' THEN 1 ELSE null END) AS in_discussion")).innerJoinRelated("project").where((builder)=>{
      builder.where("project.status", "!=", "in-progress").where("project.status", "!=", "completed")
    }).where("project.user_id", req.user.user_id).first();
  return globalCalls.okResponse(res, {countData}, "");
  } catch(error){
    console.log(error);
    throw globalCalls.badRequestError(error.message);
  }
 }
/**
 * completed project list
 * @param {*} req 
 * @param {*} res 
 */
const completedProjectlist = async (req, res) => {
  let page = req.query.page ? req.query.page : 1;
  let limit = req.query.limit ? req.query.limit : globalConst.per_page;
  let offset = req.query.offset ? req.query.offset : limit * (page - 1);

  try {
    let projectData = await project
      .query()
      .select(
        "project_id",
        "project_name",
        "one_line_description",
        "about_the_project",
        "budget",
        "expected_timeline",
        "ideal_team_size",
        "payment_type",
        "expected_timeline",
        "ideal_team_size",
        "completion_datetime"
      )
      .withGraphFetched("[servicesLabels, domain, hired_company, portfolio]")
      .modifyGraph("servicesLabels", (builder) => {
        builder.select("service_name");
      })
      .modifyGraph("domain", (builder) => {
        builder.select("domain");
      })
      .modifyGraph("hired_company", (builder) => {
        builder
          .withGraphFetched("address", { maxBatchSize: 1 })
          .modifyGraph("address", (builder) => {
            builder.select("location").limit(1);
          });
        builder.select("company_name");
      })
      .modifyGraph("portfolio", (builder) => {
        builder
          .select("portfolio_id", "status")
          .withGraphFetched("reviews")
          .modifyGraph("reviews", (builder) => {
            builder
              .select(
                "reviews",
                "feedback_received",
                Knex.raw(
                  "(SELECT CASE WHEN starsAvg IS NULL THEN 0 ELSE starsAvg END FROM (SELECT ROUND(AVG(stars),2) AS starsAvg FROM review_rating WHERE review_id = reviews.review_id) AS averageStars) AS rating_avg"
                )
              )
              .withGraphFetched("ratings")
              .modifyGraph("ratings", (builder) => {
                builder
                  .select("stars", "rating_title")
                  .oinRelated("ratingPoint")
                  .orderBy("review_rating_id");
              })
              .where("is_valid", true);
          })
          .where((builder) => {
            builder.where("status", "submitted").orWhere("status", "published");
          });
      })
      .where({
        user_id: req.user.user_id,
        status: "complete",
      })
      .orderBy("completion_datetime", "DESC")
      .offset(offset)
      .limit(limit);

    let totalCount = await project
      .query()
      .count()
      .where({
        user_id: req.user.user_id,
        status: "complete",
      })
      .first();

    if (!projectData) {
      throw globalCalls.badRequestError("No Project found.");
    } else {
      let responseData = {
        projectData,
        total: totalCount.count,
      };

      return globalCalls.okResponse(res, responseData, "");
    }
  } catch (error) {
    console.log(error);
    throw globalCalls.badRequestError(error.message);
  }
};

const MyProjectsReviews = async (req, res) => {
  let page = req.query.page ? req.query.page : 1;
  let limit = req.query.limit ? req.query.limit : globalConst.per_page;
  let offset = req.query.offset ? req.query.offset : limit * (page - 1);

  try {
    let projectData = await CompanyPortfolio
      .query()
      .select(
        "portfolio_id",
        "project_name",
        "one_line_description",
        "about_the_project",
        "budget",
        "timeline",
        "published_at"
      ).withGraphFetched("[company, reviews, project]").modifyGraph("company", builder =>{
        builder.select("company_name").withGraphFetched("address", {maxBatchSize: 1}).modifyGraph("address", builder =>{
          builder.select("location").limit(1);
        })
      }).modifyGraph("reviews", (builder) => {
        builder
          .select("reviews", "feedback_received",  Knex.raw("(SELECT CASE WHEN starsAvg IS NULL THEN 0 ELSE starsAvg END FROM (SELECT ROUND(AVG(stars),2) AS starsAvg FROM review_rating WHERE review_id = reviews.review_id) AS averageStars) AS rating_avg"))
          .withGraphFetched("ratings")
          .modifyGraph("ratings", (builder) => {
            builder
              .select("stars", "rating_title")
              .innerJoinRelated("ratingPoint")
              .orderBy("review_rating_id");
          }).where("is_valid", true);
      }).modifyGraph("project", builder =>{
        builder.select("").withGraphFetched("[domain, servicesLabels]").modifyGraph("domain", builder =>{
          builder.select("domain");}
          ).modifyGraph("servicesLabels", builder=>{
            builder.select("service_name");
          } );
      }).where({
        status: "published"}).where("belongs_to_project", "=", project.query().select("project_id").where({"user_id": req.user.user_id, "status": "complete"}).where("project_id", ref("belongs_to_project")))
      .orderBy("published_at", "DESC")
      .offset(offset)
      .limit(limit);

    let totalCount = await CompanyPortfolio
      .query()
      .count()
      .where({
        status: "published"}).where("belongs_to_project", "=", project.query().select("project_id").where({"user_id": req.user.user_id, "status": "complete"}).where("project_id", ref("belongs_to_project")))
      .first();

    if (!projectData) {
      throw globalCalls.badRequestError("No Project found.");
    } else {
      let responseData = {
        projectData,
        total: totalCount.count,
      };

      return globalCalls.okResponse(res, responseData, "");
    }
  } catch (error) {
    console.log(error);
    throw globalCalls.badRequestError(error.message);
  }
};

const completeProjectDetailsClient = async (req, res) => {
  if (req.params.id) {
    var project_id = base64decode(req.params.id);
    if(isNaN(parseInt(project_id)))
      throw globalCalls.notFoundError("Error! You have passed invalid-id.");

    let projectData = await project
      .query()
      .select(
        "project_id",
        "project_name",
        "one_line_description",
        "about_the_project",
        "budget",
        "payment_type",
        "location",
        "expected_timeline",
        "ideal_team_size",
        "domain_experience_in_months",
        "company_expected_experience"
      )
      .withGraphFetched(
        "[services, skills, project_docx,projectAnswer, quotation, portfolio]"
      )
      .modifyGraph("services", (builder) => {
        builder
          .withGraphFetched("service_details")
          .modifyGraph("service_details", (builder) => {
            builder.select("service_id", "service_name", "service_icon");
          });
        builder.select("service_id", "project_service_id");
      })
      .modifyGraph("skills", (builder) => {
        builder
          .withGraphFetched("skill_details")
          .modifyGraph("skill_details", (builder) => {
            builder.select("skill_id", "skills");
          });
        builder.select("skill_id", "project_skill_id");
      })
      .modifyGraph("projectAnswer", (builder) => {
        builder
          .withGraphFetched("question")
          .modifyGraph("question", (builder) => {
            builder.select("question_id", "question", "type");
          });
        builder.select("answer_id", "question_id", "answer");
      })
      .modifyGraph("quotation", (builder) => {
        builder
          .withGraphFetched("quotation_docx")
          .modifyGraph("skill_details", (builder) => {
            builder.select("skill_id", "file_type");
          });
        builder
          .select("estimated_timeline", "team_size", "fixed_budget", "comments")
          .where(
            "company_id",
            project
              .query()
              .select("assigned_to_company")
              .where({ project_id: project_id })
          );
      })
      .modifyGraph("portfolio", (builder) => {
        builder
          .select(
            "portfolio_id",
            "project_name",
            "one_line_description",
            "project_logo",
            "timeline",
            "budget",
            "about_the_project",
            "client_location",
            "status"
          )
          .withGraphFetched("[reviews,gallery,links,portfolioSkills]")
          .modifyGraph("reviews", (builder) => {
            builder
              .select(
                "reviews",
                "feedback_received",
                Knex.raw(
                  "(SELECT CASE WHEN starsAvg IS NULL THEN 0 ELSE starsAvg END FROM (SELECT ROUND(AVG(stars),2) AS starsAvg FROM review_rating WHERE review_id = reviews.review_id) AS averageStars) AS rating_avg"
                )
              )
              .withGraphFetched("ratings")
              .modifyGraph("ratings", (builder) => {
                builder
                  .select("stars", "rating_title")
                  .innerJoinRelated("ratingPoint")
                  .orderBy("review_rating_id");
              })
              .where("is_valid", true);
          })
          .where((builder) => {
            builder.where("status", "submitted").orWhere("status", "published");
          });
      })
      .where({
        user_id: req.user.user_id,
        project_id: project_id,
      });
    
    if (!projectData) {
      throw globalCalls.badRequestError("Project details not found.");
    } else {
      let responseData = {
        projectData,
      };

      return globalCalls.okResponse(res, responseData, "");
    }
  } else {
  }
};

const markCompleteProject = async (req, res) => {
  var portfoliodata = {};
  if (req.params.id) {
    var data = {};
    var project_id = base64decode(req.params.id);
    if(isNaN(parseInt(project_id)))
      throw globalCalls.badRequestError("Error! You have passed invalid-id.");

    try {
      //get project data start
      let projectData = await project
        .query()
        .select(
          "project_id",
          "project_name",
          "one_line_description",
          "about_the_project",
          "budget",
          "payment_type",
          "location",
          "expected_timeline",
          "ideal_team_size",
          "domain_experience_in_months",
          "company_expected_experience",
          "project_logo",
          "assigned_to_company"
        )
        .withGraphFetched("[skills,client_details]")
        .modifyGraph("skills", (builder) => {
          builder.select("skill_id");
        })
        .modifyGraph("client_details", (builder) => {
          builder.select("first_name", "last_name", "email_id", "email_id");
        })
        .where({
          user_id: req.user.user_id,
          project_id: project_id,
        })
        .first();

      if (!projectData) {
        throw globalCalls.badRequestError("No project found.");
      }

      let company_id = projectData.assigned_to_company;
      //get project data end

      portfoliodata.status = "draft";
      portfoliodata.project_name = projectData.project_name;
      portfoliodata.one_line_description = projectData.one_line_description;
      portfoliodata.project_logo = projectData.project_logo;
      
      portfoliodata.first_name = projectData.client_details[0].first_name;
      portfoliodata.last_name = projectData.client_details[0].last_name;
      portfoliodata.client_location = projectData.location;
      portfoliodata.about_the_project = projectData.about_the_project;
      portfoliodata.is_complete = false;
      portfoliodata.belongs_to_project = projectData.project_id;
      portfoliodata.company_id = projectData.assigned_to_company;
      portfoliodata.created_by = projectData.assigned_to_company;
      portfoliodata.client_email = projectData.client_details[0].email_id;
      portfoliodata.skills = projectData.skills;  
      // update project status
      let is_projectupdate = await transaction(project.knex(), async trx =>{
        let updateCompany;
        let portfolioUpdate;

        let projectUpdate = await project
        .query()
        .upsertGraph({
          completion_datetime: moment().format(),
          status: "complete",
          project_id: project_id,
        })
        .returning("*");

        if(projectUpdate){
           // update project completed count
          updateCompany = await company
          .query()
          .patch({
            no_of_project_completed: company.raw("no_of_project_completed + 1"),
          })
          .where("company_id", company_id).returning("*");

          /////////////////////////add portfolio start/////////////////////////////
          portfolioUpdate = await CompanyPortfolio.query(trx).upsertGraph(portfoliodata, {
            relate: true,
            unrelate: true,
          });
        }

        return {
          projectUpdate,
          updateCompany,
          portfolioUpdate
        };
      });

      console.log(is_projectupdate);
      console.log(is_projectupdate.updateCompany)
      let getUser = await CompanyMapping.query().select("user_id").where("Company_id", company_id).first();

      let messageNotify = Object.assign({}, globalConst.NOTIFICATION_PAYLOAD["PROJECT_COMPLETION_TO_COMPMANY"]);

      messageNotify.body = bodyMessage.replace("[PROJECT_NAME]", "<b>" + is_projectupdate.projectUpdate[0].project_name + "</b>");
      let notifyData = [];


      notifyData.push({
        "message": messageNotify.body,
        "notification_for": "PROJECT",
        "notification_for_id": project_id,
        "to_user_id": getUser.user_id,
        "by_user_id": req.user.user_id,
        "redirect_url": "/project-details/" + base64encode(is_projectupdate.portfolioUpdate[0].portfolio_id),
        "redirect_component": "PROJECT",
        "created_at": new Date().toISOString()
      });

      if (notifyData.length > 0) {
        await NotificationUpdate(notifyData)
      }
      return globalCalls.okResponse(
        res,
        is_projectupdate,
        "Project complete successfully."
      );
    } catch (error) {
      // console.log(error);
      throw globalCalls.badRequestError(error.message);
    }
  } else {
  }
};


/**
 * fetched client feedback
 * @param {*} req
 * @param {*} res
 */
const fetchClientFeedback = async (req, res) => {
  try {
    if (!req.params.id) {
      throw globalCalls.badRequestError("Invalid Requests");
    }

    let id = base64decode(req.params.id);
    if(isNaN(parseInt(id)))
      throw globalCalls.notFoundError("Error! You have passed invalid-id.");

    let projectData = await project
      .query()
      .select(
        "project_id",
        "project_name",
        "budget",
        "about_the_project",
        "one_line_description",
        "company_expected_experience",
        "location",
        "domain_experience_in_months",
        "expected_timeline",
        "quotation_remarks",
        "assigned_to_company"
      )
      .withGraphFetched(
        "[final_quotation, servicesLabels, domain, project_docx, skillLabel, projectAnswer, portfolio]"
      )
      .modifyGraph("final_quotation", (builder) => {
        builder
          .select(
            "estimated_timeline",
            "quotation_details.team_size",
            "quotation_details.fixed_budget",
            "comments"
          )
          .withGraphFetched("[technologies_label, quotation_docx]")
          .modifyGraph("technologies_label", (builder) => {
            builder.select("skills");
          })
          .modifyGraph("quotation_docx", (builder) => {
            builder.select("file_link", "file_type");
          });
      })
      .modifyGraph("servicesLabels", (builder) => {
        builder.select("service_name");
      })
      .modifyGraph("domain", (builder) => {
        builder.select("domain");
      })
      .modifyGraph("skillLabel", (builder) => {
        builder.select("skills");
      })
      .modifyGraph("project_docx", (builder) => {
        builder.select("docx_url", "file_type").where("status", "active");
      })
      .modifyGraph("projectAnswer", (builder) => {
        builder
          .select("question", "answer", "type")
          .innerJoinRelated("question")
          .orderBy("type", "ASC");
      })
      .modifyGraph("portfolio", (builder) => {
        builder
          .select(
            "portfolio_id",
            "project_logo",
            "budget",
            "timeline",
            "about_the_project",
            "one_line_description"
          )
          .withGraphFetched("[portfolioSkills, links, gallery, reviews]")
          .modifyGraph("portfolioSkills", (builder) => {
            builder.select("skills");
          })
          .modifyGraph("links", (builder) => {
            builder.select("link", "link_for");
          })
          .modifyGraph("gallery", (builder) => {
            builder.select("gallery_image");
          })
          .modifyGraph("reviews", (builder) => {
            builder
              .select("review_id", "reviews", "feedback_received")
              .withGraphFetched("ratingList")
              .modifyGraph("ratingList", (builder) => {
                builder
                  .select("rating_section.rating_id", "stars")
                  .orderBy("rating_id");
              });
          }).where("status", "submitted");
      })
      .where("project_id", id)
      .first();

    let ratingList = await ratingPointers
      .query()
      .select("rating_id", "rating_title")
      .orderBy("rating_id");

    return globalCalls.okResponse(res, { projectData, ratingList }, "");
  } catch (error) {
    console.log(error);
    throw globalCalls.badRequestError(error.message);
  }
};

const fetchEditFeedbackData = async (req, res) => {
  if (!req.params.id) {
    throw globalCalls.badRequestError("Invalid Requests");
  }

  let id = base64decode(req.params.id);
  if(isNaN(parseInt(id)))
      throw globalCalls.notFoundError("Error! You have passed invalid-id.");

  let skillSetList = await skills
    .query()
    .select("skill_id", "skills", "status");

  let ratingList = await ratingPointers
    .query()
    .select("rating_id", "rating_title");

  let projectData = await CompanyPortfolio.query()
    .select(
      "portfolio_id",
      "project_name",
      "project_logo",
      "one_line_description",
      "about_the_project",
      "timeline",
      "budget"
    )
    .withGraphFetched("[gallery, skills, links, reviews]")
    .modifyGraph("gallery", (builder) => {
      builder.select("gallery_id", "gallery_image");
    })
    .modifyGraph("skills", (builder) => {
      builder.select("skill_id", "portfolio_skill_id");
    })
    .modifyGraph("links", (builder) => {
      builder.select("link_id", "link", "link_for");
    })
    .where("belongs_to_project", id)
    .first();

  return globalCalls.okResponse(
    res,
    { projectData, skillsList: skillSetList, ratingList },
    ""
  );
};

/**
 * submits when only stars are provided
 * @param {*} req
 * @param {*} res
 */
const submitOnlyFeedbackStars = async (req, res) => {
  let data = req.body;

  let rating = data.ratings;
  rating.forEach((element) => {
    if (!element.review_id) {
      throw globalCalls.badRequestError("Invalid Request.");
    }
    if (!element.portfolio_id) {
      throw globalCalls.badRequestError("Invalid Request.");
    }
    if (!element.rating_id) {
      throw globalCalls.badRequestError("Invalid Request.");
    }
    if (!element.stars) {
      throw globalCalls.badRequestError("Please provide rating stars.");
    }
  });

  let executedQuery = await transaction(
    portfolioReviewRating.knex(),
    async (trx) => {
      const ratingInserted = await portfolioReviewRating
        .query(trx)
        .insert(rating);
      if (ratingInserted) {
        let reviewUpdate = await review
          .query()
          .patch({ feedback_received: true })
          .where("review_id", rating[0].review_id);

        if (!reviewUpdate) {
          throw globalCalls.badRequestError("Something went wrong.");
        }

        let portfolioUpdate = await CompanyPortfolio.query()
          .patch({ status: "published" })
          .where("portfolio_id", rating[0].potfolio_id).returning("*");
        if (!portfolioUpdate) {
          throw globalCalls.badRequestError("Something went wrong.");
        }
        return {
          ratingInserted,
          portfolioUpdate
        };
      }
    }
  );

  if(!executedQuery){
    throw globalCalls.badRequestError("Something went wrong.");
  }


  let messageNotify = Object.assign({}, globalConst.NOTIFICATION_PAYLOAD["REVIEW_SUBMITTED_TO_COMPANY"]);

      messageNotify.body = bodyMessage.replace("[PROJECT_NAME]", "<b>" + executedQuery.portfolioUpdate[0].project_name + "</b>");
      let notifyData = [];


      notifyData.push({
        "message": messageNotify.body,
        "notification_for": "PROJECT",
        "notification_for_id": executedQuery.portfolioUpdate[0].portfolio_id,
        "to_user_id": getUser.user_id,
        "by_user_id": req.user.user_id,
        "redirect_url": "/project-details/" + base64encode(executedQuery.portfolioUpdate[0].portfolio_id),
        "redirect_component": "PROJECT",
        "created_at": new Date().toISOString()
      });

      if (notifyData.length > 0) {
        await NotificationUpdate(notifyData)
      }

      
      /*update final budget start - zubear*/
      /*let portfolioData = await CompanyPortfolio.query()
        .select("budget","belongs_to_project")
        .where("portfolio.portfolio_id", executedQuery.portfolioUpdate[0].portfolio_id).first();

      let project_final_budget = [];
      project_final_budget.final_budget = portfolioData.budget;
      project_final_budget.client_id = req.user.user_id;
      project_final_budget.company_id = getUser.user_id;
      project_final_budget.project_id = portfolioData.belongs_to_project;
      project_final_budget.portfolio_id = executedQuery.portfolioUpdate[0].portfolio_id;
      
      let isInsertfinal_budget = await projectFinalBudget.query()
      .insert(project_final_budget)
      .returning("*");*/
      /*update final budget end - zubear*/

  return globalCalls.okResponse(
    res,
    executedQuery,
    "Thank You, Feedback Submitted successfully."
  );
};

const submitFeedbackFromClient = async (req, res) => {
  try{
    let data = req.body;

  /** check for form empty fields */
  if (!data.project_name) {
    throw globalCalls.badRequestError("Please fill up project name.");
  }

  if (!data.one_line_description) {
    throw globalCalls.badRequestError("Please fill up one line description.");
  }

  if (!data.budget) {
    throw globalCalls.badRequestError("Please enter budget.");
  }

  if (!data.about_the_project) {
    throw globalCalls.badRequestError("Please fill up about the project.");
  }

  if (!data.timeline) {
    throw globalCalls.badRequestError("Please enter your project timeline.");
  }

  if (!data.skills) {
    throw globalCalls.badRequestError("Please choose atleast one technology.");
  }

  if (data.skills) {
    data.skills = JSON.parse(data.skills);
  }

  if (data.links) {
    data.links = JSON.parse(data.links);
  } else {
    data.is_complete = false;
  }

  if (req.files && req.files.project_logo_file) {
    data.project_logo = req.files.project_logo_file[0].location;
  } else if (data.project_logo == "") {
    throw globalCalls.badRequestError("Please upload project logo.");
  }

  data.gallery = data.gallery != "" ? JSON.parse(data.gallery) : [];

  if (req.files && req.files.gallery_file) {
    for (let i = 0; i < req.files.gallery_file.length; i++) {
      let tmp = {};
      tmp.gallery_image = req.files.gallery_file[i].location;
      data.gallery.push(tmp);
    }
  } else if (data.gallery.length == 0) {
    throw globalCalls.badRequestError("Please upload project gallery.");
  }

  if(!data.reviews){
    throw globalCalls.badRequestError("Please add reviews to your client.");
  } 

  let reviewsData = JSON.parse(data.reviews);
  reviewsData["feedback_received"] = true;
  data["status"] = "published";
  data["is_complete"] = true;
  data["published_at"] = new Date().toISOString();
  delete data.reviews;
  delete data.project_logo_file;
  delete data.gallery_file;

  

  let executedQuery = await transaction(
    CompanyPortfolio.knex(),
    async (trx) => {
      const updatePortfolio = await CompanyPortfolio
        .query(trx)
        .upsertGraph(data).returning("*");
      if (updatePortfolio) {
        let reviewUpdate = await review
          .query()
          .upsertGraph(reviewsData);

        if (!reviewUpdate) {
          throw globalCalls.badRequestError("Something went wrong.");
        }
        return updatePortfolio;
      }
    });

    if(!executedQuery){
      throw globalCalls.badRequestError("Something went wrong.");
    }

    let messageNotify = Object.assign({}, globalConst.NOTIFICATION_PAYLOAD["REVIEW_SUBMITTED_TO_COMPANY"]);

      messageNotify.body = bodyMessage.replace("[PROJECT_NAME]", "<b>" + executedQuery[0].project_name + "</b>");
      let notifyData = [];


      notifyData.push({
        "message": messageNotify.body,
        "notification_for": "PROJECT",
        "notification_for_id": executedQuery[0].portfolio_id,
        "to_user_id": getUser.user_id,
        "by_user_id": req.user.user_id,
        "redirect_url": "/project-details/" + base64encode(executedQuery[0].portfolio_id),
        "redirect_component": "PROJECT",
        "created_at": new Date().toISOString()
      });

      if (notifyData.length > 0) {
        await NotificationUpdate(notifyData)
      }

      /*update final budget start - zubear*/
      /*let project_final_budget = [];
      project_final_budget.final_budget = data.budget;
      project_final_budget.client_id = req.user.user_id;
      project_final_budget.company_id = updatePortfolio[0].company_id;
      project_final_budget.project_id = updatePortfolio[0].belongs_to_project;
      project_final_budget.portfolio_id = updatePortfolio[0].portfolio_id;
      // if updatePortfolio doesn't return data call executedQuery
      
      let isInsertfinal_budget = await projectFinalBudget.query()
      .insert(project_final_budget)
      .returning("*");*/
      /*update final budget end - zubear*/


    return globalCalls.okResponse(
      res,
      executedQuery,
      "Thank You, Feedback Submitted successfully."
    );
  } catch (error){
    console.log(error)
    throw globalCalls.badRequestError(error.message)
  }

};

module.exports = {
  postProjectApi,
  postProject,
  fetchProjectDataForClientToEdit,
  getRecommendation,
  getMoreRecommendation,
  getProjectListForClient,
  ProjectDetailsClient,
  archiveProject,
  sendQuotationRequest,
  AddtoCart,
  RemovetoCart,
  getCartCompanies,
  deleteProjectDocx,
  getShortistedCompanies,
  getquotationPendingCompanies,
  getquotationReceivedCompanies,
  StartDiscussionCompany,
  getUnderDiscussionCompanies,
  RejectCompany,
  HireCompany,
  newQuotationProject,
  completedProjectlist,
  MyProjectsReviews,
  completeProjectDetailsClient,
  markCompleteProject,
  fetchClientFeedback,
  fetchEditFeedbackData,
  submitOnlyFeedbackStars,
  submitFeedbackFromClient,
  pendingQuotationList,
  allQuotationList,
  getQuotationTabCount
};
