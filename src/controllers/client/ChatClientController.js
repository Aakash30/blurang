const { base64decode, base64encode } = require("nodejs-base64");
const globalCalls = require("../../settings/functions");
const globalConst = require("../../settings/constants");

const knexConfig = require("../../../config/knex");
const Knex = require("knex")(knexConfig.development);

const chatMap = require("../../models/ChatProjectCompanyMap");
const chatMessage = require("../../models/ChatMessage");
const chatInbox = require("../../models/ChatInbox");

const {
  io
} = require('../../settings/server');

const { transaction, ref } = require("objection");
const Project = require("../../models/Project");
const Skills = require("../../models/Skills");
const ChatMessage = require("../../models/ChatMessage");

const fetchClientChat = async (req, res) => {

  try {
    let chatBox = await chatMap
      .query()
      .select(
        "map_id",
        "chat_box_id",
        "status",
        "project_id",
          "updated_at",
        Knex.raw(
          "(SELECT count(message_id) FROM chat_messages_list where chat_id = inbox.chat_id and status = 'unread' AND sender != "+req.user.user_id+" GROUP BY inbox.chat_id) AS unread"
        )
      )
      .innerJoinRelated("inbox")
      .where((builder) => {
        if (req.params.id) {
          let projectId = base64decode(req.params.id);
          builder.where("project_id", projectId);
        }

        if (req.user.user_type == "client") {
          builder.where("client_id", req.user.user_id);
        } else if (req.user.user_type == "company") {
          builder.where("company_id", req.user.default_company);
        }
      })
      .withGraphFetched("company")
      .modifyGraph("company", (builder) => {
        builder
          .select("company_name")
          .withGraphFetched("address", { maxBatchSize: 1 })
          .modifyGraph("address", (builder) => {
            builder.select("location").limit(1);
          });
      })
      .orderBy("updated_at", "DESC");

      let projectList = await Project.query().select("project_id", "project_name").where({"user_id": req.user.user_id}).whereIn("project_id", chatMap.query().select("project_id").where("client_id", ref("user_id")));
    let responseData = {
      chatBox,
      projectList
    };
    
    return globalCalls.okResponse(res, responseData, "");
  } catch (error) {
    console.log(error)
    throw globalCalls.badRequestError(error.message);
  }
};

/**
 * send message through inbox
 * @param {*} req
 * @param {*} res
 */
const sendMessage = async (req, res) => {
  let data = req.body;

  if (!data.receiver || !data.chat_id) {
    throw globalCalls.badRequestError("Invalid Request");
  }

  if (!data.message && data.message_type == "text") {
    throw globalCalls.badRequestError("Please enter message.");
  }

  if (!req.files && data.message_type == "file") {
    throw globalCalls.badRequestError("No attachement found.");
  } else if (req.files && data.message_type == "file") {
    let file_type = data.file_type ? JSON.parse(data.file_type) : [];
    let messageArray = [];
    i = 0;
    req.files.forEach((file) => {
      messageArray.push({ file_link: file.location, file_type: file_type[i] });
      i++;
    });
    data.message = JSON.stringify(messageArray);
    delete data.file_type;
  }
  let lastUpdate = new Date().toISOString();
  data["last_updated"] = lastUpdate;
  data["sender"] = req.user.user_id;

 
  try {
    let sendMessageData = await transaction(chatMessage.knex(), async (trx) => {
      const inserChatMessage = await chatMessage.query(trx).insert(data);
      if (inserChatMessage) {
        await chatInbox
          .query()
          .update({
            updated_at: lastUpdate,
          })
          .where("chat_id", data.chat_id);

          io.sockets.to(base64encode(data.chat_id)).emit("sendMessage", {
            chat : inserChatMessage
        });
      }
      

      return inserChatMessage;
    });
    return globalCalls.okResponse(res, sendMessageData, "");
  } catch (error) {
    console.log(error);
    throw globalCalls.badRequestError(error.message);
  }
};

/**
 * fetches single chat data
 * @param {*} req 
 * @param {*} res 
 */
const fetchSingleBoxData = async (req, res) => {
try{
  if (!req.params.id) {
    throw globalCalls.badRequestError("Invalid Requests.");
  }

  let id = base64decode(req.params.id);
  if(isNaN(parseInt(id)))
	  throw globalCalls.notFoundError("Error! You have passed invalid-id.");

  let singleBoxData;
  let skillsList;
  let totalMessage;

  let page = req.query.page ? req.query.page : 1;
  let limit = req.query.limit ? req.query.limit : globalConst.per_page;
  let offset = req.query.offset ? req.query.offset : limit * (page - 1);

 /* update read status */
 let updateReadStatus = await chatMessage
 .query()
 .update({
   status: "read",
 })
 .where("receiver", req.user.user_id)
 .where("chat_id", id);
  if (req.user.user_type == "client") {
    singleBoxData = await chatMap
      .query()
      .select("chat_box_id", "client_id")
      .withGraphFetched("[company, quotation]")
      .withGraphFetched("chatMessage", {maxBatchSize: 20})
      .modifyGraph("company", (builder) => {
        builder
          .select("company.company_id", "company_name", "logo", "user.user_id")
          .innerJoinRelated("user");
      })
      .modifyGraph("quotation", (builder) => {
        builder
          .select(
            "estimated_timeline",
            "fixed_budget",
            "team_size",
            "quotation_sent_at",
            "quotation_updated_at",
            "comments",
            "quotation_details.project_id",
            "shortlisted_id", "shortlisted_companies.status"
          ).join("shortlisted_companies", function(){
            this.on("shortlisted_companies.company_id", "quotation_details.company_id")
            this.on("shortlisted_companies.project_id", ref("quotation_details.project_id"))
          })
          .withGraphFetched("[technologies_label, quotation_docx]")
          .modifiers({
            fetchShortlist: query => query.modify('getShortlistedId', ref("project_id"))
            
          })
          .modifyGraph("technologies_label", (builder) => {
            builder.select("skills");
          })
          .modifyGraph("quotation_docx", (builder) => {
            builder.select("file_link", "file_type");
          });
      })
      .modifyGraph("chatMessage", (builder) => {
        builder
          .select(
            "message",
            "message_type",
            "sender",
            "receiver",
            "chat_messages_list.status",
            "last_updated"
          )
          .limit(20).offset(offset).orderBy("last_updated", "DESC");
      })
      .where("client_id", req.user.user_id)
      .where("chat_box_id", id);

      totalMessage = await ChatMessage.query().count().where({"chat_id": id}).first();

  } else if (req.user.user_type == "company") {
    singleBoxData = await chatMap
      .query()
      .select("chat_box_id", "client_id")
      .withGraphFetched("[client, quotation]")
      .withGraphFetched("chatMessage", {maxBatchSize: 20})
      .modifyGraph("client", (builder) => {
        builder.select(
          "first_name",
          "last_name",
          "designation",
          "dateofbirth",
          "location",
          "profile_pic",
          "email_id",
          "mobile_number",
          "gender"
        );
      })
      .modifyGraph("quotation", (builder) => {
        builder
          .select(
            "quotation_details.quotation_id",
            "estimated_timeline",
            "fixed_budget",
            "team_size",
            "quotation_sent_at",
            "quotation_updated_at",
            "comments","quotation_details.project_id", "shortlisted_id", "shortlisted_companies.status"
          ).join("shortlisted_companies", function(){
            this.on("shortlisted_companies.company_id", "quotation_details.company_id")
            this.on("shortlisted_companies.project_id", ref("quotation_details.project_id"))
          })
          .withGraphFetched("[technologies_label, quotation_docx, quotation_technology]")
          .modifyGraph("technologies_label", (builder) => {
            builder.select("skills");
          })
          .modifyGraph("quotation_docx", (builder) => {
            builder.select("document_id","file_link", "file_type");
          });
      })
      .modifyGraph("chatMessage", (builder) => {
        builder
          .select(
            "message",
            "message_type",
            "sender",
            "receiver",
            "chat_messages_list.status",
            "last_updated"
          )
          .limit(20).offset(offset).orderBy("last_updated", "DESC");
      })
      .where("company_id", req.user.default_company)
      .where("chat_box_id", id);

      totalMessage = await ChatMessage.query().count().where({"chat_id": id}).first();
      skillsList = await Skills.query().select("skill_id", "skills", "status");
  }

 
  let responseData = {
    singleBoxData: singleBoxData,
    skillsList,
    totalMessage: totalMessage.count
  };

  return globalCalls.okResponse(res, responseData, "");
} catch (error){
  console.log(error);
  throw globalCalls.badRequestError(error.message);
}
};

const getOnlyChatMessage = async(req, res) =>{

  if (!req.params.id) {
    throw globalCalls.badRequestError("Invalid Requests.");
  }

  let id = base64decode(req.params.id);
  if(isNaN(parseInt(id)))
    throw globalCalls.notFoundError("Error! You have passed invalid-id.");
    
  let page = req.query.page ? req.query.page : 1;
  let limit = req.query.limit ? req.query.limit : globalConst.per_page;
  let offset = req.query.offset ? req.query.offset : limit * (page - 1);

  let mychats = await ChatMessage.query().select(
    "message",
    "message_type",
    "sender",
    "receiver",
    "chat_messages_list.status",
    "last_updated"
  ).where({"chat_id": id}).limit(20).offset(offset).orderBy("last_updated", "DESC");

  let responseData = {
    mychats
  }
  return globalCalls.okResponse(res, responseData, "");
}
module.exports = {
  fetchClientChat,
  sendMessage,
  fetchSingleBoxData,
  getOnlyChatMessage
};
