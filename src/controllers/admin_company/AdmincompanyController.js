'use strict';

const { base64encode, base64decode } = require('nodejs-base64');
const globalCalls = require("../../settings/functions");
const globalConst = require("../../settings/constants");

const messageTips = require('../../models/MessageTips');
const company = require('../../models/Company');
const domain = require('../../models/Domain');
const service = require('../../models/Service');
const companyQuestion = require('../../models/CompanyQuestion');
const CompanyPortfolio = require("../../models/CompanyPortfolio");
const reviews           = require('../../models/PortfolioProjectReviews');
const project = require("../../models/Project");
const projectRecommendedCompanies = require("../../models/ProjectRecommendedCompanies");
const companyCertificates = require("../../models/CompanyCertificates");
const companyAwards = require("../../models/CompanyAwards");
const companyFeatured = require("../../models/CompanyFeatured");
const companySuccessStory = require("../../models/CompanySuccessStory");
const companyFaq = require("../../models/CompanyFaq");
const companyMembershipHistory = require("../../models/CompanyMembershipHistory");

const knexConfig = require('../../../config/knex');
const Knex = require('knex')(knexConfig.development);

const {
  transaction
} = require('objection');

const ListCompany = async (req, res) => {
  try {

    let page = req.query.page ? req.query.page : 1;
    let limit = req.query.limit ? req.query.limit : globalConst.per_page;
    let offset = req.query.offset ? req.query.offset : limit * (page - 1);
    
    let orderbydata;
    if(req.body.order_by)
    {
      if(req.query.order_by_type)
        orderbydata=req.query.order_by+' '+req.query.order_by_type;
      else
        orderbydata=req.query.order_by+' DESC';
    }
    orderbydata="company.company_id DESC";
    
  // "director_name", "company_email_id", "contact_number", 
    let companyData = await company.query()
      .select(
        "company_id", "company_name", "logo", "incorporation_date", "company_website", "team_size",
        "fixed_budget", "domain_id", "no_of_project_completed",
        Knex.raw("case when (SELECT Round (AVG ((SELECT AVG(stars) AS starsAvg FROM review_rating WHERE review_id = reviews.review_id)),2)  from reviews where company_id = company.company_id and feedback_received = true group by company_id) IS NULL THEN 0 else (SELECT Round (AVG ((SELECT AVG(stars) AS starsAvg FROM review_rating WHERE review_id = reviews.review_id)),2)  from reviews where company_id = company.company_id and feedback_received = true group by company_id) END as overall_rating "),
        Knex.raw("case when (SELECT sum(final_budget) FROM project_final_budget WHERE company_id = company.company_id) IS NULL THEN 0 else (SELECT sum(final_budget) FROM project_final_budget WHERE company_id = company.company_id) END as earning "),
        // Knex.raw("(SELECT sum(final_budget) FROM project_final_budget WHERE company_id = company.company_id) as earning"),
        // Knex.raw("(SELECT Round (AVG ((SELECT AVG(stars) AS starsAvg FROM review_rating WHERE review_id = reviews.review_id)),2)  from reviews where company_id = company.company_id and feedback_received = true group by company_id) as overall_rating ")
        Knex.raw("concat(first_name, ' ', last_name) as agent_name"), "defaultUser.email_id","defaultUser.mobile_number"
    )
    .leftJoinRelated("defaultUser")
    .withGraphFetched("[domain, services, skills, address]")
    .modifyGraph("domain", (builder) => {
      builder.select("domain");
    })
    .modifyGraph("services", (builder) => {
      builder.where((builder) => {
        if (req.query.service) {
          builder.orWhere("service_id", req.query.service);
        }
      })
    })
    .modifyGraph("skills", (builder) => {
      builder.where((builder) => {
        if (req.query.skill) {
          builder.orWhere("skill_id", req.query.skill);
        }
      })
    })
    .modifyGraph("address", (builder) => {
      builder.select("address")
      .where((builder) => {
        if (req.query.search) {
          builder.orWhere("address", "like", "%"+req.query.search+"%");
        }
      })
    })
    .where((builder) => {
        if (req.query.domain_id) {
            builder.where("domain_id", req.query.domain_id);
        }
        if (req.query.fixed_budget) {
            builder.where("fixed_budget", req.query.fixed_budget);
        }
        if (req.query.status) {
            builder.where("status", req.query.status);
        }
        // .andWhere('result', '<', res)
        if (req.query.project_done) {
            builder.where("no_of_project_completed", '>=', req.query.project_done);
        }
        // if (req.query.project_done_end) {
        //     builder.where("no_of_project_completed", '<=', req.query.project_done_end);
        // }
        // if (req.query.rating) {
        //   builder.where("overall_rating", '>=', req.query.rating);
        // }
        // if (req.query.rating_end) {
        //     builder.where("overall_rating", '<=', req.query.rating_end);
        // }
        if (req.query.project_done) {
          builder.where("no_of_project_completed", '>=', req.query.project_done);
        }
        if (req.query.search) {
          builder.orWhere("company_name", "like", "%"+req.query.search+"%");
          builder.orWhere(Knex.raw("concat(first_name, ' ', last_name)"), "like", "%"+req.query.search+"%");
          builder.orWhere("email_id", "like", "%"+req.query.search+"%");
          builder.orWhere("mobile_number", "like", "%"+req.query.search+"%");
        }
    })
    .orderByRaw(orderbydata)
    .offset(offset)
    .limit(limit)
    .runAfter((result, builder) =>{
    console.log(builder.toKnexQuery().toQuery())
    return result;
    });
    
  let totalCount = await company
  .query()
  .count()
  .where((builder) => {
    if (req.query.domain_id) {
      builder.where("domain_id", req.query.domain_id);
    }
    if (req.query.fixed_budget) {
        builder.where("fixed_budget", req.query.fixed_budget);
    }
    if (req.query.status) {
        builder.where("status", req.query.status);
    }
    if (req.query.project_done) {
        builder.where("no_of_project_completed", '>=', req.query.project_done);
    }
    // if (req.query.rating) {
    //   builder.where("overall_rating", '>=', req.query.rating);
    // }
    if (req.query.earing) {
      builder.where("earing", '>=', req.query.earing);
    }
  })
  .first();

    if (!companyData) {
      throw globalCalls.badRequestError("No company found.");
    } else {

      let responseData = {
        companyData,
        total: totalCount.count
      };

      return globalCalls.okResponse(res, responseData, "");
    }
  } catch (error) {
    console.log(error);
    throw globalCalls.badRequestError(error.message)
  }
}

const fetchCompanyInformationData = async (req, res) => {
  if (req.params.id) {
    var company_id = base64decode(req.params.id);

    if(isNaN(parseInt(company_id)))
    	throw globalCalls.notFoundError("Error! You have passed invalid-id.");

    let companyData;
    if (company_id != 0) {
      companyData = await company
        .query()
        .select(
          "company_id",
          "company_name",
          "logo",
          "incorporation_date",
          "director_name",
          "company_email_id",
          "company_website",
          "contact_number",
          "team_size",
          "fixed_budget",
          "hourly_budget"
        )
        .where("company_id", company_id)
        .first();

      if (!companyData) {
        throw globalCalls.badRequestError("company not found.");
      } else {
      }
    }

    let tips = await messageTips
      .query()
      .select("message")
      .where("module", "Company Profile Step #1");
    let responseData = {
      companyData,
      tips
    };

    return globalCalls.okResponse(res, responseData, "");
  } else {
  }
};

/**
 * function to add company information step #1
 * @param {*} req
 * @param {*} res
 */
const updateCompanyInformation = async (req, res) => {
  let data = req.body;

  if (!data.company_name) {
    throw globalCalls.badRequestError("Please fill up company name.");
  }

  if (!data.incorporation_date) {
    throw globalCalls.badRequestError("Please Choose incorporation date.");
  }

  if (!data.director_name) {
    throw globalCalls.badRequestError(
      "Please fill up atleast one director name."
    );
  }

  if (!data.company_email_id) {
    throw globalCalls.badRequestError("Please fill up your company email id.");
  }

  if (!data.company_website) {
    throw globalCalls.badRequestError("Please fill up your company website.");
  }

  if (!data.contact_number) {
    throw globalCalls.badRequestError("Please add atleast one contact number.");
  }

  if (!data.team_size) {
    throw globalCalls.badRequestError("Please enter your team size.");
  }

  if (!data.fixed_budget) {
    throw globalCalls.badRequestError("Please enter your budget.");
  }

  if (req.file) {
    if (req.file.fieldname == "logo_file") {
      data.logo = req.file.location;
    }
  }
  delete data.logo_file;
  try {
    

    let addCompanyInformation = await transaction(company.knex(), trx => {
      return company.query(trx).upsertGraph(data, {
        relate: true,
        unrelate: false
      });
    });

    if (!addCompanyInformation) {
      throw globalCalls.badRequestError("something went wrong.");
    } else {
      return globalCalls.okResponse(
        res,
        addCompanyInformation,
        "Successfully added."
      );
    }
  } catch (error) {
    throw globalCalls.badRequestError(error.message);
  }
};

/**
 * fetches data for company description
 * @param {*} req
 * @param {*} res
 */
const fetchAboutCompany = async (req, res) => {
  if (!req.params.id) {
    throw globalCalls.badRequestError("Invalid Request");
  }
  let company_id = base64decode(req.params.id);
  if(isNaN(parseInt(company_id)))
  	throw globalCalls.notFoundError("Error! You have passed invalid-id.");

  let companyData = await company
    .query()
    .select("company_id", "description", "video_link", "brochure")
    .withGraphFetched("[media, address]")
    .modifyGraph("media", builder => {
      builder.select("media_id", "media_link", "media_type");
    })
    .modifyGraph("address", builder => {
      builder.select("address_id", "address", "location");
    })
    .where("company_id", company_id)
    .first();

  let tips = await messageTips
    .query()
    .select("message")
    .where("module", "Company Profile Step #2")
    .first();
  let responseData = {
    companyData,
    tips
  };

  return globalCalls.okResponse(res, responseData, "");
};
/**
 * function to add company description step #2
 * @param {*} req
 * @param {*} res
 */
const updateAboutCompany = async (req, res) => {
  let data = req.body;

  let mediaFiles = [];
  try {
    if (!data.company_id) {
      throw globalCalls.badRequestError("Not a valid request.");
    }

    if (!data.description) {
      throw globalCalls.badRequestError("Please add description");
    }

    if (!data.video_link) {
      throw globalCalls.badRequestError("Please add a video");
    }

    if (!data.media && !req.files.gallery_image) {
      throw globalCalls.badRequestError("Please add atleast one media file.");
    } else {
      // mediaFiles = data.media != "" ? JSON.parse(data.media) : [];

      if (data.media != "") {
        let media = JSON.parse(data.media);
        //console.log(typeof media)
        media.forEach(innerdata => {
          if (innerdata.media_id == null) {
            delete innerdata.media_id;
          }
          mediaFiles.push(innerdata);
        });
      } else {
        mediaFiles = [];
      }

      // data.media = mediaFiles;
    }

    if (!data.address) {
      throw globalCalls.badRequestError("Please add atleast one address.");
    } else {
      if (data.address != "") {
        let address = JSON.parse(data.address);
        let addressArray = [];
        address.forEach(innerdata => {
          if (data.address_id == null) {
            delete innerdata.address_id;
          }
          addressArray.push(innerdata);
        });

        data.address = addressArray;
      } else {
      }
    }
    if (req.files && req.files.brochure_file) {
      data.brochure = req.files.brochure_file[0].location;
    } 
    
    if (req.files && req.files.gallery_image) {
      req.files.gallery_image.map(file => {
        mediaFiles.push({ media_link: file.location, media_type: "gallery" });
      });
    }

    data.media = mediaFiles;
    delete data.brochure_file;
    delete data.gallery_image;
    let updateCompanyData = await transaction(company.knex(), trx => {
      return company.query(trx).upsertGraph(data, {
        relate: true,
        unrelate: false
      });
    });

    if (!updateCompanyData) {
      throw globalCalls.badRequestError("Something went wrong");
    }

    return globalCalls.okResponse(res, updateCompanyData, "");
  } catch (error) {
    throw globalCalls.badRequestError(error.message);
  }
};

/**
 * fetches skill set data
 * @param {*} req
 * @param {*} res
 */
const fetchSkillsetData = async (req, res) => {
  if (!req.params.id) {
    throw globalCalls.badRequestError("Invalid Request");
  }
  let domainList = await domain.query().select("domain_id", "domain", "status");

  let serviceList = await service
    .query()
    .select("service_id", "service_name", "status")
    .withGraphFetched("skills")
    .modifyGraph("skills", builder => {
      builder.select("skill_id", "skills", "status");
    });

  let company_id = base64decode(req.params.id);
  if(isNaN(parseInt(company_id)))
	  throw globalCalls.notFoundError("Error! You have passed invalid-id.");

  let company_data = await company
    .query()
    .select(
      "company_id",
      "domain_id",
      "domain_experience_in_months",
      "language"
    )
    .withGraphFetched("[services, skills]")
    .where("company_id", company_id)
    .first();

  let tips = await messageTips
    .query()
    .select("message")
    .where("module", "Company Profile Step #3").first();

  let responseData = {
    domainList,
    serviceList,
    tips,
    company_data
  };
  return globalCalls.okResponse(res, responseData, "");
};

/**
 * post company skillset
 * @param {*} req
 * @param {*} res
 */
const updateCompanySkillset = async (req, res) => {
  let data = req.body;

  if (!data.company_id) {
    throw globalCalls.badRequestError("Invalid Request.");
  }
  if (!data.domain_id) {
    throw globalCalls.badRequestError("Choose your working domain.");
  }

  if (!data.domain_experience_in_months) {
    throw globalCalls.badRequestError("Choose your experience");
  }

  if (!data.services) {
    throw globalCalls.badRequestError("Choose atleast one service");
  } else {
    if (data.services.length != 0) {
      let service = data.services;
      let serviceArray = [];
      service.forEach(innerdata => {
        if (innerdata.service_map_id == null) {
          delete innerdata.service_map_id;
        }
        serviceArray.push(innerdata);
      });
      data.services = serviceArray;
    }
  }

  if (!data.skills) {
    throw globalCalls.badRequestError("Choose atleast one skill.");
  } else {
    if (data.skills.length != 0) {
      let skills = data.skills;
      let skillsArray = [];
      skills.forEach(innerdata => {
        if (innerdata.skill_map_id == null) {
          delete innerdata.skill_map_id;
        }
        skillsArray.push(innerdata);
      });
      data.skills = skillsArray;
    }
  }

  if (!data.language) {
    throw globalCalls.badRequestError("Enter atleast one language.");
  }
  try {
    let companyUpdateSkills = await transaction(company.knex(), trx => {
      return company.query(trx).upsertGraph(data, {
        relate: true,
        unrelate: false
      });
    });

    globalCalls.okResponse(res, companyUpdateSkills, "");
  } catch (error) {
    globalCalls.badRequestError(error.message);
  }
};

/**
 * fetches company profile verification data
 * @param {*} req
 * @param {*} res
 */
const fetchCompanyVerificationForEdit = async (req, res) => {
  let data = req.body;

  if (!req.params.id) {
    throw globalCalls.badRequestError("Invalid Requests.");
  }

  let company_id = base64decode(req.params.id);
  if(isNaN(parseInt(company_id)))
  	throw globalCalls.notFoundError("Error! You have passed invalid-id.");

  let companyVerification = await company
    .query()
    .select(
      "company_id",
      "cin_number",
      "cin_number_file",
      // "company_registration",
      "cin_number_file_input_type",
      // "company_registration_file_type"
    )
    .withGraphFetched("[company_docx]")
    .modifyGraph("company_docx", (builder) => {
      builder.select("company_docx","company_docx_file_type")
    })
    .where("company_id", company_id)
    .first();

  let responseData = {
    companyVerification,
  };
  return globalCalls.okResponse(res, responseData, "");
};
/**
 * adds company verification data
 * @param {*} req
 * @param {*} res
 */
const updateCompanyVerification = async (req, res) => {
  let data = req.body;
  let company_docx = [];
  let fileTypeArray;
  
  if (!data.company_id) {
    throw globalCalls.badRequestError("Invalid Requests.");
  }

  if (!data.cin_number != "") {
    throw globalCalls.badRequestError("Please enter your CIN Number.");
  }

  try{

    if (req.files.cin_number_file_input) {
      data.cin_number_file = req.files.cin_number_file_input[0].location;
    } else if (!data.cin_number_file) {
      throw globalCalls.badRequestError("Please upload your CIN Number image.");
    }
  
    // if (req.files.company_registration_file) {
    //   data.company_registration = req.files.company_registration_file[0].location;
    // } else if (!data.company_registration) {
    //   throw globalCalls.badRequestError(
    //     "Please upload your Company Registration document for verification."
    //   );
    // }

    if(data.company_docx)
    {
      var j = 0;
      if(!data.company_docx_file_types)
        throw globalCalls.badRequestError("Please pass company registration file types.");
      fileTypeArray = JSON.parse(data.company_docx_file_types);

      let fileUrlArray = JSON.parse(data.company_docx);
      
      for (let k = 0; k < fileUrlArray.length; k++) {
        company_docx.push({
          company_docx: fileUrlArray[k],
          company_docx_file_type: fileTypeArray[k],
        });
      }
      data.company_docx = company_docx;      
    }
    // console.log(company_docx);

    var i = 0;
    if (req.files && req.files.company_docx_file) 
    {
      if(!data.company_docx_file_types)
        throw globalCalls.badRequestError("Please pass company registration file types.");
      fileTypeArray = JSON.parse(data.company_docx_file_types);
      
      req.files.company_docx_file.map((file) => {
        company_docx.push({
          company_docx: file.location,
          company_docx_file_type: fileTypeArray[i],
        });
        i++;
      });

      data.company_docx = company_docx;
      delete data.company_docx_file;
    }
    delete data.company_docx_file_types;

    let updateCompanyVerification = await transaction(company.knex(), trx => {
      return company.query(trx).upsertGraph(data, {
        relate: true,
        unrelate: false
      });
    });

    if (!updateCompanyVerification) {
      globalCalls.badRequestError("Something went wrong.");
    }

    return globalCalls.okResponse(res, updateCompanyVerification, "");
  } catch (error) {

    throw globalCalls.badRequestError(error.message);
  }
};

/**
 * Fetches company rules
 * @param {*} req
 * @param {*} res
 */
const fetchCompanyRule = async (req, res) => {
  if (!req.params.id) {
    throw globalCalls.badRequestError("Invalid Requests");
  }

  let company_id = base64decode(req.params.id);
  if(isNaN(parseInt(company_id)))
	  throw globalCalls.notFoundError("Error! You have passed invalid-id.");

  try {
    let companyRules = await companyQuestion
      .query()
      .select("question_id", "question");

    let companyHours = await company
      .query()
      .select("company_id", "opens_at", "closes_at").withGraphFetched("rules").modifyGraph("rules", builder =>{
        builder.select("rule_id", "answer", "question_id");
      })
      .where("company_id", company_id)
      .first();

    let tips = await messageTips
      .query()
      .select("message")
      .where("module", "Company Profile Step #5").first();

    let responseData = {
      companyRules,
      companyHours,
      tips
    };
    return globalCalls.okResponse(res, responseData, "");
  } catch (error) {
    throw globalCalls.badRequestError(error.message);
  }
};

/**
 * adds company rules
 * @param {*} req
 * @param {*} res
 */
const updateCompanyRules = async (req, res) => {
  let data = req.body;

  if (!data.company_id) {
    throw globalCalls.badRequestError("Invalid Request.");
  }
  if (!data.opens_at) {
    throw globalCalls.badRequestError("Please enter opening time.");
  }

  if (!data.closes_at) {
    throw globalCalls.badRequestError("Please enter closing time.");
  }

  if (!data.rules) {
    throw globalCalls.badRequestError("Please answer the questions");
  }

  let i = 0;
  data.rules.forEach((element)=>{
    if(element.rule_id == null){
      delete data.rules[i].rule_id;
    }
    i++;
  });

  try {

    let updateRules = await transaction(company.knex(), trx => {
      return company.query(trx).upsertGraph(data, {
        relate: true,
        unrelate: false
      });
    });

    if (!updateRules) {
      globalCalls.badRequestError("Something went wrong.");
    }

    

    return globalCalls.okResponse(
      res,
      updateRules,
      "Update successfully."
    );
  } catch (error) {
    console.log(error)
    throw globalCalls.badRequestError(error.message);
  }
};

const CompanyDetails = async(req, res)=>{
    try{    
        if(req.params.id)
        {
        var company_id = base64decode(req.params.id); 
        if(isNaN(parseInt(company_id)))
          throw globalCalls.notFoundError("Error! You have passed invalid-id.");
  
      let companyData = await company
        .query()
        .select(
          "company_id", "company_name", "logo", "incorporation_date", "director_name", "company_email_id", "company_website", "contact_number", "team_size", "fixed_budget", "description", "video_link", "brochure", "cin_number", "cin_number_file", "company_registration", "domain_id", "domain_experience_in_months", "created_by", "language", "status", "cin_number_file_input_type", "company_registration_file_type", "hourly_budget", "is_blocked", "opens_at", "closes_at"
        )
        .withGraphFetched("[domain, address, serviceLabels, skillLabel, media, testimonial, defaultUser, rules]")
        .modifyGraph("domain", (builder) => {
          builder.select("domain");
        })
        .modifyGraph("address", (builder) => {
          builder.select("location");
        })
        .modifyGraph("serviceLabels", (builder) => {
          builder.select("service_name");
        })
        .modifyGraph("skillLabel", (builder) => {
          builder.select("skills");
        })
        .modifyGraph("media", (builder) => {
          builder.select("media_link").where({ media_type: 'gallery' });
        })
        .modifyGraph("testimonial", (builder) => {
          builder.select("media_link").where({ media_type: 'testimonial' });
        })
        .modifyGraph("defaultUser", (builder) => {
          builder.select("first_name", "last_name", "email_id", "mobile_number", "designation", "dateofbirth", "gender");
        })
        .modifyGraph("rules", (builder) => {
          builder.withGraphFetched('question')
            .modifyGraph('question', builder => {
              builder.select('question', 'question_type')
            })
          builder.select("question_id", "answer");
        })
        .where("company_id", company_id);


      
      if (!companyData) {
        throw globalCalls.badRequestError("company not found.");
      } else {

        let responseData = {
          companyData,
        };

        return globalCalls.okResponse(res, responseData, "");
      }
    }
    else {
    }
  } catch (error) {
    console.log(error);
    throw globalCalls.badRequestError(error.message)
  }
}

const PortfolioList_companydetails = async (req, res) => {
  try {
    let msg = "Portfolio listed";
    let portfolio = [];

    let page = req.query.page ? req.query.page : 1;
    let limit = req.query.limit ? req.query.limit : CONFIG.per_page;
    let offset = req.query.offset ? req.query.offset : limit * (page - 1);

    if (req.params.id) {
      let company_id = base64decode(req.params.id);
      if(isNaN(parseInt(company_id)))
        throw globalCalls.notFoundError("Error! You have passed invalid-id.");
  
      portfolio = await CompanyPortfolio.query()
        .select(
          "portfolio_id",
          "project_name",
          "one_line_description",
          "project_logo",
          "timeline",
          "budget",
          "first_name",
          "last_name",
          "client_location",
          "client_email",
          "is_featured",
          // Knex.raw("(SELECT is_valid from reviews where portfolio_id = portfolio.portfolio_id) as verified"),
          Knex.raw("(SELECT Round (AVG ((SELECT AVG(stars) AS starsAvg FROM review_rating WHERE review_id = reviews.review_id)),2)  from reviews where portfolio_id = portfolio.portfolio_id and feedback_received = true group by portfolio_id) as overall_rating"),
          Knex.raw("(CASE WHEN status='published' THEN true ELSE false END) as is_verified"),
          Knex.raw("(CASE WHEN status='hidden' THEN true ELSE false END) as is_blocked_by_admin")
        )
        .where("portfolio.company_id", company_id)
        .orderBy("portfolio.portfolio_id", "desc", "is_featured");

      let totalCount = await CompanyPortfolio.query()
        .count("portfolio.portfolio_id")
        .leftJoin("reviews", function () {
          this.on(
            "portfolio.portfolio_id",
            "=",
            "reviews.portfolio_id"
          ).andOn("is_valid", Knex.raw(true));
        })
        .where("portfolio.company_id", company_id) //portfolio. added by zubear bczof company_id is ambiguous
        .where("status", "!=", "deleted")
        .first();

      if (!portfolio) {
        throw globalCalls.badRequestError("Invalid request!");
      }
      msg = "Portfolio details";

      let responseData = {
        portfolio,
        total: totalCount.count,
      };
      return globalCalls.okResponse(res, responseData, "Portfolio listed.");
    }
  } catch (e) {
    console.log("Error in", e);
    throw globalCalls.badRequestError(e.message);
  }
}

const PortfolioDetails_companydetails = async (req, res) => {
    try {
      let msg = "Portfolio listed";
      let portfolio = [];
      if (req.params.id) {
        let portfolio_id = base64decode(req.params.id);
        if(isNaN(parseInt(portfolio_id)))
          throw globalCalls.notFoundError("Error! You have passed invalid-id.");
          
        portfolio = await CompanyPortfolio.query()
          .select(
            "portfolio_id",
            "project_name",
            "one_line_description",
            "project_logo",
            "timeline",
            "budget",
            "first_name",
            "last_name",
            "client_location",
            "client_email",
            "is_featured"
          )
          .withGraphFetched("[gallery, portfolioSkills, reviews, links]")
          .modifyGraph("gallery", (builder) => {
            builder.select("gallery_image");
          })
          .modifyGraph("portfolioSkills", (builder) => {
            builder.select("skills");
          })
          .modifyGraph("reviews", (builder) => {
            builder
              .select("reviews", "feedback_received")
              .withGraphFetched("ratings")
              .modifyGraph("ratings", (builder) => {
                builder
                  .select("stars", "rating_title")
                  .innerJoinRelated("ratingPoint")
                  .orderBy("review_rating_id");
              }).where("is_valid", true);
          })
          .modifyGraph("links", (builder) => {
            builder.select("link", "link_for");
          })
          .where("portfolio_id", portfolio_id)
          .first();
        if (!portfolio) {
          throw globalCalls.badRequestError("Invalid request!");
        }
        msg = "Portfolio details";

        return globalCalls.okResponse(res, portfolio, "Portfolio details get successfully");
      }
      else
      {
        throw globalCalls.badRequestError("Portfolio id required.");
      } 
    } catch (e) {
      console.log(
        "Error in CompanyPortfolioController.DeletePortfolioProject",
        e
      );
      throw globalCalls.badRequestError(e.message);
    }
  }

  const ProjectList_companydetails = async(req, res)=>{
    try{
    let page = req.query.page ? req.query.page : 1;
    let limit = req.query.limit ? req.query.limit : globalConst.per_page;
    let offset = req.query.offset ? req.query.offset : limit * (page - 1);

    if (req.params.id) {
      let company_id = base64decode(req.params.id);
      if(isNaN(parseInt(company_id)))
	      throw globalCalls.notFoundError("Error! You have passed invalid-id.");

      let projectData = await project.query()
        .select(
          "project_id", "project_name", "one_line_description", "about_the_project", "budget", "payment_type", "location", "expected_timeline", "ideal_team_size", "created_at", "status", "completion_datetime",
          "archived_by_type",
          Knex.raw("(SELECT Round (AVG ((SELECT AVG(stars) AS starsAvg FROM review_rating WHERE review_id = reviews.review_id)),2)  from reviews where portfolio_id = (select portfolio_id from portfolio where belongs_to_project=project.project_id limit 1 offset 0 ) and feedback_received = true group by portfolio_id) as overall_rating"),
          //Knex.raw("(SELECT CASE WHEN starsAvg IS NULL THEN 0 ELSE starsAvg END FROM (SELECT ROUND(AVG(stars),2) AS starsAvg FROM review_rating WHERE review_id = reviews.review_id) AS averageStars) AS rating_avg"))
        )
        .withGraphFetched("[servicesLabels, domain]")
        .modifyGraph("servicesLabels", (builder) => {
          builder.select('service_name')
        })
        .modifyGraph("domain", (builder) => {

          builder.select("domain");
        })
        .where((builder) => {
          builder.where("assigned_to_company", company_id);
        })
        .orderBy('project_id', 'DESC')
        .offset(offset)
        .limit(limit);
      // .runBefore((result, builder) =>{
      //   console.log(builder.toKnexQuery().toQuery());
      //   return result;
      // });

      let totalCount = await project
        .query()
        .count()
        .where((builder) => {
          builder.where("assigned_to_company", company_id);
        })
        .first();

      if (!projectData) {
        throw globalCalls.badRequestError("No Project found.");
      } else {

        let responseData = {
          projectData,
          total: totalCount.count
        };

        return globalCalls.okResponse(res, responseData, "");
      }
    }
    else {
      throw globalCalls.badRequestError("company id required.");
    }
  } catch (error) {
    throw globalCalls.badRequestError(error.message)
  }
}

const ApproveCompany = async (req, res) => {
  if (req.params.id) {
    var data = {};
    var company_id = base64decode(req.params.id);
    if(isNaN(parseInt(company_id)))
    	throw globalCalls.notFoundError("Error! You have passed invalid-id.");

    let companyData = await company.query().select("company_id").where("company_id", company_id);
    if (!companyData) {
      throw globalCalls.badRequestError("company not found.");
    }

    data.company_id = company_id;
    data.status = 'approved';

    let verified = await company.query().upsertGraph(data).returning("*");
    if (!verified) {
      throw globalCalls.badRequestError("something went wrong.");
    } else {
      return globalCalls.okResponse(res, verified, "Successfully verified.");
    }
  }
  else {
  }
}

const RejectCompany = async (req, res) => {
  if (req.params.id) {
    var data = {};
    var company_id = base64decode(req.params.id);
    if(isNaN(parseInt(company_id)))
	    throw globalCalls.notFoundError("Error! You have passed invalid-id.");

    let companyData = await company.query().select("company_id").where("company_id", company_id);
    if (!companyData) {
      throw globalCalls.badRequestError("company not found.");
    }

    data.company_id = company_id;
    data.status = 'rejected';

    let verified = await company.query().upsertGraph(data).returning("*");
    if (!verified) {
      throw globalCalls.badRequestError("something went wrong.");
    } else {
      return globalCalls.okResponse(res, verified, "Company rejected successfully.");
    }
  }
  else {
  }
}

const PublishUnpublishPortfolio = async (req, res) => {
  try {
    let msgdata;
    let data = req.body;

    let checkData = await CompanyPortfolio.query().select("status").where("portfolio_id", data.portfolio_id);
    if (!checkData) {
      throw globalCalls.badRequestError("Portfolio not found.");
    }
    else {
      // if(checkData[0].status=='published')
      if (data.status == 'published') {
        // data.status='hidden';
        msgdata = "Portfolio has published successfully";
      }
      // else 
      if (data.status == 'hidden') {
        // data.status='published';
        msgdata = "Portfolio has unpublished successfully";
      }
    }

    let verified = await CompanyPortfolio.query().upsertGraph(data).returning("*");
    if (!verified) {
      throw globalCalls.badRequestError("something went wrong.");
    } else {
      return globalCalls.okResponse(res, verified, msgdata);
    }

  } catch (error) {
    console.log(error);
    throw globalCalls.badRequestError(error.message)
  }
}

const getRecommendedProject_companydetails = async(req, res)=>{
  try 
  {
    if(req.params.id)
    {
    let company_id = base64decode(req.params.id);
    if(isNaN(parseInt(company_id)))
	    throw globalCalls.notFoundError("Error! You have passed invalid-id.");
    
    let page = req.query.page ? req.query.page : 1;
    let limit = req.query.limit ? req.query.limit : globalConst.per_page;
    let offset = req.query.offset ? req.query.offset : limit * (page - 1);

    
    let matchedData = await projectRecommendedCompanies.query()
      .select("project_id", "company_id")
      .withGraphFetched("[project]")
      .modifyGraph("project", (builder) => {
        builder.withGraphFetched("[services,portfolio]")
        .modifyGraph("services", (builder) => {
          builder.withGraphFetched('service_details').modifyGraph('service_details', builder => {
            builder.select('service_id', 'service_name')
          })
          builder.select("service_id", "project_service_id");
        })
        .modifyGraph("portfolio", (builder) => {
          builder
            .select("portfolio_id", "status")
            .withGraphFetched("reviews")
            .modifyGraph("reviews", (builder) => {
              builder
                .select(
                  "reviews",
                  "feedback_received",
                  Knex.raw(
                    "(SELECT CASE WHEN starsAvg IS NULL THEN 0 ELSE starsAvg END FROM (SELECT ROUND(AVG(stars),2) AS starsAvg FROM review_rating WHERE review_id = reviews.review_id) AS averageStars) AS rating_avg"
                  )
                )
                .withGraphFetched("ratings")
                .modifyGraph("ratings", (builder) => {
                  builder
                    .select("stars", "rating_title")
                    .innerJoinRelated("ratingPoint")
                    .orderBy("review_rating_id");
                })
                .where("is_valid", true);
            })
            .where((builder) => {
              builder.where("status", "submitted").orWhere("status", "published");
            });
        })
        .select("project_id", "project_name", "one_line_description", "budget", "payment_type", "expected_timeline", "ideal_team_size", "created_at")
      })
      .where({
        company_id: company_id,
      })
      .orderBy('recommended_id', 'DESC')
      .offset(offset)
      .limit(limit);

      let totalCount = await projectRecommendedCompanies
      .query()
      .count()
      .where((builder) => {
        builder.where("company_id", company_id);
      })
      .first();

      if(!matchedData) {
      throw globalCalls.badRequestError("No Project found.");
      } else {

          let responseData = {
            matchedData,
            total: totalCount.count
          };

          return globalCalls.okResponse(res, responseData, "");
      }
    }
     else
    {
      throw globalCalls.badRequestError("company id required.");
    }
  } catch (error) {
    console.log(error);
    throw globalCalls.badRequestError(error.message);
  }
}

const getRatingList_companydetails = async(req, res)=>{
  try
  {
    if(req.params.id)
    {
      let company_id = base64decode(req.params.id);
      if(isNaN(parseInt(company_id)))
	      throw globalCalls.notFoundError("Error! You have passed invalid-id.");

      let page    = (req.query.page) ? req.query.page : 1;
      let limit   = req.query.limit ? req.query.limit : CONFIG.per_page;
      let offset  = req.query.offset ? req.query.offset : limit * (page - 1);
      let reviewsCount;

      let reviewsData = await reviews.query()
      .select("review_id", "reviews", "review_verification_link", "link_expire", "feedback_received", Knex.raw("(SELECT CASE WHEN starsAvg IS NULL THEN 0 ELSE starsAvg END FROM (SELECT ROUND(AVG(stars),2) AS starsAvg FROM review_rating WHERE review_id = reviews.review_id) AS averageStars) AS rating_avg"))
      .withGraphFetched('[portfolio, ratings]')
      .modifyGraph("portfolio", builder =>{
          builder.select("portfolio_id", "project_name", "project_logo", "one_line_description", "timeline", "budget", "first_name", "last_name", "client_email")
      })
      .modifyGraph('ratings', builder => {
          builder.select('review_rating.review_rating_id', 'review_rating.stars', 'ratingPoint.rating_title')
          .joinRelated('[ratingPoint]');
      })
      .where("company_id", company_id).where("is_valid", true)
      // .limit(limit)
      // .offset(offset)
      .orderBy('review_id', 'desc');
      
      reviewsCount = await reviews.query().count("review_id").where("company_id", company_id).where("is_valid", true).first();

      return globalCalls.okResponse(res, {reviews:reviewsData, total: reviewsCount.count}, "Review list get successfully");
    }
    else
    {
      throw globalCalls.badRequestError("company id required.");
    }
  }
  catch(e)
  {
    console.log("Error in AdmincompanyController.Reviews", e);
    throw globalCalls.badRequestError(e.message);
  }
}

const PublishUnpublishCompanyCertificates = async(req, res)=>{
  try{
          let msgdata;
          let data = req.body;
          
          let checkData = await companyCertificates.query().select("is_hidden").where("certificate_id", data.certificate_id);
          if(!checkData) {
              throw globalCalls.badRequestError("Company certificate not found.");
          }
          else
          {
              // if(checkData[0].is_hidden=='true')
              if(data.is_hidden==false)
              {
                  // data.status='hidden';
                  msgdata="Company certificate has published successfully";
              }
              // else 
              if(data.is_hidden==true)
              {
                  // data.status='published';
                  msgdata="Company certificate has unpublished successfully";
              }            
          }    

          let verified = await companyCertificates.query().upsertGraph(data).returning("*");
          if(!verified) {
              throw globalCalls.badRequestError("something went wrong.");
          } else {
              return globalCalls.okResponse(res,verified,msgdata);
          }
      
  } catch (error){
  console.log(error);
  throw globalCalls.badRequestError(error.message)
  }
}

const PublishUnpublishCompanyAwards = async(req, res)=>{
  try{
          let msgdata;
          let data = req.body;
          
          let checkData = await companyAwards.query().select("is_hidden").where("award_id", data.award_id);
          if(!checkData) {
              throw globalCalls.badRequestError("Company awards not found.");
          }
          else
          {
              // if(checkData[0].is_hidden=='true')
              if(data.is_hidden==false)
              {
                  // data.status='hidden';
                  msgdata="Company awards has published successfully";
              }
              // else 
              if(data.is_hidden==true)
              {
                  // data.status='published';
                  msgdata="Company awards has unpublished successfully";
              }            
          }    

          let verified = await companyAwards.query().upsertGraph(data).returning("*");
          if(!verified) {
              throw globalCalls.badRequestError("something went wrong.");
          } else {
              return globalCalls.okResponse(res,verified,msgdata);
          }
      
  } catch (error){
  console.log(error);
  throw globalCalls.badRequestError(error.message)
  }
}

const PublishUnpublishCompanyFeatured = async(req, res)=>{
  try{
          let msgdata;
          let data = req.body;
          
          let checkData = await companyFeatured.query().select("is_hidden").where("feature_id", data.feature_id);
          if(!checkData) {
              throw globalCalls.badRequestError("Company featured not found.");
          }
          else
          {
              // if(checkData[0].is_hidden=='true')
              if(data.is_hidden==false)
              {
                  // data.status='hidden';
                  msgdata="Company featured has published successfully";
              }
              // else 
              if(data.is_hidden==true)
              {
                  // data.status='published';
                  msgdata="Company featured has unpublished successfully";
              }            
          }    

          let verified = await companyFeatured.query().upsertGraph(data).returning("*");
          if(!verified) {
              throw globalCalls.badRequestError("something went wrong.");
          } else {
              return globalCalls.okResponse(res,verified,msgdata);
          }
      
  } catch (error){
  console.log(error);
  throw globalCalls.badRequestError(error.message)
  }
}

const PublishUnpublishCompanySuccessStory = async(req, res)=>{
  try{
          let msgdata;
          let data = req.body;
          
          let checkData = await companySuccessStory.query().select("is_hidden").where("story_id", data.story_id);
          if(!checkData) {
              throw globalCalls.badRequestError("Company featured not found.");
          }
          else
          {
              // if(checkData[0].is_hidden=='true')
              if(data.is_hidden==false)
              {
                  // data.status='hidden';
                  msgdata="Company featured has published successfully";
              }
              // else 
              if(data.is_hidden==true)
              {
                  // data.status='published';
                  msgdata="Company featured has unpublished successfully";
              }            
          }    

          let verified = await companySuccessStory.query().upsertGraph(data).returning("*");
          if(!verified) {
              throw globalCalls.badRequestError("something went wrong.");
          } else {
              return globalCalls.okResponse(res,verified,msgdata);
          }
      
  } catch (error){
  console.log(error);
  throw globalCalls.badRequestError(error.message)
  }
}

const getCompanyFaq_companydetails = async (req, res) => {
  try {
    if(req.params.id)
    {
      let company_id = base64decode(req.params.id);
      if(isNaN(parseInt(company_id)))
        throw globalCalls.notFoundError("Error! You have passed invalid-id.");
        
      let faqs = await companyFaq.query()
        .select("faq_id", "question_id", "answer")
        .withGraphFetched("[question]")
        .modifyGraph("question", (builder) => {
          builder.select('question', 'is_default')
        })
        .where({company_id:company_id})
        
    let responseData = {
      faqs,
    }
    return globalCalls.okResponse(res, responseData, "");
    }
    else
    {
      throw globalCalls.badRequestError("company id required.");
    }
  } catch (e) {
    console.log("Error in CompanyFaqController.GetComapnyFaq", e);
    throw globalCalls.badRequestError(e.message);
  }
}

const getCompanyMembershipHistory = async (req, res) => {
  try {
    if(req.params.id)
    {
      let company_id = base64decode(req.params.id);
      if(isNaN(parseInt(company_id)))
        throw globalCalls.notFoundError("Error! You have passed invalid-id.");
        
      let membershipData = await companyMembershipHistory.query()
        .select("company_membership_history_id", "membership_plan_amount","start_date","end_date","status")
        .withGraphFetched("[membership_plans]")
        .modifyGraph("membership_plans", (builder) => {
          builder.select("plan_id", "plan_name", "is_paid")
        })
        .where({company_id:company_id})
        
    let responseData = {
      membershipData,
    }
    return globalCalls.okResponse(res, responseData, "");
    }
    else
    {
      throw globalCalls.badRequestError("company id required.");
    }
  } catch (e) {
    console.log("Error in CompanyFaqController.GetComapnyFaq", e);
    throw globalCalls.badRequestError(e.message);
  }
}

const updateFAQ_companydetails = async(req, res)=>{
  try{
          let msgdata;
          let data = req.body;
          
          let checkData = await companyFaq.query().select("answer").where("faq_id", data.faq_id);
          if(!checkData) {
              throw globalCalls.badRequestError("Company FAQ not found.");
          }
              

          let verified = await companyFaq.query().upsertGraph(data).returning("*");
          if(!verified) {
              throw globalCalls.badRequestError("something went wrong.");
          } else {
              return globalCalls.okResponse(res,verified,"FAQ Uupdate successfully");
          }
      
  } catch (error){
  console.log(error);
  throw globalCalls.badRequestError(error.message)
  }
}

module.exports = {
    ListCompany,
    fetchCompanyInformationData,
    updateCompanyInformation,
    fetchAboutCompany,
    updateAboutCompany,
    fetchSkillsetData,
    updateCompanySkillset,
    fetchCompanyVerificationForEdit,
    updateCompanyVerification,
    fetchCompanyRule,
    updateCompanyRules,
    CompanyDetails,
    PortfolioList_companydetails,
    PortfolioDetails_companydetails,
    ProjectList_companydetails,
    ApproveCompany,
    RejectCompany,
    PublishUnpublishPortfolio,
    getRecommendedProject_companydetails,
    getRatingList_companydetails,
    PublishUnpublishCompanyCertificates,
    PublishUnpublishCompanyAwards,
    PublishUnpublishCompanyFeatured,
    PublishUnpublishCompanySuccessStory,
    getCompanyFaq_companydetails,
    updateFAQ_companydetails,
    getCompanyMembershipHistory
}
