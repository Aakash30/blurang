"use strict";
const globalCalls = require("../settings/functions");
const Users = require("../models/UserLogin");
const UserAuth = require("../models/UserAuth");
const validator = require("validator");
const CONFIG = require("../settings/constants");
const bcrypt = require("bcryptjs");

const EMAIL = require("../middlewares/send-email");
const CompanyProgress = require("../models/CompanyProgress");

const projectProfile = require("./client/ProjectController").postProject;
/**
 * signup
 * @param {*} req
 * @param {*} res
 */
const signup = async (req, res) => {
  let data = req.body;

  if (!data.first_name) {
    throw globalCalls.badRequestError("Please fill first name.");
  }

  if (!data.last_name) {
    throw globalCalls.badRequestError("Please fill last name.");
  }

  if (!data.email_id) {
    throw globalCalls.badRequestError("Please fill emai-id.");
  }

  // if (!data.mobile_number) {
  //   throw globalCalls.badRequestError("Please fill mobile number.");
  // }

  if (!data.password) {
    throw globalCalls.badRequestError("Please add your password.");
  }

  if (!data.user_type) {
    throw globalCalls.badRequestError("Invalid request");
  }

  let verify_token = globalCalls.generateToken(15) + "-" + Date.now();
  verify_token = await bcrypt.hash(verify_token, 10);
  verify_token = verify_token.replace(/\.|\/+/g, "");

  data.verification = verify_token;

  let postProject;
  if (data.projectData) {
    postProject = data.projectData;
    delete data.projectData;
  }
  let signupUser;
  try {
    if (data.user_type == "client") {
      data.created_at = new Date().toISOString();
    }
    signupUser = await Users.query().upsertGraph(data).returning("*");

    if (signupUser) {
      let link = "";
      if (data.user_type == "company") {
        link = CONFIG.clientBusinessUrl + "auth/verify/" + verify_token;
      } else {
        link = CONFIG.clientUrl + "auth/verify/" + verify_token;
      }
      /**
       * send email functionality here
       */

      EMAIL.sendEmail(
        data.email_id,
        "Confirm Your Email",
        { name: data.first_name, link: link },
        "verify_email.pug"
      );

      // generate authenticated data
      let auth_token = await signupUser.getJWT();

      let token = await UserAuth.query()
        .insert({
          token: auth_token,
          last_login_at: new Date().toISOString(),
          user_id: signupUser.user_id,
        })
        .returning("*");

      let response = {};
      if (token && data.user_type == "company") {
        response = {
          verified: signupUser.is_user_verified,
          company: null,
          profile_pic: signupUser.profile_pic,
        };
      } else {
        if (postProject) {
          postProject.user_id = signupUser.user_id;
          response = await projectProfile(postProject);
        }
        response["profile_pic"] = signupUser.profile_pic;
        response["verified"] = signupUser.is_user_verified;
        response["name"] = signupUser.first_name;

        console.log(response);
      }

      res.setHeader("Content-Type", "application/json");
      res.setHeader("Authorization", "Bearer " + auth_token);
      res.setHeader("Access-Control-Expose-Headers", "Authorization");

      return globalCalls.okResponse(res, response, "signup successfully.");
    } else {
      throw globalCalls.badRequestError("Something went wrong.");
    }
  } catch (error) {
    console.log(error);
    throw globalCalls.badRequestError(error.message);
  }
};

/**
 * login
 * @param {*} req
 * @param {*} res
 */
const loginUser = async (req, res) => {
  let data = req.body;

  if (!data.email_id) {
    throw globalCalls.badRequestError("Please enter your registered email id.");
  }

  if (!validator.isEmail(data.email_id)) {
    throw globalCalls.badRequestError("Please enter a valid email id.");
  }

  if (!data.password) {
    throw globalCalls.badRequestError("Please enter your password.");
  }

  if (!data.user_type) {
    throw globalCalls.badRequestError("Invalid request");
  }

  try {
    let loginQuery;
    if (data.user_type == "company") {
      loginQuery = Users.query()
        .select(
          "user_id",
          "first_name",
          "last_name",
          "email_id",
          "password",
          "user_type",
          "is_user_verified",
          "default_company",
          "profile_pic"
        )
        .withGraphFetched("defaultCompany")
        .modifyGraph("defaultCompany", "getNameAndLogo")
        .where("email_id", data.email_id)
        .where("user_type", data.user_type)
        .first();
    } else {
      loginQuery = Users.query()
        .select(
          "user_id",
          "first_name",
          "last_name",
          "email_id",
          "password",
          "user_type",
          "is_user_verified",
          "default_company",
          "profile_pic"
        )
        .where("email_id", data.email_id)
        .where("user_type", data.user_type)
        .first();
    }
    let userLogin = await loginQuery;

    if (!userLogin) {
      throw globalCalls.badRequestError("Email id is not registered.");
    }
    if (!(await userLogin.comparePassword(data.password))) {
      throw globalCalls.badRequestError("Please enter valid credentials.");
    }

    // generate authenticated data
    let auth_token = await userLogin.getJWT();

    let token = await UserAuth.query()
      .insert({
        token: auth_token,
        last_login_at: new Date().toISOString(),
        user_id: userLogin.user_id,
      })
      .returning("*");

    let response = {};
    if (data.user_type == "company") {
      let companyProgressBar = await CompanyProgress
        .query()
        .select("progress")
        .where("company_id", userLogin.default_company);

      let progressBar = [];
      if (companyProgressBar.length > 0) {
        companyProgressBar.forEach((value) => {
          progressBar.push(value.progress);
        });
      }
      response = {
        verified: userLogin.is_user_verified,
        company: userLogin.defaultCompany,
        profile_pic: userLogin.profile_pic,
        progressBar
      };
    } else {
      // console.log((data.projectData))
      if (data.projectData) {
        data.projectData.user_id = userLogin.user_id;
        response = await projectProfile(data.projectData);
      }
      console.log(response);
      response["profile_pic"] = userLogin.profile_pic;
      response["verified"] = userLogin.is_user_verified;
      response["name"] = userLogin.first_name;
      console.log(response);
    }
    res.setHeader("Content-Type", "application/json");
    res.setHeader("Authorization", "Bearer " + auth_token);
    res.setHeader("Access-Control-Expose-Headers", "Authorization");

    return globalCalls.okResponse(res, response, "Login Successful");
  } catch (error) {
    console.log(error);
    throw globalCalls.badRequestError(error.message);
  }
};

/**
 * resends activation link
 * @param {*} req
 * @param {*} res
 */
const resendVerificationLink = async (req, res) => {
  if (!req.user.user_id) {
    throw globalCalls.badRequestError("Invalid Requests.");
  }

  let verify_token = globalCalls.generateToken(15) + "-" + Date.now();
  verify_token = await bcrypt.hash(verify_token, 10);
  verify_token = verify_token.replace(/\.|\/+/g, "");

  let verification = verify_token;

  let resendLink = await Users.query()
    .update({ verification: verification })
    .where("user_id", req.user.user_id);

  if (resendLink) {
    let email = req.user.email_id;

    let link;
    if (req.user.user_type == "company") {
      link = CONFIG.clientBusinessUrl + "auth/verify/" + verification;
    } else {
      link = CONFIG.clientUrl + "auth/verify/" + verification;
    }
    /**
     * send email functionality here
     */
    EMAIL.sendEmail(
      req.user.email_id,
      "Confirm Your Email",
      { name: req.user.first_name, link: link },
      "verify_email.pug"
    );
    console.log(req.user.email_id);
  }

  return globalCalls.okResponse(
    res,
    {},
    "An Activation Link has been sent to your registered email id."
  );
};

/**
 * activate user
 * @param {*} req
 * @param {*} res
 */
const activateUserBusiness = async (req, res) => {
  try {
    if (!req.params.verify) {
      throw globalCalls.badRequestError("Invalid Requests.");
    }

    let verifyUser = await Users.query()
      .select("user_id")
      .where("verification", req.params.verify)
      .first();

    if (!verifyUser) {
      throw globalCalls.badRequestError(
        "Either invalid link or user is already activated."
      );
    }

    let updateUser = await Users.query()
      .update({
        verification: "",
        is_user_verified: true,
        user_status: "active",
      })
      .where("user_id", verifyUser.user_id);

    if (!updateUser) {
      throw globalCalls.badRequestError(
        "Something went wrong. User could not be activated."
      );
    }

    return globalCalls.okResponse(
      res,
      updateUser,
      "Congratulations! Your account has been activated now."
    );
  } catch (error) {
    throw globalCalls.badRequestError(error.message);
  }
};

/**
 * Forgot Password Link
 * @param {*} req
 * @param {*} res
 */
const forgotPasswordRecoveryLink = async (req, res) => {
  let data = req.body;

  if (!data.email_id) {
    throw globalCalls.badRequestError(
      "Please enter your regiestered email id."
    );
  }

  if (!data.user_type) {
    throw globalCalls.badRequestError("Invalid Requests.");
  }

  let checkUser = await Users.query()
    .select("user_id", "user_status")
    .where("email_id", data.email_id)
    .where("user_type", data.user_type)
    .first();

  if (!checkUser) {
    throw globalCalls.badRequestError("This is not a registered email id.");
  }

  if (checkUser.user_status == "blocked") {
    throw globalCalls.badRequestError(
      "Your account has been blocked. Please contact admin."
    );
  }

  let verify_token = globalCalls.generateToken(15) + "-" + Date.now();
  verify_token = await bcrypt.hash(verify_token, 10);
  verify_token = verify_token.replace(/\.|\/+/g, "");

  let resendLink = await Users.query()
    .update({ verification: verify_token })
    .where("user_id", checkUser.user_id);

  if (resendLink) {
    let email = data.email_id;
    let link = "";
    if (data.user_type == "company") {
      link = CONFIG.clientBusinessUrl + "auth/recoverPassword/" + verify_token;
    } else {
      link = CONFIG.clientUrl + "auth/recoverPassword/" + verify_token;
    }
    /**
     * send email functionality here
     */

    EMAIL.sendEmail(
      email,
      "Forgot Password",
      { link: link },
      "forgot_password_email.pug"
    );
  }
  return globalCalls.okResponse(
    res,
    {},
    "We have sent you password recovery instructions on your email."
  );
};

/**
 * verify the link
 * @param {*} req
 * @param {*} res
 */
const verifyAccountForPassword = async (req, res) => {
  try {
    if (!req.params.verify) {
      throw globalCalls.badRequestError("Invalid Requests.");
    }

    let verifyUser = await Users.query()
      .select("user_id")
      .where("verification", req.params.verify)
      .first();

    if (!verifyUser) {
      throw globalCalls.badRequestError("Either invalid link or is expired.");
    }
    return globalCalls.okResponse(res, {}, "Verified");
  } catch (error) {
    throw globalCalls.badRequestError(error.message);
  }
};

/**
 * recover password for forgot passwords
 * @param {*} req
 * @param {*} res
 */

const recoverMyPassword = async (req, res) => {
  if (!req.params.verify) {
    throw globalCalls.badRequestError("Invalid Requests.");
  }

  let verifyUser = await Users.query()
    .select("user_id")
    .where("verification", req.params.verify)
    .first();

  if (!verifyUser) {
    throw globalCalls.badRequestError("Invalid link");
  }

  let data = req.body;

  if (!data.password) {
    throw globalCalls.badRequestError("Please enter password.");
  }

  let updatePassword = await Users.query()
    .update({
      password: data.password,
      user_status: "active",
      verification: "",
      is_user_verified: true,
    })
    .where("user_id", verifyUser.user_id);

  if (!updatePassword) {
    throw globalCalls.badRequestError("Something went wrong.");
  }

  return globalCalls.okResponse(
    res,
    updatePassword,
    "Password has been updated successfully."
  );
};

const ChangePassword = async (req, res) => {
  console.log("asdasd");
  let user = await Users.query().where("user_id", req.user.user_id).first();
  if (!req.body.newPassword) {
    throw badRequestError("New Password is required");
  }

  if (await user.comparePassword(req.body.oldPassword)) {
    let user = await Users.query().patchAndFetchById(req.user.user_id, {
      password: req.body.newPassword,
      // restaurant_password: req.body.newPassword
    });
    // return okResponse(res, user);

    if (!user) {
      throw globalCalls.badRequestError("Something went wrong.");
    }

    return globalCalls.okResponse(
      res,
      user,
      "Password has been chnaged successfully."
    );
  } else {
    throw globalCalls.badRequestError("Old Password is Incorrect!");
  }
};

const updateProfile = async (req, res) => {
  let data = req.body;
  // console.log(req.body);
  if (!data.first_name) {
    throw globalCalls.badRequestError("Please fill up first name.");
  }
  if (!data.mobile_number) {
    throw globalCalls.badRequestError("Please fill up mobile no.");
  }

  if (req.file) {
    if (req.file.fieldname == "profile_pic_file") {
      data.profile_pic = req.file.location;
    }
    delete data["profile_pic_file"];
  }

  if (data.email_id) {
    delete data["email_id"];
  }

  try {
    data.user_id = req.user.user_id;

    let updated = await Users.query().upsertGraph(data).returning("*");
    if (!updated) {
      throw globalCalls.badRequestError("something went wrong.");
    } else {
      return globalCalls.okResponse(
        res,
        updated,
        "Profile updated successfully."
      );
    }
  } catch (error) {
    throw globalCalls.badRequestError(error.message);
  }
};

const getProfile = async (req, res) => {
  var user_id = req.user.user_id;
  let userData = await Users.query()
    .select(
      "first_name",
      "last_name",
      "mobile_number",
      "designation",
      "profile_pic",
      "email_id",
      "dateofbirth",
      "gender"
    )
    .where("user_id", user_id);

  if (!userData) {
    throw globalCalls.badRequestError("User not found.");
  } else {
    let responseData = {
      userData,
    };

    return globalCalls.okResponse(res, responseData, "");
  }
};

const ChangePasswordClient = async (req, res) => {
  console.log("asdasd");
  let user = await Users.query().where("user_id", req.user.user_id).first();
  if (!req.body.newPassword) {
    throw badRequestError("New Password is required");
  }

  if (await user.comparePassword(req.body.oldPassword)) {
    let user = await Users.query().patchAndFetchById(req.user.user_id, {
      password: req.body.newPassword,
      // restaurant_password: req.body.newPassword
    });
    // return okResponse(res, user);

    if (!user) {
      throw globalCalls.badRequestError("Something went wrong.");
    }

    return globalCalls.okResponse(
      res,
      user,
      "Password has been chnaged successfully."
    );
  } else {
    throw globalCalls.badRequestError("Old Password is Incorrect!");
  }
};

const updateProfileClient = async (req, res) => {
  let data = req.body;
  // console.log(req.body);
  if (!data.first_name) {
    throw globalCalls.badRequestError("Please fill up first name.");
  }
  if (!data.mobile_number) {
    throw globalCalls.badRequestError("Please fill up mobile no.");
  }

  if (req.file) {
    if (req.file.fieldname == "profile_pic_file") {
      data.profile_pic = req.file.location;
    }
    delete data["profile_pic_file"];
  }

  if (data.email_id) {
    delete data["email_id"];
  }

  try {
    data.user_id = req.user.user_id;

    let updated = await Users.query().upsertGraph(data).returning("*");
    if (!updated) {
      throw globalCalls.badRequestError("something went wrong.");
    } else {
      return globalCalls.okResponse(
        res,
        updated,
        "Profile updated successfully."
      );
    }
  } catch (error) {
    throw globalCalls.badRequestError(error.message);
  }
};

const getProfileClient = async (req, res) => {
  var user_id = req.user.user_id;
  let userData = await Users.query()
    .select(
      "first_name",
      "last_name",
      "mobile_number",
      "designation",
      "profile_pic",
      "email_id",
      "dateofbirth",
      "location",
      "gender"
    )
    .where("user_id", user_id);

  if (!userData) {
    throw globalCalls.badRequestError("user not found.");
  } else {
    let responseData = {
      userData,
    };

    return globalCalls.okResponse(res, responseData, "");
  }
};

const socialLogin = async (req, res) => {
  let data = req.body;

  if (!data.email_id) {
    throw globalCalls.badRequestError("Please enter your registered email id.");
  }

  if (!validator.isEmail(data.email_id)) {
    throw globalCalls.badRequestError("Please enter a valid email id.");
  }

  if (!data.user_type) {
    throw globalCalls.badRequestError("Invalid request");
  }

  try {
    ///////social login start////////
    if (
      !data.email_id ||
      data.email_id.trim() == "" ||
      !data.google_id ||
      !data.first_name ||
      data.first_name.trim() == "" ||
      !data.last_name ||
      data.last_name.trim() == ""
    ) {
      throw globalCalls.badRequestError(
        "Not suffiecient permission. Can not login"
      );
    }

    let checkUser = await Users.query()
      .select("user_id", "user_type")
      .where("email_id", data.email_id)
      .first();
    if (checkUser) {
      data.user_id = checkUser.user_id;
      delete data.first_name;
      delete data.last_name;
    }
    let dataUpsertLogin = await Users.query().upsertGraph(data).returning("*");

    if (!dataUpsertLogin) {
      throw globalCalls.badRequestError("something went wrong");
    }
    ///////social login end////////

    let loginQuery;
    if (data.user_type == "company") {
      loginQuery = Users.query()
        .select(
          "user_id",
          "first_name",
          "last_name",
          "email_id",
          "password",
          "user_type",
          "is_user_verified",
          "default_company",
          "profile_pic"
        )
        .withGraphFetched("defaultCompany")
        .modifyGraph("defaultCompany", "getNameAndLogo")
        .where("email_id", data.email_id)
        .where("user_type", data.user_type)
        .first();
    } else {
      loginQuery = Users.query()
        .select(
          "user_id",
          "first_name",
          "last_name",
          "email_id",
          "password",
          "user_type",
          "is_user_verified",
          "default_company",
          "profile_pic"
        )
        .where("email_id", data.email_id)
        .where("user_type", data.user_type)
        .first();
    }
    let userLogin = await loginQuery;

    if (!userLogin) {
      throw globalCalls.badRequestError("Email id is not registered.");
    }

    // generate authenticated data
    let auth_token = await userLogin.getJWT();

    let token = await UserAuth.query()
      .insert({
        token: auth_token,
        last_login_at: new Date().toISOString(),
        user_id: userLogin.user_id,
      })
      .returning("*");

    let response = {};
    if (data.user_type == "company") {
      response = {
        verified: userLogin.is_user_verified,
        company: userLogin.defaultCompany,
        profile_pic: userLogin.profile_pic,
      };
    } else {
      // console.log((data.projectData))
      if (data.projectData) {
        data.projectData.user_id = userLogin.user_id;
        response = await projectProfile(data.projectData);
      }
      console.log(response);
      response["profile_pic"] = userLogin.profile_pic;
      response["verified"] = userLogin.is_user_verified;
      response["name"] = userLogin.first_name;
    }
    res.setHeader("Content-Type", "application/json");
    res.setHeader("Authorization", "Bearer " + auth_token);
    res.setHeader("Access-Control-Expose-Headers", "Authorization");

    return globalCalls.okResponse(res, response, "Login Successful");
  } catch (error) {
    console.log(error);
    throw globalCalls.badRequestError(error.message);
  }
};

const logout = async (req, res) => {};

module.exports = {
  signup,
  loginUser,
  resendVerificationLink,
  activateUserBusiness,
  forgotPasswordRecoveryLink,
  verifyAccountForPassword,
  recoverMyPassword,
  ChangePassword,
  updateProfile,
  getProfile,
  ChangePasswordClient,
  updateProfileClient,
  getProfileClient,
  socialLogin,
};
