"use strict";

const Model = require("objection").Model;

class QuotationData extends Model {
  static get tableName() {
    return "quotation_details";
  }

  static get idColumn() {
    return "quotation_id";
  }

  
  static relationMappings() {
    return {
      quotation_docx: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/QuotationDataDocx",
        join: {
          from: "quotation_details.quotation_id",
          to: "quotation_data_documents_from_company.quotation_id",
        },
      },
      quotation_technology: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/QuotationTechSkill",
        join: {
          from: "quotation_details.quotation_id",
          to: "quotation_tech_skill.quotation_id",
        },
      },
      technologies_label:{
        relation: Model.ManyToManyRelation,
        modelClass: __dirname + "/Skills",
        join:{
          from: "quotation_details.quotation_id",
          through:{
            from: "quotation_tech_skill.quotation_id",
            to: "quotation_tech_skill.skill_id"
          },
          to: "skillset_list.skill_id"
        }
      },
      inbox_thread:{
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname+"/ChatInbox",
        join:{
          from:"quotation_details.quotation_id",
          to: "chat_inbox_thread.quotation_id"
        }
      },
      shortlisted: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/ShortlistedCompanies",
        join: {
          from: "quotation_details.company_id",
          to: "shortlisted_companies.company_id",
        } 
      } 
    }
  }

  async $beforeInsert() {
    await super.$beforeInsert();
    this.quotation_sent_at =  new Date().toISOString();
  }

  async $beforeUpdate(opt, queryContext) {
    await super.$beforeUpdate(opt, queryContext);

    this.quotation_updated_at =  new Date().toISOString();
  }
}
module.exports = QuotationData;
