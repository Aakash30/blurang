'use strict';

const Model = require('objection').Model;

class ProjectServices extends Model {
    static get tableName() {
      return 'project_service_mapping';
    }
  
    static get idColumn() {
      return 'project_service_id';
    }

    static get relationMappings(){
      return {
        service: {
          relation: Model.BelongsToOneRelation,
          modelClass: __dirname+"/Service",
          join:{
            from: "project_service_mapping.service_id",
            to: "service_list.service_id"
          }
        },
        service_details: {
          relation: Model.BelongsToOneRelation,
          modelClass: __dirname + '/Service',
          join: {
              to: 'service_list.service_id',
              from: 'project_service_mapping.service_id'
          }
        }
      }
    }
}

module.exports = ProjectServices;