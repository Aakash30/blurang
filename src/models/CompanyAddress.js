'use strict';

const Model = require('objection').Model;

class CompanyAddress extends Model {
    static get tableName() {
      return 'company_address';
    }
  
    static get idColumn() {
      return 'address_id';
    }
}

module.exports = CompanyAddress;