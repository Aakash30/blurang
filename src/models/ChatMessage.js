"use strict";

const Model = require("objection").Model;
const globalCall = require("../settings/functions");

class ChatMessage extends Model {
  static get tableName() {
    return "chat_messages_list";
  }

  static get idColumn() {
    return "message_id";
  }

  async $beforeInsert() {
    await super.$beforeInsert();
  this.status = "unread";
  }

}

module.exports = ChatMessage;