'use strict';
const Model = require('objection').Model;

class PortfolioProjectGallery extends Model {
    static get tableName() {
      return 'portfolio_gallery';
    }
  
    static get idColumn() {
      return 'gallery_id';
    }

}
module.exports = PortfolioProjectGallery;