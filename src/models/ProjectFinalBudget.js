'use strict';

const Model = require('objection').Model;

class ProjectFinalBudget extends Model {
    static get tableName() {
      return 'project_final_budget';
    }
  
    static get idColumn() {
      return 'project_final_budget_id';
    }

    static get relationMappings() {
		return {
			project_details: {
				relation: Model.BelongsToOneRelation,
				modelClass: __dirname + "/Project",
				join:{
				  from: "project_final_budget.project_id",
				  to: "project.project_id"
				}
			},
    	}
	}
	
	// async $beforeInsert() {
	// await super.$beforeInsert();
	// this.created_at = new Date().toISOString();
	// }

	// async $beforeUpdate(opt, queryContext) {
	// await super.$beforeUpdate(opt, queryContext);

	// this.updated_at =  new Date().toISOString();
	// }
}

module.exports = ProjectFinalBudget;