"use strict";

const Model = require("objection").Model;

class CompanyFeatured extends Model {
  static get tableName() {
    return "company_achievement_featured";
  }

  static get idColumn() {
    return "feature_id";
  }
}
module.exports = CompanyFeatured;