'use strict';
const Model = require('objection').Model;

class PortfolioProjectLinks extends Model {
    static get tableName() {
      return 'portfolio_project_links';
    }
  
    static get idColumn() {
      return 'link_id';
    }
}
module.exports = PortfolioProjectLinks;