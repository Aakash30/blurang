"use strict";
const Model = require("objection").Model;

class CompanyProgress extends Model {
  static get tableName() {
    return "company_progress";
  }

  static get idColumn() {
    return "progress_id";
  }

  static get relationMappings() {
    return {
        userProgress: {
            relation: Model.BelongsToOneRelation,
            modelClass: __dirname+"/UserLogin",
            join:{
                from: "company_progress.company_id",
                to:"user_login.default_company"
            }
        }
    }
}
}

module.exports = CompanyProgress;
