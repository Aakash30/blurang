"use strict";
const Model = require("objection").Model;

class RatingPoints extends Model {
  static get tableName() {
    return "rating_section";
  }

  static get idColumn() {
    return "rating_id";
  }
}
module.exports = RatingPoints;
