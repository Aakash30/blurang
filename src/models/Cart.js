'use strict';

const Model = require('objection').Model;

class Cart extends Model {
    static get tableName() {
      return 'cart';
    }
  
    static get idColumn() {
      return 'cart_id';
    }

    static relationMappings() {
      return {
        company_details: {
          relation: Model.BelongsToOneRelation,
          modelClass: __dirname + "/Company",
          join: {
            from: "cart.company_id",
            to: "company.company_id",
          },
        }
      }
    }
}

module.exports = Cart;