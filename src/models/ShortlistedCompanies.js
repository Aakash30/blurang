'use strict';

const QuotationData = require('./QuotationData');

const Model = require('objection').Model;

class ShortlistedCompanies extends Model {
    static get tableName() {
      return 'shortlisted_companies';
    }
  
    static get idColumn() {
      return 'shortlisted_id';
    }

    static getModifiers() {
      return {
        getShortlistedId(builder, projectId) {
          const {ref} = QuotationData;
          console.log(projectId)
          builder.select("shortlisted_id").join("quotation_details", function(){
            this.on("shortlisted_companies.company_id", "quotation_details.company_id")
            this.on("shortlisted_companies.project_id", "quotation_details.project_id")
          });
          //console.log(builder.select("shortlisted_id").where("project_id", projectId).toKnexQuery().toQuery())
        },
      };
    }

    static relationMappings() {
      return {
        project: {
          relation: Model.BelongsToOneRelation,
          modelClass: __dirname + "/Project",
          join: {
            from: "shortlisted_companies.project_id",
            to: "project.project_id",
          },
        },
        company_details: {
          relation: Model.BelongsToOneRelation,
          modelClass: __dirname + "/Company",
          join: {
            from: "shortlisted_companies.company_id",
            to: "company.company_id",
          }
        },
        quotation_details: {
          relation: Model.BelongsToOneRelation,
          modelClass: __dirname + "/QuotationData",
          join: {
            from: "shortlisted_companies.company_id",
            to: "quotation_details.company_id",
          } 
        },
        quotation_details_for_project: {
          relation: Model.BelongsToOneRelation,
          modelClass: __dirname + "/QuotationData",
          join: {
            from: "shortlisted_companies.project_id",
            to: "quotation_details.project_id",
          }
        }
      }
    }

    async $beforeInsert() {
      await super.$beforeInsert();
      this.status = "quotation_request";
      this.created_at =  new Date().toISOString();
    }

    async $beforeUpdate(opt, queryContext) {
      await super.$beforeUpdate(opt, queryContext);

      this.updated_at =  new Date().toISOString();
    }
}

module.exports = ShortlistedCompanies;