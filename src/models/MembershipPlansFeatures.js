'use strict';

const Model = require('objection').Model;

// const CompanyServices = require('../models/CompanyServices');

class MembershipPlansFeatures extends Model {
    static get tableName() {
      return 'membership_plans_features';
    }
  
    static get idColumn() {
      return 'feature_id';
    }

    static get relationMappings() {
      return {
        // skills: {
        //   relation: Model.HasManyRelation,
        //   modelClass: __dirname+'/Skills',
        //   join:{
        //     from:"service_list.service_id",
        //     to: "skillset_list.service_id"
        //   }
        // },
        // service_details: {
        //     relation: BaseModel.BelongsToOneRelation,
        //     modelClass: __dirname + '/Service',
        //     join: {
        //         to: 'service_list.service_id',
        //         from: 'company_service_mapping.service_id'
        //     }
        // }
      }
    }

    async $beforeInsert() {
      // await super.$beforeInsert();
      // this.status = "active";
    }
}

module.exports = MembershipPlansFeatures;
