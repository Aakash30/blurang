'use strict';

const Model = require('objection').Model;

class ProjectDocx extends Model {
    static get tableName() {
      return 'project_docx';
    }
  
    static get idColumn() {
      return 'project_docx_id';
    }

    // static relationMappings() {
    //   return {
    //     project: {
    //       relation: Model.HasManyRelation,
    //       modelClass: __dirname + "/Project",
    //       join: {
    //         from: "project_docx.project_id",
    //         to: "project.project_id",
    //       },
    //     }
    //   }
    // }
}

module.exports = ProjectDocx;