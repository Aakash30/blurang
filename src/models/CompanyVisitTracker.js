"use strict";

const Model = require("objection").Model;

class CompanyVisitTracker extends Model {
  static get tableName() {
    return "track_company_views";
  }

  static get idColumn() {
    return "tracking_id";
  }
  
}

module.exports = CompanyVisitTracker;