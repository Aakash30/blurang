"use strict";

const Model = require("objection").Model;
const jwt = require("jsonwebtoken");
const validator = require("validator");
const bcrypt = require("bcryptjs");

const globalCall = require("../settings/functions");

class UserLogin extends Model {
  static get tableName() {
    return "user_login";
  }

  static get idColumn() {
    return "user_id";
  }

  async comparePassword(password) {
    if (!password) {
      return false;
    }

    let pass = await bcrypt.compare(password, this.password);
    return pass;
  }
  async getJWT() {
    return await jwt.sign(
      {
        userId: this.user_id,
        type: this.user_type
      },
      CONFIG.jwt_encryption
    );
  }

  static get relationMappings() {
    return {
      company: {
        relation: Model.ManyToManyRelation,
        modelClass: __dirname + "/Company",
        join: {
          from: "user_login.user_id",
          through: {
            from: "company_mapping.user_id",
            to: "company_mapping.company_id"
          },
          to: "company.company_id"
        }
      },
      defaultCompany: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + "/Company",
        join: {
          from: "user_login.default_company",
          to: "company.company_id"
        }
      },
      authToken: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/UserAuth",
        join: {
          from: "user_login.user_id",
          to: "user_auth_token.user_id"
        }
      },
      company_count: {
        relation: Model.ManyToManyRelation,
        modelClass: __dirname + "/CompanyMapping",
        join: {
          from: "user_login.user_id",
          to: "company_mapping.user_id"
        }
      },
      project_data: {
        relation: Model.ManyToManyRelation,
        modelClass: __dirname + "/Project",
        join: {
          from: "user_login.user_id",
          to: "project.user_id"
        }
      },
      project_details: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/Project",
        join: {
          from: "user_login.user_id",
          to: "project.user_id"
        }
      },
      project_details_last_month: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/Project",
        join: {
          from: "user_login.user_id",
          to: "project.user_id"
        }
      },
      project_details_secondlast_month: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/Project",
        join: {
          from: "user_login.user_id",
          to: "project.user_id"
        }
      },
    };
  }

  async $beforeInsert() {
    await super.$beforeInsert();

    if (this.email_id) {
      if (!validator.isEmail(this.email_id || "")) {
        throw globalCall.badRequestError("Not a valid email address!");
      }

      let result = await this.constructor
        .query()
        .skipUndefined()
        .select("user_id")
        .where("email_id", this.email_id)
        .first();
      if (result) {
        throw globalCall.badRequestError(
          "Account with this email-id already exists!"
        );
      }
      this.email_id = this.email_id.toLowerCase();
    }
    if (this.mobile_number) {
      let result = await this.constructor
        .query()
        .skipUndefined()
        .select("user_id")
        .where("mobile_number", this.mobile_number)
        .first();
      if (result) {
        throw globalCall.badRequestError(
          "Account with this mobile number already exists!"
        );
      }
    }

    if (this.password) {
      this.password = await bcrypt.hash(this.password, 10);
      console.log(this.password);
    }
  }

  async $beforeUpdate() {
    await super.$beforeUpdate();

    if (this.password) {
      this.password = await bcrypt.hash(this.password, 10);
      console.log(this.password);
    }
  }
}

module.exports = UserLogin;
