"use strict";

const Model = require("objection").Model;
const moment = require("moment");
const globalCall = require("../settings/functions");
const validator = require("validator");

class Company extends Model {
  static get tableName() {
    return "company";
  }

  static get idColumn() {
    return "company_id";
  }

  static getModifiers() {
    return {
      getNameAndLogo(builder) {
        builder.select("company_id", "company_name", "logo");
      },
    };
  }

  static relationMappings() {
    return {
      companyMapping: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/CompanyMapping",
        join: {
          from: "company.company_id",
          to: "company_mapping.company_id",
        },
      },
      domain: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + "/Domain",
        join: {
          from: "company.domain_id",
          to: "domain_list.domain_id",
        },
      },
      media: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/CompanyMedia",
        join: {
          from: "company.company_id",
          to: "company_media.company_id",
        },
      },
      testimonial: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/CompanyMedia",
        join: {
          from: "company.company_id",
          to: "company_media.company_id",
        },
      },
      address: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/CompanyAddress",
        join: {
          from: "company.company_id",
          to: "company_address.company_id",
        },
      },
      portfolio: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/CompanyPortfolio",
        join: {
          from: "company.company_id",
          to: "portfolio.company_id",
        },
      },
      reviews: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/PortfolioProjectReviews",
        join: {
          from: "company.company_id",
          to: "reviews.company_id",
        },
      },
      services: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/CompanyServices",
        join: {
          from: "company.company_id",
          to: "company_service_mapping.company_id",
        },
      },
      serviceLabels: {
        relation: Model.ManyToManyRelation,
        modelClass: __dirname+"/Service",
        join:{
          from: "company.company_id",
          through:{
            from: "company_service_mapping.company_id",
            to: "company_service_mapping.service_id"
          },
          to: "service_list.service_id"
        }
      },
      skills: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/CompanySkills",
        join: {
          from: "company.company_id",
          to: "company_skills_mapping.company_id",
        },
      },
      skillLabel: {
        relation: Model.ManyToManyRelation,
        modelClass: __dirname + "/Skills",
        join: {
          from: "company.company_id",
          through:{
            from: "company_skills_mapping.company_id",
            to: "company_skills_mapping.skill_id"
          },
          to: "skillset_list.skill_id"
          
        },
      },
      rules: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/CompanyRules",
        join: {
          from: "company.company_id",
          to: "company_rules_answer.company_id",
        },
      },
      membership_plan: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/MembershipPlans",
        join: {
          from: "company.membership_plan_id",
          to: "membership_plans.plan_id",
        },
      },
      membership_history: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/CompanyMembershipHistory",
        join: {
          from: "company.membership_plan_id",
          to: "company_membership_history.membership_plan_id",
        },
      },
      defaultUser: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/UserLogin",
        join: {
          from: "company.company_id",
          to: "user_login.default_company",
        },
      },
      user: {
        relation: Model.ManyToManyRelation,
        modelClass: __dirname+"/UserLogin",
        join:{
          from: "company.company_id",
          through:{
            from: "company_mapping.company_id",
            to: "company_mapping.user_id"
          },
          to: "user_login.user_id"
        }
      },
      shortlisted: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/ShortlistedCompanies",
        join: {
          from: "company.company_id",
          to: "shortlisted_companies.company_id",
        },
      },
      hired: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/ShortlistedCompanies",
        join: {
          from: "company.company_id",
          to: "shortlisted_companies.company_id",
        },
      },
      company_docx:{
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/CompanyDocx",
        join: {
          from: "company.company_id",
          to: "company_docx.company_id",
        },
      },
      earning_this_month:{
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/ProjectFinalBudget",
        join: {
          from: "company.company_id",
          to: "project_final_budget.company_id",
        }
      },
      earning_last_month:{
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/ProjectFinalBudget",
        join: {
          from: "company.company_id",
          to: "project_final_budget.company_id",
        }
      },
      earning_secondlast_month:{
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/ProjectFinalBudget",
        join: {
          from: "company.company_id",
          to: "project_final_budget.company_id",
        }
      },
    };
  }

  async $beforeInsert() {
    await super.$beforeInsert();

    if (this.company_name) {
      let result = await this.constructor
        .query()
        .skipUndefined()
        .select("company_id")
        .where("company_name", "ilike", this.company_name)
        .first();
      if (result) {
        throw globalCall.badRequestError("This Company name already exists.");
      }
    }

    if (this.company_email_id) {
      if (!validator.isEmail(this.company_email_id || "")) {
        throw globalCall.badRequestError("Not a valid email address!");
      }

      let result = await this.constructor
        .query()
        .skipUndefined()
        .select("company_id")
        .where("company_email_id", this.company_email_id)
        .first();
      if (result) {
        throw globalCall.badRequestError(
          "Account with this email-id already exists!"
        );
      }
      this.company_email_id = this.company_email_id.toLowerCase();
    }
  }

  async $beforeUpdate() {
    await super.$beforeUpdate();

    if (this.company_name) {
      let result = await this.constructor
        .query()
        .skipUndefined()
        .select("company_id")
        .where("company_name", "ilike", this.company_name)
        .where("company_id", "!=", this.company_id)
        .first();
      if (result) {
        throw globalCall.badRequestError("This Company name already exists.");
      }
    }
    // if(!this.created_at)
    // {
    //   throw globalCall.badRequestError("Created at is not inserted");
    // }

    if (this.company_email_id) {
      if (!validator.isEmail(this.company_email_id || "")) {
        throw globalCall.badRequestError("Not a valid email address!");
      }

      let result = await this.constructor
        .query()
        .skipUndefined()
        .select("company_id")
        .where("company_email_id", this.company_email_id)
        .where("company_id", "!=", this.company_id)
        .first();
      if (result) {
        throw globalCall.badRequestError(
          "Account with this email-id already exists!"
        );
      }
      this.company_email_id = this.company_email_id.toLowerCase();
    }

    if (this.cin_number) {
      let result = await this.constructor
        .query()
        .skipUndefined()
        .select("company_id")
        .where("cin_number", this.cin_number)
        .where("company_id", "!=", this.company_id)
        .first();
      if (result) {
        throw globalCall.badRequestError("This CIN Number already exists.");
      }
      // this.company_email_id = this.company_email_id.toLowerCase();
    }
  }
}
module.exports = Company;
