'use strict';

const Model = require('objection').Model;

class CompanyDocx extends Model {
    static get tableName() {
      return 'company_docx';
    }
  
    static get idColumn() {
      return 'company_docx_id';
    }

    static get relationMappings() {
		return {
			// question: {
			// 	relation: Model.BelongsToOneRelation,
			// 	modelClass: __dirname + "/FaqQuestions",
			// 	join:{
			// 	  from: "company_faq.question_id",
			// 	  to: "faq_questions.question_id"
			// 	}
			// },
    	}
	}
	
	async $beforeInsert() {
	await super.$beforeInsert();
	this.created_at = new Date().toISOString();
	}

	async $beforeUpdate(opt, queryContext) {
	await super.$beforeUpdate(opt, queryContext);

	this.updated_at =  new Date().toISOString();
	}
}

module.exports = CompanyDocx;