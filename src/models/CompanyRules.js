"use strict";

const Model = require("objection").Model;

class CompanyRules extends Model {
  static get tableName() {
    return "company_rules_answer";
  }

  static get idColumn() {
    return "rule_id";
  }

  static get relationMappings(){
    return {
      question: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname+"/CompanyQuestion",
        join:{
          from: "company_rules_answer.question_id",
          to: "questions_for_company_profile.question_id"
        }
      }
    }
  }
}

module.exports = CompanyRules;
