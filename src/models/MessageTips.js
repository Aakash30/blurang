"use strict";

const Model = require("objection").Model;

class MessageTips extends Model {
  static get tableName() {
    return "message_tips";
  }

  static get idColumn() {
    return "message_id";
  }
}

module.exports = MessageTips;