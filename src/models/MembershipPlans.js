'use strict';

const Model = require('objection').Model;

// const CompanyServices = require('../models/CompanyServices');

class MembershipPlans extends Model {
    static get tableName() {
      return 'membership_plans';
    }
  
    static get idColumn() {
      return 'plan_id';
    }

    static get relationMappings() {
      return {
        features: {
          relation: Model.HasManyRelation,
          modelClass: __dirname+'/MembershipPlansFeatures',
          join:{
            from:"membership_plans.plan_id",
            to: "membership_plans_features.plan_id"
          }
        }
      }
    }

    async $beforeInsert() {
      // await super.$beforeInsert();
      // this.status = "active";
    }
}

module.exports = MembershipPlans;
