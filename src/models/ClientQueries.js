"use strict";

const Model = require("objection").Model;

class CompanySkills extends Model {
  static get tableName() {
    return "client_queries";
  }

  static get idColumn() {
    return "query_id";
  }

  static get relationMappings() {
    return {
      company: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + "/Comapny",
        join: {
          from: "company.company_id",
          to: "client_queries.company_id"
        }
      }
    };
  }
}

module.exports = CompanySkills;
