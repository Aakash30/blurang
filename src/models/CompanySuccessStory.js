"use strict";

const Model = require("objection").Model;

class CompanySuccessStory extends Model {
  static get tableName() {
    return "company_success_story";
  }

  static get idColumn() {
    return "story_id";
  }

  static get relationMappings(){
    return {
        project : {
            relation: Model.BelongsToOneRelation,
            modelClass: __dirname+'/CompanyPortfolio',
            join:{
                from: "company_success_story.portfolio_id",
                to: "portfolio.portfolio_id"
            }
        }
    }
  }
}
module.exports = CompanySuccessStory;
