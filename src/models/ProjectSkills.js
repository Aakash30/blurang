'use strict';

const Model = require('objection').Model;

class ProjectSkills extends Model {
    static get tableName() {
      return 'project_skill_mapping';
    }
  
    static get idColumn() {
      return 'project_skill_id';
    }

    static get relationMappings(){
      return {
        service: {
          relation: Model.BelongsToOneRelation,
          modelClass: __dirname+"/Skills",
          join:{
            from: "project_skill_mapping.skill_id",
            to: "skillset_list.skill_id"
          }
        },
        skill_details: {
          relation: Model.BelongsToOneRelation,
          modelClass: __dirname + '/Skills',
          join: {
              to: 'skillset_list.skill_id',
              from: 'project_skill_mapping.skill_id'
          }
        }
      }
    }
}

module.exports = ProjectSkills;