"use strict";

const Model = require("objection").Model;
const moment = require("moment");
const globalCall = require("../settings/functions");
const validator = require("validator");

class Country extends Model {
  static get tableName() {
    return "countries";
  }

  static get idColumn() {
    return "country_id";
  }
}

module.exports = Country;
