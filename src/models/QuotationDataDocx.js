"use strict";

const Model = require("objection").Model;

class QuotationDataDocx extends Model {
  static get tableName() {
    return "quotation_data_documents_from_company";
  }

  static get idColumn() {
    return "document_id";
  }

  

}
module.exports = QuotationDataDocx;
