'use strict';

const Model = require('objection').Model;

class CompanyMapping extends Model {
    static get tableName() {
      return 'company_mapping';
    }
  
    static get idColumn() {
      return 'company_map_id';
    }

    static get relationMappings() {
        return {
            companies: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + "/Company",
                join:{
                  from: "company_mapping.company_id",
                  to: "company.comapny_id"
                }
            },
            companyUser: {
              relation: Model.BelongsToOneRelation,
              modelClass: __dirname+"/UserLogin",
              join:{
                from: "company_mapping.user_id",
                to: "user_login.user_id"
              }
            }
        }
    }

}
module.exports = CompanyMapping;