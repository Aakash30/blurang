"use strict";

const Model = require("objection").Model;

class CompanyAwards extends Model {
  static get tableName() {
    return "company_achievement_award";
  }

  static get idColumn() {
    return "award_id";
  }
}
module.exports = CompanyAwards;
