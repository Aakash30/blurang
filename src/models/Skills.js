'use strict';

const Model = require('objection').Model;

class Skills extends Model {
    static get tableName() {
      return 'skillset_list';
    }
  
    static get idColumn() {
      return 'skill_id';
    }

    async $beforeInsert() {
      await super.$beforeInsert();
      this.status = "active";
    }

}

module.exports = Skills;