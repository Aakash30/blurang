'use strict';
const Model = require('objection').Model;

class PortfolioProjectSkills extends Model {
    static get tableName() {
      return 'portfolio_skills';
    }
  
    static get idColumn() {
      return 'portfolio_skill_id';
    }
}
module.exports = PortfolioProjectSkills;