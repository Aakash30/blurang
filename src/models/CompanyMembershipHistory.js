'use strict';

const Model = require('objection').Model;

// const CompanyServices = require('../models/CompanyServices');

class CompanyMembershipHistory extends Model {
    static get tableName() {
      return 'company_membership_history';
    }
  
    static get idColumn() {
      return 'company_membership_history_id';
    }


    static get relationMappings() {
      return {
        membership_plans: {
          relation: Model.HasManyRelation,
          modelClass: __dirname+'/MembershipPlans',
          join:{
            from:"company_membership_history.membership_plan_id",
            to: "membership_plans.plan_id"
          }
        }
      }
    }

    async $beforeInsert() {
      // await super.$beforeInsert();
      // this.status = "active";
    }
}

module.exports = CompanyMembershipHistory;
