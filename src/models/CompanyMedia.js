'use strict';

const Model = require('objection').Model;

class CompanyMedia extends Model {
    static get tableName() {
      return 'company_media';
    }
  
    static get idColumn() {
      return 'media_id';
    }
}

module.exports = CompanyMedia;