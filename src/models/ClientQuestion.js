"use strict";

const Model = require("objection").Model;

class CompanyQuestion extends Model {
  static get tableName() {
    return "questions_for_client";
  }

  static get idColumn() {
    return "question_id";
  }
  
}

module.exports = CompanyQuestion;