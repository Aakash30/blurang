"use strict";

const Model = require("objection").Model;
const globalCall = require("../settings/functions");

class ChatProjectCompanyMap extends Model {
  static get tableName() {
    return "chat_project_company_map";
  }

  static get idColumn() {
    return "map_id";
  }
  static get relationMappings() {
    return {
      inbox: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + "/ChatInbox",
        join: {
          from: "chat_project_company_map.chat_box_id",
          to: "chat_inbox_thread.chat_id",
        },
      },
      project:{
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname+ "/Project",
        join:{
          from: "chat_project_company_map.project_id",
          to: "project.project_id"
        }
      },
      company:{
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname+ "/Company",
        join:{
          from: "chat_project_company_map.company_id",
          to: "company.company_id"
        }
      },
      chatMessage:{
        relation: Model.ManyToManyRelation,
        modelClass: __dirname+"/ChatMessage",
        join:{
          from: "chat_project_company_map.chat_box_id",
          through:{
            from: "chat_inbox_thread.chat_id",
            to: "chat_inbox_thread.chat_id"
          },
          to: "chat_messages_list.chat_id"
        }
      },
      quotation:{
        relation: Model.ManyToManyRelation, 
        modelClass: __dirname+"/QuotationData",
        join:{
          from: "chat_project_company_map.chat_box_id",
          through:{
            from: "chat_inbox_thread.chat_id",
            to: "chat_inbox_thread.quotation_id"
          },
          to: "quotation_details.quotation_id"
        }
      },
      client: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname+"/UserLogin",
        join:{
          from: "chat_project_company_map.client_id",
          to:"user_login.user_id"
        }
      },
      Shortlistedcompanies:{
        relation: Model.HasManyRelation,
        modelClass: __dirname+"/ShortlistedCompanies",
        join:{
          from: "chat_project_company_map.company_id",
          to:"shortlisted_companies.company_id"
        }
      }
    };
  }
}

module.exports = ChatProjectCompanyMap;
