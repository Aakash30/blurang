"use strict";

const Model = require("objection").Model;
const globalCall = require("../settings/functions");

class ChatInbox extends Model {
  static get tableName() {
    return "chat_inbox_thread";
  }

  static get idColumn() {
    return "chat_id";
  }

  static get relationMappings() {
    return {
      chatMap: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/ChatProjectCompanyMap",
        join: {
          from: "chat_inbox_thread.chat_id",
          to: "chat_project_company_map.chat_box_id",
         
        },
      },
    };
  }
}

module.exports = ChatInbox;
