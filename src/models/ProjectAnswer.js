"use strict";

const Model = require("objection").Model;

class ProjectAnswer extends Model {
  static get tableName() {
    return "answer_for_client";
  }

  static get idColumn() {
    return "answer_id";
  }

  static get relationMappings(){
    return {
      question: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname+"/ClientQuestion",
        join:{
          from: "answer_for_client.question_id",
          to: "questions_for_client.question_id"
        }
      },
    }
  }
}

module.exports = ProjectAnswer;
