'use strict';
const Model = require('objection').Model;

class PortfolioProjectReviews extends Model {
    static get tableName() {
      return 'reviews';
    }
  
    static get idColumn() {
      return 'review_id';
    }

    static get relationMappings() {
        return {
            portfolio: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + "/CompanyPortfolio",
                join:{
                  from: "reviews.portfolio_id",
                  to: "portfolio.portfolio_id"
                }
            },

            ratings: {
              relation: Model.HasManyRelation,
              modelClass: __dirname + "/PortfolioProjectReviewRating",
              join:{
                from: "reviews.review_id",
                to: "review_rating.review_id"
              }
            },
            ratingList:{
              relation: Model.ManyToManyRelation,
              modelClass: __dirname+"/RatingPoints",
              join:{
                from: "reviews.review_id",
                through: {
                  from: "review_rating.review_id",
                  to: "review_rating.rating_id"
                },
                to: "rating_section.rating_id"
              }
            }
        }
    }
}
module.exports = PortfolioProjectReviews;