"use strict";

const Model = require("objection").Model;

class FaqQuestions extends Model {
  static get tableName() {
    return "faq_questions";
  }

  static get idColumn() {
    return "question_id";
  }

  static get relationMappings() {
    return {
      answer: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/CompanyFaq",
        join: {
          from: "faq_questions.question_id",
          to: "company_faq.question_id",
        },
      },
    };
  }
}

module.exports = FaqQuestions;
