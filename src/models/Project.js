"use strict";

const Model = require("objection").Model;
const moment = require("moment");

class Project extends Model {
  static get tableName() {
    return "project";
  }

  static get idColumn() {
    return "project_id";
  }

  static relationMappings() {
    return {
      services: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/ProjectServices",
        join: {
          from: "project.project_id",
          to: "project_service_mapping.project_id",
        },
      },
      servicesLabels: {
        relation: Model.ManyToManyRelation,
        modelClass: __dirname + "/Service",
        join: {
          from: "project.project_id",
          through:{
            from:"project_service_mapping.project_id",
            to: "project_service_mapping.service_id"
          },
          to: "service_list.service_id",
        },
      },
      skills: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/ProjectSkills",
        join: {
          from: "project.project_id",
          to: "project_skill_mapping.project_id",
        },
      },
      skillLabel: {
        relation: Model.ManyToManyRelation,
        modelClass: __dirname + "/Skills",
        join: {
          from: "project.project_id",
          through:{
            from: "project_skill_mapping.project_id",
            to: "project_skill_mapping.skill_id"
          },
          to: "skillset_list.skill_id"
          
        },
      },
      projectAnswer:{
        relation: Model.HasManyRelation,
        modelClass: __dirname+"/ProjectAnswer",
        join:{
          from: "project.project_id",
          to: "answer_for_client.project_id"
        }
      },
      quotation: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/QuotationData",
        join: {
          from: "project.project_id",
          to: "quotation_details.project_id",
        },
      },
      shortlisted_companies: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/ShortlistedCompanies",
        join: {
          from: "project.project_id",
          to: "shortlisted_companies.project_id",
        },
      },
      quotation_companies: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/ShortlistedCompanies",
        join: {
          from: "project.project_id",
          to: "shortlisted_companies.project_id",
        },
      },
      domain:{
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname+"/Domain",
        join: {
          from: "project.domain_id",
          to: "domain_list.domain_id"
        }
      },
      project_docx: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/ProjectDocx",
        join: {
          from: "project.project_id",
          to: "project_docx.project_id",
        },
      },
      hired_company: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + "/Company",
        join: {
          from: "project.assigned_to_company",
          to: "company.company_id",
        },
      },
      portfolio: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/CompanyPortfolio",
        join: {
          from: "project.project_id",
          to: "portfolio.belongs_to_project",
        },
      },
      client_details: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/UserLogin",
        join: {
          from: "project.user_id",
          to: "user_login.user_id",
        },
      },
      final_quotation: {
        relation: Model.ManyToManyRelation, 
        modelClass: __dirname + "/QuotationData",
        join:{
          from: "project.assigned_to_company",
          through:{
            from: "company.company_id",
            to: "company.company_id"
          },
          to: "quotation_details.company_id"
        }
      }
    };
  }

  async $beforeInsert() {
    await super.$beforeInsert();
    this.status = "draft";
    this.created_at = new Date().toISOString();
  }

  async $beforeUpdate(opt, queryContext) {
    await super.$beforeUpdate(opt, queryContext);

    this.updated_at =  new Date().toISOString();
  }

}

module.exports = Project;
