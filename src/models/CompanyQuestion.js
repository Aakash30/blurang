"use strict";

const Model = require("objection").Model;

class CompanyQuestion extends Model {
  static get tableName() {
    return "questions_for_company_profile";
  }

  static get idColumn() {
    return "question_id";
  }
  static get relationMappings() {
    return {
      rulesAnswer: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/CompanyRules",
        join:{
          from: "questions_for_company_profile.question_id",
          to: "company_rules_answer.question_id"
        }
      }
    };
  }
}

module.exports = CompanyQuestion;
