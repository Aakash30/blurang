"use strict";

const Model = require("objection").Model;
const moment = require("moment");
const globalCall = require("../settings/functions");
const validator = require("validator");

class City extends Model {
  static get tableName() {
    return "cities";
  }

  static get idColumn() {
    return "city_id";
  }

  static get relationMappings() {
    return {
      states: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + "/State",
        join:{
          from: "states.state_id",
          to: "cities.state_id"
        }
      },
      countries: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + "/Country",
        join:{
          from: "countries.country_id",
          to: "cities.country_id"
        }
      }
    };
  }
}

module.exports = City;