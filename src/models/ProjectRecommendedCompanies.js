"use strict";

const Model = require("objection").Model;

class ProjectRecommendedCompanies extends Model {
  static get tableName() {
    return "project_recommended_companies";
  }

  static get idColumn() {
    return "recommended_id";
  }

  static get relationMappings(){
    return {
      project: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname+"/Project",
        join:{
          from: "project_recommended_companies.project_id",
          to: "project.project_id"
        }
      },
    }
  }

  async $beforeInsert() {
  await super.$beforeInsert();
  this.created_at = new Date().toISOString();
  }

  async $beforeUpdate(opt, queryContext) {
  await super.$beforeUpdate(opt, queryContext);

  this.updated_at =  new Date().toISOString();
  }
}
module.exports = ProjectRecommendedCompanies;
