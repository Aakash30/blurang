'use strict';

const Model = require('objection').Model;

class CompanyServices extends Model {
    static get tableName() {
      return 'company_service_mapping';
    }
  
    static get idColumn() {
      return 'service_map_id';
    }

    static get relationMappings(){
      return {
        service: {
          relation: Model.BelongsToOneRelation,
          modelClass: __dirname+"/Service",
          join:{
            from: "company_service_mapping.service_id",
            to: "service_list.service_id"
          }
        },
        service_details: {
            relation: Model.BelongsToOneRelation,
            modelClass: __dirname + '/Service',
            join: {
                to: 'service_list.service_id',
                from: 'company_service_mapping.service_id'
            }
        }
      }
    }
}

module.exports = CompanyServices;