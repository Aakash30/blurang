'use strict';

const Model = require('objection').Model;

class CompanyFaq extends Model {
    static get tableName() {
      return 'company_faq';
    }
  
    static get idColumn() {
      return 'faq_id';
    }

    static get relationMappings() {
		return {
			question: {
				relation: Model.BelongsToOneRelation,
				modelClass: __dirname + "/FaqQuestions",
				join:{
				  from: "company_faq.question_id",
				  to: "faq_questions.question_id"
				}
			},
    	}
    }
}

module.exports = CompanyFaq;