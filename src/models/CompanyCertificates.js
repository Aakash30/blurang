"use strict";

const Model = require("objection").Model;

class CompanyCertificates extends Model {
  static get tableName() {
    return "company_achievement_certificate";
  }

  static get idColumn() {
    return "certificate_id";
  }
}
module.exports = CompanyCertificates;
