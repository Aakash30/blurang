'use strict';
const Model = require('objection').Model;
const jwt = require('jsonwebtoken');

class CompanyPortfolio extends Model {
    static get tableName() {
      return 'portfolio';
    }
  
    static get idColumn() {
      return 'portfolio_id';
    }

    async getVerifyToken() {
      return await jwt.sign({
        portfolio_id: this.portfolio_id,
      }, CONFIG.jwt_encryption);
    }

    static get relationMappings() {
      return {
        company: {
            relation: Model.BelongsToOneRelation,
            modelClass: __dirname + "/Company",
            join:{
              from: "portfolio.company_id",
              to: "company.company_id"
            }
        },

        gallery: {
            relation: Model.HasManyRelation,
            modelClass: __dirname + "/PortfolioProjectGallery",
            join:{
              from: "portfolio.portfolio_id",
              to: "portfolio_gallery.portfolio_id"
            }
        },
        
        links: {
            relation: Model.HasManyRelation,
            modelClass: __dirname + "/PortfolioProjectLinks",
            join:{
              from: "portfolio.portfolio_id",
              to: "portfolio_project_links.portfolio_id"
            }
        },

        portfolioSkills: {
            relation: Model.ManyToManyRelation,
            modelClass: __dirname + '/Skills',
            join: {
                from: 'portfolio.portfolio_id',
                through:{
                    from: 'portfolio_skills.portfolio_id',
                    to:'portfolio_skills.skill_id'
                },
                to: 'skillset_list.skill_id'
            }
        },

        skills: {
            relation: Model.HasManyRelation,
            modelClass: __dirname + "/PortfolioProjectSkills",
            join:{
              from: "portfolio.portfolio_id",
              to: "portfolio_skills.portfolio_id"
            }
        },

        reviews: {
          relation: Model.HasManyRelation,
          modelClass: __dirname + "/PortfolioProjectReviews",
          join:{
            from: "portfolio.portfolio_id",
            to: "reviews.portfolio_id"
          }
        },

        project: {
          relation: Model.BelongsToOneRelation,
          modelClass: __dirname+ "/Project",
          join: {
            from: "portfolio.belongs_to_project",
            to: "project.project_id"
          }
        }
      }
    }
}
module.exports = CompanyPortfolio;