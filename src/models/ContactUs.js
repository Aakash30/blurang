'use strict';
const Model = require('objection').Model;

class ContactUs extends Model {
    static get tableName() {
      return 'contact_us';
    }
  
    static get idColumn() {
      return 'contact_id';
    }
}
module.exports = ContactUs;