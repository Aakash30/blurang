'use strict';

const Model = require('objection').Model;

class Notification extends Model {
    static get tableName() {
      return 'notification';
    }
  
    static get idColumn() {
      return 'notification_id';
    }

    // static relationMappings() {
    //   return {
    //     project: {
    //       relation: Model.HasManyRelation,
    //       modelClass: __dirname + "/Project",
    //       join: {
    //         from: "project_docx.project_id",
    //         to: "project.project_id",
    //       },
    //     }
    //   }
    // }
}

module.exports = Notification;