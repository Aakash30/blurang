"use strict";
const Model = require("objection").Model;

class PortfolioProjectReviewRating extends Model {
  static get tableName() {
    return "review_rating";
  }

  static get idColumn() {
    return "review_rating_id";
  }

  static get relationMappings() {
    return {
      reviews: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + "/PortfolioProjectReviews",
        join: {
          from: "review_rating.review_id",
          to: "reviews.review_id",
        },
      },

      ratingPoint: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + "/RatingPoints",
        join: {
          from: "review_rating.rating_id",
          to: "rating_section.rating_id",
        },
      },
    };
  }
}
module.exports = PortfolioProjectReviewRating;
