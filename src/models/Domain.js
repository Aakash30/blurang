'use strict';

const Model = require('objection').Model;

class Domain extends Model {
    static get tableName() {
      return 'domain_list';
    }
  
    static get idColumn() {
      return 'domain_id';
    }
    async $beforeInsert() {
      await super.$beforeInsert();
      this.status = "active";
    }

    static relationMappings() {
      return {
        project:{
          relation: Model.BelongsToOneRelation,
          modelClass: __dirname+"/Project",
          join: {
            from: "domain_list.domain_id",
            to: "project.domain_id"
          }
        }
      };
    }
}

module.exports = Domain;