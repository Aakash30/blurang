'use strict';

const Model = require('objection').Model;

class UserAuth extends Model {
    static get tableName() {
      return 'user_auth_token';
    }
  
    static get idColumn() {
      return 'token_id';
    }

    static get relationMappings() {
      return {
          user: {
              relation: Model.BelongsToOneRelation,
              modelClass: __dirname + "/UserLogin",
              join:{
                from: "user_auth_token.user_id",
                to: "user_login.user_id"
              }
              
          }
      }
  }
}
module.exports = UserAuth;