"use strict";

const Model = require("objection").Model;

class QuotationTechSkill extends Model {
  static get tableName() {
    return "quotation_tech_skill";
  }

  static get idColumn() {
    return "quotation_tech_skill_id";
  }

  static get relationMappings() {
    return {
      skills: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + "/Skills",
        join: {
          from: "quotation_tech_skill.skill_id",
          to: "skillset_list.skill_id"
        }
      }
    };
  }

}
module.exports = QuotationTechSkill;
