"use strict";

const Model = require("objection").Model;
const moment = require("moment");
const globalCall = require("../settings/functions");
const validator = require("validator");

class State extends Model {
  static get tableName() {
    return "states";
  }

  static get idColumn() {
    return "state_id";
  }
}

module.exports = State;