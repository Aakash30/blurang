"use strict";

const Model = require("objection").Model;

class CompanySkills extends Model {
  static get tableName() {
    return "company_skills_mapping";
  }

  static get idColumn() {
    return "skill_map_id";
  }

  static get relationMappings() {
    return {
      skills: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + "/Skills",
        join: {
          from: "company_skills_mapping.skill_id",
          to: "skillset_list.skill_id"
        }
      },
      skill_details: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + '/Skills',
        join: {
            to: 'skillset_list.skill_id',
            from: 'company_skills_mapping.skill_id'
        }
    }
    };
  }
}

module.exports = CompanySkills;
