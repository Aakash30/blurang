const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const passport = require('passport');
const fileupload = require("./../../middlewares/aws-fileupload");
const checkPermission = require('./../../middlewares/checkpermission');

require('./../../middlewares/passport')(passport);

upload = require ('../../middlewares/aws-fileupload').upload;

// load the dependent controller
const DomainController = require('./../../controllers').DomainController;
const ServiceController = require('./../../controllers').ServiceController;
const SkillController = require('./../../controllers').SkillController;

router.get('/getDomain', [passport.authenticate('jwt', {session:false})], checkPermission.adminAccess, DomainController.getDomain);
// router.post('/addDomain',[passport.authenticate('jwt', {session:false}), upload.single('domain_image_file')], DomainController.addDomain);
router.post('/addDomain',[passport.authenticate('jwt', {session:false}), upload.fields([{name: 'domain_image_file', maxCount:1}, {name: 'domain_icon_file', maxCount: 1}])], DomainController.addDomain);
router.get('/domainDetails/:id',[passport.authenticate('jwt', {session:false})], DomainController.domainDetails);

// upload.fields([{name: 'gallery_image', maxCount:6}, {name: 'brochure_file', maxCount: 1}])]

router.get('/getServices', [passport.authenticate('jwt', {session:false})], ServiceController.getServices);
router.post('/addService', [passport.authenticate('jwt', {session:false}), upload.single('service_icon_file')], ServiceController.addService);

router.get('/getSkills', [passport.authenticate('jwt', {session:false})], SkillController.getSkills);
router.post('/addSkill', [passport.authenticate('jwt', {session:false})], SkillController.addSkill);


module.exports = router;