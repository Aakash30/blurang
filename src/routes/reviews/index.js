const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const passport = require('passport');
require('./../../middlewares/passport')(passport);

// load the dependent controller
const ReviewsController = require('./../../controllers').ReviewsController;

/*************************** Add Company Profile *********************************/
router.get('/reviews/:id?', [passport.authenticate('jwt', {session:false})], ReviewsController.Reviews);
router.put('/withdraw-review', [passport.authenticate('jwt', {session:false})], ReviewsController.WithdrawReview);
router.post('/resend-feedback-request', [passport.authenticate('jwt', {session:false})], ReviewsController.ResendFeedbackRequest);

module.exports = router;