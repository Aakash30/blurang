const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const passport = require('passport');
const fileupload = require("./../../middlewares/aws-fileupload");
const checkPermission = require('./../../middlewares/checkpermission');

require('./../../middlewares/passport')(passport);

upload = require ('../../middlewares/aws-fileupload').upload;

// load the dependent controller
const AdmincompanyController = require('./../../controllers').AdmincompanyController;
const CompanyController = require('./../../controllers').CompanyController;


router.get('/listCompany', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdmincompanyController.ListCompany);

router.get('/fetchCompanyInformationAdmin/:id', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], CompanyController.fetchCompanyInformationData);
router.post('/updateCompanyInformation', [passport.authenticate('jwt', { session: false }), upload.single("logo_file")], AdmincompanyController.updateCompanyInformation);

router.get('/fetchAboutCompanyAdmin/:id', [passport.authenticate('jwt', {session: false}), checkPermission.adminAccess], CompanyController.fetchAboutCompany);
router.post('/updateAboutCompany', [passport.authenticate('jwt', {session: false}), upload.fields([{name: 'gallery_image', maxCount:6}, {name: 'brochure_file', maxCount: 1}])], AdmincompanyController.updateAboutCompany);

router.get('/fetchSkillsetDataAdmin/:id', [passport.authenticate('jwt', {session: false}), checkPermission.adminAccess], CompanyController.fetchSkillsetData);
router.post('/updateCompanySkillset', [passport.authenticate('jwt', {session: false}), checkPermission.adminAccess], AdmincompanyController.updateCompanySkillset);

router.get('/fetchCompanyVerificationDataAdmin/:id', [passport.authenticate('jwt', {session: false}), checkPermission.adminAccess], AdmincompanyController.fetchCompanyVerificationForEdit);
// above api is using AdmincompanyController rather than CompanyController bcz I have updated this function in AdmincompanyController Zubear
router.post('/updateCompanyVerification', [passport.authenticate('jwt', {session: false}), checkPermission.adminAccess, upload.fields([{name: 'cin_number_file_input'}, {name: 'company_docx_file', maxCount:5}])], AdmincompanyController.updateCompanyVerification);

router.get('/fetchCompanyRulesAdmin/:id',  [passport.authenticate('jwt', {session: false}), checkPermission.adminAccess], CompanyController.fetchCompanyRule);
router.post('/updateCompanyRules', passport.authenticate('jwt', {session: false}, checkPermission.adminAccess), AdmincompanyController.updateCompanyRules);

router.get('/CompanyDetails/:id', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdmincompanyController.CompanyDetails);

router.post('/ApproveCompany/:id', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdmincompanyController.ApproveCompany);
router.post('/RejectCompany/:id', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdmincompanyController.RejectCompany);
router.get('/PortfolioList_companydetails/:id', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdmincompanyController.PortfolioList_companydetails);
router.get('/PortfolioDetails_companydetails/:id', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdmincompanyController.PortfolioDetails_companydetails);

router.get('/ProjectList_companydetails/:id', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdmincompanyController.ProjectList_companydetails);
router.post('/PublishUnpublishPortfolio', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdmincompanyController.PublishUnpublishPortfolio);
router.get('/getRecommendedProject_companydetails/:id', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdmincompanyController.getRecommendedProject_companydetails);
router.get('/getRatingList_companydetails/:id', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdmincompanyController.getRatingList_companydetails);
router.post('/PublishUnpublishCompanyCertificates', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdmincompanyController.PublishUnpublishCompanyCertificates);
router.post('/PublishUnpublishCompanyAwards', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdmincompanyController.PublishUnpublishCompanyAwards);
router.post('/PublishUnpublishCompanyFeatured', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdmincompanyController.PublishUnpublishCompanyFeatured);
router.post('/PublishUnpublishCompanySuccessStory', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdmincompanyController.PublishUnpublishCompanySuccessStory);
router.get('/getCompanyFaq_companydetails/:id', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdmincompanyController.getCompanyFaq_companydetails);
router.post('/updateFAQ_companydetails', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdmincompanyController.updateFAQ_companydetails);
router.get('/getCompanyMembershipHistory/:id', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdmincompanyController.getCompanyMembershipHistory);

module.exports = router;