const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const passport = require('passport');
const checkPermission = require('./../../middlewares/checkpermission');

const AdminProjectsController=require("../../controllers/admin_projects/adminprojectsController")
require('./../../middlewares/passport')(passport);

router.get('/projects', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdminProjectsController.FetchAllProjects);
router.post('/project/unpublish/:id', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdminProjectsController.ProjectUnblished);
router.post('/project/publish/:id', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdminProjectsController.ProjectPublished);
router.get('/projects-in-progress', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdminProjectsController.FetchInProgressProjects);
router.get('/draftprojects', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdminProjectsController.FetchDraftProjects);
router.get("/archive-projects", [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdminProjectsController.FetchArchivedProjects)
router.get("/complete-projects", [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdminProjectsController.FetchCompletedProjects)
router.get("/under-discussion", [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdminProjectsController.FetchUnderDiscussionProjects)


//router.get('/quotation-pending', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdminProjectsController.FetchProjectsQuotationPending);
//router.get('/quotation-received', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdminProjectsController.FetchProjectsQuotationReceived);
module.exports=router