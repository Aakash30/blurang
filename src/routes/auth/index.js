const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const passport = require('passport');

require('./../../middlewares/passport')(passport);
upload = require ('../../middlewares/aws-fileupload').upload;

// load the dependent controller
const AuthController = require('./../../controllers').AuthController;
const app_authcontroller=require("./../../appApis/authcontroller")
const checkPermission = require('./../../middlewares/checkpermission');

router.post('/login', AuthController.loginUser);
router.post('/signup', AuthController.signup);
router.post("/checkuser",passport.authenticate('jwt', {session: false}), app_authcontroller.CheckUserVerified)
router.get('/resendActivationLink',passport.authenticate('jwt', {session: false}) ,AuthController.resendVerificationLink);
router.get('/verifyUser/:verify', AuthController.activateUserBusiness);

router.post('/forgotPassword', AuthController.forgotPasswordRecoveryLink);
router.get('/recoverMyPassword/:verify', AuthController.verifyAccountForPassword);
router.post('/recoverMyPassword/:verify', AuthController.recoverMyPassword);

router.post('/changePassword', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], AuthController.ChangePassword);
router.post('/updateProfile',[passport.authenticate('jwt', {session:false}), checkPermission.companyAccess, upload.single('profile_pic_file')], AuthController.updateProfile);
router.get('/getProfile',[passport.authenticate('jwt', {session:false}), checkPermission.companyAccess], AuthController.getProfile);

router.post('/changePasswordClient', [passport.authenticate('jwt', {session: false}), checkPermission.clientAccess], AuthController.ChangePasswordClient);
router.post('/updateProfileClient',[passport.authenticate('jwt', {session:false}), checkPermission.clientAccess, upload.single('profile_pic_file')], AuthController.updateProfileClient);
router.get('/getProfileClient',[passport.authenticate('jwt', {session:false}), checkPermission.clientAccess], AuthController.getProfileClient);

router.post('/socialLogin', AuthController.socialLogin);

module.exports = router;