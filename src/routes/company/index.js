const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const passport = require('passport');
const checkPermission = require('./../../middlewares/checkpermission');
const checkcompanysteps = require('./../../middlewares/checkcompanysteps');

require('./../../middlewares/passport')(passport);

upload = require ('../../middlewares/aws-fileupload').upload;

// load the dependent controller
const CompanyController = require('./../../controllers').CompanyController;
const PortfolioController = require('./../../controllers').CompanyPortfolioController;
const CompanyAchievement = require('../../controllers').CompanyAchievement;
const CompanyFaqController = require('../../controllers').CompanyFaqController;
const CompanyProject = require('../../controllers').CompanyProject;
const CompanyChatProjectListing=require("../../controllers").CompanyChatProjectListing

const CompanyQuotations = require('../../controllers').CompanyQuotations;
const CompanyMembershipController = require('../../controllers').CompanyMembershipController;
const dashboardCompany = require('../../controllers').Dashboard;

/*************************** Add Company Profile *********************************/
router.get('/fetchCompanyInformation/:id', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyController.fetchCompanyInformationData);
router.post('/addCompanyInformation', [passport.authenticate('jwt', { session: false }), checkPermission.companyAccess, upload.single("logo_file")], CompanyController.addCompanyInformation);

router.get('/fetchAboutCompany/:id', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess, checkcompanysteps.stepAccess], CompanyController.fetchAboutCompany);
router.post('/addAboutCompany', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess, upload.fields([{name: 'gallery_image', maxCount:6}, {name: 'brochure_file', maxCount: 1}])], CompanyController.addAboutCompany);

router.get('/fetchSkillsetData/:id', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess, checkcompanysteps.stepAccess], CompanyController.fetchSkillsetData);
router.post('/addCompanySkillset', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyController.addCompanySkillset);

router.get('/fetchCompanyVerificationDataToEdit/:id', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess, checkcompanysteps.stepAccess], CompanyController.fetchCompanyVerificationForEdit);
router.post('/checkDuplicateCIN', [passport.authenticate('jwt', {session: false})], CompanyController.checkUniqueCINNumber);
router.post('/addCompanyVerificationData', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess, upload.fields([{name: 'cin_number_file_input'}, {name: 'company_registration_file'}])], CompanyController.addCompanyVerification);

router.get('/fetchCompanyRules/:id',  [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess, checkcompanysteps.stepAccess], CompanyController.fetchCompanyRule);
router.post('/addCompanyRules', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyController.addCompanyRules);

router.get('/fetchMyCompanyPreview/:id', [passport.authenticate('jwt', {session:false}), checkPermission.companyAccess], CompanyController.companyProfilePreview);

router.get('/submitForReview/:id', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyController.submitForCompanyReview);

router.get('/deactivateMyCompany/:id', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyController.deactivateMyProfile);

router.get('/publicProfileHeader/:id', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyController.viewMyCompanyPublicProfileHeader);
router.get('/portfolioListForPublicProfile/:id', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyController.portfolioListForPublicProfile);
router.get('/reviewsListForPublicProfile/:id', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyController.reviewsListForPublicProfile);
router.get('/certificateListForPublicProfile/:id', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyController.achievementCertificateListForPublicProfile);
router.get('/awardListForPublicProfile/:id', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyController.achievementAwardsListForPublicProfile);
router.get('/featuredListForPublicProfile/:id', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyController.achivementFeaturedListForPublicProfile);
router.get('/successStoryListForPublicProfile/:id', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyController.successStoriesForPublicProfile);
router.get('/companyInfoForPublicProfile/:id', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyController.fetchCompanyInfoForPublicProfile);
router.get('/fetchCompanyFAQForPublicProfile/:id', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess, checkcompanysteps.stepAccess], CompanyController.fetchCompanyFAQForPublicProfile);

/*************************** COMPANY PORTFOLIO ROUTING  *********************************/
router.post('/company/company-portfolio', [passport.authenticate('jwt', { session: false }), checkPermission.companyAccess, upload.fields([{ name : 'project_logo_file' }, { name : 'gallery_file', maxCount:6 }])], PortfolioController.AddPortfolioProject);
router.delete('/company/delete-portfolio/:id', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], PortfolioController.DeletePortfolioProject);
router.get('/company/company-portfolio/:id?', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], PortfolioController.GetCompanyPortfolio);
router.get('/company/fetchFeedbackForm/:id', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], PortfolioController.fetchFeedbackForm);
router.post('/company/get-client-feedback', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], PortfolioController.SendFeedbackToClient);
router.post('/company/verify-client-feedback', PortfolioController.VerifyClientFeedback);
router.post('/company/client-feedback', PortfolioController.ClientFeedback);
router.post('/company/featured-portfolio', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], PortfolioController.AddRemoveToFeatured);
router.get('/company/fetchAddEditPortfolio/:id?', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], PortfolioController.fetchEditPortfolio);

/*************************** COMPANY FAQ ROUTING  *********************************/
router.post('/company/company-faq', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyFaqController.AddUpdateFaqQuestion);
router.get('/company/company-faq', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyFaqController.GetCompanyFaq);
router.get('/company/your-queries', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyFaqController.fetchQueryList);
router.post('/company/post-query', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyFaqController.SendQueryAnswer);
router.delete('/company/delete-query/:id', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyFaqController.deleteQueryQuestion);

/************************** COMPANY ACHIEVEMENTS ***************************************/

router.post('/addUpdateComapnyCertificates', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyAchievement.addUpdateAchievementCertificates);
router.get('/fetchCertificateForEdit/:id?', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyAchievement.fetchCertificateForEditing);
router.get('/deleteCertificate/:id', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyAchievement.deleteCertificate);
router.get('/fetchCompanyCertificates', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyAchievement.fetchCompanyCertificate);

router.post('/addUpdateAchievementAwards', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyAchievement.addUpdateAchievementAwards);
router.get('/fetchAwardForEdit/:id?', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyAchievement.fetchAwardEditData);
router.get('/deleteAward/:id', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyAchievement.deleteAward);
router.get('/fetchCompanyAwards', passport.authenticate('jwt', {session:false}), CompanyAchievement.fetchCompanyAwards);

router.post('/addUpdateFeaturedData', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyAchievement.addUpdateFeaturedData);
router.get('/fetchFeaturedForEdit/:id?', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyAchievement.fetchFeaturesForEditing);
router.get('/deleteFeaturedData/:id', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyAchievement.deleteFeaturedData);
router.get('/fetchCompanyFeatured', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyAchievement.fetchCompanyFeaturedData);

router.post('/addUpdateSuccessStory', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyAchievement.addUpdateSuccessStory);
router.get('/fetchSuccessStory/:id?', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyAchievement.fetchCompanySuccessStoryToEdit);
router.get('/deleteSuccessStory/:id', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyAchievement.deleteSuccessStory);
router.get('/fetchCompanySuccessStories', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyAchievement.fetchCompanySuccessStories);

/******QUOTATIONS ********/
router.get('/fetchQuotationListForCompany', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyQuotations.fetchQuotation);
router.post('/addUpdateQuotation', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess, upload.array('quotation_docx_files')], CompanyQuotations.addUpdateQuotation);
router.get('/getQuotationData/:id', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyQuotations.getQuotationData);
router.post('/actionByCompanyOnQuotation', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyQuotations.withdrawOrRejectQuotation);

/****** Membership ********/
router.post('/buyMembership', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyMembershipController.buyMembership);

/******projects ********/
router.get('/getproject_company', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyProject.getproject_company);
router.get('/getcompleteproject_company', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyProject.getcompleteproject_company);
router.get('/ProjectDetails_company/:id', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyProject.ProjectDetails_company);
router.get('/completeProjectDetails_company/:id', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyProject.completeProjectDetails_company);
router.get('/RequestForCompleteProject/:id', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyProject.RequestForCompleteProject);
router.get('/dashboard_company', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], dashboardCompany.dashboard_company);
router.get('/get_matched_project', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyProject.get_matched_project);


/*******chat box *****/

router.get('/company/chatproject-list', [passport.authenticate('jwt', {session: false}), checkPermission.companyAccess], CompanyChatProjectListing.getCompanyChatList);
module.exports = router;