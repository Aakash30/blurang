const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const passport = require('passport');
const upload = require ('../../middlewares/aws-fileupload').upload;
const checkPermission = require('./../../middlewares/checkpermission');

require('./../../middlewares/passport')(passport);

const ProjectManagement = require('../../controllers').ProjectController;
const dashboardCompany = require('../../controllers').Dashboard;

/** project post edit and recommendations */
router.post('/postProject', [passport.authenticate('jwt', {session:false}), checkPermission.clientAccess], ProjectManagement.postProjectApi);
router.get('/fetchProjectForAddWithoutLogin', ProjectManagement.fetchProjectDataForClientToEdit);
router.get('/fetchProjectForEdit/:id?', [passport.authenticate('jwt', {session:false}), checkPermission.clientAccess], ProjectManagement.fetchProjectDataForClientToEdit);
router.get('/getRecommendationsForProject/:id', [passport.authenticate('jwt', {session: false}), checkPermission.clientAccess], ProjectManagement.getRecommendation);
router.get('/getMoreRecommendationsForProject/:id', [passport.authenticate('jwt', {session: false}), checkPermission.clientAccess], ProjectManagement.getMoreRecommendation);
router.post('/addtoCart', [passport.authenticate('jwt', {session:false}), checkPermission.clientAccess], ProjectManagement.AddtoCart);
router.delete('/removetoCart/:id?', [passport.authenticate('jwt', {session:false}), checkPermission.clientAccess], ProjectManagement.RemovetoCart);
router.get('/getcartCompanies/:id?', [passport.authenticate('jwt', {session:false}), checkPermission.clientAccess], ProjectManagement.getCartCompanies);
router.get('/deleteProjectdocx/:id?', [passport.authenticate('jwt', {session:false}), checkPermission.clientAccess], ProjectManagement.deleteProjectDocx);
router.get('/getShortlistedcompanies/:id?', [passport.authenticate('jwt', {session:false}), checkPermission.clientAccess], ProjectManagement.getShortistedCompanies);

/** project list  */
router.get('/get-my-project-list', [passport.authenticate('jwt', {session:false}), checkPermission.clientAccess], ProjectManagement.getProjectListForClient);
router.get('/project-details-client/:id?', [passport.authenticate('jwt', {session:false}), checkPermission.clientAccess], ProjectManagement.ProjectDetailsClient);

// router.post('/shortlistCompanies', [passport.authenticate('jwt', {session:false}), checkPermission.clientAccess], ProjectManagement.shortlistCompanies);
router.post('/request-for-quotation', [passport.authenticate('jwt', {session:false}), upload.fields([{name: 'project_docx_files', maxCount:3}])], ProjectManagement.sendQuotationRequest);

router.get('/getQuotationpendingcompanies/:id?', [passport.authenticate('jwt', {session:false}), checkPermission.clientAccess], ProjectManagement.getquotationPendingCompanies);
router.get('/getQuotationreceivedcompanies/:id?', [passport.authenticate('jwt', {session:false}), checkPermission.clientAccess], ProjectManagement.getquotationReceivedCompanies);
router.get('/getUnderDiscussionCompanies/:id?', [passport.authenticate('jwt', {session:false}), checkPermission.clientAccess], ProjectManagement.getUnderDiscussionCompanies);
router.get('/newQuotationProject', [passport.authenticate('jwt', {session:false}), checkPermission.clientAccess], ProjectManagement.newQuotationProject);
router.get('/completedProjectlist', [passport.authenticate('jwt', {session:false}), checkPermission.clientAccess], ProjectManagement.completedProjectlist);
router.get('/clientReviewList', [passport.authenticate('jwt', {session:false}), checkPermission.clientAccess], ProjectManagement.MyProjectsReviews);
router.get('/fetchClientFeedback/:id', [passport.authenticate('jwt', {session: false}), checkPermission.clientAccess], ProjectManagement.fetchClientFeedback);
router.get('/pendingQuotationList', [passport.authenticate('jwt', {session: false}), checkPermission.clientAccess],ProjectManagement.pendingQuotationList);
router.get('/allquotationlist', [passport.authenticate('jwt', {session: false}), checkPermission.clientAccess], ProjectManagement.allQuotationList);
router.get('/getQuotationTabCount', [passport.authenticate('jwt', {session: false}), checkPermission.clientAccess], ProjectManagement.getQuotationTabCount);

/** Action ON Projects */
router.get('/archiveProject/:id', [passport.authenticate('jwt', {session:false}), checkPermission.clientAccess], ProjectManagement.archiveProject);
router.post('/startDiscussionCompany/:id', [passport.authenticate('jwt', {session:false}), checkPermission.clientAccess], ProjectManagement.StartDiscussionCompany);
router.get('/complete-projectdetails-client/:id?', [passport.authenticate('jwt', {session:false}), checkPermission.clientAccess], ProjectManagement.completeProjectDetailsClient);
router.get('/hireCompany/:id', [passport.authenticate('jwt', {session:false}), checkPermission.clientAccess], ProjectManagement.HireCompany);
router.get('/rejectCompany/:id', [passport.authenticate('jwt', {session:false}), checkPermission.clientAccess], ProjectManagement.RejectCompany);
router.get('/markCompleteProject/:id', [passport.authenticate('jwt', {session:false}), checkPermission.clientAccess], ProjectManagement.markCompleteProject);
router.get('/dashboard_client', [passport.authenticate('jwt', {session: false}), checkPermission.clientAccess], dashboardCompany.dashboard_client);
router.get('/fetchEditFeedbackData/:id', [passport.authenticate('jwt', {session: false}), checkPermission.clientAccess], ProjectManagement.fetchEditFeedbackData);
router.post('/submitRatings', [passport.authenticate('jwt', {session: false}), checkPermission.clientAccess], ProjectManagement.submitOnlyFeedbackStars);
router.post('/submitFeedbackFromClient', [passport.authenticate('jwt', {session: false}), checkPermission.clientAccess, upload.fields([{ name : 'project_logo_file' }, { name : 'gallery_file', maxCount:6 }])], ProjectManagement.submitFeedbackFromClient);
module.exports = router;

