const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const passport = require('passport');
const fileupload = require("./../../middlewares/aws-fileupload");
const checkPermission = require('./../../middlewares/checkpermission');

require('./../../middlewares/passport')(passport);

upload = require ('../../middlewares/aws-fileupload').upload;

// load the dependent controller
// const AdminclientController = require('./../../controllers').AdminclientController;
const AdminAgencyOwnerController = require('./../../controllers').AdminAgencyOwnerController;



router.get('/ListAgencyOwner', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdminAgencyOwnerController.ListAgencyOwner);
router.get('/AgencyOwnerDetails/:id', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdminAgencyOwnerController.AgencyOwnerDetails);
router.post('/ActiveBlockAgencyOwner', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdminAgencyOwnerController.ActiveBlockAgencyOwner);
router.post('/updateAgencyOwnerProfileAdmin', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdminAgencyOwnerController.updateAgencyOwnerProfileAdmin);

module.exports = router;