const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const passport = require('passport');
const checkPermission = require('./../../middlewares/checkpermission');

const DashboardController=require("../../controllers/admin_dashboard/dashboardcontroller")
require('./../../middlewares/passport')(passport);

router.get('/fetch-dashboard', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess],DashboardController.dashboard);

module.exports=router