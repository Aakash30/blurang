const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const passport = require('passport');

const checkPermission = require('./../../middlewares/checkpermission');
const fileupload = require("./../../middlewares/aws-fileupload");

const ChatClientController = require("../../controllers").ChatClientController;

require('./../../middlewares/passport')(passport);

router.get("/my-inbox-list",[passport.authenticate('jwt', {session:false})], ChatClientController.fetchClientChat);
router.post("/send-message",[passport.authenticate('jwt', {session:false}),fileupload.upload.array("message_file")], ChatClientController.sendMessage);
router.get("/my-chats/:id", [passport.authenticate('jwt', {session: false})], ChatClientController.fetchSingleBoxData);

router.get('/load-chats/:id', passport.authenticate('jwt', {session: false}), ChatClientController.getOnlyChatMessage);
module.exports = router;