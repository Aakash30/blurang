const Auth = require('./auth');
const Company = require('./company');
const ContactUs=require("./contact_us")
const Notification=require("./notification")
const AdminMaster = require('./admin_master');
const AdminCompany = require('./admin_company');
const AdminDashboard=require("./admin_dashboard");
const AdminProject=require("./admin_projects")
const AdminClient = require('./admin_client');
const Admin_agencyowner = require('./admin_agencyowner');
const Reviews = require('./reviews');
const Common = require('./common');
const Client = require('./client');
const Landing_page = require('./landing_page');
const chat = require('./chat');
module.exports = [
    Auth,
    AdminDashboard,
    Company,
    AdminProject,
    ContactUs,
    Notification,
    AdminMaster,
    AdminCompany,
    AdminClient,
    Admin_agencyowner,
    Reviews,
    Common,
    Client,
    Landing_page,
    chat
]