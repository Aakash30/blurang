const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const passport = require('passport');

require('./../../middlewares/passport')(passport);

const Common = require('../../controllers').Common;

router.get('/fetchMyProgress', passport.authenticate('jwt', {session: false}), Common.fetchCompanyProgress);
router.get('/fetchCountryStateCityLocation', Common.fetchCountryStateCityLocation);

module.exports = router;