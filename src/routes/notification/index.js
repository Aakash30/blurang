const NotificationController=require("../../controllers/notification/notificationcontroller")
const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const passport = require('passport');
const notificationcontroller = require("../../controllers/notification/notificationcontroller");

require('./../../middlewares/passport')(passport);

router.get('/notification/', passport.authenticate('jwt', {session: false}), NotificationController.GetNotification);

router.get('/readNotification/:id?', passport.authenticate('jwt', {session: false}), notificationcontroller.readStatusOnClick);
router.get('/clearNotification/:id?', passport.authenticate('jwt', {session: false}), notificationcontroller.clearStatusOnClick);
module.exports = router;