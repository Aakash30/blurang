const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const passport = require('passport');
const fileupload = require("./../../middlewares/aws-fileupload");
const checkPermission = require('./../../middlewares/checkpermission');

require('./../../middlewares/passport')(passport);

upload = require ('../../middlewares/aws-fileupload').upload;

// load the dependent controller
const AdminclientController = require('./../../controllers').AdminclientController;
// const AdmincompanyController = require('./../../controllers').AdmincompanyController;



router.get('/ListClient', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdminclientController.ListClient);
router.get('/ClientDetails/:id', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdminclientController.ClientDetails);
router.post('/ActiveBlockClient', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdminclientController.ActiveBlockClient);
router.get('/reports/client-by-country', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdminclientController.client_registration_by_country);
router.get('/reports/project-by-client', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdminclientController.client_project_post);
router.get('/reports/company-earning', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdminclientController.company_earning);
router.get('/reports/categories', [passport.authenticate('jwt', {session:false}), checkPermission.adminAccess], AdminclientController.reportCategories);
module.exports = router;