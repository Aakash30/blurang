const ContactUsController=require("../../controllers/ContactusController")
const promiseRouter = require('express-promise-router');
const router = promiseRouter();
router.get('/contact-us/',  ContactUsController.postContactInfo);
module.exports = router;