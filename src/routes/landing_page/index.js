const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const passport = require('passport');
const fileupload = require("./../../middlewares/aws-fileupload");

const requestIp = require('request-ip');
require('./../../middlewares/passport')(passport);

const LandingpageController = require('../../controllers').LandingpageController;
const CompanyController = require("../../controllers").CompanyController;

router.get('/getDomain_home', LandingpageController.getDomain_home);
router.get('/getServices_home', LandingpageController.getServices_home);
router.get('/browse-list-company', LandingpageController.BrowseListCompany);
router.get('/getproject_home', LandingpageController.getproject_home);
router.get('/projectDetails_home/:id', LandingpageController.ProjectDetails_home);
router.post('/post-your-query', LandingpageController.postClientQuery);

router.get("/company-details-header/:id",requestIp.mw(), CompanyController.viewMyCompanyPublicProfileHeader);
router.get("/company-details-portfolio/:id", CompanyController.portfolioListForPublicProfile);
router.get("/company-details-reviews/:id", CompanyController.reviewsListForPublicProfile);
router.get("/company-details-certificate/:id", CompanyController.achievementCertificateListForPublicProfile);
router.get("/company-details-awards/:id", CompanyController.achievementAwardsListForPublicProfile);
router.get("/company-details-feature/:id", CompanyController.achivementFeaturedListForPublicProfile);
router.get("/company-details-stories/:id", CompanyController.successStoriesForPublicProfile);
router.get("/company-details-company-info/:id", CompanyController.fetchCompanyInfoForPublicProfile);
router.get("/company-details-faq/:id", CompanyController.fetchCompanyFAQForPublicProfile);
router.get("/company-details-queries/:id", LandingpageController.companyDetailAskQueries);
router.get('/fetchContentForListYourCompany', LandingpageController.contentForListYourCompany);

module.exports = router;