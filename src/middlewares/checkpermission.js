const globalCalls = require('../settings/functions');

module.exports = {
    adminAccess: (req, res, next) => {        
            if (!req.user) {
                throw globalCalls.forbiddenError("You are not authorized to perform this action.");
            }
            if (req.user.user_type === 'admin') {
                return next();
            } else { console.log('You are not authorized to perform this action');
                throw globalCalls.badRequestError("You are not authorized to perform this action.");
                // throw globalCalls.badRequestError("company not found.");
                // return forbiddenError("You are not authorized to perform this action.");
            }
        
    },

    companyAccess: (req, res, next) => {
        if (!req.user) {
            throw globalCalls.forbiddenError("You are not authorized to perform this action.");
        }
        if (req.user.user_type === 'company') {
            return next();
        } else {
            throw globalCalls.forbiddenError("You are not authorized to perform this action.");
        }
    },

    clientAccess: (req, res, next) => {
        if (!req.user) {
            throw globalCalls.forbiddenError("You are not authorized to perform this action.");
        }
        if (req.user.user_type === 'client') {
            return next();
        } else {
            throw globalCalls.forbiddenError("You are not authorized to perform this action.");
        }
    },

    checkRole: (roleIds) => {
        return function (req, res, next) {
            if (!req.user) {
                throw globalCalls.forbiddenError("You are not authorized to perform this action.");
            }
            if (req.user.user_role.indexOf(roleIds) !== -1) {
                return next();
            } else {
                throw globalCalls.forbiddenError("You are not authorized to perform this action.");
            }
        }
    }
}
