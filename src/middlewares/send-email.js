const sgMail    = require('@sendgrid/mail');
const sendgrid  = require('../settings/constants').sendgrid;
const pug = require("pug");

sgMail.setApiKey(sendgrid);

module.exports.sendEmail = async (to, subject, mailData, templateId) => {
  sgMail.setApiKey(sendgrid);
  const msg = {
    to: to,
    from: "jankilal.patidar@engineerbabu.in",
    subject:subject,
    html: pug.renderFile("public/template/"+templateId, mailData)
  };
  sgMail
    .send(msg)
    .then(() => {
      //Celebrate
      //console.log("sendgrid mail sended success");
    })
    .catch(error => {
      //Log friendly error
      console.error(error.toString());
      //Extract error msg
      const { message, code, response } = error;
      //Extract response msg
      const { headers, body } = response;
    });
};