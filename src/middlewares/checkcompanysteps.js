"use strict";

const knexConfig        = require('../../config/knex');
const Knex              = require('knex')(knexConfig.development);

const { transaction, ref } = require("objection");

const globalCalls = require('../settings/functions');
const { base64decode } = require("nodejs-base64");
const company = require("../models/Company");
const companyMapping= require("../models/CompanyMapping");

module.exports = {
    stepAccess: async (req, res, next) => {        
            if (!req.user) {
                throw globalCalls.forbiddenError("You are not authorized to perform this action.");
            }
            let company_id=base64decode(req.params.id);
            if(isNaN(parseInt(company_id)))
                throw globalCalls.badRequestError("Error! You have passed invalid-id.");
    
            let companyDataz = await companyMapping.query().select("company_map_id").where({user_id: req.user.user_id, company_id: company_id}).first();
            // console(companyDataz);
            if (companyDataz) {
                return next();
            } else {
                throw globalCalls.forbiddenError("You are not authorized to perform this action.");
            }
    },

    
    // CompanyRulesAccess: (req, res, next) => {
    //     if (!req.user) {
    //         throw globalCalls.forbiddenError("You are not authorized to perform this action.");
    //     }
    //     if (req.user.steps_completed === 4) {
    //         return next();
    //     } else {
    //         throw globalCalls.forbiddenError("You are not authorized to perform this action.");
    //     }
    // },
}
